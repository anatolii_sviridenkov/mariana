package mariana.examples;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class DisriptorPerfromanceEx {
	
	
	
	public static void run(Runnable r, int threads) 
	{
		List<Thread> th = new ArrayList<>();
		
		for (int t = 0; t < threads; t++) {
			th.add(new Thread(r));
		}
		
		for (int t = 0; t < threads; t++) {
			th.get(t).start();
		}
		
		for (int t = 0; t < threads; t++) {
			try {
				th.get(t).join();
			} 
			catch (InterruptedException e) {
			}
		}
	}
	
	static class QueueConsumer implements Runnable {

		private BlockingQueue<Long> queue;
		private final long total;
		
		QueueConsumer(BlockingQueue<Long> queue, long total) 
		{
			this.queue = queue;
			this.total = total;		
		}
		
		@Override
		public void run() 
		{
			long t0 = System.currentTimeMillis();
			
			for (long c = 0; c < total; c++) 
			{
				try {
					queue.take();
				} 
				catch (InterruptedException e) {
					break;
				}
			}
			
			System.out.println(this.getClass() + " -- " + (System.currentTimeMillis() - t0));
		}
		
	}

	public static void main(String[] args) throws InterruptedException 
	{
//		new FreeCounter().run();
//		new ().run();
//		new LockedCounter().run();
//		new AtomicCounter().run();
		
//		run(new MonitoredCounter(), 8);
		
		
		BlockingQueue<Long> queue = new ArrayBlockingQueue<Long>(10000);

		long total = 10000000;
		int thn = 8;
		
		List<Thread> threads = new ArrayList<>();
		
		
		for (int n = 0; n < thn; n++) 
		{
			Thread th1 = new Thread(new QueueConsumer(queue, total));
			th1.start();
			threads.add(th1);
		}
		
		Long value = new Long(12345);
		long t0 = System.currentTimeMillis();
		
		for (long c = 0; c < total * thn; c++) 
		{
			queue.put(value);
		}
		
		System.out.println("Enqueued in " + (System.currentTimeMillis() - t0));
		
		for (Thread th: threads) th.join();
	}
}
