
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.examples;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

import com.google.common.primitives.UnsignedBytes;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.set.bytes.SetBIterator;
import mariana.v1.api.ctr.set.bytes.fx.SetFxB;
import mariana.v1.api.io.BufferProducer;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.FileOutputBuffer;
import mariana.v1.tools.codecs.Int64Codec;

public class SetFxBBulkInsertTest
{
    static
    {
        Mariana.init();
    }
    
   
    
    
    static class ListSetLInput implements BufferProducer {

    	private int current = 0;

		private final byte[] buf;
		
		private final Int64Codec codec = new Int64Codec();

		private final int dataLength;
		
    	public ListSetLInput(byte[] buf, int dataLength) {    		
    		this.buf = buf;
			this.dataLength = dataLength;
    	}
    	
    	@Override
    	public int populate(ByteBuffer buffer) 
    	{
    		int total = 0;
    		
    		for(; current < buf.length; total++, current += dataLength) 
    		{
    			if (!encodeIfFits(buffer, buf, current))
    			{
    				return total;
    			}
    		}
    		
    		return -total;
    	}
    	
		
    	private boolean encodeIfFits(ByteBuffer buffer, byte[] data, int from) 
    	{
    		int lenOfLen = codec.length(dataLength);
    		int total 	 = lenOfLen + dataLength;
    		int pos 	 = buffer.position();
    		
    		if (pos + total <= buffer.limit())
    		{
    			codec.encode(buffer, dataLength);
    			buffer.put(data, from, dataLength);
    			return true;
    		}
    		else {
    			return false;
    		}
    	}
    }
    
    public static void main( String[] args ) throws Exception
    {
    	Random RNG = new Random(0);
    	
    	try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{
    			try (Snapshot snp = master.branch()) {
    				try(SetFxB set = snp.create(SetFxB.class, UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fd64")))
    				{
    					int size = 30000000;
    					int dataLength = 16;
    					
    					byte[][] array = new byte[size][];
    					
    					for (int c = 0; c < array.length; c++) 
    					{
    						byte[] data = new byte[16];
    						RNG.nextBytes(data);
    						array[c] = data;
    					}
    					
    					Arrays.parallelSort(array, UnsignedBytes.lexicographicalComparator());
    					
    					byte[] longData = new byte[size * dataLength];
    					
    					for (int c = 0; c < array.length; c++) 
    					{
    						System.arraycopy(array[c], 0, longData, c * dataLength, dataLength);
    					}
    					
    					array = null;
    					
    					System.gc();
    					System.gc();
    					
    					long t0 = System.currentTimeMillis();
    					
    					set.setNewPageSize(65536);
    					
    					try(SetBIterator iter = set.begin()) 
    					{
    						iter.insert(1024*64, 1014*256, new ListSetLInput(longData, dataLength));
    					}
    					
    					System.out.println("Size: " + set.size() + ", time = " + (System.currentTimeMillis() - t0)); 					
    				}
    				
    				snp.commit();

    				snp.setAsMaster();
    			}
    		}

    		long td0 = System.currentTimeMillis();
    		
    		try(FileOutputBuffer fileBuffer = new FileOutputBuffer("target/setfxb.memoria")) 
    		{
    			alloc.store(fileBuffer);
    		}
    		
    		System.out.println("Store time: " + (System.currentTimeMillis() - td0) + " ms");
    	}
    }
}
