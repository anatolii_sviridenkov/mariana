
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.examples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.map.str.str.AbstractMapSSBufferProducer;
import mariana.v1.api.ctr.map.str.str.MapSSIterator;
import mariana.v1.api.ctr.map.str.str.MapSS;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.FileOutputBuffer;

public class MapSSBulkInsertTest
{
    static
    {
        Mariana.init();
    }
    
    static class Pair {
    	final String key;
    	final String value;
    	public Pair(String key, String value) {this.key = key; this.value = value;}
		public String getKey() {
			return key;
		}
		public String getValue() {
			return value;
		}
    }
    
    static class ListMapSSInput extends AbstractMapSSBufferProducer {

    	private java.util.Iterator<Pair> iter;
    	
    	private Pair current;
		
    	public ListMapSSInput(List<Pair> pairs) {    		
    		iter = pairs.iterator();
    	}
		
		public String getKey() {
			return current.getKey();
		}

		public String getValue() {			
			return current.getValue();
		}


		@Override
		public boolean hasNext() 
		{			
			return iter.hasNext();
		}

		@Override
		public void next() {
			current = iter.next();
		}    	
    }
    
    public static void main( String[] args ) throws Exception
    {
    	try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) {

    			try(Snapshot snp = master.branch())
    			{
    				try(MapSS map = snp.create(MapSS.class, UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fd64")))
    				{
    					System.out.println("Ctr name: " + map.name());

    					map.setNewPageSize(65536);

    					int size = 10000000;

    					List<Pair> pairs = new ArrayList<Pair>();

    					for (int c = 0; c < size; c++)
    					{
    						pairs.add(new Pair("xxxxxxxxxxxxxxxxxx Boo " + c, "xxxxxxxxxxxxxxxxxx Zoo "+c));
    					}

    					Collections.sort(pairs, new Comparator<Pair>() {
    						public int compare(Pair o1, Pair o2) {
    							return o1.getKey().compareTo(o2.getKey());
    						}
    					});

    					System.out.println("Starting insertion...");
    					
    					try(MapSSIterator iter = map.begin()) 
    					{
    						ListMapSSInput input = new ListMapSSInput(pairs);
    						
    						long t0 = System.currentTimeMillis();    						
    						iter.insert(input);
    						System.out.println("Insertion time: "+(System.currentTimeMillis() - t0));    						
    					}
    					
    					Mariana.dumpCounters();

    					System.out.println("Size: " + map.size());

    					try (MapSSIterator i = map.seek(map.size() - 1)) {
    						System.out.println("Last key: " + i.key());
    					}

    					
    				}

    				snp.commit();

    				snp.setAsMaster();
    			}

    			try(FileOutputBuffer fileBuffer = new FileOutputBuffer("target/map_bulk.dump"))
    			{
    				long t0 = System.currentTimeMillis();
    				alloc.store(fileBuffer);
    				System.out.println("Store time: "+(System.currentTimeMillis() - t0));
    			}
    		}
    	}
    }
}
