package mariana.examples;

import java.util.Arrays;
import java.util.Random;

import com.google.common.primitives.UnsignedBytes;

public class SortingTest {

	public static void main(String[] args) 
	{
		int arraySize = 100000000;
		int rowSize   = 20;
		
		byte[][] array = new byte[arraySize][]; 

		Random rng = new Random();
		
		long t0 = System.currentTimeMillis();
		for (int c = 0; c < arraySize; c++) 
		{
			byte[] data = new byte[rowSize];
			rng.nextBytes(data);
			array[c] = data;
		}
		
		System.out.println("Array creation time: " + (System.currentTimeMillis() - t0));
		
		long t1 = System.currentTimeMillis();
		
		Arrays.parallelSort(array, UnsignedBytes.lexicographicalComparator());
		
		System.out.println("Array sorting time: " + (System.currentTimeMillis() - t1));
		
	}

}
