package mariana.examples.serialization;

public class SerClassLoader extends ClassLoader {

	public SerClassLoader() {
		super();
	}

	public SerClassLoader(ClassLoader parent) {
		super(parent);
	}
	
	public void createClass(byte[] data) 
	{
		super.defineClass(data, 0, data.length);
	}
}
