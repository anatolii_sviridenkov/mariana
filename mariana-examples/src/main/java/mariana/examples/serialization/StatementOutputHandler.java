package mariana.examples.serialization;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.serialization.AbstractClassOutputHandler;
import mariana.v1.io.serialization.OutputHandler;
import mariana.v1.io.serialization.PrimitiveOutputHandler;
import mariana.v1.io.serialization.PrimitivePropertyAccessor;

public class StatementOutputHandler extends AbstractClassOutputHandler {

	protected StatementOutputHandler() {
		super(createOutputHandlers());
	}
	
	private static OutputHandler[] createOutputHandlers() 
	{
		return new OutputHandler[] {
				new PrimitiveOutputHandler(new PrimitivePropertyAccessor() {
					public boolean apply(Object tgt, IOBuffer buf) {
						if (buf.hasLong()) 
						{
							buf.putLong(((Statement)tgt).getSubject());							
							return true;
						}
						return false;
					}
				}),

				new PrimitiveOutputHandler(new PrimitivePropertyAccessor() {
					public boolean apply(Object tgt, IOBuffer buf) {
						if (buf.hasLong()) 
						{
							buf.putLong(((Statement)tgt).getProperty());							
							return true;
						}
						return false;
					}
				}),


				new PrimitiveOutputHandler(new PrimitivePropertyAccessor() {
					public boolean apply(Object tgt, IOBuffer buf) {
						if (buf.hasLong()) 
						{
							buf.putLong(((Statement)tgt).getObject());							
							return true;
						}
						return false;
					}
				}),


				new PrimitiveOutputHandler(new PrimitivePropertyAccessor() {
					public boolean apply(Object tgt, IOBuffer buf) {
						if (buf.hasLong()) 
						{
							buf.putLong(((Statement)tgt).getContext());							
							return true;
						}
						return false;
					}
				}),
		};
	}


}
