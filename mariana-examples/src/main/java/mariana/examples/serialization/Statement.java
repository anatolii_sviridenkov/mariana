package mariana.examples.serialization;

public class Statement {
	
	private boolean bval;
	private double prefix;
	
	private long subject;
	private long property;
	private long object;
	private long context;
	
	public Statement() {}
	public Statement(long subject, long property, long object, long context) {
		this.subject 	= subject;
		this.property 	= property;
		this.object 	= object;
		this.context 	= context;
	}
	
	public long getSubject() {
		return subject;
	}
	public void setSubject(long subject) {
		this.subject = subject;
	}
	public long getProperty() {
		return property;
	}
	public void setProperty(long property) {
		this.property = property;
	}
	public long getObject() {
		return object;
	}
	public void setObject(long object) {
		this.object = object;
	}
	public long getContext() {
		return context;
	}
	public void setContext(long context) {
		this.context = context;
	}
	public boolean isBval() {
		return bval;
	}
	public boolean getBval() {
		return bval;
	}
	public void setBval(boolean bval) {
		this.bval = bval;
	}
	public double getDval() {
		return prefix;
	}
	public void setPrefix(double prefix) {
		this.prefix = prefix;
	}
}
