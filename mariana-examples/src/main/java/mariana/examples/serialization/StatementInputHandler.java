package mariana.examples.serialization;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.serialization.AbstractClassInputHandler;
import mariana.v1.io.serialization.InputHandler;
import mariana.v1.io.serialization.PrimitiveInputHandler;
import mariana.v1.io.serialization.PrimitivePropertyAccessor;

public class StatementInputHandler extends AbstractClassInputHandler {

	protected StatementInputHandler() {
		super(createInputHandlers());		
	}

	private static InputHandler[] createInputHandlers() 
	{
		return new InputHandler[] {
				new PrimitiveInputHandler((Object tgt, IOBuffer buf) -> {
					if (buf.hasLong()) {
						((Statement)tgt).setSubject(buf.getLong());
						return true;
					}
					return false;
				}),
				
				new PrimitiveInputHandler((Object tgt, IOBuffer buf) -> {
					if (buf.hasLong()) {
						((Statement)tgt).setProperty(buf.getLong());
						return true;
					}
					return false;
				}),

				new PrimitiveInputHandler((Object tgt, IOBuffer buf) -> {
					if (buf.hasLong()) {
						((Statement)tgt).setObject(buf.getLong());
						return true;
					}
					return false;
				}),

				new PrimitiveInputHandler((Object tgt, IOBuffer buf) -> {
					if (buf.hasLong()) {
						((Statement)tgt).setContext(buf.getLong());
						return true;
					}
					return false;
				}),
		};
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected Object newInstance() {
		return new Statement();
	}
}
