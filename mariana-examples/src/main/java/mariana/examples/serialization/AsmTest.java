package mariana.examples.serialization;

import java.io.PrintStream;
import java.io.PrintWriter;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.commons.Method;
import org.objectweb.asm.util.TraceClassVisitor;

public class AsmTest {

	public static void main(String[] args) throws Exception 
	{
		 ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
		 cw.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC, "Example", null, "java/lang/Object", null);
		  
		 Method m = Method.getMethod("void <init> ()");
		 GeneratorAdapter mg = new GeneratorAdapter(Opcodes.ACC_PUBLIC, m, null, null, cw);
		 mg.loadThis();
		 mg.invokeConstructor(Type.getType(Object.class), m);
		 mg.returnValue();
		 mg.endMethod();
		 
		 m = Method.getMethod("void main (String[])");
		 mg = new GeneratorAdapter(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, m, null, null, cw);
		 mg.getStatic(Type.getType(System.class), "out", Type.getType(PrintStream.class));
		 mg.push("Hello world!");
		 mg.invokeVirtual(Type.getType(PrintStream.class), Method.getMethod("void println (String)"));
		 mg.returnValue();
		 mg.endMethod();
		 
		 cw.visitEnd();

		 byte[] data = cw.toByteArray();
		 
		 

		 ClassReader cr = new ClassReader(data);
		 cr.accept(new TraceClassVisitor(new PrintWriter(System.out)), 0);
		 
		 SerClassLoader loader = new SerClassLoader(Thread.currentThread().getContextClassLoader());
		 
		 loader.createClass(data);
		 
		 Class<?> clazz = loader.loadClass("Example");
		 
		 java.lang.reflect.Method method = clazz.getMethod("main", String[].class);
		 method.invoke(null);
	}

}
