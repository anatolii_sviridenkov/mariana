package mariana.examples.serialization;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Random;

import mariana.v1.io.buffer.IOByteBuffer;

public class SerializationTest {
	public static void main(String[] args) 
	{
		int arraySize = 100000000;
		Random rng = new Random();

		ByteBuffer buffer = ByteBuffer.allocateDirect(12345).order(ByteOrder.LITTLE_ENDIAN);
		IOByteBuffer iobuffer = new IOByteBuffer(buffer);
		
		
		long t1 = System.currentTimeMillis();
		

		
//		StatementOutputHandler iter = new StatementOutputHandler();
//		Statement st = new Statement(rng.nextLong(), rng.nextLong(), rng.nextLong(), rng.nextLong());
//		
//		for (int c = 0; c < arraySize; c++)
//		{
//			iter.reset(st);
//
//			while(!iter.writeTo(iobuffer))
//			{
//				buffer.rewind();
//			}
//		}
		
		
		
		Statement st = new Statement(rng.nextLong(), rng.nextLong(), rng.nextLong(), rng.nextLong());
		StatementInputHandler iter = new StatementInputHandler();
		
		buffer.limit((buffer.capacity() / 8) * 8);
				
		for (int c = 0; c < arraySize; c++)
		{
			iter.reset(st);

			while(!iter.readFrom(iobuffer))
			{
				buffer.rewind();
			}
		}
		
		System.out.println("Array serialization time: " + (System.currentTimeMillis() - t1));
	}
}
