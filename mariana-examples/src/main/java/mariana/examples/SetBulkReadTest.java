
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.examples;

import java.nio.ByteBuffer;
import java.util.UUID;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.set.bytes.AbstractSetBBufferConsumer;
import mariana.v1.api.ctr.set.bytes.SetB;
import mariana.v1.api.ctr.set.bytes.SetBIterator;
import mariana.v1.inmem.alloc.InMemoryAllocator;

public class SetBulkReadTest
{
    static
    {
        Mariana.init();
    }
    
    static class EntryReader extends AbstractSetBBufferConsumer {
    	int count = 0;
    	
		public int getCount() {
			return count;
		}

		@Override
		public boolean consume(ByteBuffer key) {
			count++;
			return true;
		}
    }
    
    
    public static void main( String[] args ) throws Exception
    {
    	try (InMemoryAllocator alloc = InMemoryAllocator.load("target/set.memoria")) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{
    			try(SetB set = master.find(SetB.class, UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fd64")))
    			{
    				System.out.println("Set with " + set.size() + " entries");

    				try(SetBIterator iter = set.begin()) 
    				{
    					EntryReader reader = new EntryReader();
    					
    					long t0 = System.currentTimeMillis();
    					long total = iter.read(reader);
    					System.out.println("Size: " + reader.getCount() + ", time = " + (System.currentTimeMillis() - t0)+", total = " + total);    					
    				}    				 					
    			}
    		}
    	}
    }
}
