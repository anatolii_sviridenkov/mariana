
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.examples;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

import com.google.common.primitives.UnsignedBytes;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLB;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLBEntryProducer;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLBIterator;
import mariana.v1.api.ctr.set.bytes.SetB;
import mariana.v1.api.ctr.set.bytes.SetBIterator;
import mariana.v1.api.ctr.set.bytes.vl.SetVlB;
import mariana.v1.api.io.BufferProducer;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.FileOutputBuffer;
import mariana.v1.tools.Ticker;
import mariana.v1.tools.codecs.Int64Codec;

public class MultimapTest
{
    static
    {
        Mariana.init();
    }
    
    public static void main( String[] args ) throws Exception
    {
    	Random RNG = new Random(1110);
    	
    	try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{
    			try (Snapshot snp = master.branch()) 
    			{
    				try(MultimapLB mmap = snp.create(MultimapLB.class, UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fd64")))
    				{
    					Ticker ticker = new Ticker(100000);
    					
    					for (int c = 0; c < 10000000; c++) 
    					{
    						byte[] data = new byte[200];
    						
    						try(MultimapLBIterator iter = mmap.find(c)) 
    						{
    							int length = RNG.nextInt(100);
    							
    							for (int d = 0; d < length; d++) 
    							{
    								data[d] = (byte)(c+1);
    							}
    							
    							iter.insert(new MultimapLBEntryProducer(RNG.nextLong(), data, length));
    						}
    						
    						if (ticker.isThreshold()) {
    							System.out.println("Inserted: " + ticker.getTicks() + " in " + ticker.duration());
    							ticker.nextThreshold();
    						}
    						
    						ticker.tick();
    					}
    					
    					System.out.println("Total insertion time: " + (System.currentTimeMillis() - ticker.getStartTime()));
    				}
    				
    				snp.commit();

    				snp.setAsMaster();
    				
//    				snp.dump("target/mmap.dir");
    			}
    		}
    		
    		

    		long td0 = System.currentTimeMillis();
    		
//    		try(FileOutputBuffer fileBuffer = new FileOutputBuffer("target/mmap.memoria")) 
//    		{
//    			alloc.store(fileBuffer);
//    		}
    		
    		try(FileOutputBuffer fileBuffer = new FileOutputBuffer("/dev/null")) 
    		{
    			alloc.store(fileBuffer);
    		}
    		
    		//alloc.store("target/mmap.memoria");
    		//alloc.store("/dev/null");
    		
    		System.out.println("Store time: " + (System.currentTimeMillis() - td0) + " ms");
    	}
    }
}
