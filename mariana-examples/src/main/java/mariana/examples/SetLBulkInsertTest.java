
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.examples;

import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.set.lng.AbstractSetLBufferProducer;
import mariana.v1.api.ctr.set.lng.SetL;
import mariana.v1.api.ctr.set.lng.SetLIterator;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.FileOutputBuffer;

public class SetLBulkInsertTest
{
    static
    {
        Mariana.init();
    }
    
    static class ListSetLInput extends AbstractSetLBufferProducer {

    	private int current = -1;

		private long[] buf;
		
    	public ListSetLInput(long[] buf) {    		
    		this.buf = buf;
    	}
		
		public long getKey() {
			return buf[current];
		}


		@Override
		public boolean hasNext() 
		{			
			return current < buf.length - 1;
		}

		@Override
		public void next() {
			current++;
		}
    }
    
    public static void main( String[] args ) throws Exception
    {
    	Random RNG = new Random(0);
    	
    	try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{
    			try (Snapshot snp = master.branch()) {
    				try(SetL set = snp.create(SetL.class, UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fd64")))
    				{
    					int size = 100000000;
    					
    					long[] array = new long[size];
    					
    					for (int c = 0; c < array.length; c++) 
    					{
    						array[c] = RNG.nextLong();
    					}
    					
    					Arrays.parallelSort(array);
    					
    					long t0 = System.currentTimeMillis();
    					
    					set.setNewPageSize(65536);
    					
    					try(SetLIterator iter = set.begin()) 
    					{
    						iter.insert(new ListSetLInput(array));
    					}
    					
    					System.out.println("Size: " + set.size() + ", time = " + (System.currentTimeMillis() - t0)); 					
    				}
    				
    				snp.commit();
    				
//    				snp.dump("target/setl.dir");

    				snp.setAsMaster();
    			}
    		}

    		long td0 = System.currentTimeMillis();
    		
    		try(FileOutputBuffer fileBuffer = new FileOutputBuffer("target/setl.memoria")) 
    		{
    			alloc.store(fileBuffer);
    		}
    		
    		System.out.println("Store time: " + (System.currentTimeMillis() - td0) + " ms");
    	}
    }
}
