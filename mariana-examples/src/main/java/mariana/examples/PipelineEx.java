package mariana.examples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fi.jumi.actors.ActorRef;
import fi.jumi.actors.ActorThread;
import mariana.v1.framework.actors.Async;
import mariana.v1.framework.actors.MarianaActors;
import mariana.v1.framework.actors.MarianaMultiThreadedActors;
import mariana.v1.framework.pipeline.StreamJoiner;
import mariana.v1.framework.pipeline.StreamProducer;
import mariana.v1.tools.Ticker;

public class PipelineEx {
	
	static class EventProducer implements StreamProducer<List<Long>> {

		private long num;
		private long events;
		private long event;
		
		private Random RNG;
		
		private static final long BATCH_SIZE = 500;

		public EventProducer(int num, int events) {
			this.num = num;
			this.events = events;
			
			RNG = new Random(num);
		}
		
		@Override
		public Async<List<Long>> produce() 
		{
			if (event < events) 
			{
				long remaining = events - event;
				
				if (remaining > 1) 
				{
					long limit = remaining > BATCH_SIZE ? BATCH_SIZE : remaining;
					long size  = RNG.nextInt((int)limit);

					List<Long> list = new ArrayList<>();

					for (long l = 0; l < size; l++, event++) 
					{
						list.add((num << 32) + event);
					}
					
					int ndelay = RNG.nextInt(1000000);
					for (int c = 0; c < ndelay; c++) {
						RNG.nextLong();
					}
					
//					if (event > events / 2) {
//						throw new RuntimeException("Booo!");
//					}

					return Async.of(list);
				}
				else {
					return Async.of(Collections.singletonList(event++));
				}
			}
			else {
				return Async.of(null);
			}
		}		
	}
	
	public static void main(String[] args) 
	{
		ExecutorService actorsThreadPool = Executors.newCachedThreadPool();
        MarianaActors actors = new MarianaMultiThreadedActors(actorsThreadPool);

        List<ActorThread> threads = new ArrayList<>();
        List<StreamProducer<List<Long>>> producers = new ArrayList<>();
        
        int events = 1000000;
        
        for (int c = 0; c < 8; c++)
        {
        	ActorThread actorThread = actors.startActorThread();

        	@SuppressWarnings({ "unchecked", "rawtypes" })
			ActorRef<StreamProducer<List<Long>>> ref = (ActorRef)actorThread.bindActor(StreamProducer.class, new EventProducer(c, events));
        	
        	producers.add(ref.tell());
        	
        	threads.add(actorThread);
        }
        
        try {

        	StreamJoiner<List<Long>> joiner = new StreamJoiner<>(producers);
        	
        	List<Long> str;
        	int counter = 0;

        	Ticker ticker = new Ticker(100000);

        	while ((str = joiner.produce().get()) != null)
        	{
        		counter += str.size();

        		if (ticker.isThreshold()) 
        		{
        			System.out.println("Done " + (ticker.getTicks() + 1) + " in " + ticker.duration());
        			ticker.nextThreshold();
        		}

        		ticker.tick(str.size());
        	}

        	System.out.println("Total: " + counter);
        }
        catch (Exception ex) {
        	ex.printStackTrace();
        }
        finally 
        {
        	for (ActorThread thread: threads) {
        		thread.stop();
        	}

        	System.out.println("Threads stop requested");

        	actorsThreadPool.shutdown();
        }
        
        System.out.println("Done...");
	}

}
