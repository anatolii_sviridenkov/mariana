
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.examples;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import java.util.Random;

import com.google.common.primitives.UnsignedBytes;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.set.bytes.AbstractSetBBufferConsumer;
import mariana.v1.api.ctr.set.bytes.SetB;
import mariana.v1.api.ctr.set.bytes.SetBEntryConsumer;
import mariana.v1.api.ctr.set.bytes.SetBIterator;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.FileOutputBuffer;
import mariana.v1.tools.BufferTools;

public class SetTest
{
    static
    {
        Mariana.init();
    }
    
    public static void main( String[] args ) throws Exception
    {
    	Random RNG = new Random(0);
    	
    	try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{
    			try (Snapshot snp = master.branch()) {
    				try(SetB set = snp.create(SetB.class))
    				{
    					byte[][] array = new byte[10000][];
    					
    					for (int c = 0; c < array.length; c++) 
    					{
    						byte[] data = new byte[36];
    						RNG.nextBytes(data);    						
    						array[c] = data;
    					}
    					
    					Arrays.parallelSort(array, UnsignedBytes.lexicographicalComparator());
    					
    					ByteBuffer bb = ByteBuffer.allocate(36);
    					
    					for (int c = 0; c < array.length; c++) 
    					{
    						bb.rewind();
    						bb.put(array[c]);
    						bb.rewind();
    						set.insert(bb);
    					}
    					
    					System.out.println(set.size());
    					
    					try (SetBIterator iter = set.begin()) 
    					{
//    						while (!iter.isEnd())
//    						{
//    							System.out.println(BufferTools.toString(iter.key()));
//    							
//    							iter.next();
//    						}
//    						
    						iter.read(new AbstractSetBBufferConsumer(){

								@Override
								public boolean consume(ByteBuffer key) 
								{
									System.out.println(BufferTools.toString(key));
									return true;
								}
							}); 
    					}
    				}
    				
    				snp.commit();

    				snp.dump("target/set-snp.dump");

    				snp.setAsMaster();
    			}
    		}

    		alloc.dump("target/myalloc.dir");
    		alloc.store("target/myalloc.dump");

    		try(FileOutputBuffer fileBuffer = new FileOutputBuffer("target/myalloc_raf.dump")) 
    		{
    			alloc.store(fileBuffer);
    		}
    	}
    }
}
