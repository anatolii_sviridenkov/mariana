
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.examples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.map.lng.bytes.AbstractMapLBBufferProducer;
import mariana.v1.api.ctr.map.lng.bytes.MapLB;
import mariana.v1.api.ctr.map.lng.bytes.MapLBIterator;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.FileOutputBuffer;

public class MapLBBulkInsertTest
{
    static
    {
        Mariana.init();
    }
    
    static class Pair {
    	final long key;
    	final byte[] value;
    	public Pair(long key, byte[] value) {this.key = key; this.value = value;}
		public long getKey() {
			return key;
		}
		public byte[] getValue() {
			return value;
		}
    }
    
    static class ListMapInput extends AbstractMapLBBufferProducer {

    	private java.util.Iterator<Pair> iter;
    	
    	private Pair current;
		
    	public ListMapInput(List<Pair> pairs) {    		
    		iter = pairs.iterator();
    	}
		
		public long getKey() {
			return current.getKey();
		}

		public byte[] getValue() {			
			return current.getValue();
		}


		@Override
		public boolean hasNext() 
		{			
			return iter.hasNext();
		}

		@Override
		public void next() {
			current = iter.next();
		}    	
    }
    
    public static void main( String[] args ) throws Exception
    {
    	try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) {

    			try(Snapshot snp = master.branch())
    			{
    				try(MapLB map = snp.create(MapLB.class, UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fd64")))
    				{
    					System.out.println("Ctr name: " + map.name());

    					map.setNewPageSize(65536);

    					int size = 10000000;

    					List<Pair> pairs = new ArrayList<Pair>();

    					for (int c = 0; c < size; c++)
    					{
    						pairs.add(new Pair(c, ("xxxx00xxx Zoo "+c).getBytes()));
    					}

    					Collections.sort(pairs, new Comparator<Pair>() {
    						public int compare(Pair o1, Pair o2) {
    							return Long.compare(o1.getKey(), o2.getKey());
    						}
    					});

    					System.out.println("Starting insertion...");
    					
    					try(MapLBIterator iter = map.begin()) 
    					{
    						ListMapInput input = new ListMapInput(pairs);
    						
    						long t0 = System.currentTimeMillis();    						
    						iter.insert(64*1024, 16*1024, input);
    						System.out.println("Insertion time: "+(System.currentTimeMillis() - t0));    						
    					}
    					
    					System.out.println("Size: " + map.size());

    					try (MapLBIterator i = map.seek(map.size() - 1)) {
    						System.out.println("Last key: " + i.key());
    					}
    				}

    				snp.commit();
    				
//    				snp.dump("target/maplb.dir");

    				snp.setAsMaster();
    			}

    			try(FileOutputBuffer fileBuffer = new FileOutputBuffer("target/maplb_bulk.dump"))
    			{
    				long t0 = System.currentTimeMillis();
    				alloc.store(fileBuffer);
    				System.out.println("Store time: "+(System.currentTimeMillis() - t0));
    			}
    		}
    	}
    }
}
