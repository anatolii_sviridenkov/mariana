
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.examples;

import java.util.UUID;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.map.str.str.AbstractMapSSBufferConsumer;
import mariana.v1.api.ctr.map.str.str.MapSSIterator;
import mariana.v1.api.ctr.map.str.str.MapSS;
import mariana.v1.inmem.alloc.InMemoryAllocator;

public class MapBulkReadTest
{
    static
    {
        Mariana.init();
    }
    
    static class Pair {
    	final String key;
    	final String value;
    	public Pair(String key, String value) {this.key = key; this.value = value;}
		public String getKey() {
			return key;
		}
		public String getValue() {
			return value;
		}
    }
    
    
  
    
    public static void main( String[] args ) throws Exception
    {
    	try (InMemoryAllocator alloc = InMemoryAllocator.load("target/map_bulk.dump")) 
    	{
    		try(Snapshot snp = alloc.master().branch()) 
    		{
    			UUID name = UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fd64");
    			
    			try(MapSS map = snp.find(MapSS.class, name))
    			{
    				try(MapSSIterator iter = map.begin())
    				{
    					long t0 = System.currentTimeMillis();
    					
    					//iter.skip(map.size() - 1000);
    					
    					long count = iter.read(new AbstractMapSSBufferConsumer() {
							@Override
							public boolean consume(String key, String value) 
							{
								//System.out.println("Entry: " + key+" -- " + value);
								return true;
							}
    					});
    					
    					System.out.println("Reading time: "+(System.currentTimeMillis() - t0)+", count: "+count);
    					System.out.println("Iter pos = " + iter.pos());
    				}
    			}
    		}
    	}
    }
}
