package mariana.examples;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

public class CounterPerfromanceEx {
	
	static class FreeCounter implements Runnable {

		long value = 0;
		
		@Override
		public void run() {
			long t0 = System.currentTimeMillis();
			while (value < 1000000000l)
			{
				value++;
			}
			System.out.println(this.getClass() + ": " + value + " -- " + (System.currentTimeMillis() - t0));
		}
	}
	
	static class LockedCounter implements Runnable {

		private final ReentrantLock lock = new ReentrantLock(); 

		long value = 0;
		
		@Override
		public void run() {
			long t0 = System.currentTimeMillis();
			while (value < 1000000000l)
			{
				lock.lock();
				value++;
				lock.unlock();
			}
			System.out.println(this.getClass() + ": " + value + " -- " + (System.currentTimeMillis() - t0));
		}
	}
	
	static class MonitoredCounter implements Runnable { 

		long value = 0;
		
		@Override
		public void run() {
			long t0 = System.currentTimeMillis();
			
			while (value < 1000000000l)
			{
				synchronized(this) {					
					value++;
				}
			}
			
			System.out.println(this.getClass() + ": " + value + " -- " + (System.currentTimeMillis() - t0));
		}
	}
	
	static class VolatileCounter implements Runnable { 

		volatile long value = 0;
		
		@Override
		public void run() {
			long t0 = System.currentTimeMillis();
			
			while (value < 1000000000l)
			{
				value++;				
			}
			
			System.out.println(this.getClass() + ": " + value + " -- " + (System.currentTimeMillis() - t0));
		}
	}
	
	static class AtomicCounter implements Runnable { 

		AtomicInteger value = new AtomicInteger(0);
		
		@Override
		public void run() {
			long t0 = System.currentTimeMillis();
			
			while (value.incrementAndGet() < 1000000000l)
			{
			}
			
			System.out.println(this.getClass() + ": " + value.get() + " -- " + (System.currentTimeMillis() - t0));
		}
	}
	
	public static void run(Runnable r, int threads) 
	{
		List<Thread> th = new ArrayList<>();
		
		for (int t = 0; t < threads; t++) {
			th.add(new Thread(r));
		}
		
		for (int t = 0; t < threads; t++) {
			th.get(t).start();
		}
		
		for (int t = 0; t < threads; t++) {
			try {
				th.get(t).join();
			} 
			catch (InterruptedException e) {
			}
		}
	}
	

	public static void main(String[] args) throws InterruptedException 
	{
//		new FreeCounter().run();
//		new ().run();
//		new LockedCounter().run();
//		new AtomicCounter().run();
		
		run(new AtomicCounter(), 8);
		
		

	}
}
