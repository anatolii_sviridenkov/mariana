
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.examples;

import java.nio.ByteBuffer;
import java.util.UUID;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.map.str.str.MapSS;
import mariana.v1.api.ctr.map.str.str.MapSSIterator;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.FileOutputBuffer;

public class NativeApp
{
    static
    {
        Mariana.init();
    }
    
    public static void main( String[] args ) throws Exception
    {
    	UUID name = null;
    	
    	int size = 10;
    	
    	try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot snp = alloc.master().branch()) 
    		{
    			try(MapSS map = snp.create(MapSS.class, UUID.randomUUID()))
    			{
    				name = map.name();
    				
    				ByteBuffer buf = ByteBuffer.allocateDirect(1024);
    				
    				buf.position(16);
    				buf.limit(128);
    				
    				ByteBuffer slice = buf.slice();
    				
    				Mariana.testBB(slice);

    				System.out.println("Name: "+name);
    				
    				long t0 = System.currentTimeMillis();
    				for (int c = 0; c < size; c++)
    				{
    					map.assign("Boo " + c, "Zoo " + c).close();    						
    				}
    				
    				System.out.println("Size: " + map.size() + ", time = " + (System.currentTimeMillis() - t0));
    				
    				Mariana.dumpCounters();
    				
    				String key = "Boo " + (size / 2);
    				
    				try(MapSSIterator iter = map.find(key)) 
    				{
    					if (iter.isFound(key))
    					{
    						System.out.println("Found key '"+ key + "', value = "+iter.value());
    					}
    					else {
    						System.out.println("Key '" + key + "' not found!, pos = " + iter.pos());
    						if (!iter.isEnd())
    						{
    							System.out.println("Value for pos " + iter.pos()+" is "+iter.value()+", key is "+iter.key());
    						}
    					}
    				}
    			}

    			snp.commit();
    			
//    			snp.dump("target/snp.dump");

    			snp.setAsMaster();
    		}

//    		alloc.dump("target/myalloc.dir");
    		alloc.store("target/myalloc.dump");

    		try(FileOutputBuffer fileBuffer = new FileOutputBuffer("target/myalloc_raf.dump")) 
    		{
    			alloc.store(fileBuffer);
    		}
    	}
    	
//    	try(FileInputBuffer ibuf = new FileInputBuffer("target/myalloc_raf.dump")) 
//    	{
//    		try (InMemoryAllocator alloc = InMemoryAllocator.load(ibuf)) 
//    		{
//    			try(Snapshot snp = alloc.master()) 
//    			{
//    				try (MapSS map = snp.find(MapSS.class, name)) 
//    				{
//    					try(MapIterator iter = map.begin()) 
//    					{
//    						while (!iter.isEnd()) {
//    							System.out.println("Key: " + iter.key()+" -- " + iter.value());    				
//    							iter.next();
//    						}
//    					}
//    				}
//    			}
//    		}
//    	}
    }
}
