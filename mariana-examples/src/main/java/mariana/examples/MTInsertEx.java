
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.examples;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.set.bytes.SetBIterator;
import mariana.v1.api.ctr.set.bytes.fx.SetFxB;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.FileOutputBuffer;
import mariana.v1.tools.BufferTools;
import mariana.v1.tools.Ticker;

public class MTInsertEx
{
    static
    {
        Mariana.init();
    }
    

    static class Inserter implements Runnable {
    	
    	private Snapshot parent;
    	
    	private UUID name;
    	private byte[][] data;
    	
    	private UUID snapshotId;
    	
    	public Inserter(Snapshot parent, UUID name, byte[][] data) {
			this.parent = parent;
			this.name = name;
			this.data = data;
		}
    	
		@Override
		public void run() 
		{
			try(Snapshot snp = parent.branch()) 
			{
				snapshotId = snp.uuid();
				
				Ticker ticker = new Ticker(100000);

				try(SetFxB set = snp.create(SetFxB.class, name)) 
				{
					for (int c = 0; c < data.length; c++) 
					{
						set.insert(ByteBuffer.wrap(data[c]));

						if (ticker.isThreshold())
						{
							System.out.println(Thread.currentThread().getName() + ": inserted " + (ticker.getTicks() + 1) +" in " + ticker.duration());
							ticker.nextThreshold();
						}

						ticker.tick();
					}
				}
				finally {
					System.out.println(Thread.currentThread().getName() + ": total time: " + (System.currentTimeMillis() - ticker.getStartTime()));
				}
				
				snp.commit();
			}
		}

		public UUID getSnapshotId() {
			return snapshotId;
		}

		public UUID getName() {
			return name;
		}
    }
    
    static class Checker implements Runnable {

    	private Snapshot snp;
    	private UUID ctrName;
		private byte[][] data;
    	
    	public Checker(Snapshot snp, UUID ctrName, byte[][] data) 
    	{
    		this.snp = snp;
			this.ctrName = ctrName;
			this.data = data;
    	}
    	
		@Override
		public void run() 
		{
			long t0 = System.currentTimeMillis();
			try (SetFxB set = snp.find(SetFxB.class, ctrName))
			{
				for (int c = 0; c < data.length; c++) 
				{
					ByteBuffer key = ByteBuffer.wrap(data[c]);
//					try(SetBIterator ii = set.findB(key))
//					{
//						if (!ii.isFoundB(key)) {
//							System.out.println(Thread.currentThread().getName() + " can't find key " + BufferTools.toString(key));
//						}
//					}
					if (!set.contains(key))
					{
						System.out.println(Thread.currentThread().getName() + " can't find key " + BufferTools.toString(key));
					}
				}
			}
			finally {
				snp.close();
				System.out.println(Thread.currentThread().getName() + ": check time: " + (System.currentTimeMillis() - t0));
			}
		}
    }
    
    
    public static void main( String[] args ) throws Exception
    {
    	Random RNG = new Random(0);
    	
    	try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{    			
    			int size 		= 3000000;
    			int dataLength 	= 20;
    			int threads 	= 4;

    			byte[][] array = new byte[size][];

    			for (int c = 0; c < array.length; c++) 
    			{
    				byte[] data = new byte[dataLength];
    				RNG.nextBytes(data);
    				array[c] = data;
    			}
    			
    			List<Inserter> inserters 	 = new ArrayList<>();
    			List<Thread> inserterThreads = new ArrayList<>();

    			for (int c = 0; c < threads; c++)
    			{
    				UUID ctr1 = UUID.randomUUID();
    				Inserter inserter = new Inserter(master, ctr1, array);
    				
    				inserters.add(inserter);

    				Thread thread = new Thread(inserter);
    				inserterThreads.add(thread);
    				thread.start();
    			}
    			
    			for (Thread th: inserterThreads) {
					th.join();
    			}
    			
    			try(Snapshot snp = master.branch()) 
    			{
    				snp.lockDataForImport();
    				
    				for (Inserter inserter: inserters) 
    				{
    					try (Snapshot src = alloc.find(inserter.getSnapshotId())) 
    					{	
    						snp.importNewCtrFrom(src, inserter.getName());
    						src.drop();
    					}
    				}
    				
    				snp.commit();
    				snp.setAsMaster();
    			}
    			
    			List<Thread> checkers = new ArrayList<>();
    			
    			for (Inserter inserter: inserters) 
				{
    				Checker checker = new Checker(alloc.master(), inserter.getName(), array); 
    				Thread thread = new Thread(checker);
    				checkers.add(thread);
    				thread.start();
				}
    			
    			for (Thread th: checkers) {
					th.join();
    			}
    		}
    		
    		
    		long td0 = System.currentTimeMillis();
    		
    		try(FileOutputBuffer fileBuffer = new FileOutputBuffer("target/mt.memoria")) 
    		{
    			alloc.store(fileBuffer);
    		}

    		System.out.println("Store time: " + (System.currentTimeMillis() - td0) + " ms");
    		
//    		alloc.dump("target/mt.dir");
    	}
    }
}
