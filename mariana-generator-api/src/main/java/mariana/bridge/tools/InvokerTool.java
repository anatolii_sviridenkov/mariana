
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.bridge.tools;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import mariana.bridge.Import;
import mariana.bridge.MarianaConstants;

public class InvokerTool {
	
	public static String getPairClassName(Class<?> cls) 
	{
		String pkgName 		= cls.getPackage().getName();		
		String className 	= cls.getSimpleName() + MarianaConstants.IMPORTED_JAVA_CLASS_SUFFIX;
		
		return pkgName + "." + className;
	}
	
	public static Class<?> getPairClass(Class<?> cls) 
	{
		String peerClassName = getPairClassName(cls);
		
		try {
			return InvokerTool.class.getClassLoader().loadClass(peerClassName);
		} 
		catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static MethodHandle getPairHandle(MethodHandles.Lookup lookup, Class<?> cls, String name, Class... args) 
	{
		try {
			Class peerClass = getPairClass(cls);
			
			return lookup.findStatic(peerClass, name, MethodType.methodType(cls, args));
		} 
		catch (NoSuchMethodException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	public static <Ctr> Ctr createJavaPair(Class<Ctr> clazz, long handle) 
	{
		try {
			@SuppressWarnings("unchecked")
			Class<Ctr> peerClass = (Class<Ctr>) InvokerTool.class.getClassLoader().loadClass(getPairClassName(clazz));
			
			Constructor<Ctr> ctr = peerClass.getConstructor(long.class);
			
			return ctr.newInstance(handle);
		} 
		catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String getNativeType(Class<?> clazz) 
	{
		mariana.bridge.Ctr c = clazz.getAnnotation(mariana.bridge.Ctr.class);
		
		if (c != null) 
		{
			return c.name();
		}
		else {
			throw new RuntimeException("Class " + clazz.getName() + " has no any native type annotation");
		}
	}
}
