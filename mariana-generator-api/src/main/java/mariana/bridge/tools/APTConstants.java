package mariana.bridge.tools;

public interface APTConstants {
	public static final String METADATA_CLASS_PACKAGE 	= "mariana.v1";
	public static final String METADATA_CLASS_NAME 		= "MarianaMetadataImpl";
	public static final String METADATA_FULL_CLASS_NAME = METADATA_CLASS_PACKAGE + "." + METADATA_CLASS_NAME;
}
