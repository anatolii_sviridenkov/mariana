package mariana.v1.rdf.dictionary;

import java.util.Collections;
import java.util.UUID;

import org.openrdf.model.Value;

import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLB;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLBEntryWalkerAdapter;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLBIterator;
import mariana.v1.framework.consumers.LastValueConsumer;
import mariana.v1.hashmap.MMapEntriesProducer;
import mariana.v1.hashmap.MMapEntry;
import mariana.v1.hashmap.MMapEntryParser;
import mariana.v1.io.buffer.IOByteBuffer;
import mariana.v1.io.serialization.FlatSequenceParser;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.rdf.model.ValueInputHandler;
import mariana.v1.rdf.model.ValueOutputHandler;

public class IDValueMapping implements AutoCloseable {
	private MultimapLB mmap;

	private ObjectInputHandler inputHandler 	= new ValueInputHandler();
	private ObjectOutputHandler outputHandler	= new ValueOutputHandler();
	
	public static class Entry {
		private long key;
		private Value value;
		
		public Entry(long key, Value value) {
			this.key 	= key;
			this.value  = value;
		}
		
		public long getKey() {
			return key;
		}
		
		public Value getValue() {
			return value;
		}
		
		@Override
		public String toString() {
			return "Entry[" + key + ", " + value + "]";
		}
	}
	
	
	public class Iterator implements java.util.Iterator<Entry>, AutoCloseable {

		private MultimapLBIterator mmapIterator;
		private MultimapLBEntryWalkerAdapter walker;
		
		private IDValueScanner scanner = new IDValueScanner();
		private java.util.Iterator<Entry> valuesIter;
		
		private long fetches = 0;
		private boolean done = false;

		private MMapEntryParser parser = new MMapEntryParser(inputHandler, scanner);
				
		public Iterator(MultimapLBIterator mmapIterator) 
		{
			this.mmapIterator = mmapIterator;
			if (mmapIterator != null) {
				walker = new MultimapLBEntryWalkerAdapter();
			}
		}
		
		public void close() 
		{
			if (mmapIterator != null) 
			{
				mmapIterator.close();
				walker.close();
			}
		}
		
		public boolean hasNext() 
		{
			if (mmapIterator == null) {
				return false;
			}
			
			if (valuesIter == null) 
			{
				fetch();
				return valuesIter.hasNext();
			}
			
			if (valuesIter.hasNext()) {
				return true;
			}
			else if (!done) 
			{
				fetch();
				return valuesIter.hasNext();
			}
			else {
				return false;	
			}
		}
		
		public Entry next() {
			return valuesIter.next();
		}
		
		private void fetch()
		{			
			scanner.clear();
			
			IOByteBuffer ioBuffer = new IOByteBuffer(walker.getBuffer());
			
			long total = mmapIterator.populate(walker, Long.MAX_VALUE, fetches > 0);
			
			ioBuffer.flip();
			parser.apply(ioBuffer, 1);
			
			fetches++;			
			
			done = total <= 0;
			valuesIter = scanner.getEntries().iterator();
		}
	}

	
	
	
	
	private IDValueMapping(MultimapLB mmap) 
	{
		this.mmap = mmap;
	}
	
	public Iterator iterator() {
		return new Iterator(mmap.begin());
	}
	
	public Value get(long id) 
	{
		try(MultimapLBIterator ii = mmap.find(id)) 
		{
			if (ii.isFound(id)) 
			{
				ii.next();
				
				LastValueConsumer<Value> consumer = new LastValueConsumer<>();
				ii.readValues(new FlatSequenceParser(inputHandler, consumer));
				return consumer.getValue();
			}
		}
		
		return null;
	}
	
	public void set(long id, Value value) 
	{
		try (MultimapLBIterator ii = mmap.find(id)) 
		{
			if (ii.isFound(id)) 
			{
				ii.remove(1);
			}
			
			ii.insert(new MMapEntriesProducer(
					Collections.singletonList(new MMapEntry(id, Collections.singletonList(value))).iterator(), 
					outputHandler
			));
		}
	}
	
	public boolean remove(long id) 
	{
		try(MultimapLBIterator ii = mmap.find(id)) 
		{
			if (ii.isFound(id)) 
			{
				ii.remove(1);
				return true;
			}
		}
		
		return false;
	}
	

	public static IDValueMapping findOrCreate(Snapshot snapshot, UUID name) {
		return new IDValueMapping(snapshot.findOrCreate(MultimapLB.class, name));
	}
	
	public static IDValueMapping find(Snapshot snapshot, UUID name) {
		return new IDValueMapping(snapshot.find(MultimapLB.class, name));
	}


	@Override
	public void close() {
		mmap.close();
	}
}
