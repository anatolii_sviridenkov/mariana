package mariana.v1.rdf.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.openrdf.model.Value;

import mariana.v1.hashmap.MMapEntryConsumer;

public class IDValueScanner implements MMapEntryConsumer {

	private long key;
	
	private List<IDValueMapping.Entry> entries = new ArrayList<>();
	
	@Override
	public void start() {}

	@Override
	public void startEntry(long key) {
		this.key = key;
	}

	@Override
	public void startValues() {}

	@Override
	public void value(Object value) {
		entries.add(new IDValueMapping.Entry(key, (Value)value));
	}

	@Override
	public void endValues() {}

	@Override
	public void endEntry() {}

	@Override
	public void end() {}

	public void clear() {
		entries.clear();
	}

	public List<IDValueMapping.Entry> getEntries() {
		return entries;
	}
}
