package mariana.v1.rdf.dictionary;

import java.util.UUID;

import org.openrdf.model.Resource;
import org.openrdf.model.Value;

import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLB;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLBIterator;
import mariana.v1.hashmap.MHashMap;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.UVLenObjectInputHandler;
import mariana.v1.io.serialization.UVLenObjectOutputHandler;
import mariana.v1.rdf.model.ValueInputHandler;
import mariana.v1.rdf.model.ValueOutputHandler;

public class ValueIDMapping extends MHashMap<Value, Long> {

	protected ValueIDMapping(
			MultimapLB mmap, 
			ObjectOutputHandler keyOutputHandler, 
			ObjectInputHandler keyInputHandler,
			ObjectOutputHandler valueOutputHandler, 
			ObjectInputHandler valueInputHandler) 
	{
		super(mmap, keyOutputHandler, keyInputHandler, valueOutputHandler, valueInputHandler);		
	}
	
	
	public static ValueIDMapping findOrCreate(Snapshot snp, UUID name) 
	{
		return new ValueIDMapping(
				snp.findOrCreate(MultimapLB.class, name), 
				new ValueOutputHandler(), 
				new ValueInputHandler(), 
				new UVLenObjectOutputHandler(), 
				new UVLenObjectInputHandler()
		);
	}
	
	public static ValueIDMapping find(Snapshot snp, UUID name) 
	{
		return new ValueIDMapping(
				snp.find(MultimapLB.class, name), 
				new ValueOutputHandler(), 
				new ValueInputHandler(), 
				new UVLenObjectOutputHandler(), 
				new UVLenObjectInputHandler()
		);
	}


	public void begin() {
		try(MultimapLBIterator ii = mmap.begin()) 
		{
			ii.dump();
		}
	}
}
