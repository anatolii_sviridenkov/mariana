package mariana.v1.rdf.store.actors.triples;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.mariana.rdf.triples.IndexSpec;
import org.mariana.rdf.triples.Triple;
import org.mariana.rdf.triples.TriplePattern;
import org.mariana.rdf.triples.TripleSet;
import org.mariana.rdf.triples.iterator.TripleIterator;

import mariana.v1.api.alloc.Snapshot;
import mariana.v1.framework.actors.Async;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.rdf.store.actors.Status;
import mariana.v1.rdf.store.config.CtrNames;
import mariana.v1.rdf.store.config.TripleIndexNames;

public class TripleIndexShardActor implements TripleIndexShard {

	private final Snapshot snp;
	private final UUID ctrName;
	private final String order;
	private final String branchName;
	
	private TripleSet set;
	

	public TripleIndexShardActor(Snapshot snp, UUID ctrName, String order, String branchName) 
	{
		this.snp = snp;
		this.ctrName = ctrName;
		this.order = order;
		this.branchName = branchName;
	}
	
	@Override
	public void addStatements(List<Triple> triples) 
	{
		long t0 = System.currentTimeMillis();
		
		if (set == null) 
		{
			set = TripleSet.findOrCreate(snp, ctrName, order);
		}
		
		for (Triple triple: triples) 
		{
			set.insert(triple);
		}
		
		System.out.println("++ " + order + ": Insert " + triples.size() + " triples in " + (System.currentTimeMillis() - t0));
	}

	@Override
	public Async<Status> getStatus() {
		return Async.ofNull();
	}

	@Override
	public Async<Void> commit() 
	{
		snp.commit();
		snp.dropParent();
		
		snp.setAsBranch(branchName);
		
		return Async.ofNull();
	}

	@Override
	public Async<Void> rollback() 
	{
		snp.drop();
		return Async.ofNull();
	}

	@Override
	public Async<Void> close() 
	{
		if (set != null) {
			set.close();
		}
		snp.close();
	
		return Async.ofNull();
	}

	@Override
	public Async<Void> flush() 
	{		
		return Async.ofNull();
	}
	
	private class TripleIteration implements IterationState 
	{
		private final TripleIterator<Triple> iterator;
		private final int batchSize;

		TripleIteration(TripleIterator<Triple> iterator, int batchSize) {
			this.iterator = iterator;
			this.batchSize = batchSize;
		}
				
		public void close() 
		{
			try {
				iterator.close();
			}
			catch (Exception ex) 
			{
				ex.printStackTrace();
			}
		}

		TripleIterator<Triple> getIterator() {
			return iterator;
		}

		public int getBatchSize() {
			return batchSize;
		}
	}

	@Override
	public Async<IterationState> find(TriplePattern pattern, int batchSize) 
	{
		if (set == null) 
		{
			set = TripleSet.find(snp, ctrName, order);
		}
		
		return Async.of(new TripleIteration(set.find(pattern), batchSize));
	}

	@Override
	public Async<List<Triple>> next(IterationState state) 
	{
		TripleIteration ii = (TripleIteration) state;
		TripleIterator<Triple> iterator = ii.getIterator();
		final int batchSize = ii.getBatchSize();
		
		long t0 = System.currentTimeMillis();

		int size = 0;
		
		try {
			
			if (iterator.hasNext()) 
			{
				List<Triple> list = new ArrayList<>();

				for (int c = 0; c < batchSize && iterator.hasNext(); c++)
				{
					list.add(iterator.next());
				}
				
				size = list.size();

				return Async.of(list);
			}
			else {
				return Async.ofNull();
			}
		}
		finally {
//			System.out.println("Get Triples time for "+order+": " + (System.currentTimeMillis() - t0) + " -- " + size);
		}
	}

	@Override
	public Async<Void> close(IterationState state) 
	{
		TripleIteration ii = (TripleIteration) state;
		ii.close();
		return Async.ofNull();
	}

	public static void init(InMemoryAllocator allocator, int shard, CtrNames ctrNames, IndexSpec order) 
	{
		TripleIndexNames names = ctrNames.getTripleIndexNames(order, shard);
		
		try (Snapshot master = allocator.master()) 
		{
			try (Snapshot snp = master.branch())
			{
				String orderSpec = order.name();
				
				TripleSet.findOrCreate(snp, names.getCtrName(), orderSpec).close();
				
				snp.commit();
				snp.setAsBranch(orderSpec + "-" + shard);
			}
		}	
	}
}
