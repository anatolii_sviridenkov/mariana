package mariana.v1.rdf.store;

import java.io.File;
import java.io.FilenameFilter;

import org.openrdf.model.ValueFactory;
import org.openrdf.query.algebra.evaluation.federation.FederatedServiceResolver;
import org.openrdf.query.algebra.evaluation.federation.FederatedServiceResolverClient;
import org.openrdf.query.algebra.evaluation.federation.FederatedServiceResolverImpl;
import org.openrdf.query.algebra.evaluation.impl.EvaluationStatistics;
import org.openrdf.sail.NotifyingSailConnection;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;
import org.openrdf.sail.helpers.AbstractNotifyingSail;

import mariana.bridge.MarianaException;
import mariana.v1.Mariana;
import mariana.v1.framework.actors.MarianaActorSystem;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.rdf.model.RdfValueFactory;
import mariana.v1.rdf.store.config.StoreMetadata;
import mariana.v1.rdf.store.config.TripleStoreParameters;



public class TripleStore extends AbstractNotifyingSail implements FederatedServiceResolverClient, AutoCloseable {

	static {
		Mariana.init();
	}
	
	private static final String DATA_MEMORIA = "data.memoria";
	
	private RdfValueFactory 				valueFactory = new RdfValueFactory();
	private FederatedServiceResolver 		serviceResolver;
	private FederatedServiceResolverImpl 	dependentServiceResolver;
	private TripleStoreParameters 			config = new TripleStoreParameters.Builder().build();
	
	private InMemoryAllocator allocator;
	
	private MarianaActorSystem actorSystem;
	
	@Override
	public boolean isWritable() throws SailException {
		return true;
	}

	@Override
	public ValueFactory getValueFactory() {		
		return valueFactory;
	}

	

	@Override
	public void setFederatedServiceResolver(FederatedServiceResolver resolver) {
		this.serviceResolver = resolver;		
	}
	
	public synchronized FederatedServiceResolver getFederatedServiceResolver() {
		if (serviceResolver == null) {
			if (dependentServiceResolver == null) {
				dependentServiceResolver = new FederatedServiceResolverImpl();
			}
			return serviceResolver = dependentServiceResolver;
		}
		return serviceResolver;
	}

	@Override
	protected NotifyingSailConnection getConnectionInternal() throws SailException {		
		return new TripleStoreConnection(allocator, this);		
	}

	@Override
	protected void shutDownInternal() throws SailException {
		if (allocator != null) 
		{
			allocator.close();
			allocator = null;
		}
		
		if (actorSystem != null) 
		{
			actorSystem.shutdown();
		}
	}
	
	public EvaluationStatistics getEvaluationStatistics() {
		return new EvaluationStatistics();
	}

	
	@Override
	public void close() {
		shutDown();
	}
	
	@Override
	protected void connectionClosed(SailConnection connection) {
		super.connectionClosed(connection);
	}

	
	
	@Override
	protected void initializeInternal() throws SailException 
	{
		File dataDir = getDataDir();
		
		if (dataDir.exists() && dataDir.isDirectory() && containsDataFiles(dataDir)) 
		{
			openExistingStore();
		}
		else {
			dataDir.mkdirs();
			createNewStore();
		}
		
		actorSystem = new MarianaActorSystem();
	}
	
	private boolean containsDataFiles(File dataDir) 
	{
		String[] list = dataDir.list(new FilenameFilter() {			
			@Override
			public boolean accept(File dir, String name) 
			{
				return DATA_MEMORIA.equalsIgnoreCase(name);
			}
		});
		
		return list.length == 1;
	}


	private void openExistingStore() 
	{
		allocator = InMemoryAllocator.load(getDataDir() + File.separator + DATA_MEMORIA);
		
		try {
			StoreMetadata metadata = ShardedTripleStoreTxn.getMetadata(allocator);
			config = metadata.getConfig();
		}
		catch (Exception ex) 
		{
			allocator.close();
			allocator = null;
			
			throw new MarianaException("Provided data file does not contain store metadata or store has wrong format");
		}
	}
	

	private void createNewStore() 
	{
		allocator = InMemoryAllocator.create();
		
		try {
			ShardedTripleStoreTxn.init(allocator, config);
		}
		catch (Exception ex) 
		{
			allocator.close();
			allocator = null;
		}
	}

	public void store() {
		allocator.store(getDataDir() + File.separator + DATA_MEMORIA);
	}
	

	public TripleStoreParameters getConfig() {
		return config;
	}

	public void setConfig(TripleStoreParameters config) {
		this.config = config;
	}

	public MarianaActorSystem getActorSystem() {
		return actorSystem;
	}

	public InMemoryAllocator getAllocator() {
		return allocator;
	}
}
