package mariana.v1.rdf.store.actors.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.mariana.rdf.triples.TriplePattern;
import org.openrdf.model.IRI;
import org.openrdf.model.Resource;
import org.openrdf.model.Value;

import com.gs.collections.api.iterator.LongIterator;
import com.gs.collections.api.iterator.MutableLongIterator;
import com.gs.collections.api.list.primitive.LongList;
import com.gs.collections.api.map.primitive.LongObjectMap;
import com.gs.collections.api.map.primitive.ObjectLongMap;
import com.gs.collections.api.set.MutableSet;
import com.gs.collections.api.set.primitive.LongSet;
import com.gs.collections.api.tuple.Pair;
import com.gs.collections.impl.map.mutable.primitive.LongObjectHashMap;
import com.gs.collections.impl.map.mutable.primitive.ObjectLongHashMap;
import com.gs.collections.impl.set.mutable.UnifiedSet;
import com.gs.collections.impl.set.mutable.primitive.LongHashSet;
import com.gs.collections.impl.tuple.Tuples;

import mariana.v1.framework.actors.Async;
import mariana.v1.rdf.model.Values;
import mariana.v1.rdf.store.actors.AsyncTxnPerformer;
import mariana.v1.rdf.store.actors.Status;
import mariana.v1.rdf.store.actors.triples.TripleMetadata;

public class DictionaryActor implements ShardedDictionary {

	private final List<DictionaryShard> shards;
	private final int nShards;
	
	public DictionaryActor(List<DictionaryShard> shards) 
	{
		this.shards = new ArrayList<>(shards);
		this.nShards = shards.size();
	}
	
	@Override
	public Async<Status> getStatus() {
		return Async.ofNull();
	}

	
	@Override
	public Async<ObjectLongMap<Value>> addValues(MutableSet<Value> values) 
	{
		long t0 = System.currentTimeMillis();

		try {

			@SuppressWarnings("unchecked")
			MutableSet<Value>[] splits = new MutableSet[nShards];

			for (int c = 0; c < splits.length; c++) 
			{
				splits[c] = UnifiedSet.newSet();
			}

			for (Value val: values)
			{
				int shardNum = shard(val.hashCode());
				splits[shardNum].add(val);
			}

			List<Async<ObjectLongMap<Value>>> results = new ArrayList<>();

			for (int c = 0; c < splits.length; c++)
			{
				results.add(shards.get(c).addValues(splits[c]));
			}

			ObjectLongHashMap<Value> map = new ObjectLongHashMap<>();

			for (Async<ObjectLongMap<Value>> result: results)
			{
				map.putAll(result.get());
			}

			return Async.of(map);
		}
		finally {
			System.out.println("## AddValues finished in " + (System.currentTimeMillis() - t0));			
		}
	}
	
	private int shard(int hashCode) {
		return hashCode >= 0 ? hashCode % nShards : (-hashCode) % nShards;  
	}
	
	private int shard(long hashCode) {
		return (int)(hashCode >= 0 ? hashCode % nShards : (-hashCode) % nShards);  
	}

	@Override
	public Async<Void> commit() 
	{
		AsyncTxnPerformer.perform(shards, AsyncTxnPerformer::commit);
		return Async.ofNull();
	}

	@Override
	public Async<Void> rollback() 
	{
		AsyncTxnPerformer.perform(shards, AsyncTxnPerformer::rollback);
		return Async.ofNull();
	}

	@Override
	public Async<Void> close() 
	{
		AsyncTxnPerformer.perform(shards, AsyncTxnPerformer::close);
		return Async.ofNull();
	}

	@Override
	public Async<Void> flush() 
	{
		AsyncTxnPerformer.perform(shards, AsyncTxnPerformer::flush);
		return Async.ofNull();
	}

//	@Override
//	public void setIDBase(long idBase) 
//	{
//		for (DictionaryShard shard: shards) 
//		{
//			shard.setIDBase(idBase);
//		}
//	}

	@Override
	public Async<Long> getMaxID() 
	{
		long max = -1;
		
		for (DictionaryShard shard: shards) 
		{
			long id = shard.getMaxID().get();
			if (id > max) {
				max = id;
			}
		}
		
		max += nShards - (max % nShards);
		
		return Async.of(max);
	}
	
	public Async<Long> resolveID(Value value) 
	{
		if (value != null)
		{
			return Async.of(resolveID_(value));
		}
		else {
			return Async.of(Values.CODE_NULL);
		}
	}
	
	public Async<LongObjectMap<Value>> resolve(List<Value> values) 
	{
		LongObjectHashMap<Value> map = new LongObjectHashMap<>();
		
		List<Pair<Value, Async<Long>>> results = new ArrayList<>();
		
		for (Value value: values) 
		{
			results.add(Tuples.pair(value, resolveIDAsync(value)));
		}
		
		for (Pair<Value, Async<Long>> result: results) 
		{
			map.put(result.getTwo().get(), result.getOne());
		}
		
		return Async.of(map);
	}
	
	private Long resolveID_(Value value) 
	{
		return resolveIDAsync(value).get();
	}
	
	private Async<Long> resolveIDAsync(Value value) 
	{
		int shard = shard(value.hashCode());
		return shards.get(shard).resolveID(value);
	}
	
	public Async<LongSet> resolve(Value... values) 
	{
		if (values != null) 
		{
			if (values.length < nShards) 
			{
				LongHashSet result = new LongHashSet();
				
				for (Value value: values) 
				{
					int shard = shard(value.hashCode());
					Long id = shards.get(shard).resolveID(value).get();
					if (id != null) 
					{
						result.add(id);
					}
				}
				
				return Async.of(result);
			}
			else {
				@SuppressWarnings("unchecked")
				List<Value>[] splits = new List[nShards];

				for (int s = 0; s < nShards; s++)
				{
					splits[s] = new ArrayList<>();
				}

				for (Value value: values) 
				{
					int shard = shard(value.hashCode());
					splits[shard].add(value);
				}

				List<Async<LongList>> results = new ArrayList<>();

				for (int s = 0; s < nShards; s++)
				{
					if (splits[s].size() > 0) 
					{
						results.add(shards.get(s).resolveIDs(splits[s]));
					}
				}
				
				LongHashSet ids = new LongHashSet();
				
				for (Async<LongList> result: results) 
				{					
					ids.addAll(result.get());
				}
				
				return Async.of(ids);
			}
		}
		else {
			return Async.ofNull();
		}
	}
	
	public Async<TriplePattern> resolve(Resource subject, IRI predicate, Value object, Resource... context) 
	{
		TriplePattern triple = new TriplePattern();

		if (subject != null) 
		{
			Long id = resolveID_(subject);
			if (id != null) 
			{
				triple.setSubject(id);
			}
			else {
				triple.setSubject(Values.CODE_EMPTY);
			}
		}
		else {			
			triple.setSubject(Values.CODE_NULL);
		}

		if (predicate != null) 
		{
			Long id = resolveID_(predicate);
			if (id != null) 
			{
				triple.setProperty(id);
			}
			else {				
				triple.setProperty(Values.CODE_EMPTY);
			}
		}
		else {
			triple.setProperty(Values.CODE_NULL);
		}

		if (object != null) 
		{
			Long id = resolveID_(object);
			if (id != null) 
			{
				triple.setObject(id);
			}
			else {
				triple.setObject(Values.CODE_EMPTY);
			}
		}
		else {			
			triple.setObject(Values.CODE_NULL);
		}
		
		triple.setContext(resolve(context).get());
		
		return Async.of(triple);
	}

	@Override
	public Async<LongObjectMap<Value>> resolveValues(LongSet set) 
	{
		LongIterator ii = set.longIterator();
		
		LongHashSet[] splits = new LongHashSet[nShards];
		
		for (int c = 0; c < nShards; c++) 
		{
			splits[c] = new LongHashSet();
		}
		
		while (ii.hasNext())
		{
			long id = ii.next();
			int shard = shard(id);
			splits[shard].add(id);
		}
		
		List<Async<LongObjectMap<Value>>> results = new ArrayList<>();
		
		for (int c = 0; c < nShards; c++) 
		{
			results.add(shards.get(c).resolveValues(splits[c]));			
		}
		
		LongObjectHashMap<Value> mapping = new LongObjectHashMap<>();
		
		for (Async<LongObjectMap<Value>> result: results) 
		{
			mapping.putAll(result.get());
		}
		
		return Async.of(mapping);
	}
}
