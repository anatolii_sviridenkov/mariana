package mariana.v1.rdf.store;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mariana.rdf.triples.IndexSpec;
import org.mariana.rdf.triples.iterator.IteratorCloseableIteration;
import org.openrdf.model.IRI;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.evaluation.impl.EvaluationStatistics;
import org.openrdf.sail.SailException;
import org.openrdf.sail.base.SailSource;
import org.openrdf.sail.base.SailStore;

import fi.jumi.actors.ActorThread;
import info.aduna.iteration.CloseableIteration;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.framework.actors.MarianaActorSystem;
import mariana.v1.framework.pipeline.CloseableSequenceIterator;
import mariana.v1.framework.pipeline.CloseableStreamProducer;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.rdf.model.RdfValueFactory;
import mariana.v1.rdf.model.Values;
import mariana.v1.rdf.store.actors.TripleStoreTxn;
import mariana.v1.rdf.store.actors.TripleStoreTxnActor;
import mariana.v1.rdf.store.actors.dictionary.DictionaryActor;
import mariana.v1.rdf.store.actors.dictionary.DictionaryShard;
import mariana.v1.rdf.store.actors.dictionary.DictionaryShardActor;
import mariana.v1.rdf.store.actors.dictionary.ShardedDictionary;
import mariana.v1.rdf.store.actors.triples.TripleIndex;
import mariana.v1.rdf.store.actors.triples.TripleIndexActor;
import mariana.v1.rdf.store.actors.triples.TripleIndexShard;
import mariana.v1.rdf.store.actors.triples.TripleIndexShardActor;
import mariana.v1.rdf.store.config.ConfigStore;
import mariana.v1.rdf.store.config.CtrNames;
import mariana.v1.rdf.store.config.DictionaryNames;
import mariana.v1.rdf.store.config.StoreMetadata;
import mariana.v1.rdf.store.config.TripleIndexNames;
import mariana.v1.rdf.store.config.TripleStoreParameters;

public class ShardedTripleStoreTxn implements SailStore {

	private ValueFactory valueFactory = new RdfValueFactory();
	private InMemoryAllocator allocator;
	
	private StoreMetadata metadata;
	
	private final MarianaActorSystem actorSystem;
	
	private List<ActorThread> threads = new ArrayList<>();
	private final boolean readOnly;
	
	private TripleStoreTxn txn;
	
	public ShardedTripleStoreTxn(InMemoryAllocator allocator, boolean readOnly, MarianaActorSystem actorSystem) 
	{
		this.allocator 	 = allocator;
		this.readOnly 	 = readOnly;
		this.actorSystem = actorSystem;
		
		metadata = getMetadata(allocator);
		
		if (readOnly)
		{
			List<Snapshot> dictionaryBranches 			   = findDictionaryBranches();
			Map<IndexSpec, List<Snapshot>> triplesBranches = findTriplesBranches();
			
			Snapshot master = allocator.master();
			
			txn = buildActors(master, dictionaryBranches, triplesBranches);
		}
		else {
			List<Snapshot> dictionaryBranches 			   = newDictionaryBranches();
			Map<IndexSpec, List<Snapshot>> triplesBranches = newTriplesBranches();
			
			try(Snapshot master = allocator.master()) 
			{
				txn = buildActors(master.branch(), dictionaryBranches, triplesBranches);
			}
		}
	}
	
	private TripleStoreTxn buildActors(Snapshot master, List<Snapshot> dictionarySnp, Map<IndexSpec, List<Snapshot>> triplesSnp) 
	{
		List<DictionaryShard> dictionaryShards = new ArrayList<>();
		
		CtrNames ctrNames = metadata.getCtrNames();
		
		for (int c = 0; c < dictionarySnp.size(); c++) 
		{
			ActorThread thread = actorSystem.getActors().startActorThread();
			threads.add(thread);
			
			DictionaryNames dictionaryNames = ctrNames.getDictionaryNames(c);
			
			DictionaryShard actor = thread.bindActor(DictionaryShard.class, new DictionaryShardActor(
					dictionarySnp.get(c),
					c,
					dictionarySnp.size(),
					metadata.getIdCounter(),
					dictionaryNames.getIdValueName(), 
					dictionaryNames.getValueIdName(), 
					Values.DICTIONARY + "-" + c
			)).tell();
			
			dictionaryShards.add(actor);
		}
		
		ActorThread dthread = actorSystem.getActors().startActorThread();
		threads.add(dthread);
		
		ShardedDictionary dictionary = dthread.bindActor(
				ShardedDictionary.class, 
				new DictionaryActor(dictionaryShards)
		).tell();

		
		Map<IndexSpec, TripleIndex> triplesIndexes = new HashMap<>();
		
		for (Map.Entry<IndexSpec, List<Snapshot>> entry: triplesSnp.entrySet()) 
		{
			IndexSpec spec 		= entry.getKey();
			List<Snapshot> snps = entry.getValue();
			
			List<TripleIndexShard> actors = new ArrayList<>();
			
			for (int c = 0; c < snps.size(); c++) 
			{
				ActorThread thread = actorSystem.getActors().startActorThread();
				threads.add(thread);
				
				TripleIndexNames tripleIndexNames = ctrNames.getTripleIndexNames(spec, c);
				
				TripleIndexShard actor = thread.bindActor(TripleIndexShard.class, new TripleIndexShardActor(
						snps.get(c),
						tripleIndexNames.getCtrName(),
						spec.name(),
						spec.name() + "-" + c
				)).tell();
				
				actors.add(actor);
			}
			
			ActorThread thread = actorSystem.getActors().startActorThread();
			threads.add(thread);
			
			TripleIndex index = thread.bindActor(TripleIndex.class, new TripleIndexActor(actors)).tell();
			
			triplesIndexes.put(spec, index);
		}
		
		ActorThread thread = actorSystem.getActors().startActorThread();
		threads.add(thread);
		
		TripleStoreTxn txnActor = thread.bindActor(TripleStoreTxn.class, new TripleStoreTxnActor(
				thread,
				master,
				dictionary, 
				triplesIndexes
		)).tell();
	
		return txnActor;
	}
	
	private List<Snapshot> findDictionaryBranches() 
	{
		List<Snapshot> dictionarySnp = new ArrayList<>();
		
		try {
			for (int c = 0; c < metadata.getConfig().getDictionaryShards(); c++) 
			{
				dictionarySnp.add(allocator.findBranch(Values.DICTIONARY + "-" + c));			
			}
		}
		catch (Exception ex)
		{
			close(dictionarySnp);
			
			dictionarySnp.clear();
		}
		
		return dictionarySnp;
	}
	
	private List<Snapshot> newDictionaryBranches() 
	{
		List<Snapshot> dictionarySnp = new ArrayList<>();
		
		try {
			for (int c = 0; c < metadata.getConfig().getDictionaryShards(); c++) 
			{
				try (Snapshot snp = allocator.findBranch(Values.DICTIONARY + "-" + c)) 
				{
					dictionarySnp.add(snp.branch());
				}
			}
		}
		catch (Exception ex)
		{
			dropAndClose(dictionarySnp);
			
			dictionarySnp.clear();
		}
		
		return dictionarySnp;
	}
	
	
	private Map<IndexSpec, List<Snapshot>> findTriplesBranches() 
	{
		Map<IndexSpec, List<Snapshot>> triples = new HashMap<>();
				
		try {
			for (IndexSpec spec: metadata.getConfig().getIndexes()) 
			{
				List<Snapshot> list = new ArrayList<>();
				
				for (int c = 0; c < metadata.getConfig().getTriplesShards(); c++) 
				{
					list.add(allocator.findBranch(spec.name() + "-" + c));			
				}
				
				triples.put(spec, list);
			}
		}
		catch (Exception ex)
		{
			for (List<Snapshot> list: triples.values()) 
			{
				close(list);

				list.clear();
			}
		}
		
		return triples;
	}
	
	private Map<IndexSpec, List<Snapshot>> newTriplesBranches() 
	{
		Map<IndexSpec, List<Snapshot>> triples = new HashMap<>();
				
		try {
			for (IndexSpec spec: metadata.getConfig().getIndexes()) 
			{
				List<Snapshot> list = new ArrayList<>();
				
				for (int c = 0; c < metadata.getConfig().getTriplesShards(); c++) 
				{
					try (Snapshot snp = allocator.findBranch(spec.name() + "-" + c)) 
					{
						list.add(snp.branch());
					}
				}
				
				triples.put(spec, list);
			}
		}
		catch (Exception ex)
		{
			for (List<Snapshot> list: triples.values()) 
			{
				dropAndClose(list);

				list.clear();
			}
		}
		
		return triples;
	}
	

	@Override
	public void close() throws SailException 
	{
		if (txn != null) 
		{
			txn.close().get();
			txn = null;
		}
		
		for (ActorThread thread: threads) 
		{
			thread.stop();
		}
		
		threads.clear();
	}

	@Override
	public ValueFactory getValueFactory() {
		return valueFactory;
	}

	@Override
	public EvaluationStatistics getEvaluationStatistics() {
		return null;
	}

	@Override
	public SailSource getExplicitSailSource() {
		return null;
	}

	@Override
	public SailSource getInferredSailSource() {
		return null;
	}
	
	public static StoreMetadata getMetadata(InMemoryAllocator allocator) 
	{
		try(Snapshot snp = allocator.master()) 
		{
			try(ConfigStore cfg = new ConfigStore(snp))
			{			
				return (StoreMetadata) cfg.get(Values.METADATA_CFG_NAME);
			}
		}
	}
	
	public static void init(InMemoryAllocator allocator, TripleStoreParameters params) 
	{
		try(Snapshot master = allocator.master()) 
		{
			try(Snapshot snp = master.branch()) 
			{
				try(ConfigStore cfg = new ConfigStore(snp))
				{			
					StoreMetadata meta = new StoreMetadata();
					meta.setConfig(params);
					meta.init();
					cfg.set(Values.METADATA_CFG_NAME, meta);
					
					snp.commit();
					snp.setAsMaster();
					
					for (int c = 0; c < params.getDictionaryShards(); c++) 
					{
						DictionaryShardActor.init(allocator, c, meta.getCtrNames());
					}
					
					for (IndexSpec spec: params.getIndexes()) 
					{
						for (int c = 0; c < params.getTriplesShards(); c++)
						{
							TripleIndexShardActor.init(allocator, c, meta.getCtrNames(), spec);
						}
					}
				}
			}
		}
	}
	
	private void close(Collection<Snapshot> col) 
	{
		for (Snapshot snp: col) {
			snp.close();
		}
	}
	
	private void dropAndClose(Collection<Snapshot> col) 
	{
		for (Snapshot snp: col) 
		{
			snp.drop();
			snp.close();
		}
	}

	public long indexSize() {		
		return 0;
	}

	public void commit() 
	{
		txn.commit().get();
	}

	public void rollback() 
	{
		txn.rollback().get();
	}

	public void addStatement(boolean inferred, Resource subj, IRI pred, Value obj, Resource[] contexts) 
	{
		txn.addStatement(inferred, subj, pred, obj, contexts);		
	}

	public CloseableIteration<? extends Statement, QueryEvaluationException> getStatements(
			Resource subj, 
			IRI pred,
			Value obj, boolean b, Resource[] contexts) 
	{		
		CloseableStreamProducer<List<Statement>> statements = txn.getStatements(subj, pred, obj, false, contexts).get().tell();
		
		CloseableSequenceIterator<Statement> ii = new CloseableSequenceIterator<>(statements);
		
		return new IteratorCloseableIteration<>(ii);
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void flush() 
	{
		txn.flush().get();
	}
}