package mariana.v1.rdf.store.actors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.mariana.rdf.triples.IndexSpec;
import org.mariana.rdf.triples.Triple;
import org.mariana.rdf.triples.TriplePattern;
import org.openrdf.model.BNode;
import org.openrdf.model.IRI;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;

import com.gs.collections.api.map.primitive.LongObjectMap;
import com.gs.collections.api.map.primitive.ObjectLongMap;
import com.gs.collections.api.set.MutableSet;
import com.gs.collections.impl.set.mutable.UnifiedSet;
import com.gs.collections.impl.set.mutable.primitive.LongHashSet;

import fi.jumi.actors.ActorRef;
import fi.jumi.actors.ActorThread;
import mariana.bridge.MarianaException;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.framework.actors.Async;
import mariana.v1.framework.pipeline.CloseableStreamProducer;
import mariana.v1.framework.pipeline.EmptyProducer;
import mariana.v1.rdf.model.RdfBNode;
import mariana.v1.rdf.model.RdfContextStatement;
import mariana.v1.rdf.model.RdfStatement;
import mariana.v1.rdf.model.Values;
import mariana.v1.rdf.store.actors.dictionary.ShardedDictionary;
import mariana.v1.rdf.store.actors.triples.TripleIndex;
import mariana.v1.rdf.store.actors.triples.TripleIndex.IterationState;
import mariana.v1.rdf.store.config.ConfigStore;
import mariana.v1.rdf.store.config.StoreMetadata;
import mariana.v1.rdf.tools.RdfTools;

public class TripleStoreTxnActor implements TripleStoreTxn {

	private static final int ITERATOR_BATCH_SIZE = 400000;
	private static final int BUFFER_SIZE = 400000;
	private final ShardedDictionary dictionary;
	private final Map<IndexSpec, TripleIndex> tripleIndexes;
	private List<AsyncTxnPerformer> stores = new ArrayList<>();
	
	private final List<Statement> buffer = new ArrayList<>();
	private Snapshot snp;
	
	private ConfigStore configStore;
	private StoreMetadata storeMetadata;
	
	private ActorThread myThread;

	public TripleStoreTxnActor(ActorThread myThread, Snapshot snp, ShardedDictionary dictionary, Map<IndexSpec, TripleIndex> tripleIndexes) 
	{
		this.myThread 	= myThread;
		this.snp 		= snp;
		
		this.dictionary 	= dictionary;
		this.tripleIndexes	= tripleIndexes;
		
		stores.add(dictionary);
		stores.addAll(tripleIndexes.values());
	}
	
	@Override
	public Async<Status> getStatus() {
		return Async.of(null);
	}

	@Override
	public void addStatement(Statement st) 
	{
		buffer.add(st);
		flushIfNecessary();
	}

	@Override
	public void addStatements(Collection<Statement> st) 
	{
		buffer.addAll(st);
		flushIfNecessary();
	}
	
	@Override
	public void addStatement(boolean inferred, Resource subj, IRI pred, Value obj, Resource[] contexts) 
	{
		if (contexts != null && contexts.length > 0) 
		{
			for (Resource context: contexts) 
			{
				buffer.add(new RdfContextStatement(subj, pred, obj, context));
			}
		}
		else {
			buffer.add(new RdfStatement(subj, pred, obj));
		}
		
		flushIfNecessary();		
	}
	
	private void flushIfNecessary() 
	{
		if (buffer.size() > BUFFER_SIZE)
		{
			flush();			
		}
	}

	@Override
	public Async<Void> flush() 
	{
		if (configStore == null) 
		{
			configStore   = new ConfigStore(snp);			
			storeMetadata = (StoreMetadata) configStore.get(Values.METADATA_CFG_NAME);
		}
		
		long t00 = System.currentTimeMillis();
		long size00 = buffer.size();

		try {

			if (buffer.size() > 0) 
			{


				MutableSet<Value> values = UnifiedSet.newSet();

				for (Statement st: buffer)
				{
					values.add(st.getSubject());
					values.add(st.getPredicate());
					values.add(st.getObject());

					if (st.getContext() != null) {
						values.add(st.getContext());
					}
				}

				Async<ObjectLongMap<Value>> result = dictionary.addValues(values);

				//			long t0 = System.currentTimeMillis();
				result.join();
				//			System.out.println("AddValues: " + (System.currentTimeMillis() - t0) + " -- " + (t0 - t00));

				try {
					ObjectLongMap<Value> valueMap = result.get();

					//				long a0 = System.currentTimeMillis();
					List<Triple> triples = new ArrayList<>();
					for (Statement st: buffer)
					{
						triples.add(new Triple(
								getValueId(valueMap, filterBNode(st.getSubject())),
								getValueId(valueMap, st.getPredicate()),
								getValueId(valueMap, filterBNode(st.getObject())),
								getContextId(valueMap, st.getContext())
								));
					}

					//				System.out.println("CreateTriples: " + (System.currentTimeMillis() - a0));

					for (TripleIndex idx: tripleIndexes.values()) 
					{
						idx.addStatements(triples);
					}

					buffer.clear();
				}
				catch (RuntimeException ex) {
					throw ex;
				}
			}

			AsyncTxnPerformer.perform(stores, AsyncTxnPerformer::flush);

			return Async.ofNull();
		}
		finally {
			System.out.println("Flush TxnBuffer of " + size00 + " in " + (System.currentTimeMillis() - t00) + "\n");
		}
	}
	
	private Value filterBNode(Value value) 
	{
		if (value instanceof BNode) 
		{
			setBNodeIdIfNecessary((BNode)value);
		}		
		
		return value;
	}
	
	private void setBNodeIdIfNecessary(BNode resource) 
	{
		RdfBNode bnode = (RdfBNode) RdfTools.internalize(resource);
		if (bnode.getID() == null) 
		{
			bnode.setID(String.valueOf(newBNodeId()));
		}
	}

	private long newBNodeId() {
		return storeMetadata.newBNodeId();
	}
	
	private long getValueId(ObjectLongMap<Value> valueMap, Value value) 
	{
		return valueMap.get(value);
	}
	
	private long getContextId(ObjectLongMap<Value> valueMap, Value value) 
	{
		if (value != null) {
			return valueMap.get(value);
		}
		else {
			return Values.CODE_NULL;
		}
	}


	
	
	@Override
	public Async<Void> commit() 
	{
		if (buffer.size() > 0) {
			flush();
		}
		
		AsyncTxnPerformer.perform(stores, AsyncTxnPerformer::commit);
		
		Long maxId = dictionary.getMaxID().get();
		
		if (configStore != null) 
		{
			configStore   = new ConfigStore(snp);
			storeMetadata = (StoreMetadata) configStore.get(Values.METADATA_CFG_NAME);
			this.storeMetadata.setIdCounter(maxId + storeMetadata.getConfig().getDictionaryShards());
			configStore.set(Values.METADATA_CFG_NAME, storeMetadata);
		}
		
		snp.commit();
		snp.dropParent();
		
		snp.setAsMaster();
		
		return Async.ofNull();
	}

	@Override
	public Async<Void> rollback() 
	{
		AsyncTxnPerformer.perform(stores, AsyncTxnPerformer::rollback);
		
		snp.drop();
		
		return Async.ofNull();
	}

	@Override
	public Async<Void> close() 
	{
		AsyncTxnPerformer.perform(stores, AsyncTxnPerformer::close);
		
		if (configStore != null) {
			configStore.close();
		}
		snp.close();
		
		return Async.ofNull();
	}
	
	static class TripleProducer implements CloseableStreamProducer<List<Triple>>{

		private TripleIndex index;
		private IterationState state;

		TripleProducer(TripleIndex index, TripleIndex.IterationState state) {
			this.index = index;
			this.state = state;			
		}
		
		@Override
		public Async<List<Triple>> produce() {
			return index.next(state);
		}

		@Override
		public Async<Void> close() {
			return index.close(state);
		}
	}

	
	CloseableStreamProducer<List<Triple>> getTriples(
			IndexSpec indexSpec, 
			Resource subj,
			IRI pred, 
			Value obj, 
			boolean includeInferred, 
			Resource... contexts
	) {
		Async<TriplePattern> ids = dictionary.resolve(subj, pred, obj, contexts != null && contexts.length == 0 ? null : contexts);
		
		TriplePattern tmeta = ids.get();
		
		if (tmeta.getSubject() == Values.CODE_EMPTY) {
			return new EmptyProducer<List<Triple>>();
		}
		
		if (tmeta.getProperty() == Values.CODE_EMPTY) {
			return new EmptyProducer<List<Triple>>();
		}
		
		if (tmeta.getObject() == Values.CODE_EMPTY) {
			return new EmptyProducer<List<Triple>>();
		}
		
		if (tmeta.getContext() != null && (tmeta.getContext().size() == 0 && contexts.length > 0)) 
		{
			return new EmptyProducer<List<Triple>>();
		}
		
		TripleIndex index;
		
		if (indexSpec == null) 
		{
			String rangeSpec = inferRangeSpec(
				new Triple(
						tmeta.getSubject(), 
						tmeta.getProperty(), 
						tmeta.getObject(), 
						tmeta.getContext() == null ? 0 : 1
				)
			);

			index = findSuitableIndex(rangeSpec);
		}
		else {
			index = tripleIndexes.get(indexSpec);
		}
		
		IterationState state = index.find(tmeta, ITERATOR_BATCH_SIZE).get();
		
		return new TripleProducer(index, state);
	}
	
	
	static class StatementStreamProducer implements CloseableStreamProducer<List<Statement>> {

		private ShardedDictionary dictionary;
		private CloseableStreamProducer<List<Triple>> triplesProducer;
		
		StatementStreamProducer(CloseableStreamProducer<List<Triple>> triplesProducer, ShardedDictionary dictionary) 
		{
			this.triplesProducer = triplesProducer;
			this.dictionary = dictionary;			
		}
		
		@Override
		public Async<List<Statement>> produce() 
		{
			long t0 = System.currentTimeMillis();

			try {

				List<Triple> list = triplesProducer.produce().get();
				//			System.out.println("Get Triples: " +(System.currentTimeMillis() - t0) + " -- " + (list != null ? list.size() : 0));

				if (list != null) 
				{
					long ta = System.currentTimeMillis();
					LongHashSet set = new LongHashSet(); 

					for (Triple t: list) 
					{
						t.addIdentifiers(set);
					}

					long tt = System.currentTimeMillis();
					LongObjectMap<Value> mapping = dictionary.resolveValues(set).get();
					System.out.println("Dictionary Resolve time: " + (System.currentTimeMillis() - tt) +" -- " + (tt - ta) + " -- " + mapping.size());

					List<Statement> statements = new ArrayList<>();

					for (Triple t: list) 
					{
						Statement st = new RdfContextStatement(
								(Resource)  resolveValue(mapping, t.getSubject()),
								(IRI)	    resolveValue(mapping, t.getProperty()),
								resolveValue(mapping, t.getObject()),
								(Resource)  resolveValue(mapping, t.getContext())
								);

						statements.add(st);
					}

					return Async.of(statements);
				}
				else {
					return Async.ofNull();
				}
			}
			finally {
				System.out.println("Total batch time: " + (System.currentTimeMillis() - t0));
			}
		}
		
		private Value resolveValue(LongObjectMap<Value> mapping, long id) 
		{
			if (id != Values.CODE_NULL) 
			{
				Value value = mapping.get(id);
				if (value != null) 
				{
					return value;
				}
				else {
					throw new MarianaException("Value for id " + id + " is not found");
				}
			}
			else {
				return null;
			}
		}

		@Override
		public Async<Void> close() 
		{
			return triplesProducer.close();
		}
	}
	
	
	
	
	public String inferRangeSpec(Triple data) 
	{
		StringBuilder sb = new StringBuilder();
		
		if (data.getSubject() != Values.CODE_NULL) {
			sb.append(Values.SUBJECT_CHAR);
		}
		
		if (data.getProperty() != Values.CODE_NULL) {
			sb.append(Values.PROPERTY_CHAR);
		}
		
		if (data.getObject() != Values.CODE_NULL) {
			sb.append(Values.OBJECT_CHAR);
		}
		
		if (data.getContext() != Values.CODE_NULL) {
			sb.append(Values.CONTEXT_CHAR);
		}
		
		return sb.toString();
	}
	
	private TripleIndex findSuitableIndex(String rangeSpec) 
	{	
		if (!"".equals(rangeSpec)) 
		{
			for (Map.Entry<IndexSpec, TripleIndex> entry: tripleIndexes.entrySet()) 
			{
				if (entry.getKey().name().toLowerCase().startsWith(rangeSpec)) 
				{
					return entry.getValue();
				}
			}
		}
		
		return getDefaultIndex();
	}
	
	public TripleIndex getDefaultIndex() 
	{
		TripleIndex idx = tripleIndexes.get(Values.DEFAULT_INDEX);
		
		if (idx != null) 
		{
			return idx;
		}
		else {
			return tripleIndexes.values().iterator().next();
		}
	}
	
	public Async<ActorRef<CloseableStreamProducer<List<Statement>>>> 
	getStatements(Resource subj, IRI pred, Value obj, boolean includeInferred, Resource... contexts) 
	{
		CloseableStreamProducer<List<Triple>> triplesProducer = getTriples(null, subj, pred, obj, includeInferred, contexts);
		
		@SuppressWarnings({ "rawtypes", "unchecked" })
		ActorRef<CloseableStreamProducer<List<Statement>>> actor = (ActorRef)myThread.bindActor(
				CloseableStreamProducer.class, 
				new StatementStreamProducer(triplesProducer, dictionary)
		);
		
		return Async.of(actor);
	}
}
