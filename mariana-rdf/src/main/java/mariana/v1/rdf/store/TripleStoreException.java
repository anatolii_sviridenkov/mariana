package mariana.v1.rdf.store;

public class TripleStoreException extends RuntimeException {
	
	private static final long serialVersionUID = 1756166445825566602L;

	public TripleStoreException()
	{
		super();
	}

	public TripleStoreException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public TripleStoreException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public TripleStoreException(String message)
	{
		super(message);
	}

	public TripleStoreException(Throwable cause)
	{
		super(cause);
	}
}
