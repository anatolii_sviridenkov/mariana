package mariana.v1.rdf.store.actors.dictionary;

import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import org.openrdf.model.Value;

import com.gs.collections.api.iterator.LongIterator;
import com.gs.collections.api.list.primitive.LongList;
import com.gs.collections.api.map.primitive.LongObjectMap;
import com.gs.collections.api.map.primitive.ObjectLongMap;
import com.gs.collections.api.set.MutableSet;
import com.gs.collections.api.set.primitive.LongSet;
import com.gs.collections.impl.list.mutable.primitive.LongArrayList;
import com.gs.collections.impl.map.mutable.primitive.LongObjectHashMap;
import com.gs.collections.impl.map.mutable.primitive.ObjectLongHashMap;

import mariana.v1.api.alloc.Snapshot;
import mariana.v1.framework.actors.Async;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.rdf.dictionary.IDValueMapping;
import mariana.v1.rdf.dictionary.ValueIDMapping;
import mariana.v1.rdf.model.RdfLiteral;
import mariana.v1.rdf.model.Values;
import mariana.v1.rdf.store.TripleStoreException;
import mariana.v1.rdf.store.actors.Status;
import mariana.v1.rdf.store.config.CtrNames;
import mariana.v1.rdf.store.config.DictionaryNames;
import mariana.v1.rdf.tools.RdfTools;

public class DictionaryShardActor implements DictionaryShard {

	private final int shardNum;
	private final Snapshot snp;
	private final UUID idValueCtrName;
	private final UUID valieIdCtrName;
	private final String branchName;
	
	private IDValueMapping idValueMapping;
	private ValueIDMapping valueIdMapping;
	
	private long idBase;
	private int nShards;
	
	private boolean skipAll = false;

	public DictionaryShardActor(Snapshot snp, int shardNum, int nShards, long idBase, UUID idValueCtrName, UUID valieIdCtrName, String branchName) 
	{
		this.snp 	  = snp;
		this.shardNum = shardNum;
		this.nShards  = nShards;
		this.idBase   = idBase;
		
		this.idValueCtrName = idValueCtrName;
		this.valieIdCtrName = valieIdCtrName;
		this.branchName = branchName;
		
//		System.out.println(valieIdCtrName);
	}
	
	@Override
	public Async<ObjectLongMap<Value>> addValues(MutableSet<Value> values)
	{
		long t0 = System.currentTimeMillis();
		try {
			if (valueIdMapping == null)
			{
//				snp.dump("target/zzzzz-" + System.currentTimeMillis());
				
				idValueMapping = IDValueMapping.findOrCreate(snp, idValueCtrName);
				valueIdMapping = ValueIDMapping.findOrCreate(snp, valieIdCtrName);
			}

			ObjectLongHashMap<Value> map = new ObjectLongHashMap<>();

			for (Value value: values) 
			{
				map.put(value, getOrCreateValueID(value));
			}

			return Async.of(map);
		}
		finally {
			System.out.println("++ AddValues: " + values.size() + " in " + (System.currentTimeMillis() - t0));
		}
	}
	
	
	private class IDProvider implements Supplier<Long> {

		private boolean invoked;
		
		@Override
		public Long get() 
		{			
			invoked = true;
			return newId();
		}

		public boolean isInvoked() {
			return invoked;
		}
	}
	
	private Long getOrCreateValueID(Value value) 
	{
		if (value != null) 
		{
			value = RdfTools.internalize(value);
			
			IDProvider idProvider = new IDProvider();
			
			Long valueId = valueIdMapping.firdOrCreate(value, idProvider);

			if (idProvider.isInvoked()) {
				idValueMapping.set(valueId, value);
			}

			return valueId;
		}
		else {
			throw new TripleStoreException("Value must not be null");
		}
	}

	private Long newId() 
	{
		idBase += nShards;
		return idBase + shardNum;
	}

	@Override
	public Async<Status> getStatus() 
	{
		return Async.ofNull();
	}

	@Override
	public Async<Void> commit() 
	{
//		snp.dump("target/before-" + branchName + System.currentTimeMillis() + ".dir");
		
		snp.commit();
//		snp.dump("target/" + branchName + System.currentTimeMillis() + ".dir");
		
		snp.dropParent();
		
		snp.setAsBranch(branchName);
		
		return Async.ofNull();
	}

	@Override
	public Async<Void> rollback() 
	{
		snp.drop();
		return Async.ofNull();
	}

	@Override
	public Async<Void> close() 
	{
		if (idValueMapping != null) idValueMapping.close();
		if (valueIdMapping != null) valueIdMapping.close();
		
		snp.close();
		
		return Async.ofNull();
	}

	@Override
	public Async<Void> flush() 
	{
		return Async.ofNull();
	}

//	@Override
//	public void setIDBase(long idBase)
//	{
//		this.idBase = idBase + shardNum;
//	}

	@Override
	public Async<Long> getMaxID() {
		return Async.of(idBase);
	}

	@Override
	public Async<Long> resolveID(Value value) 
	{
		if (value != null) 
		{
			return Async.of(valueIdMapping.get(value));
		}
		else {
			return Async.of(Values.CODE_NULL);
		}
	}

	public Async<LongObjectMap<Value>> resolve(List<Value> values) 
	{
		LongObjectHashMap<Value> map = new LongObjectHashMap<>();
		
		for (Value value: values) 
		{
			map.put(resolveID(value).get(), value);
		}
		
		return Async.of(map);
	}

	@Override
	public Async<LongList> resolveIDs(List<Value> values) 
	{
		if (valueIdMapping == null) {
			valueIdMapping = ValueIDMapping.find(snp, valieIdCtrName);
		}
		
		LongArrayList result = new LongArrayList();
		
		for (Value v: values) 
		{
			Long id = valueIdMapping.get(v);
			if (id != null)
			{
				result.add(id);
			}
		}

		return Async.of(result);
	}

	@Override
	public Async<LongObjectMap<Value>> resolveValues(LongSet set) 
	{
		long t0 = System.currentTimeMillis();
		try {
			if (idValueMapping == null) {
				idValueMapping = IDValueMapping.find(snp, idValueCtrName);
			}

			LongObjectHashMap<Value> mapping = new LongObjectHashMap<>();

			LongIterator ii = set.longIterator();

			while (ii.hasNext()) 
			{
				long id = ii.next();

				Value value = idValueMapping.get(id);
				if (value != null) 
				{
					mapping.put(id, value);
				}
			}

			return Async.of(mapping);
		}
		finally {
			System.out.println("== Resolved values of " + set.size() + " in " + (System.currentTimeMillis() - t0));
		}
	}

	public static void init(InMemoryAllocator allocator, int shard, CtrNames ctrNames) 
	{
		DictionaryNames names = ctrNames.getDictionaryNames(shard);
		
		try (Snapshot master = allocator.master()) 
		{
			try (Snapshot snp = master.branch())
			{
				IDValueMapping.findOrCreate(snp, names.getIdValueName()).close();
				ValueIDMapping.findOrCreate(snp, names.getValueIdName()).close();
				
				snp.commit();
				snp.setAsBranch(Values.DICTIONARY + "-" + shard);
			}
		}
	}
}
