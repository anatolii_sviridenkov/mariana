package mariana.v1.rdf.store.actors.triples;

import com.gs.collections.api.set.primitive.LongSet;

public class TripleMetadata {
	private long subjectId;
	private long predicateId;
	private long objectId;
	
	private LongSet contextIds;
	
	public TripleMetadata() {}

	public long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}

	public long getPredicateId() {
		return predicateId;
	}

	public void setPredicateId(long predicateId) {
		this.predicateId = predicateId;
	}

	public long getObjectId() {
		return objectId;
	}

	public void setObjectId(long objectId) {
		this.objectId = objectId;
	}

	public LongSet getContextIds() {
		return contextIds;
	}

	public void setContextIds(LongSet contextIds) {
		this.contextIds = contextIds;
	}
}
