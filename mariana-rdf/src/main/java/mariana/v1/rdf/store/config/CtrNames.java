package mariana.v1.rdf.store.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.mariana.rdf.triples.IndexSpec;

public class CtrNames implements Serializable {

	private static final long serialVersionUID = 8631585031601624664L;
	private int dictionaryShards;
	private int tripleShards;
	
	private List<DictionaryNames> dictionaryNames;
	private Map<IndexSpec, List<TripleIndexNames>> tripleIndexNames;
	
	protected CtrNames() {
		
	}
	
	public CtrNames(int dictionaryShards, int tripleShards, Set<IndexSpec> tripleIndexes) 
	{
		this.dictionaryShards 	= dictionaryShards;
		this.tripleShards 		= tripleShards;
		
		this.dictionaryNames = new ArrayList<>();
		tripleIndexNames	 = new HashMap<>();
		
		for (int c = 0; c < dictionaryShards; c++) 
		{
			dictionaryNames.add(new DictionaryNames(
				UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID()
			));
		}
		
		for (IndexSpec spec: tripleIndexes) 
		{
			List<TripleIndexNames> list = new ArrayList<>();
			
			for (int c = 0; c < tripleShards; c++) 
			{
				list.add(new TripleIndexNames(
					UUID.randomUUID()
				));
			}
			
			tripleIndexNames.put(spec, list);
		}
	}
	
	public DictionaryNames getDictionaryNames(int shard) 
	{
		return dictionaryNames.get(shard);
	}
	
	public TripleIndexNames getTripleIndexNames(IndexSpec spec, int shard) 
	{
		return tripleIndexNames.get(spec).get(shard);
	}
}
