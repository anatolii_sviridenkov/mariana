package mariana.v1.rdf.store.actors.triples;

import java.util.List;

import org.mariana.rdf.triples.Triple;
import org.mariana.rdf.triples.TriplePattern;

import mariana.v1.framework.actors.Async;
import mariana.v1.rdf.store.actors.AsyncTxnPerformer;

public interface TripleIndex extends AsyncTxnPerformer {
	void addStatements(List<Triple> triples);
	
	interface IterationState {}
	
	Async<IterationState> find(TriplePattern pattern, int batchSize);
	Async<List<Triple>> next(IterationState state);
	Async<Void> close(IterationState state);
}
