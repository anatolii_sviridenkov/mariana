package mariana.v1.rdf.store.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.mariana.rdf.triples.IndexSpec;
import org.openrdf.sail.SailException;

public class TripleStoreParameters implements java.io.Serializable {

	
	private static final long serialVersionUID = -6992161010175357493L;

	public static final IndexSpec[] DEFAULT_INDEXES = new IndexSpec[]{IndexSpec.SPOC, IndexSpec.POSC}; //, IndexSpec.OPSC
	public static final int DEFAULT_DICTIONARY_SHARDS 	= 16;
	public static final int DEFAULT_TRIPLES_SHARDS 		= 4;
	
	public static final String STORAGE_FORMAT_VERSION = "1";
	
	private String storageFormatVersion = STORAGE_FORMAT_VERSION;
	
	private int dictionaryShards = DEFAULT_DICTIONARY_SHARDS;
	private int triplesShards 	 = DEFAULT_TRIPLES_SHARDS;
	
	private Set<IndexSpec> indexes = new HashSet<>(Arrays.asList(DEFAULT_INDEXES));
	
	private TripleStoreParameters() {}
	
	public static class Builder {
		private Set<IndexSpec> orderSpecs = new HashSet<>(Arrays.asList(DEFAULT_INDEXES));
		
		private int dictionaryShards = DEFAULT_DICTIONARY_SHARDS;
		private int triplesShards = DEFAULT_TRIPLES_SHARDS;
		
		public Builder(){}
		
		public Builder withIndexes(Set<IndexSpec> specs) 
		{
			orderSpecs = specs;	
			return this;
		}
		
		public Builder withIndexes(String specs) 
		{
			String[] tokens = specs.split("\\s*,\\s*");
			orderSpecs = new HashSet<>();
			
			for (String token: tokens) 
			{
				orderSpecs.add(IndexSpec.valueOf(token.toUpperCase()));
			}
			
			return this;
		}
		
		
		public Builder withIndexes(IndexSpec... specs) 
		{
			if (specs == null || specs.length == 0) 
			{
				throw new SailException("Indexes list must not be null");
			}
			else {
				Set<IndexSpec> slist = new HashSet<>();
				
				for (IndexSpec spec: specs) 
				{
					slist.add(spec);
				}
	
				orderSpecs = slist;
				return this;
			}			
		}
		
		public Builder withDictionaryShards(int n) {
			this.dictionaryShards = n;
			return this;
		}
		
		public Builder withTriplesShards(int n) {
			this.triplesShards = n;
			return this;
		}

		
		public TripleStoreParameters build() 
		{	
			TripleStoreParameters config = new TripleStoreParameters();
			
			config.indexes			= orderSpecs;
			config.dictionaryShards = dictionaryShards;
			config.triplesShards 	= triplesShards;
			
			return config;
		}
	}

	public Set<IndexSpec> getIndexes() {
		return indexes;
	}

	
	public TripleStoreParameters withNewIndex(IndexSpec spec) {
		TripleStoreParameters params = new TripleStoreParameters();
		
		params.indexes = new HashSet<>(this.indexes);
		
		params.indexes.add(spec);
		return params;
	}
	
	public TripleStoreParameters withoutIndex(IndexSpec spec) {
		TripleStoreParameters params = new TripleStoreParameters();
		
		params.indexes = new HashSet<>(this.indexes);
		
		params.indexes.remove(spec);
		
		return params;
	}

	public String getStorageFormatVersion()
	{
		return storageFormatVersion;
	}


	public int getDictionaryShards() {
		return dictionaryShards;
	}


	public int getTriplesShards() {
		return triplesShards;
	}
}
