package mariana.v1.rdf.store.config;

import java.io.Serializable;
import java.util.UUID;

public class TripleIndexNames implements Serializable {
	private static final long serialVersionUID = 7667357499195996182L;
	
	private UUID ctrName;
	
	protected TripleIndexNames() {}
	
	public TripleIndexNames(UUID ctrName) {
		this.ctrName = ctrName;		
	}
	
	public UUID getCtrName() {
		return ctrName;
	}
}
