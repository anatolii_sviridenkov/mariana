package mariana.v1.rdf.store.config;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.UUID;

import com.google.common.base.Throwables;

import mariana.v1.api.alloc.Snapshot;
import mariana.v1.hashmap.MHashMap;
import mariana.v1.io.serialization.ByteArrayInputHandler;
import mariana.v1.io.serialization.ByteArrayOutputHandler;
import mariana.v1.io.serialization.UTF8StringInputHandler;
import mariana.v1.io.serialization.UTF8StringOutputHandler;

public class ConfigStore implements AutoCloseable {
	
	public static final UUID NAME = UUID.fromString("e523a21b-9723-4cbd-a15e-91585bb95607");
	
	private MHashMap<String, byte[]> store;
	
	public ConfigStore(Snapshot snp) 
	{
		if (snp.isActive()) 
		{
			store = MHashMap.<String, byte[]>findOrCreate(
					snp, 
					NAME, 
					new UTF8StringOutputHandler(), 
					new UTF8StringInputHandler(),
					new ByteArrayOutputHandler(),
					new ByteArrayInputHandler()
					);

		}
		else {
			store = MHashMap.<String, byte[]>find(
					snp, 
					NAME, 
					new UTF8StringOutputHandler(), 
					new UTF8StringInputHandler(),
					new ByteArrayOutputHandler(),
					new ByteArrayInputHandler()
					);
		}
	}
	
	public Object get(String key) 
	{
		byte[] data = store.get(key);
		if (data != null) 
		{
			try {
				try(ObjectInputStream is = new ObjectInputStream(new ByteArrayInputStream(data))) 
				{
					return is.readObject();
				}
			} 
			catch (ClassNotFoundException | IOException e) {
				throw Throwables.propagate(e);
			}
		}
		else {
			return null;
		}
	}
	
	public void set(String key, Object value) 
	{
		try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) 
		{
			try(ObjectOutputStream os = new ObjectOutputStream(baos)) 
			{
				os.writeObject(value);
			}
			
			baos.flush();
			
			store.set(key, baos.toByteArray());
		}
		catch (IOException e1) {
			throw Throwables.propagate(e1);
		}
	}
	
	@Override
	public void close() {
		store.close();
	}
}
