package mariana.v1.rdf.store;

import org.openrdf.model.IRI;
import org.openrdf.model.ValueFactory;

import mariana.v1.rdf.model.RdfValueFactory;

public class TripleStoreSchema {

	// FIXME: change this prefix
	public static final String NAMESPACE = "http://example.com/triple-store";
 
	public final static IRI TRIPLE_INDEXES;
	public final static IRI TRIPLES_SHARDS;
	public final static IRI DICTIONARY_SHARDS;

	static {
		ValueFactory factory 	= new RdfValueFactory();		
		
		TRIPLE_INDEXES 			= factory.createIRI(NAMESPACE, "tripleIndexes");
		TRIPLES_SHARDS 			= factory.createIRI(NAMESPACE, "triplesShards");
		DICTIONARY_SHARDS 		= factory.createIRI(NAMESPACE, "dictionaryShards");
	}
}
