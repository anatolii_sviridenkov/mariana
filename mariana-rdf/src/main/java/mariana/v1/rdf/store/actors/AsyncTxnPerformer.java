package mariana.v1.rdf.store.actors;

import mariana.v1.framework.actors.Async;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import com.google.common.base.Throwables;

public interface AsyncTxnPerformer {
	Async<Status> getStatus();
	
	Async<Void> commit();
	Async<Void> rollback();
	Async<Void> close();
	Async<Void> flush();
	
	public static void perform(Collection<? extends AsyncTxnPerformer> targets, Function<AsyncTxnPerformer, Async<Void>> targetFn) 
	{		
		List<Async<Void>> results = new ArrayList<>();
		
		for (AsyncTxnPerformer t: targets) 
		{
			results.add(targetFn.apply(t));
		}
		
		Throwable error = null;
		
		for (Async<Void> result: results) 
		{
			result.join();
			if (result.getException() != null) 
			{
				error = result.getException();
				error.printStackTrace();
			}
		}
		
		if (error != null) 
		{
			throw Throwables.propagate(error);
		}
	}
}
