package mariana.v1.rdf.store.actors.triples;

import java.util.ArrayList;
import java.util.List;

import org.mariana.rdf.triples.Triple;
import org.mariana.rdf.triples.TriplePattern;

import com.google.common.base.Throwables;

import mariana.v1.framework.actors.Async;
import mariana.v1.framework.pipeline.StreamJoiner;
import mariana.v1.framework.pipeline.StreamProducer;
import mariana.v1.rdf.store.actors.AsyncTxnPerformer;
import mariana.v1.rdf.store.actors.Status;

public class TripleIndexActor implements TripleIndex {

	private final List<TripleIndexShard> shards;
	private final int nShards;
	
	public TripleIndexActor(List<TripleIndexShard> shards) 
	{
		this.shards = new ArrayList<>(shards);
		this.nShards = shards.size();
	}
	
	@Override
	public void addStatements(List<Triple> triples) 
	{
		long t0 = System.currentTimeMillis();
		
		try {
			@SuppressWarnings("unchecked")
			List<Triple>[] splits = new List[nShards];

			for (int c = 0; c < splits.length; c++) 
			{
				splits[c] = new ArrayList<Triple>();
			}

			for (Triple triple: triples) 
			{
				int shard = shard(triple.hashCode());
				splits[shard].add(triple);
			}

			for (int c = 0; c < splits.length; c++) 
			{
				shards.get(c).addStatements(splits[c]);
			}

			AsyncTxnPerformer.perform(shards, AsyncTxnPerformer::flush);
		}
		finally {
			System.out.println("== TripleIndex finished in " + (System.currentTimeMillis() - t0));
		}
	}
	
	@Override
	public Async<Status> getStatus() {
		return Async.ofNull();
	}

	@Override
	public Async<Void> commit() {
		AsyncTxnPerformer.perform(shards, AsyncTxnPerformer::commit);
		return Async.ofNull();
	}

	@Override
	public Async<Void> rollback() {
		AsyncTxnPerformer.perform(shards, AsyncTxnPerformer::rollback);
		return Async.ofNull();
	}

	@Override
	public Async<Void> close() {
		AsyncTxnPerformer.perform(shards, AsyncTxnPerformer::close);
		return Async.ofNull();
	}

	@Override
	public Async<Void> flush() {
		AsyncTxnPerformer.perform(shards, AsyncTxnPerformer::flush);
		return Async.ofNull();
	}
	
	static class StreamProducerWrapper implements StreamProducer<List<Triple>> {

		private final TripleIndexShard shard;
		private final IterationState state;
		

		public StreamProducerWrapper(TripleIndexShard shard, IterationState state) 
		{
			this.shard = shard;
			this.state = state;
		}
		
		@Override
		public Async<List<Triple>> produce()
		{
			return shard.next(state);
		}
		
		public void close() {
			shard.close(state).get();
		}
	}
	
	static class TripleStream extends StreamJoiner<List<Triple>> implements IterationState {

		public TripleStream(List<TripleIndexShard> shards, TriplePattern pattern, int batchSize) {
			super(createProducers(shards, pattern, batchSize));
		}

		private static List<StreamProducer<List<Triple>>> createProducers(List<TripleIndexShard> shards, TriplePattern pattern, int batchSize) 
		{
			List<Async<IterationState>> results = new ArrayList<>();
			for (TripleIndexShard shard: shards) 
			{
				results.add(shard.find(pattern, batchSize));
			}
			
			IterationState[] states = new IterationState[shards.size()];
			
			int c = 0;
			try {
				for (Async<IterationState> result: results) 
				{
					states[c] = result.get();
					c++;
				}
			}
			catch (Exception ex) 
			{
				for (int d = 0; d < c; d++) 
				{
					shards.get(d).close(states[d]).get();
				}
				
				throw Throwables.propagate(ex);
			}
			
			List<StreamProducer<List<Triple>>> producers = new ArrayList<>();
			
			int cc = 0;
			for (IterationState state: states) 
			{
				producers.add(new StreamProducerWrapper(shards.get(cc++), state));
			}
			
			return producers;
		}
		
		public void close() 
		{			
			for (StreamProducer<List<Triple>> shard: producers) 
			{
				StreamProducerWrapper wrapper = (StreamProducerWrapper) shard;		
				wrapper.close();
			}
		}
	}
	

	@Override
	public Async<IterationState> find(TriplePattern pattern, int batchSize) 
	{
		return Async.of(new TripleStream(shards, pattern, batchSize));
	}

	@Override
	public Async<List<Triple>> next(IterationState state) 
	{
		TripleStream ii = (TripleStream) state;		
		return ii.produce();
	}

	@Override
	public Async<Void> close(IterationState state) 
	{		
		TripleStream ii = (TripleStream) state;
		
		ii.close();
		
		return Async.ofNull();
	}
	
	private int shard(int hashCode) {
		return hashCode >= 0 ? hashCode % nShards : (-hashCode) % nShards;  
	}
	
	private int shard(long hashCode) {
		return (int)(hashCode >= 0 ? hashCode % nShards : (-hashCode) % nShards);  
	}
}
