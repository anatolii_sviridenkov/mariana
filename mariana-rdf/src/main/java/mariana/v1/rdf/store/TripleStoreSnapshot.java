package mariana.v1.rdf.store;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Supplier;

import org.mariana.rdf.triples.IndexSpec;
import org.mariana.rdf.triples.Triple;
import org.mariana.rdf.triples.TripleSet;
import org.mariana.rdf.triples.TriplePattern;
import org.mariana.rdf.triples.iterator.EmptyIterator;
import org.mariana.rdf.triples.iterator.IteratorCloseableIteration;
import org.openrdf.model.BNode;
import org.openrdf.model.IRI;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.evaluation.impl.EvaluationStatistics;
import org.openrdf.sail.SailException;
import org.openrdf.sail.base.SailSource;
import org.openrdf.sail.base.SailStore;

import com.gs.collections.api.set.primitive.LongSet;
import com.gs.collections.impl.set.mutable.primitive.LongHashSet;


import info.aduna.iteration.CloseableIteration;
import info.aduna.iteration.ConvertingIteration;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.framework.pipeline.CloseableIterator;
import mariana.v1.rdf.dictionary.IDValueMapping;
import mariana.v1.rdf.dictionary.ValueIDMapping;
import mariana.v1.rdf.model.RdfBNode;
import mariana.v1.rdf.model.RdfLiteral;
import mariana.v1.rdf.model.RdfResource;
import mariana.v1.rdf.model.RdfValueFactory;
import mariana.v1.rdf.model.Values;
import mariana.v1.rdf.store.config.ConfigStore;
import mariana.v1.rdf.store.config.StoreMetadata;
import mariana.v1.rdf.tools.RdfTools;

public class TripleStoreSnapshot implements SailStore {
	
	private static final String METADATA = Values.METADATA_CFG_NAME;
	
	public static final UUID ID_VALUE_MAPPING = UUID.fromString("306d3830-4e97-409d-b960-376a1732fe7f");
	public static final UUID VALUE_ID_MAPPING = UUID.fromString("5b5663be-cb79-4fcf-8946-08a277c38094");
	
	public static final Map<IndexSpec, UUID> INDEX_NAMES;
	
	static {
		Map<IndexSpec, UUID> map = new HashMap<>();
		
		map.put(IndexSpec.SPOC, UUID.fromString("7933a661-fc16-49e4-8c05-4ffe7a510ef4"));
		map.put(IndexSpec.POSC, UUID.fromString("58282670-20ae-459c-925e-983e88ae6f3f"));
		map.put(IndexSpec.OPSC, UUID.fromString("0f6a2b11-4711-442f-91b2-6e2d0a6ec42a"));
		
		INDEX_NAMES = Collections.unmodifiableMap(map);
	}
	
	private Snapshot snapshot;
	
	private IDValueMapping idValueMapping;
	private ValueIDMapping valueIdMapping;
	
	private Map<IndexSpec, TripleSet> tripleIndexes = new HashMap<>();
	
	private ConfigStore configs;
	
	private StoreMetadata metadata;
	
	private ValueFactory valueFactory = new RdfValueFactory();
	
	public TripleStoreSnapshot(Snapshot snapshot, Set<IndexSpec> indexes) 
	{
		this.snapshot = snapshot;
		
		configs = new ConfigStore(snapshot);
		
		metadata = (StoreMetadata) configs.get(METADATA);
		
		if (metadata == null) {
			metadata = new StoreMetadata(); 
		}
		
		idValueMapping = IDValueMapping.findOrCreate(snapshot, ID_VALUE_MAPPING);
		valueIdMapping = ValueIDMapping.findOrCreate(snapshot, VALUE_ID_MAPPING);
		
		for (IndexSpec spec: indexes) 
		{
			tripleIndexes.put(spec, TripleSet.findOrCreate(snapshot, getIndexName(spec), spec.toString()));
		}
	}
	
	public static UUID getIndexName(IndexSpec spec) 
	{
		UUID name = INDEX_NAMES.get(spec);
		if (name != null) {
			return name;
		}
		else {
			throw new TripleStoreException("Unknown IndexSpec: " + spec);
		}
	}
	
	public void commit() 
	{
		configs.set(METADATA, metadata);
		snapshot.commit();
		snapshot.setAsMaster();
	}
	
	public void close() 
	{
		idValueMapping.close();
		valueIdMapping.close();
		
		for (TripleSet idx: tripleIndexes.values()) {
			idx.close();
		}
		
		configs.close();
		
		snapshot.close();
	}	
	
	public void addStatement(boolean inferred, Resource subj, IRI pred, Value obj, Resource... contexts) throws SailException 
	{
		if (subj instanceof BNode) {
			setBNodeIdIfNecessary((BNode)subj);
		}
		
		if (obj instanceof BNode) {
			setBNodeIdIfNecessary((BNode)obj);
		}
		
		Long subjID = getOrCreateValueID(subj);
		Long predID = getOrCreateValueID(pred);
		
		Long objID  = getOrCreateValueID(obj);
		
		List<Triple> triples = new ArrayList<Triple>();
		
		if (contexts == null || contexts.length == 0 || (contexts.length == 1 && contexts[0] == null)) 
		{
			Triple data = new Triple(
					subjID,
					predID,
					objID,
					Values.CODE_NULL
			);			
			triples.add(data);
		}
		else {			
			List<Long> contextIDs = getOrCreateContextIDs(contexts);
			
			for (Long ctxId: contextIDs) 
			{
				Triple data = new Triple(
					subjID,
					predID,
					objID,
					ctxId
				);
				triples.add(data);
			}
		}
		
		for (TripleSet index: tripleIndexes.values()) 
		{
			for (Triple triple: triples) 
			{
				index.insert(triple);
			}
		}
	}
	
	private void setBNodeIdIfNecessary(BNode resource) 
	{
		RdfBNode bnode = (RdfBNode) RdfTools.internalize(resource);
		if (bnode.getID() == null) 
		{
			bnode.setID(String.valueOf(newBNodeId()));
		}
	}

	private List<Long> getOrCreateContextIDs(Resource[] contexts) 
	{
		List<Long> ids = new ArrayList<Long>();
		
		for (Resource ctx: contexts) 
		{
			ids.add(getOrCreateValueID(ctx));
		}
		
		return ids;
	}
	
	private class IDProvider implements Supplier<Long> {

		private boolean invoked;
		
		@Override
		public Long get() 
		{			
			invoked = true;
			return newId();
		}

		public boolean isInvoked() {
			return invoked;
		}
	}
	
	private Long getOrCreateValueID(Value value) 
	{
		if (value != null) 
		{
			value = RdfTools.internalize(value);
			
			IDProvider idProvider = new IDProvider();

			Long valueId = valueIdMapping.firdOrCreate(value, idProvider);

			if (idProvider.isInvoked()) {
				idValueMapping.set(valueId, value);
			}

			return valueId;
		}
		else {
			throw new TripleStoreException("Value must not be null");
		}
	}
	

	public long newId() {
		//return -1;//metadata.newId();
		throw new UnsupportedOperationException();
	}
	
	public long newBNodeId() {
		return metadata.newBNodeId();
	}
	
	public TripleSet getDefaultIndex() 
	{
		TripleSet idx = tripleIndexes.get(Values.DEFAULT_INDEX);
		
		if (idx != null) 
		{
			return idx;
		}
		else {
			return tripleIndexes.values().iterator().next();
		}
	}
	
	
	private TripleSet findSuitableIndex(String rangeSpec) 
	{	
		if (!"".equals(rangeSpec)) 
		{
			for (Map.Entry<IndexSpec, TripleSet> entry: tripleIndexes.entrySet()) 
			{
				if (entry.getKey().name().toLowerCase().startsWith(rangeSpec)) 
				{
					return entry.getValue();
				}
			}
		}
		
		return getDefaultIndex();
	}
	
	
	public String inferRangeSpec(Triple data) 
	{
		StringBuilder sb = new StringBuilder();
		
		if (data.getSubject() != Values.CODE_NULL) {
			sb.append(Values.SUBJECT_CHAR);
		}
		
		if (data.getProperty() != Values.CODE_NULL) {
			sb.append(Values.PROPERTY_CHAR);
		}
		
		if (data.getObject() != Values.CODE_NULL) {
			sb.append(Values.OBJECT_CHAR);
		}
		
		if (data.getContext() != Values.CODE_NULL) {
			sb.append(Values.CONTEXT_CHAR);
		}
		
		return sb.toString();
	}
	
	
	public LongSet getContextIds(Resource... resources) 
	{
		if (isEmpty(resources)) {
			return null;
		}
		else 
		{
			LongHashSet set = new LongHashSet();
			
			for (Resource resource: resources) 
			{
				Long ctxId = getValueId(resource);
				
				if (ctxId != null) 
				{				
					set.add(ctxId);
				}
			}
			
			return set.toImmutable();
		}
	}
	
	
	public boolean isEmpty(Resource... resources) {
		return resources == null || resources.length == 0;
	}
	
	
	public CloseableIterator<Triple> getTriples(TripleSet index, Resource subj, IRI pred, Value obj, long inferred, Resource... contexts) 
	{
		long subjId;
		
		if (subj != null) 
		{
			Long id = valueIdMapping.get(subj);
			if (id != null) 
			{
				subjId = id;
			}
			else {
				return new EmptyIterator<>();
			}
		}
		else {
			subjId = Values.CODE_NULL;
		}
		
		
		long predId;
		
		if (pred != null) 
		{
			Long id = valueIdMapping.get(pred);
			if (id != null) 
			{
				predId = id;
			}
			else {
				return new EmptyIterator<>();
			}
		}
		else {
			predId = Values.CODE_NULL;
		}

		long objId;
		
		if (obj != null) 
		{
			Long id = valueIdMapping.get(obj);
			if (id != null) 
			{
				objId = id;
			}
			else {
				return new EmptyIterator<>();
			}
		}
		else {
			objId = Values.CODE_NULL;
		}
		
		LongSet contextIds = getContextIds(contexts);
		
		if (contextIds != null && contextIds.isEmpty()) 
		{
			return new EmptyIterator<>();
		}

		if (index == null) 
		{
			String rangeSpec = inferRangeSpec(
				new Triple(
						subjId, 
						predId, 
						objId, 
						contextIds == null ? 0 : 1
				)
			);

			index = findSuitableIndex(rangeSpec);
		}
		
		return index.find(new TriplePattern(subjId, predId, objId, contextIds));
	}

	public CloseableIteration<? extends Statement, QueryEvaluationException> 
	getStatements(Resource subj, IRI pred, Value obj, boolean includeInferred, Resource... contexts) throws QueryEvaluationException 
	{
		CloseableIterator<Triple> statements = getTriples(null, subj, pred, obj, includeInferred ? Values.CODE_NULL : Values.DECLARED, contexts);
		
		return new ConvertingIteration<Triple, Statement, QueryEvaluationException>(new IteratorCloseableIteration<Triple, QueryEvaluationException>(statements)){
			@Override
			protected Statement convert(Triple src) throws QueryEvaluationException 
			{
				Resource subject 	= (Resource) decode(src.getSubject());
				IRI property 		= (IRI) decode(src.getProperty());				
				Value object		= decode(src.getObject());
				
				Resource ctx;
				
				if (src.getContext() != Values.CODE_NULL) {
					ctx = (Resource) decode(src.getContext());
				}
				else {
					ctx = null;
				}
				
				return valueFactory.createStatement(subject, property, object, ctx);
			}
		};
	}
	
	

	protected Value decode(long id) {
		return idValueMapping.get(id);
	}

	private Long getValueId(Value value) 
	{
		return value != null ? valueIdMapping.get(value) : Values.CODE_NULL;
	}

	public static void main1(String[] args) {
		System.out.println(UUID.randomUUID());
	}

	@Override
	public ValueFactory getValueFactory() {
		return valueFactory;
	}

	@Override
	public EvaluationStatistics getEvaluationStatistics() {
		return null;
	}

	@Override
	public SailSource getExplicitSailSource() {
		return null;
	}

	@Override
	public SailSource getInferredSailSource() {
		return null;
	}

	public void rollback() {
		snapshot.drop();
	}

	public long indexSize() {
		return tripleIndexes.values().iterator().next().size();
	}
}
