package mariana.v1.rdf.store.config;

import java.io.Serializable;

import org.openrdf.model.Model;
import org.openrdf.model.Resource;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.SimpleValueFactory;
import org.openrdf.model.util.ModelException;
import org.openrdf.model.util.Models;
import org.openrdf.sail.config.AbstractSailImplConfig;
import org.openrdf.sail.config.SailConfigException;

import mariana.v1.rdf.store.TripleStoreFactory;
import mariana.v1.rdf.store.TripleStoreSchema;

public class TripleStoreConfig extends AbstractSailImplConfig implements Serializable {

	private static final long serialVersionUID = 3160127416907525336L;
	
	private String tripleIndexes;
	
	private int dictionaryShards = TripleStoreParameters.DEFAULT_DICTIONARY_SHARDS;
	private int triplesShards = TripleStoreParameters.DEFAULT_TRIPLES_SHARDS;
	
	public TripleStoreConfig() {
		super(TripleStoreFactory.SAIL_TYPE);
	}
	
	@Override
	public Resource export(Model graph) {
		
		Resource implNode = super.export(graph);

		ValueFactory factory = SimpleValueFactory.getInstance();
		
		if (tripleIndexes != null) {
			graph.add(implNode, TripleStoreSchema.TRIPLE_INDEXES, factory.createLiteral(tripleIndexes));
		}
		
		graph.add(implNode, TripleStoreSchema.TRIPLES_SHARDS, factory.createLiteral(triplesShards));
		graph.add(implNode, TripleStoreSchema.DICTIONARY_SHARDS, factory.createLiteral(dictionaryShards));
		
		return implNode;
	}
	
	@Override
	public void parse(Model graph, Resource implNode) throws SailConfigException 
	{
		super.parse(graph, implNode);

		try {
			Models.objectLiteral(graph.filter(implNode, TripleStoreSchema.TRIPLE_INDEXES, null)).ifPresent(syncDelayValue -> {
				try {
					setTripleIndexes(syncDelayValue.stringValue());
				}
				catch (NumberFormatException e) {
					throw new SailConfigException(
							"String value required for " + TripleStoreSchema.TRIPLE_INDEXES + " property, found " + syncDelayValue);
				}
			});
			
			Models.objectLiteral(graph.filter(implNode, TripleStoreSchema.TRIPLES_SHARDS, null)).ifPresent(syncDelayValue -> {
				try {
					setTripleShards(syncDelayValue.intValue());
				}
				catch (NumberFormatException e) {
					throw new SailConfigException(
							"Int value required for " + TripleStoreSchema.TRIPLES_SHARDS + " property, found " + syncDelayValue);
				}
			});
			
			Models.objectLiteral(graph.filter(implNode, TripleStoreSchema.DICTIONARY_SHARDS, null)).ifPresent(syncDelayValue -> {
				try {
					setDictionaryShards(syncDelayValue.intValue());
				}
				catch (NumberFormatException e) {
					throw new SailConfigException(
							"Int value required for " + TripleStoreSchema.DICTIONARY_SHARDS + " property, found " + syncDelayValue);
				}
			});
		}
		catch (ModelException e) {
			throw new SailConfigException(e.getMessage(), e);
		}
	}

	public String getTripleIndexes() {
		return tripleIndexes;
	}

	public void setTripleIndexes(String tripleIndexes) {
		this.tripleIndexes = tripleIndexes;
	}

	public int getDictionaryShards() {
		return dictionaryShards;
	}

	public void setDictionaryShards(int dictionaryShards) {
		this.dictionaryShards = dictionaryShards;
	}

	public int getTriplesShards() {
		return triplesShards;
	}

	public void setTripleShards(int tripleShards) {
		this.triplesShards = tripleShards;
	}
	
	public TripleStoreParameters asStoreParameters() {
		return new TripleStoreParameters.Builder()
				.withIndexes(tripleIndexes)
				.withDictionaryShards(dictionaryShards)
				.withTriplesShards(triplesShards)
				.build();
	}
}
