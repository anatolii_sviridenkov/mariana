package mariana.v1.rdf.store.config;

import java.io.Serializable;

import mariana.bridge.MarianaException;

public class StoreMetadata implements Serializable {
	private static final long serialVersionUID = -3023495879982176183L;
	
	private long idCounter;
	private long bNodeIdCounter;
	
	private TripleStoreParameters config;
	
	private CtrNames ctrNames;
	
	public StoreMetadata() {}

	public long getIdCounter() {
		return idCounter;
	}

	public void setIdCounter(long idCounter) {
		this.idCounter = idCounter;
	}
	
//	public long newId() {
//		return ++idCounter;
//	}

	public long getbNodeIdCounter() {
		return bNodeIdCounter;
	}

	public void setbNodeIdCounter(long bNodeIdCounter) {
		this.bNodeIdCounter = bNodeIdCounter;
	}
	
	public long newBNodeId() {
		return ++bNodeIdCounter;
	}

	public TripleStoreParameters getConfig() {
		return config;
	}

	public void setConfig(TripleStoreParameters config) {
		this.config = config;
	}

	public CtrNames getCtrNames() {
		return ctrNames;
	}
	
	public void init()
	{
		if (ctrNames == null) 
		{
			ctrNames = new CtrNames(config.getDictionaryShards(), config.getTriplesShards(), config.getIndexes());
			idCounter = config.getDictionaryShards();
		}
		else {
			throw new MarianaException("Containers names for this triple store have been already configured");
		}
	}
}
