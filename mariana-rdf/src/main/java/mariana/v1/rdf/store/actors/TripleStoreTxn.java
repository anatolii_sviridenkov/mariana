package mariana.v1.rdf.store.actors;

import java.util.Collection;
import java.util.List;

import org.openrdf.model.IRI;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;

import fi.jumi.actors.ActorRef;
import mariana.v1.framework.actors.Async;
import mariana.v1.framework.pipeline.CloseableStreamProducer;

public interface TripleStoreTxn extends AsyncTxnPerformer {
	
	void addStatement(Statement st);
	void addStatements(Collection<Statement> st);
	
	Async<ActorRef<CloseableStreamProducer<List<Statement>>>> 
	getStatements(Resource subj, IRI pred, Value obj, boolean includeInferred, Resource... contexts);
	
	void addStatement(boolean inferred, Resource subj, IRI pred, Value obj, Resource[] contexts);
}
