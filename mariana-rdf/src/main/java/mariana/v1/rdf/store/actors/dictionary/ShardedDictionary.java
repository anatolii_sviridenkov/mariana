package mariana.v1.rdf.store.actors.dictionary;



import org.mariana.rdf.triples.TriplePattern;
import org.openrdf.model.IRI;
import org.openrdf.model.Resource;
import org.openrdf.model.Value;

import com.gs.collections.api.map.primitive.LongObjectMap;
import com.gs.collections.api.map.primitive.ObjectLongMap;
import com.gs.collections.api.set.MutableSet;
import com.gs.collections.api.set.primitive.LongSet;
import com.gs.collections.impl.set.mutable.primitive.LongHashSet;

import mariana.v1.framework.actors.Async;

public interface ShardedDictionary extends Dictionary {
	Async<ObjectLongMap<Value>> addValues(MutableSet<Value> values);
	
//	void setIDBase(long idBase);
	Async<Long> getMaxID();
	
	Async<TriplePattern> resolve(Resource subject, IRI predicate, Value object, Resource... context);

	
}
