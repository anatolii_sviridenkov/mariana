package mariana.v1.rdf.store;

import java.util.ArrayList;
import java.util.List;

import org.openrdf.IsolationLevel;
import org.openrdf.model.IRI;
import org.openrdf.model.Namespace;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.BindingSet;
import org.openrdf.query.Dataset;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.QueryRoot;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.evaluation.EvaluationStrategy;
import org.openrdf.query.algebra.evaluation.TripleSource;
import org.openrdf.query.algebra.evaluation.impl.BindingAssigner;
import org.openrdf.query.algebra.evaluation.impl.CompareOptimizer;
import org.openrdf.query.algebra.evaluation.impl.ConjunctiveConstraintSplitter;
import org.openrdf.query.algebra.evaluation.impl.ConstantOptimizer;
import org.openrdf.query.algebra.evaluation.impl.DisjunctiveConstraintOptimizer;
import org.openrdf.query.algebra.evaluation.impl.FilterOptimizer;
import org.openrdf.query.algebra.evaluation.impl.IterativeEvaluationOptimizer;
import org.openrdf.query.algebra.evaluation.impl.OrderLimitOptimizer;
import org.openrdf.query.algebra.evaluation.impl.QueryJoinOptimizer;
import org.openrdf.query.algebra.evaluation.impl.QueryModelNormalizer;
import org.openrdf.query.algebra.evaluation.impl.SameTermFilterOptimizer;
import org.openrdf.query.algebra.evaluation.impl.SimpleEvaluationStrategy;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.sail.NotifyingSailConnection;
import org.openrdf.sail.SailConnectionListener;
import org.openrdf.sail.SailException;
import org.openrdf.sail.UnknownSailTransactionStateException;
import org.openrdf.sail.UpdateContext;
import org.openrdf.sail.inferencer.InferencerConnection;

import info.aduna.iteration.CloseableIteration;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.rdf.store.config.TripleStoreParameters;

public class TripleStoreConnection implements NotifyingSailConnection, TripleSource, InferencerConnection, AutoCloseable {

	private InMemoryAllocator allocator;	
	private ShardedTripleStoreTxn txn;
	
	private boolean opened = true;
	private TripleStoreParameters parameters;
	
	private List<SailConnectionListener> listeners = new ArrayList<>();
	private TripleStore store;
	private boolean readOnly = false;

	public TripleStoreConnection(InMemoryAllocator allocator, TripleStore store) {
		this.allocator = allocator;
		this.store = store;
		this.parameters = store.getConfig();
	}

	@Override
	public boolean isOpen() throws SailException {
		return opened;
	}

	@Override
	public void close() {
		opened = false;
		store.connectionClosed(this);
	}

	@Override
	public CloseableIteration<? extends BindingSet, QueryEvaluationException> evaluate(
			TupleExpr tupleExpr,
			Dataset dataset, 
			BindingSet bindings, 
			boolean includeInferred
	) throws SailException 
	{		
		flush();
//		logger.trace("Incoming query model:\n{}", tupleExpr);

		// Clone the tuple expression to allow for more aggresive optimizations
		tupleExpr = tupleExpr.clone();

		if (!(tupleExpr instanceof QueryRoot)) {
			// Add a dummy root node to the tuple expressions to allow the
			// optimizers to modify the actual root node
			tupleExpr = new QueryRoot(tupleExpr);
		}

		try {

			TripleSource tripleSource = getTripleSource();
			EvaluationStrategy strategy = getEvaluationStrategy(dataset, tripleSource);

			new BindingAssigner().optimize(tupleExpr, dataset, bindings);
			new ConstantOptimizer(strategy).optimize(tupleExpr, dataset, bindings);
			new CompareOptimizer().optimize(tupleExpr, dataset, bindings);
			new ConjunctiveConstraintSplitter().optimize(tupleExpr, dataset, bindings);
			new DisjunctiveConstraintOptimizer().optimize(tupleExpr, dataset, bindings);
			new SameTermFilterOptimizer().optimize(tupleExpr, dataset, bindings);
			new QueryModelNormalizer().optimize(tupleExpr, dataset, bindings);
			new QueryJoinOptimizer(store.getEvaluationStatistics()).optimize(tupleExpr, dataset, bindings);
			// new SubSelectJoinOptimizer().optimize(tupleExpr, dataset, bindings);
			new IterativeEvaluationOptimizer().optimize(tupleExpr, dataset, bindings);
			new FilterOptimizer().optimize(tupleExpr, dataset, bindings);
			new OrderLimitOptimizer().optimize(tupleExpr, dataset, bindings);

//			logger.trace("Optimized query model:\n{}", tupleExpr);

			CloseableIteration<BindingSet, QueryEvaluationException> iter;
			iter = strategy.evaluate(tupleExpr, EmptyBindingSet.getInstance());
			return iter;
		}
		catch (QueryEvaluationException e) {
			throw new SailException(e);
		}
	}
	
	
	protected TripleSource getTripleSource() {
		return this;
	}

	@Override
	public CloseableIteration<? extends Resource, SailException> getContextIDs() throws SailException 
	{
		return null;
	}

	@Override
	public CloseableIteration<? extends Statement, SailException> getStatements(
			Resource subj, IRI pred, Value obj,
			boolean includeInferred, Resource... contexts
	) throws SailException 
	{		
		return null;
	}

	@Override
	public long size(Resource... contexts) throws SailException 
	{
		checkIfTxnIsActive();
		return txn.indexSize();
	}

	@Override
	public void begin() throws SailException 
	{
		if (txn == null) 
		{
			txn = new ShardedTripleStoreTxn(allocator, readOnly, store.getActorSystem());
		}
		else {
			throw new TripleStoreException("Transaction is already active");
		}
	}

	@Override
	public void begin(IsolationLevel level) throws UnknownSailTransactionStateException, SailException 
	{
		if (txn == null) 
		{
			txn = new ShardedTripleStoreTxn(allocator, readOnly, store.getActorSystem());
		}
		else {
			throw new TripleStoreException("Transaction is already active");
		}
	}

	@Override
	public void flush() throws SailException {
		if (txn != null) 
		{
			txn.flush();
		}
	}

	@Override
	public void prepare() throws SailException {}

	@Override
	public void commit() throws SailException {
		if (txn != null)
		{
			txn.commit();
			txn.close();
			
			txn = null;
		}
		else {
			throw new TripleStoreException("Transaction is not active");
		}
	}

	@Override
	public void rollback() throws SailException 
	{
		checkIfTxnIsActive();

		txn.rollback();
		txn.close();
		
		txn = null;		
	}

	@Override
	public boolean isActive() throws UnknownSailTransactionStateException {
		return txn != null;
	}

	@Override
	public void addStatement(Resource subj, IRI pred, Value obj, Resource... contexts) throws SailException {
		checkIfTxnIsActive();
		txn.addStatement(false, subj, pred, obj, contexts);
	}

	@Override
	public void removeStatements(Resource subj, IRI pred, Value obj, Resource... contexts) throws SailException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void startUpdate(UpdateContext op) throws SailException {
		
	}

	@Override
	public void addStatement(UpdateContext op, Resource subj, IRI pred, Value obj, Resource... contexts) throws SailException {
		addStatement(subj, pred, obj, contexts);
	}

	@Override
	public void removeStatement(UpdateContext op, Resource subj, IRI pred, Value obj, Resource... contexts) throws SailException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void endUpdate(UpdateContext op) throws SailException {}

	@Override
	public void clear(Resource... contexts) throws SailException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public CloseableIteration<? extends Namespace, SailException> getNamespaces() throws SailException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public String getNamespace(String prefix) throws SailException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public void setNamespace(String prefix, String name) throws SailException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeNamespace(String prefix) throws SailException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public void clearNamespaces() throws SailException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public boolean addInferredStatement(Resource arg0, IRI arg1, Value arg2, Resource... arg3) throws SailException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clearInferred(Resource... arg0) throws SailException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public void flushUpdates() throws SailException {}

	@Override
	public boolean removeInferredStatement(Resource arg0, IRI arg1, Value arg2, Resource... arg3) throws SailException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public CloseableIteration<? extends Statement, QueryEvaluationException> getStatements(
			Resource subj, 
			IRI pred,
			Value obj, 
			Resource... contexts
	) throws QueryEvaluationException 
	{
		checkIfTxnIsActive();
		return txn.getStatements(subj, pred, obj, true, contexts);
	}

	@Override
	public ValueFactory getValueFactory() {
		checkIfTxnIsActive();
		return txn.getValueFactory();
	}

	private void checkIfTxnIsActive() 
	{
		if (!isActive()) {
			throw new TripleStoreException("Transaction is not active");
		}
	}
	
	protected EvaluationStrategy getEvaluationStrategy(Dataset dataset, TripleSource tripleSource) {
		return new SimpleEvaluationStrategy(tripleSource, dataset, store.getFederatedServiceResolver());
	}
	
	
	@Override
	public void addConnectionListener(SailConnectionListener listener) {
		synchronized (listeners) {
			listeners.add(listener);
		}
	}

	@Override
	public void removeConnectionListener(SailConnectionListener listener) {
		synchronized (listeners) {
			listeners.remove(listener);
		}
	}

	protected boolean hasConnectionListeners() {
		synchronized (listeners) {
			return !listeners.isEmpty();
		}
	}

	protected void notifyStatementAdded(Statement st) {
		synchronized (listeners) {
			for (SailConnectionListener listener : listeners) {
				listener.statementAdded(st);
			}
		}
	}

	protected void notifyStatementRemoved(Statement st) {
		synchronized (listeners) {
			for (SailConnectionListener listener : listeners) {
				listener.statementRemoved(st);
			}
		}
	}


}
