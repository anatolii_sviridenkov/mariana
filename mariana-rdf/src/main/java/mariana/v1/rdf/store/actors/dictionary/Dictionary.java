package mariana.v1.rdf.store.actors.dictionary;

import java.util.List;

import org.openrdf.model.Value;

import com.gs.collections.api.map.primitive.LongObjectMap;
import com.gs.collections.api.map.primitive.ObjectLongMap;
import com.gs.collections.api.set.MutableSet;
import com.gs.collections.api.set.primitive.LongSet;

import mariana.v1.framework.actors.Async;
import mariana.v1.rdf.store.actors.AsyncTxnPerformer;

public interface Dictionary extends AsyncTxnPerformer {
	Async<ObjectLongMap<Value>> addValues(MutableSet<Value> values);
	
//	void setIDBase(long idBase);
	Async<Long> getMaxID();
	
	Async<Long> resolveID(Value value);
	Async<LongObjectMap<Value>> resolve(List<Value> values);
	
	Async<LongObjectMap<Value>> resolveValues(LongSet set);
}
