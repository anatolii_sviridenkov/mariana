package mariana.v1.rdf.store.config;

import java.io.Serializable;
import java.util.UUID;

public class DictionaryNames implements Serializable {
	private static final long serialVersionUID = 5140371929139505278L;

	private UUID idValueName;
	private UUID valueIdName;
	
	private UUID idNsName;
	private UUID nsIdName;
	
	protected DictionaryNames() {}
	
	public DictionaryNames(UUID idValueName, UUID valueIDName, UUID idNsName, UUID nsIdName) 
	{
		this.idValueName = idValueName;
		this.valueIdName = valueIDName;
		this.idNsName 	 = idNsName;
		this.nsIdName 	 = nsIdName;		
	}

	public UUID getIdValueName() {
		return idValueName;
	}

	public UUID getValueIdName() {
		return valueIdName;
	}

	public UUID getIdNsName() {
		return idNsName;
	}

	public UUID getNsIdName() {
		return nsIdName;
	}
}
