package mariana.v1.rdf.store.actors.dictionary;

import java.util.List;

import org.openrdf.model.Value;

import com.gs.collections.api.list.primitive.LongList;

import mariana.v1.framework.actors.Async;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.rdf.store.config.CtrNames;

public interface DictionaryShard extends Dictionary {
	
	Async<LongList> resolveIDs(List<Value> value);
}
