package mariana.v1.rdf.store;

import org.openrdf.sail.Sail;
import org.openrdf.sail.config.SailConfigException;
import org.openrdf.sail.config.SailFactory;
import org.openrdf.sail.config.SailImplConfig;

import mariana.v1.rdf.store.config.TripleStoreConfig;

public class TripleStoreFactory implements SailFactory {

	public static final String SAIL_TYPE = "mariana:TripleStore";
	
	@Override
	public String getSailType() {
		return SAIL_TYPE;
	}

	@Override
	public SailImplConfig getConfig() {
		return new TripleStoreConfig();
	}

	@Override
	public Sail getSail(SailImplConfig config) throws SailConfigException 
	{		
		if (!SAIL_TYPE.equals(config.getType())) {
			throw new SailConfigException("Invalid Sail type: " + config.getType());
		}
		
		TripleStore store = new TripleStore();
		
		if (config instanceof TripleStoreConfig) 
		{
			TripleStoreConfig rdfConfig = (TripleStoreConfig)config;

			if (rdfConfig.getIterationCacheSyncThreshold() > 0) 
			{
				store.setIterationCacheSyncThreshold(rdfConfig.getIterationCacheSyncThreshold());
			}
			
			store.setConfig(rdfConfig.asStoreParameters());
		}
		
		return store;
	}

}
