package mariana.v1.rdf;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fi.jumi.actors.ActorRef;
import fi.jumi.actors.ActorThread;
import mariana.v1.framework.actors.Async;
import mariana.v1.framework.actors.MarianaActors;
import mariana.v1.framework.actors.MarianaMultiThreadedActors;

public class ActorEx {

	public static void main(String[] args) {
		ExecutorService actorsThreadPool = Executors.newCachedThreadPool();
        MarianaActors actors = new MarianaMultiThreadedActors(actorsThreadPool);

        ActorThread actorThread = actors.startActorThread();

        ActorRef<Greeter> helloGreeter = actorThread.bindActor(Greeter.class, new Greeter() {
            @Override
            public void sayGreeting(String name) 
            {
                System.out.println("Hello " + name + " from " + Thread.currentThread().getName());
                try {
					Thread.sleep(10);
				} 
                catch (InterruptedException e) {
					e.printStackTrace();
				}
            }

			@Override
			public Async<String> getSomething() {
				return Async.of("This is something");
//				throw new RuntimeException("Boo!");
			}
        });
        
        for (int c = 0; c < 10; c++) {
        	helloGreeter.tell().sayGreeting("World " + c);        	
        }

        System.out.println("Messages have sent " + helloGreeter.tell().getSomething().get());
        
        
        actorThread.stop();

        actorsThreadPool.shutdown();

	}

	public interface Greeter {

        void sayGreeting(String name);
        
        Async<String> getSomething();
	}
}
