package mariana.v1.rdf;

import java.io.File;

import org.mariana.rdf.triples.IndexSpec;
import org.openrdf.model.Statement;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.QueryEvaluationException;

import info.aduna.iteration.CloseableIteration;
import mariana.v1.Mariana;
import mariana.v1.rdf.store.TripleStore;
import mariana.v1.rdf.store.TripleStoreConnection;
import mariana.v1.rdf.store.config.TripleStoreParameters;

public class TripleStoreEx {
	
	{
		Mariana.init();
	}
	
	public static void main(String[] args) 
	{
		TripleStore store = new TripleStore();

		try {
			store.setConfig(
					new TripleStoreParameters.Builder()
					.withIndexes(IndexSpec.SPOC, IndexSpec.POSC, IndexSpec.OPSC) //
					.withDictionaryShards(1)
					.withTriplesShards(1)
					.build()
			);

			File dataDir = new File("target/store");

			store.setDataDir(dataDir);
			store.initialize();
			
//			store.getAllocator().dump("target/ts.memoria-onload-" + System.currentTimeMillis() + ".dir");
			
			try (TripleStoreConnection conn = (TripleStoreConnection) store.getConnection())  
			{					
				conn.begin();

				ValueFactory vf = conn.getValueFactory();

				long t0 = System.currentTimeMillis();

				for (int c = 0; c < 1000000; c++)
				{
					conn.addStatement(
						vf.createIRI("http://example.com/resource" + (c / 10)), 
						vf.createIRI("http://example.com/property0"),
						vf.createLiteral("Hello, World!!!! -- " + c)
					);
				}

				conn.flush();

				System.out.println("Ingestion completed in " + (System.currentTimeMillis() - t0));

				long t1 = System.currentTimeMillis();

				int cnt = 0;

				try(CloseableIteration<? extends Statement, QueryEvaluationException> st = conn.getStatements(null, null, null)) 
				{
					while (st.hasNext()) 
					{
						//System.out.println(st.next());
						st.next();
						cnt++;
					}
				}

				System.out.println("Total rows: " + cnt + ", time = " + (System.currentTimeMillis() - t1));

				conn.commit();
			}			

			long t2 = System.currentTimeMillis();
			store.store();

			System.out.println("Stored in " + (System.currentTimeMillis() - t2));

//			store.getAllocator().dump("target/ts.memoria-" + System.currentTimeMillis() + ".dir");
			store.close();
		}
		catch (Exception ex) {			
			ex.printStackTrace();
			System.exit(0);
		}
	}
}
