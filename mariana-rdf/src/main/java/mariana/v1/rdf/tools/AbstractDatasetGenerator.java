package mariana.v1.rdf.tools;

public abstract class AbstractDatasetGenerator {
	
	private final int rows;
	private final int cols;

	public AbstractDatasetGenerator(int rows, int cols) 
	{
		this.rows = rows;
		this.cols = cols;
		
	}
	
	public void generate() 
	{
		for (int r = 0; r < rows; r++) 
		{
			for (int c = 0; c < cols; c++) 
			{
				onItem(r, c);
			}
		}
	}

	protected abstract void onItem(int row, int col);
}
