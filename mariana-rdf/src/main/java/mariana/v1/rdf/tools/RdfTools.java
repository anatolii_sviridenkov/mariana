package mariana.v1.rdf.tools;

import org.openrdf.model.BNode;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Value;

import mariana.v1.rdf.model.RdfBNode;
import mariana.v1.rdf.model.RdfLiteral;
import mariana.v1.rdf.model.RdfResource;
import mariana.v1.rdf.store.TripleStoreException;

public class RdfTools {
	public static Value internalize(Value value) 
	{
		if (value != null) 
		{
			if (value instanceof RdfLiteral) {
				return value;
			}
			else if (value instanceof RdfResource) {
				return value;
			}
			else if (value instanceof RdfBNode) {
				return value;
			}
			else if (value instanceof Literal) 
			{
				Literal l = (Literal) value;
				return new RdfLiteral(l.getLabel(), l.getDatatype());
			}
			else if (value instanceof BNode) 
			{
				BNode n = (BNode) value;
				return new RdfBNode(n.getID());
			}
			else if (value instanceof Resource)
			{
				Resource r = (Resource) value;
				return new RdfResource(r.stringValue());
			}
			else {
				throw new TripleStoreException("Unrecognized foreign value: " + value);
			}
		}
		
		return null;
	}
}
