package mariana.v1.rdf;

import java.util.UUID;

import org.mariana.rdf.triples.TripleSet;
import org.openrdf.model.Resource;
import org.openrdf.model.Value;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.rdf.dictionary.IDValueMapping;
import mariana.v1.rdf.model.RdfValueFactory;

/**
 * Hello world!
 *
 */
public class IDMappingEx 
{
	static {
		Mariana.init();
	}
	
	public static void main(String[] args) 
	{
		try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{
    			try (Snapshot snp = master.branch()) 
    			{
    				try(IDValueMapping mmap = IDValueMapping.findOrCreate(snp, UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fd64")))
    				{
    					RdfValueFactory vf = new RdfValueFactory();
    					
    					int size = 100000;
    					
//    					for (int c = 0; c < size; c++) 
//    					{
//    						Literal lit = vf.createLiteral("Str__" + c);
//    						mmap.set(c, lit);
//    					}
    					
    					for (int c = 0; c < size; c++) 
    					{
    						Resource lit = vf.createIRI("http://example.com#booo" + c);
    						mmap.set(c, lit);
    					}
    					
    					for (int c = 0; c < size; c++) 
    					{
    						Value value = mmap.get(c);
    						System.out.println(value + " -- " + c);
    					}
    					
    					try (IDValueMapping.Iterator ii = mmap.iterator()) 
    					{
    						int c = 0;
    						while (ii.hasNext()) {
    							System.out.println(ii.next() + " -- " + (c++));
    						}
    					}
    				}
    				
    				snp.commit();

    				snp.setAsMaster();
    				
    				snp.dump("target/idmap.dir");
    			}
    		}
    	}
	}
}
