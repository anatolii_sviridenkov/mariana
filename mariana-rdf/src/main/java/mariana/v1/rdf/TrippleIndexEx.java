package mariana.v1.rdf;

import java.util.Random;
import java.util.UUID;

import org.mariana.rdf.triples.Triple;
import org.mariana.rdf.triples.TriplePattern;
import org.mariana.rdf.triples.TripleSet;
import org.mariana.rdf.triples.iterator.FullIterator;
import org.mariana.rdf.triples.iterator.RangeIterator;
import org.mariana.rdf.triples.iterator.TripleIterator;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.inmem.alloc.InMemoryAllocator;

/**
 * Hello world!
 *
 */
public class TrippleIndexEx 
{
	static {
		Mariana.init();
	}
	
	private static int abs(int value) {
		return value < 0 ? -value : value;
	}
	
	public static void main(String[] args) 
	{
		try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{
    			try (Snapshot snp = master.branch()) 
    			{
    				try(TripleSet tidx = TripleSet.findOrCreate(snp, UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fd64"), "SPOC"))
    				{
    					Random RNG = new Random(0);
    					
    					long t0 = System.currentTimeMillis();
    					
    					for (int c = 0; c < 1000; c++) 
    					{
    						Triple st = new Triple(abs(RNG.nextInt()), abs(RNG.nextInt()), abs(RNG.nextInt()), abs(RNG.nextInt()));
    						tidx.insert(st);
    					}
    					
    					System.out.println("Insertion time: " + (System.currentTimeMillis() - t0));
    					    					
    					int c = 0;
    					long t1 = System.currentTimeMillis();
//    					try(StatementFullIterator iterator = tidx.iterator()) 
//    					{	
//    						while (iterator.hasNext()) 
//    						{
//    							StatementData next = iterator.next();
//    							
////    							if (c % 10000 == 0) {
//    								System.out.println(next + " -- " + c);	
////    							}
//    							
//    							c++;
//    						}
//    					}
    					
    					System.out.println("Iteration time: " + (System.currentTimeMillis() - t1) + ", c = " + c);
    					
    					System.out.println("Size: " + tidx.size());
    					
    					//1531753685
    					TripleIterator<Triple> find = tidx.find(new TriplePattern(1531753685, 0, 0, null));
    					
    					if (find.hasNext()) 
    					{
    						System.out.println("Find: " + find.next() + " -- " + find.idx());
    					}
    					else {
    						System.out.println("NotFound");
    					}
    				}
    				
    				snp.commit();

    				snp.setAsMaster();
    				
    				snp.dump("target/tidx.dir");
    			}
    		}
    	}
	}
}
