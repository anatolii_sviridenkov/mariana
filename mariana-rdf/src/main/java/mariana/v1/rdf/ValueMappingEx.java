package mariana.v1.rdf;

import java.util.UUID;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.rdf.dictionary.IDValueMapping;
import mariana.v1.rdf.dictionary.ValueIDMapping;
import mariana.v1.rdf.model.RdfValueFactory;

/**
 * Hello world!
 *
 */
public class ValueMappingEx 
{
	static {
		Mariana.init();
	}
	
	public static void main(String[] args) 
	{
		try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{
    			try (Snapshot snp = master.branch()) 
    			{
    				try(ValueIDMapping mmap = ValueIDMapping.findOrCreate(snp, UUID.fromString("a962dead-254f-4180-9c73-f5666c37fbbb")))
    				{
    					RdfValueFactory vf = new RdfValueFactory();
    					
    					int size = 100000;
    					
    					long t0 = System.currentTimeMillis();
    					
    					for (int c = 0; c < size; c++) 
    					{
    						Literal lit = vf.createLiteral("Str__" + c);
    						mmap.set(lit, Long.valueOf(c));
    					}
    					
    					System.out.println("Insert time: " + (System.currentTimeMillis() - t0));
    					
    					long t1 = System.currentTimeMillis();
    					
    					for (int c = 0; c < size; c++) 
    					{
    						Literal lit = vf.createLiteral("Str__" + c);
    						
    						Long id = mmap.get(lit);
    						
//    						System.out.println(lit + " -- " + c + " -- " + id);
    					}
    					
    					System.out.println("Query time: " + (System.currentTimeMillis() - t1));
    					
//    					try (ValueIDMapping.Iterator ii = mmap.iterator()) 
//    					{    						
//    						while (ii.hasNext()) {
//    							System.out.println(ii.next());
//    						}
//    					}
    				}
    				
    				snp.commit();

    				snp.setAsMaster();
    				
//    				snp.dump("target/idmap.dir");
    			}
    		}
    	}
	}
}
