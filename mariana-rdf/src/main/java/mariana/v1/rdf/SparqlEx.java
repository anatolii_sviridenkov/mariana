package mariana.v1.rdf;

import java.io.File;

import org.openrdf.model.Statement;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.resultio.sparqlxml.SPARQLResultsXMLWriter;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.repository.sail.SailRepositoryConnection;

import info.aduna.iteration.CloseableIteration;
import mariana.v1.rdf.store.TripleStore;
import mariana.v1.rdf.store.TripleStoreConnection;

public class SparqlEx {
	public static void main(String[] args) 
	{
		TripleStore store = new TripleStore();
		
		store.setDataDir(new File("target/store"));
		
		SailRepository repo = new SailRepository(store);
		
		repo.initialize();
		
		
		try (SailRepositoryConnection conn =  repo.getConnection())  
		{
			conn.begin();
			
			ValueFactory vf = conn.getValueFactory();
			
			conn.add(
					vf.createIRI("http://example.com/resource0"), 
					vf.createIRI("http://example.com/property0"),
					vf.createLiteral("Hello, World!!!!")
			);
			
			TupleQuery qry = conn.prepareTupleQuery("select ?s ?p ?o where {?s ?p ?o}");
			
			qry.evaluate(new SPARQLResultsXMLWriter(System.out));
			
			conn.commit();
		} 
		
		repo.shutDown();
	}
}
