package mariana.v1.rdf.model;

import org.openrdf.model.Literal;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.OutputHandler;
import mariana.v1.io.serialization.PropertyGetter;
import mariana.v1.io.serialization.UTF8StringOutputHandler;

public class LiteralOutputHandler implements ObjectOutputHandler {
	protected OutputHandler[] handlers;
	
	private int idx;

	private PropertyGetter getter;
	
	private String label;
	private String iri;
	
	public LiteralOutputHandler() {
		this(null);
	}
	
	public LiteralOutputHandler(PropertyGetter getter) 
	{
		this.getter = getter;
		
		this.handlers = new OutputHandler[]{
			new UTF8StringOutputHandler(src -> {
				return ((LiteralOutputHandler)src).label;
			}),
			new UTF8StringOutputHandler(src -> {
				return ((LiteralOutputHandler)src).iri;
			})
		};
	}
	
	@Override
	public boolean writeTo(IOBuffer buf) 
	{
		for (; idx < handlers.length; idx++) 
		{
			if (!handlers[idx].writeTo(buf)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void reset(Object source) 
	{
		Literal src2 = (Literal) getter.apply(source);
		
		label = src2.getLabel();
		iri = src2.getDatatype().toString();
		
		idx = 0;
		for (int c = 0; c < handlers.length; c++) {
			handlers[c].reset(this);
		}
	}

	@Override
	public void setPropertyGetter(PropertyGetter getter) {
		this.getter = getter;		
	}
}
