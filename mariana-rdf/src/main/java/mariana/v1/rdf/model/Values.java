package mariana.v1.rdf.model;

import org.mariana.rdf.triples.IndexSpec;

import com.gs.collections.api.set.primitive.LongSet;
import com.gs.collections.impl.set.mutable.primitive.LongHashSet;

import mariana.v1.tools.codecs.UInt63Codec;

public interface Values {	
	public static final int LITERAL_VALUE  		= 1;
	public static final int RESOURCE_VALUE  	= 2;
	public static final int BNODE_VALUE  		= 3;
	
	public static final long UVL_NULL_VALUE		= UInt63Codec.NULL;
	public static final long CODE_NULL 			= 0;
	public static final long CODE_EMPTY 		= -1;
	public static final int PATTERN_LENGTH 		= 4;
	
	public static final LongSet emptyLongSet = LongHashSet.newSetWith().toImmutable();
	
	public static final char SUBJECT_CHAR 		= 'S';
	public static final char PROPERTY_CHAR 		= 'P';
	public static final char OBJECT_CHAR 		= 'O';
	public static final char CONTEXT_CHAR 		= 'C';
	
	public static final IndexSpec DEFAULT_INDEX = IndexSpec.SPOC;
	
	public static final int DECLARED = 1;
	public static final int INFERRED = 2;
	
	public static final String METADATA_CFG_NAME = "METADATA";
	
	public static final String DICTIONARY 	= "DICTIONARY";

}
