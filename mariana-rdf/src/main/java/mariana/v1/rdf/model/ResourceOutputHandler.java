package mariana.v1.rdf.model;

import org.openrdf.model.BNode;
import org.openrdf.model.IRI;
import org.openrdf.model.Resource;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.OutputHandler;
import mariana.v1.io.serialization.PrimitiveOutputHandler;
import mariana.v1.io.serialization.PropertyGetter;
import mariana.v1.io.serialization.UTF8StringOutputHandler;

public class ResourceOutputHandler implements ObjectOutputHandler {	
	
	protected OutputHandler[] handlers;
	
	private int idx;

	private PropertyGetter getter;
	
	private int typeCode;
	private String iri;
	
	public ResourceOutputHandler(PropertyGetter getter) 
	{
		this.getter = getter;
		this.handlers = new OutputHandler[]{
			new PrimitiveOutputHandler((src, buf) -> {
				int len = buf.uvlength(typeCode);
				if (buf.remaining() >= len) {
					buf.putUVLen(typeCode);
					return true;
				}
				return false;
			}),
			
			new UTF8StringOutputHandler(src -> {
				return ((ResourceOutputHandler)src).iri;
			})
		};
	}
	
	public ResourceOutputHandler() {
		this(null);
	}
	
	@Override
	public boolean writeTo(IOBuffer buf) 
	{
		for (; idx < handlers.length; idx++) 
		{
			if (!handlers[idx].writeTo(buf)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void reset(Object source) 
	{
		Resource resource = (Resource) getter.apply(source);
		
		if (resource instanceof BNode) {
			typeCode = Values.BNODE_VALUE;
			iri = ((BNode)resource).getID();
		}
		else {
			typeCode = Values.RESOURCE_VALUE;
			iri = ((IRI)resource).stringValue();
		}
		
		idx = 0;
		for (int c = 0; c < handlers.length; c++) {
			handlers[c].reset(this);
		}
	}

	@Override
	public void setPropertyGetter(PropertyGetter getter) {
		this.getter = getter;		
	}
	
}
