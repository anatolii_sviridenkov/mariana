package mariana.v1.rdf.model;

import org.openrdf.model.impl.SimpleBNode;

public class RdfBNode extends SimpleBNode {

	private static final long serialVersionUID = 539760010812634211L;

	public RdfBNode() {
		super();
	}

	public RdfBNode(String id) {
		super(id);
	}
	
	public void setID(String id) {
		super.setID(id);
	}
}
