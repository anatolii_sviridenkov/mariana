package mariana.v1.rdf.model;

import org.openrdf.model.IRI;
import org.openrdf.model.impl.SimpleLiteral;
import org.openrdf.model.vocabulary.XMLSchema;

public class RdfLiteral extends SimpleLiteral {

	private static final long serialVersionUID = 7061296208103466838L;

	public RdfLiteral() {
		super();		
	}

	public RdfLiteral(String label, IRI datatype) {
		super(label, datatype);
	}

	public RdfLiteral(String label, String language) {
		super(label, language);
		setDatatype(XMLSchema.STRING);
	}

	public RdfLiteral(String label) {
		super(label);
	}

}
