package mariana.v1.rdf.model;

import org.openrdf.model.impl.SimpleIRI;

public class RdfResource extends SimpleIRI {

	private static final long serialVersionUID = -3476089132541833479L;
	
	public RdfResource() {
		super();
	}

	public RdfResource(String iriString) {
		super(iriString);
	}
}
