package mariana.v1.rdf.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.openrdf.model.BNode;
import org.openrdf.model.IRI;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.datatypes.XMLDatatypeUtil;
import org.openrdf.model.vocabulary.XMLSchema;

import mariana.v1.rdf.store.TripleStoreSnapshot;

public class RdfValueFactory implements ValueFactory {

	private static final DatatypeFactory datatypeFactory;
	private final TripleStoreSnapshot snapshot;

	static {
		try {
			datatypeFactory = DatatypeFactory.newInstance();
		}
		catch (DatatypeConfigurationException e) {
			throw new Error("Could not instantiate javax.xml.datatype.DatatypeFactory", e);
		}
	}
	
	public RdfValueFactory() {
		snapshot = null;
	}
	
	public RdfValueFactory(TripleStoreSnapshot snapshot) {
		this.snapshot = snapshot;
	}
	
	@Override
	public IRI createIRI(String iri) {
		return new RdfResource(iri);
	}

	@Override
	public IRI createIRI(String namespace, String localName) {
		return new RdfResource(namespace + localName);
	}

	@Override
	public BNode createBNode() 
	{
		if (snapshot != null) {
			return new RdfBNode(String.valueOf(snapshot.newBNodeId()));
		}
		else {
			return new RdfBNode();
		}
	}

	@Override
	public BNode createBNode(String nodeID) {
		return new RdfBNode(nodeID);
	}

	@Override
	public Literal createLiteral(String label) {
		return new RdfLiteral(label, XMLSchema.STRING);
	}

	@Override
	public Literal createLiteral(String label, String language) {
		return new RdfLiteral(label, language);
	}

	@Override
	public Literal createLiteral(String label, IRI datatype) {
		return new RdfLiteral(label, datatype);
	}

	@Override
	public Literal createLiteral(boolean value) {
		return new RdfLiteral(Boolean.toString(value), XMLSchema.BOOLEAN);
	}

	@Override
	public Literal createLiteral(byte value) {
		return new RdfLiteral(Byte.toString(value), XMLSchema.BYTE);
	}

	@Override
	public Literal createLiteral(short value) {
		return new RdfLiteral(Short.toString(value), XMLSchema.SHORT);
	}

	@Override
	public Literal createLiteral(int value) {
		return new RdfLiteral(Integer.toString(value), XMLSchema.INT);
	}

	@Override
	public Literal createLiteral(long value) {
		return new RdfLiteral(Long.toString(value), XMLSchema.LONG);
	}

	@Override
	public Literal createLiteral(float value) {
		return new RdfLiteral(Float.toString(value), XMLSchema.FLOAT);
	}

	@Override
	public Literal createLiteral(double value) {
		return new RdfLiteral(Double.toString(value), XMLSchema.DOUBLE);
	}

	@Override
	public Literal createLiteral(BigDecimal bigDecimal) {
		return new RdfLiteral(bigDecimal.toString(), XMLSchema.DECIMAL);
	}

	@Override
	public Literal createLiteral(BigInteger bigInteger) {
		return new RdfLiteral(bigInteger.toString(), XMLSchema.INTEGER);
	}

	@Override
	public Literal createLiteral(XMLGregorianCalendar calendar) {
		return new RdfLiteral(calendar.toXMLFormat(), XMLDatatypeUtil.qnameToURI(calendar.getXMLSchemaType()));
	}

	@Override
	public Literal createLiteral(Date date) 
	{
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);

		XMLGregorianCalendar xmlGregCalendar = datatypeFactory.newXMLGregorianCalendar(c);
		return createLiteral(xmlGregCalendar);
	}

	@Override
	public Statement createStatement(Resource subject, IRI predicate, Value object) {
		return new RdfStatement(subject, predicate, object);
	}

	@Override
	public Statement createStatement(Resource subject, IRI predicate, Value object, Resource context) {
		return new RdfContextStatement(subject, predicate, object, context);
	}
}
