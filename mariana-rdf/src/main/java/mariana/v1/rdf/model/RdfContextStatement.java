package mariana.v1.rdf.model;

import org.openrdf.model.IRI;
import org.openrdf.model.Resource;
import org.openrdf.model.Value;
import org.openrdf.model.impl.ContextStatement;

public class RdfContextStatement extends ContextStatement {

	private static final long serialVersionUID = 2711432019840626322L;

	public RdfContextStatement(Resource subject, IRI predicate, Value object, Resource context) 
	{
		super(subject, predicate, object, context);
	}
}
