package mariana.v1.rdf.model;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.PropertyGetter;

public class ValueOutputHandler implements ObjectOutputHandler {

	private PropertyGetter getter;
	
	private ObjectOutputHandler literalHandler  = new LiteralOutputHandler(src -> {
		return ((ValueOutputHandler)src).value;
	});
	
	private ObjectOutputHandler resourceHandler = new ResourceOutputHandler(src -> {
		return ((ValueOutputHandler)src).value;
	});

	private ObjectOutputHandler valueHandler;
	
	private int typeCode;
	private Value value;
	private int field;
	
	public ValueOutputHandler() {}
	
	public ValueOutputHandler(PropertyGetter getter) {
		this.getter = getter;
	}
	
	@Override
	public boolean writeTo(IOBuffer buf) 
	{
		if (field == 0) 
		{
			if (buf.uvlength(typeCode) <= buf.remaining()) 
			{
				buf.putUVLen(typeCode);
				field++;
			}
			else {
				return false;
			}
		}
		
		if (valueHandler.writeTo(buf)) 
		{
			return true;
		}
		
		return false;
	}

	@Override
	public void reset(Object source) 
	{
		field = 0;		
		value = (Value) getter.apply(source);
		
		if (value instanceof Literal) 
		{
			typeCode = Values.LITERAL_VALUE;
			valueHandler = literalHandler;
		}
		else {
			typeCode = Values.RESOURCE_VALUE;
			valueHandler = resourceHandler;
		}
		
		valueHandler.reset(this);
	}

	public void setPropertyGetter(PropertyGetter getter) 
	{
		this.getter = getter;
	}
}
