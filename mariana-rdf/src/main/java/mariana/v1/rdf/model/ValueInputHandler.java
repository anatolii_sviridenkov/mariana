package mariana.v1.rdf.model;

import org.openrdf.model.Value;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.PropertySetter;

public class ValueInputHandler implements ObjectInputHandler {

	private ResourceInputHandler resourceInputHandler = new ResourceInputHandler((tgt, value) -> {
		((ValueInputHandler)tgt).value = (Value) value;
	});
	
	private LiteralInputHandler literalInputHandler = new LiteralInputHandler((tgt, value) -> {
		((ValueInputHandler)tgt).value = (Value) value;
	});
	
	
	private PropertySetter setter;
	private Value value;
	private int field;
	private int typeCode;

	private Object target;

	public ValueInputHandler() {}
	
	public ValueInputHandler(PropertySetter setter) {
		this.setter = setter;
	}
	
	@Override
	public boolean readFrom(IOBuffer buf) 
	{
		if (field == 0) 
		{
			if (buf.hasUVLen()) 
			{
				typeCode = (int) buf.getUVLen();
				field++;
			}
			else {
				return false;
			}
		}
		
		if (typeCode == Values.RESOURCE_VALUE) 
		{
			if (!resourceInputHandler.readFrom(buf)) {
				return false;
			}
		}
		else if (typeCode == Values.LITERAL_VALUE) 
		{
			if (!literalInputHandler.readFrom(buf)) {
				return false;
			}
		}
		else {
			throw new RuntimeException("Unexpected value typeCode: " + typeCode);
		}
		
		setter.accept(target, value);
		
		return true;
	}

	@Override
	public void reset(Object target) 
	{
		this.target = target;
		this.field = 0;
		
		resourceInputHandler.reset(this);
		literalInputHandler.reset(this);
	}

	@Override
	public void finish() {}

	@Override
	public void setPropertySetter(PropertySetter setter) {
		this.setter = setter;
	}
}
