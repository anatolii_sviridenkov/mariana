package mariana.v1.rdf.model;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.serialization.InputHandler;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.PropertySetter;
import mariana.v1.io.serialization.UTF8StringInputHandler;

public class LiteralInputHandler implements ObjectInputHandler {

	protected InputHandler[] handlers;
	
	private int idx;

	protected PropertySetter setter;
	
	private Object target;
	
	private String label;
	private String iri;
	
	public LiteralInputHandler() {
		this(null);
	}
	
	public LiteralInputHandler(PropertySetter setter) 
	{		
		this.setter = setter;
		
		this.handlers = new InputHandler[] {
			new UTF8StringInputHandler((tgt, value) -> {
				((LiteralInputHandler)tgt).label = (String) value;
			}),
			new UTF8StringInputHandler((tgt, value) -> {
				((LiteralInputHandler)tgt).iri = (String) value;
			})
		};
	}
	
	@Override
	public boolean readFrom(IOBuffer buf) 
	{
		for (; idx < handlers.length; idx++) 
		{
			if (!handlers[idx].readFrom(buf)) {
				return false;
			}
		}
		
		setter.accept(target, new RdfLiteral(label, new RdfResource(iri)));
		
		return true;
	}
	
	protected void resetHandlers(Object target) 
	{
		idx = 0;

		for (int c = 0; c < handlers.length; c++) {
			handlers[c].reset(target);
		}
	}
	
	@Override
	public void reset(Object target) 
	{
		this.target = target; 
		
		resetHandlers(this);
	}
	
	public void setPropertySetter(PropertySetter setter) 
	{
		this.setter = setter;
	}
	
	
	public void finish() {}
}
