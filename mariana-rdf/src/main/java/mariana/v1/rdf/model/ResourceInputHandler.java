package mariana.v1.rdf.model;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.serialization.InputHandler;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.PrimitiveInputHandler;
import mariana.v1.io.serialization.PropertySetter;
import mariana.v1.io.serialization.UTF8StringInputHandler;

public class ResourceInputHandler implements ObjectInputHandler {

	protected InputHandler[] handlers;
	
	private int idx;

	protected PropertySetter setter;
	
	private Object target;
	
	private long typeCode;
	private String iri;
	
	public ResourceInputHandler() {
		this(null);
	}
	
	public ResourceInputHandler(PropertySetter setter) 
	{		
		this.setter = setter;
		
		this.handlers = new InputHandler[] {
			new PrimitiveInputHandler((tgt, buf) -> {
				if (buf.hasUVLen()) 
				{
					((ResourceInputHandler)tgt).typeCode = buf.getUVLen();
					return true;
				}
				return false;
			}),
			new UTF8StringInputHandler((tgt, value) -> {
				((ResourceInputHandler)tgt).iri = (String) value;
			})
		};
	}
	
	@Override
	public boolean readFrom(IOBuffer buf) 
	{
		for (; idx < handlers.length; idx++) {
			if (!handlers[idx].readFrom(buf)) {
				return false;
			}
		}
		
		if (typeCode == Values.BNODE_VALUE) {
			setter.accept(target, new RdfBNode(iri));
		}
		else if (typeCode == Values.RESOURCE_VALUE) {
			setter.accept(target, new RdfResource(iri));
		}
		else {
			throw new RuntimeException("Invalid resource typeCode: " + typeCode);
		}
		
		return true;
	}
	
	protected void resetHandlers(Object target) 
	{
		idx = 0;

		for (int c = 0; c < handlers.length; c++) {
			handlers[c].reset(this);
		}
	}
	
	@Override
	public void reset(Object target) 
	{
		this.target = target; 
		
		resetHandlers(this);
	}
	
	public void setPropertySetter(PropertySetter setter) 
	{
		this.setter = setter;
	}
	
	
	public void finish() {}
}
