package mariana.v1.rdf.model;

import org.openrdf.model.IRI;
import org.openrdf.model.Resource;
import org.openrdf.model.Value;
import org.openrdf.model.impl.SimpleStatement;

public class RdfStatement extends SimpleStatement {

	private static final long serialVersionUID = -11868314864409608L;

	public RdfStatement(Resource subject, IRI predicate, Value object) {
		super(subject, predicate, object);
	}
}
