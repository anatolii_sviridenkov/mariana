package mariana.v1.rdf;

import java.io.File;

import org.mariana.rdf.triples.IndexSpec;
import org.openrdf.model.Statement;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.QueryEvaluationException;

import info.aduna.iteration.CloseableIteration;
import mariana.v1.rdf.store.TripleStore;
import mariana.v1.rdf.store.TripleStoreConnection;
import mariana.v1.rdf.store.config.TripleStoreParameters;

public class TripleStoreQryEx {
	public static void main(String[] args) 
	{
		TripleStore store = new TripleStore();
		try {

			//		store.setConfig(
			//			new TripleStoreParameters.Builder()
			//				.withIndexes(IndexSpec.SPOC, IndexSpec.POSC, IndexSpec.OPSC)
			//				.withDictionaryShards(16)
			//				.withTriplesShards(8)
			//				.build()
			//		);

			File dataDir = new File("target/store");

			store.setDataDir(dataDir);
			store.initialize();

			try (TripleStoreConnection conn = (TripleStoreConnection) store.getConnection())  
			{
				conn.begin();

				long t1 = System.currentTimeMillis();
				int cnt = 0;


				try(CloseableIteration<? extends Statement, QueryEvaluationException> st = conn.getStatements(null, null, null)) 
				{
					while (st.hasNext()) 
					{
						//System.out.println(st.next());
						st.next();
						cnt++;
					}
				}

				System.out.println("Total rows: " + cnt+", time = " + (System.currentTimeMillis() - t1));


				conn.commit();
			}

//			long t2 = System.currentTimeMillis();
//			store.store();
//
//			System.out.println("Stored in " + (System.currentTimeMillis() - t2));

			//		store.getAllocator().dump("target/ts.memoria");

			store.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		finally {
			System.exit(1);
		}
	}
}
