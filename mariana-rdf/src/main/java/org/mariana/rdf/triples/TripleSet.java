package org.mariana.rdf.triples;

import java.util.UUID;

import org.mariana.rdf.triples.iterator.FullIterator;
import org.mariana.rdf.triples.iterator.RangeIterator;
import org.mariana.rdf.triples.iterator.TripleIterator;

import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.set.bytes.SetBIterator;
import mariana.v1.api.ctr.set.bytes.fx.SetFxB;
import mariana.v1.io.buffer.IOByteBuffer;
import mariana.v1.rdf.model.Values;

public class TripleSet implements AutoCloseable {

	private IOByteBuffer buffer = new IOByteBuffer(20);
	
	private SetFxB set;
	private String order;
	
	protected TripleSet(SetFxB set, String order) {
		this.set = set;
		this.order = order;
	}
	
	public TripleIterator<Triple> iterator() 
	{
		return new FullIterator(set.begin(), order);
	}
	
	public TripleIterator<Triple> find(TriplePattern pattern) 
	{
		buffer.rewind();
		pattern.asData().serialize(order, buffer);
		buffer.putInt((int)Values.CODE_NULL);
		buffer.flip();
		
		return new RangeIterator(set.findB(buffer.getDelegate()), order, pattern);
	}
	
	public boolean insert(Triple data) 
	{
		buffer.rewind();
		data.serialize(order, buffer);
		buffer.putInt((int)Values.CODE_NULL);
		buffer.flip();
		
		try(SetBIterator ii = set.find(buffer.getDelegate())) 
		{
			if (!ii.isFound(buffer.getDelegate()))
			{
				ii.insertB(buffer.getDelegate());
				return true;
			}
		}
		
		return false;
	}
	
	public boolean remove(Triple data) 
	{
		return false;
	}
	
	@Override
	public void close() {
		set.close();
	}
	
	public static TripleSet findOrCreate(Snapshot snp, UUID name, String order) {
		return new TripleSet(snp.findOrCreate(SetFxB.class, name), order);
	}
	
	public static TripleSet find(Snapshot snp, UUID name, String order) {
		return new TripleSet(snp.find(SetFxB.class, name), order);
	}

	public long size() {
		return set.size();
	}
}
