package org.mariana.rdf.triples.iterator;

import java.util.Iterator;

import mariana.v1.framework.pipeline.CloseableIterator;

public class CloseableIteratorProxy<T> implements CloseableIterator<T>{

	private Iterator<T> iter;
	
	private T current;

	public CloseableIteratorProxy(Iterator<T> iter) {
		this.iter = iter;
	}
	
	@Override
	public boolean hasNext()
	{
		return iter.hasNext();
	}

	@Override
	public T next()
	{
		current = iter.next();
		return current;
	}

	@Override
	public void close()
	{}

//	@Override
//	public T current()
//	{
//		return current;
//	}
}
