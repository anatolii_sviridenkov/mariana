package org.mariana.rdf.triples.iterator;

import java.nio.ByteBuffer;
import java.util.function.Function;

import mariana.v1.api.ctr.set.bytes.SetBIterator;




public class ScanIterator<T> extends TripleIterator<T> {

	private Function<ByteBuffer, T> fn;

	public ScanIterator(SetBIterator ii, String order, Function<ByteBuffer, T> fn) {
		super(ii, order);
		this.fn = fn;		
	}

	@Override
	protected T convert(ByteBuffer entry) {
		return fn.apply(entry);
	}

	@Override
	protected boolean acceptRange(T data) {
		return true;
	}

	@Override
	protected boolean acceptData(T data) {
		return true;
	}
}
