package org.mariana.rdf.triples;

import com.gs.collections.impl.set.mutable.primitive.LongHashSet;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.rdf.model.Values;

public class Triple {
	private long subject;
	private long property;
	private long object;
	private long context;
	
	public Triple() {}
	public Triple(long subject, long property, long object, long context) {
		this.subject = subject;
		this.property = property;
		this.object = object;
		this.context = context;
	}
	
	public long getSubject() {
		return subject;
	}
	
	public void setSubject(long subject) {
		this.subject = subject;
	}
	
	public long getProperty() {
		return property;
	}
	
	public void setProperty(long property) {
		this.property = property;
	}
	
	public long getObject() {
		return object;
	}
	
	public void setObject(long object) {
		this.object = object;
	}
	
	public long getContext() {
		return context;
	}
	
	public void setContext(long context) {
		this.context = context;
	}
	
	public void reset() 
	{
		subject  = Values.CODE_NULL;
		property = Values.CODE_NULL;
		object   = Values.CODE_NULL;
		context  = Values.CODE_NULL;
	}
	
	public void serialize(String order, IOBuffer bb) 
	{
		int c;
		for (c = 0; c < order.length(); c++) 
		{
			switch (order.charAt(c)) 
			{
				case Values.SUBJECT_CHAR  : bb.putInt((int) subject);  break;
				case Values.PROPERTY_CHAR : bb.putInt((int) property); break;
				case Values.OBJECT_CHAR   : bb.putInt((int) object);   break;
				case Values.CONTEXT_CHAR  : bb.putInt((int) context);  break;
			}
		}
		
		for (; c < Values.PATTERN_LENGTH; c++) 
		{
			bb.putInt((int)Values.CODE_NULL);
		}
	}
	
	public static Triple readFrom(String order, IOBuffer bb, Triple data) 
	{
		data.subject  = Values.CODE_NULL;
		data.property = Values.CODE_NULL;
		data.object   = Values.CODE_NULL;
		data.context  = Values.CODE_NULL;
		
		for (int c = 0; c < order.length(); c++)
		{
			switch (order.charAt(c)) 
			{
				case Values.SUBJECT_CHAR  : data.subject  = bb.getInt(); break;
				case Values.PROPERTY_CHAR : data.property = bb.getInt(); break;
				case Values.OBJECT_CHAR   : data.object   = bb.getInt(); break;
				case Values.CONTEXT_CHAR  : data.context  = bb.getInt(); break;
			}
		}
		
		return data;
	}
	
	public static Triple readFrom(String order, IOBuffer bb) {
		return readFrom(order, bb, new Triple());
	}
	
	
	public boolean matches(TriplePattern pattern) 
	{
		if (pattern.getSubject() != Values.CODE_NULL  && subject != pattern.getSubject()) {
			return false;
		}
		else if (pattern.getProperty() != Values.CODE_NULL && property != pattern.getProperty()) {
			return false;
		}
		else if (pattern.getObject() != Values.CODE_NULL && object != pattern.getObject()) {
			return false;
		}
		else {
			if (pattern.getContext() != null && !pattern.getContext().contains(context))
			return false;
		}
		
		return true;
	}
	
	public String toString() {
		return "StatementData[" + subject + ", " + property + ", " + object + ", " + context + "]";
	}
	
	@Override
	public int hashCode() {		
		return Long.hashCode(subject ^ property ^ object ^ context);
	}
	
	public boolean equals(Object other) 
	{
		if (other == this) 
		{
			return true;
		}
		else if (other instanceof Triple) 
		{
			Triple t = (Triple) other;			
			return subject == t.subject && property == t.property && object == t.object && context == t.context;
		}
		
		return false;		
	}
	
	public void addIdentifiers(LongHashSet set) 
	{
		set.add(subject);
		set.add(property);
		set.add(object);
		
		if (context != Values.CODE_NULL) 
		{
			set.add(context);
		}
	}
}
