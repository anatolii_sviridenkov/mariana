package org.mariana.rdf.triples.iterator;

import org.openrdf.sail.NotifyingSailConnection;

import mariana.v1.framework.pipeline.CloseableIterator;

public class ConnectedClosableIterator<T> implements CloseableIterator<T> {

	private final CloseableIterator<T> delegate;
	private final NotifyingSailConnection connection;
	private boolean commitOnclode;
	
	public ConnectedClosableIterator(NotifyingSailConnection connection, CloseableIterator<T> delegate, boolean commitOnclode) 
	{
		this.connection = connection;
		this.delegate = delegate;
		this.commitOnclode = commitOnclode;
	}
	
	public ConnectedClosableIterator(NotifyingSailConnection connection, CloseableIterator<T> delegate) 
	{
		this.connection = connection;
		this.delegate = delegate;
		this.commitOnclode = true;
	}
	
	@Override
	public boolean hasNext()
	{
		return delegate.hasNext();
	}

	@Override
	public T next()
	{
		return delegate.next();
	}

	@Override
	public void close()
	{
		delegate.close();
		
		if (commitOnclode) 
		{
			connection.commit();
		}
		
		connection.close();
	}

//	@Override
//	public T current()
//	{
//		return delegate.current();
//	}

	public NotifyingSailConnection getConnection()
	{
		return connection;
	}
}
