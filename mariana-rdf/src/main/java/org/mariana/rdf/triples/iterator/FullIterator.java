package org.mariana.rdf.triples.iterator;


import java.nio.ByteBuffer;

import org.mariana.rdf.triples.Triple;
import org.mariana.rdf.triples.TriplePattern;

import mariana.v1.api.ctr.set.bytes.SetBIterator;
import mariana.v1.io.buffer.IOByteBuffer;


public class FullIterator extends TripleIterator<Triple> {

	private final TriplePattern pattern;
	
	public FullIterator(SetBIterator iterator, String orderSpec, TriplePattern pattern) {
		super(iterator, orderSpec);
		this.pattern = pattern;
	}
	
	public FullIterator(SetBIterator iterator, String orderSpec) {
		this(iterator, orderSpec, new TriplePattern());
	}
	
	
	@Override
	protected boolean acceptRange(Triple data) {
		return true;
	}

	@Override
	protected boolean acceptData(Triple data) {
		return data.matches(pattern);
	}

	@Override
	protected Triple convert(ByteBuffer entry) {
		return Triple.readFrom(order, new IOByteBuffer(entry));
	}
}