package org.mariana.rdf.triples;

import com.gs.collections.api.set.primitive.LongSet;

import mariana.v1.rdf.model.Values;

public class TriplePattern {
	
	private long subject  = Values.CODE_NULL;
	private long property = Values.CODE_NULL;
	private long object   = Values.CODE_NULL;
	private LongSet context;

	public TriplePattern() 
	{
	}
	
	public TriplePattern(long subject, long property, long object, LongSet contexts) {
		this.subject  = subject;
		this.property = property;
		this.object   = object;
		this.context  = contexts;
	}
		
	public long getSubject() {
		return subject;
	}
	
	public void setSubject(long subject) {
		this.subject = subject;
	}
	
	public long getProperty() {
		return property;
	}
	
	public void setProperty(long property) {
		this.property = property;
	}
	
	public long getObject() {
		return object;
	}
	
	public void setObject(long object) {
		this.object = object;
	}
	
	public LongSet getContext() {
		return context;
	}
	
	public void setContext(LongSet context) {
		this.context = context;
	}
	
	public boolean matchesDeclaredContext(long contextId) 
	{
		if (context != null && !context.contains(contextId)) 
		{
			return false;
		}
		
		return true;
	}
	
	public Triple asData() {
		return new Triple(subject, property, object, Values.CODE_NULL);
	}
	
	@Override
	public String toString() {
		return "StatementPatter[" + subject + ", " + property + ", " + objectString(object) + ", " + context + "]";
	}
	
	private String objectString(Object object) 
	{
		if (object != null) 
		{
			return object.toString();
		}
		
		return "null";
	}
	
	public String computeRangeSpec(String orderSpec) {
		return computeRangeSpec(subject, property, object, context, orderSpec);
	}
	
	public static String computeRangeSpec(long subjectId, long propertyId, long objectId, LongSet contexts, String orderSpec) 
	{
		StringBuilder sb = new StringBuilder();
		
		for (int c = 0; c < orderSpec.length(); c++) 
		{
			char ch = orderSpec.charAt(c);
			switch (ch) {
				case Values.SUBJECT_CHAR:  if (subjectId != Values.CODE_NULL) sb.append(Values.SUBJECT_CHAR);   else return sb.toString(); break;
				case Values.PROPERTY_CHAR: if (propertyId != Values.CODE_NULL) sb.append(Values.PROPERTY_CHAR); else return sb.toString(); break;
				case Values.OBJECT_CHAR:   if (objectId != Values.CODE_NULL) sb.append(Values.OBJECT_CHAR);     else return sb.toString(); break;
				case Values.CONTEXT_CHAR:  if (contexts != null) sb.append(Values.CONTEXT_CHAR); 				else return sb.toString(); break;
				default: throw new RuntimeException("Unknown letter in orderSpec: "+orderSpec);
			}
		}
		
		return sb.toString();
	}
}
