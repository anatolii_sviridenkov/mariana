package org.mariana.rdf.triples.iterator;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.NoSuchElementException;

import mariana.v1.api.ctr.set.bytes.SetBIterator;
import mariana.v1.framework.pipeline.CloseableIterator;

public abstract class TripleIterator<T> implements CloseableIterator<T> {

	private long cnt;	
	private T data;
	
	protected SetBIterator setIterator;
	protected String order;
	
	public TripleIterator(SetBIterator setIterator, String order)
	{
		this.setIterator = setIterator;
		this.order = order;
	}
	
	public long idx() 
	{
		return setIterator.pos();
	}

	@Override
	public boolean hasNext() 
	{
		prefetchFirst();
		return data != null;
	}
	
	public T current() 
	{
		if (data == null) 
		{
			throw new NoSuchElementException();
		}
		
		return data;
	}

	@Override
	public T next() 
	{
		prefetchFirst();
		
		if (data == null) 
		{
			throw new NoSuchElementException();
		}

		try {
			return data;
		}
		finally {
			findNext();
		}
	}
	
	private void findFirst() 
	{
		ByteBuffer entry = firstEntry();
		
		if (entry != null) 
		{
			T data = convert(entry);
			if (acceptRange(data))
			{
				if (acceptData(data)) {
					this.data = data;
				}
				else {
					findNext();
				}
			}
		}
	}
	
	private void findNext() 
	{
		this.data = null;
		
		while (true) 
		{
			ByteBuffer entry = nextEntry();
			if (entry != null) 
			{
				T data = convert(entry);
				if (acceptRange(data)) 
				{
					if (acceptData(data)) 
					{
						this.data = data;
						break;
					}
				}
				else {
					break;
				}
			}
			else {
				break;
			}
		}
	}
	
	private boolean prefetchFirst() 
	{
		if (cnt == 0) 
		{
			cnt++;
			findFirst();
			return true;
		}
		return false;
	}

	@Override
	public void close() 
	{
		setIterator.close();
	}
	
	protected ByteBuffer firstEntry()
	{
		if (!setIterator.isEnd()) 
		{
			return ByteBuffer.wrap(setIterator.key()).order(ByteOrder.LITTLE_ENDIAN);
		}
		
		return null;
	}
	
	protected ByteBuffer nextEntry()
	{
		setIterator.next();
		if (!setIterator.isEnd()) 
		{
			return ByteBuffer.wrap(setIterator.key()).order(ByteOrder.LITTLE_ENDIAN);
		}
		
		return null;
	}
	
	protected abstract boolean acceptRange(T data);	
	protected abstract boolean acceptData(T data);
	
	protected abstract T convert(ByteBuffer entry);
	
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}