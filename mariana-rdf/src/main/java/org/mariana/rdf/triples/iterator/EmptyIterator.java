package org.mariana.rdf.triples.iterator;

import java.util.NoSuchElementException;

import mariana.v1.framework.pipeline.CloseableIterator;

public class EmptyIterator<T> implements CloseableIterator<T> {

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public T next() {
		throw new NoSuchElementException();
	}

	@Override
	public void close() {}

//	@Override
//	public T current() {
//		throw new NoSuchElementException();
//	}
}
