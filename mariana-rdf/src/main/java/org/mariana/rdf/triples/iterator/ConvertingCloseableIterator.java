package org.mariana.rdf.triples.iterator;

import mariana.v1.framework.pipeline.CloseableIterator;

public abstract class ConvertingCloseableIterator<Src, Dst> implements CloseableIterator<Dst> {
	
	private CloseableIterator<Src> delegate;
	
	public ConvertingCloseableIterator(CloseableIterator<Src> delegate) {
		this.delegate = delegate;
	}
	
	public void close() {
		delegate.close();
	}
		
//	public Dst current() {
//		return convert(delegate.current());
//	}

	@Override
	public boolean hasNext()
	{
		return delegate.hasNext();
	}

	@Override
	public Dst next()
	{
		return convert(delegate.next());
	}
	
	protected abstract Dst convert(Src src);
}
