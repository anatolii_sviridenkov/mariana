package org.mariana.rdf.triples.iterator;

import java.nio.ByteBuffer;

import org.mariana.rdf.triples.Triple;
import org.mariana.rdf.triples.TriplePattern;

import mariana.v1.api.ctr.set.bytes.SetBIterator;
import mariana.v1.io.buffer.IOByteBuffer;
import mariana.v1.rdf.model.Values;

public class RangeIterator extends TripleIterator<Triple> {

	private final TriplePattern pattern;
	private final String rangeSpec;
	
	public RangeIterator(
			SetBIterator iterator, 
			String orderSpec, 			
			TriplePattern pattern 
			
	) {
		super(iterator, orderSpec);
		
		this.pattern   = pattern;
		this.rangeSpec = pattern.computeRangeSpec(orderSpec);
	}

	@Override
	protected Triple convert(ByteBuffer entry) {		
		return Triple.readFrom(order, new IOByteBuffer(entry));
	}
	
	@Override
	protected boolean acceptRange(Triple data) 
	{
		for (int c = 0; c < rangeSpec.length(); c++) 
		{
			char ch = rangeSpec.charAt(c);
			switch (ch) {
				case Values.SUBJECT_CHAR  : if (data.getSubject() != pattern.getSubject()) return false; break;
				case Values.PROPERTY_CHAR : if (data.getProperty() != pattern.getProperty()) return false; break;
				case Values.OBJECT_CHAR   : if (data.getObject() != pattern.getObject()) return false; break;
				case Values.CONTEXT_CHAR  : if (!pattern.matchesDeclaredContext(data.getContext())) return false; break;
			}
		}
		
		return true;
	}

	@Override
	protected boolean acceptData(Triple data) {
		return data.matches(pattern);
	}
}