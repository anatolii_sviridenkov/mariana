package org.mariana.rdf.triples.iterator;



import info.aduna.iteration.CloseableIteration;
import info.aduna.iteration.IteratorIteration;
import mariana.v1.framework.pipeline.CloseableIterator;

public class IteratorCloseableIteration<E,X extends Exception> extends IteratorIteration<E,X> implements CloseableIteration<E,X> {

	private CloseableIterator<? extends E> iter;

	public IteratorCloseableIteration(CloseableIterator<? extends E> iter) {
		super(iter);
		this.iter = iter;		
	}

	@Override
	public void close() throws X {
		iter.close();
	}
}
