package org.mariana.rdf.triples.iterator;

import java.util.NoSuchElementException;

import mariana.v1.framework.pipeline.CloseableIterator;

public class ChainedCloseableIterator<T> implements CloseableIterator<T> {

	private final CloseableIterator<T>[] iterators;
	private int current;

	private static final CloseableIterator<?>[] EMPTY = new CloseableIterator<?>[0];
	
	@SafeVarargs
	@SuppressWarnings("unchecked")
	public ChainedCloseableIterator(CloseableIterator<T>... iterators) 
	{
		this.iterators = iterators != null ? iterators : (CloseableIterator<T>[]) EMPTY;		
	}
	
	@Override
	public boolean hasNext() 
	{
		for (int c = current; c < iterators.length; c++) 
		{
			if (iterators[c].hasNext()) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public T next() 
	{
		if (current < iterators.length) 
		{
			T value = iterators[current].next();

			while (current < iterators.length && !iterators[current].hasNext()) 
			{
				current++;
			}

			return value;
		}
		else {
			throw new NoSuchElementException();
		}
	}

	@Override
	public void close() 
	{
		for (CloseableIterator<T> iter: iterators) 
		{
			iter.close();
		}
	}
	
	@Override
	public void remove() 
	{
		if (current < iterators.length) 
		{
			iterators[current].remove();

			while (current < iterators.length && !iterators[current].hasNext()) 
			{
				current++;
			}
		}
		else {
			throw new NoSuchElementException();
		}
	}

//	@Override
//	public T current() {
//		return iterators[current].current();
//	}
}
