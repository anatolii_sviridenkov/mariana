
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.inmem.ctr.map;

import mariana.bridge.Entry;
import mariana.bridge.Parent;
import mariana.v1.api.Iterator;
import mariana.v1.api.ctr.map.Map;
import mariana.v1.api.ctr.map.lng.bytes.MapLB;
import mariana.v1.api.ctr.map.lng.bytes.MapLBIterator;
import mariana.v1.inmem.alloc.InMemoryTypeMap;

@Entry(javaType = Map.class, pairType = InMemMapLB.class)
@Entry(javaType = MapLB.class, pairType = InMemMapLB.class)
@Entry(javaType = Iterator.class, pairType = InMemMapLB.IteratorImpl.class)
@Entry(javaType = MapLBIterator.class, pairType = InMemMapLB.IteratorImpl.class)

@Parent(InMemoryTypeMap.class)
public interface InMemMapLBTypeMap {

}
