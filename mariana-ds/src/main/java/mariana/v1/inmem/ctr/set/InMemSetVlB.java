
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.inmem.ctr.set;

import java.nio.ByteBuffer;

import mariana.bridge.CppType;
import mariana.bridge.Ctr;
import mariana.bridge.DirectBuffer;
import mariana.bridge.ImportedMethod;
import mariana.bridge.Iter;
import mariana.v1.api.ctr.set.bytes.vl.SetVlB;
import mariana.v1.api.ctr.set.bytes.vl.SetVlBIterator;
import mariana.v1.api.CtrFactories;


@Ctr(name = "memoria::v1::Set<Bytes>", defaults = InMemSetVlBTypeMap.class, factory = CtrFactories.SET)
public abstract class InMemSetVlB implements SetVlB {

	@Iter
	public static abstract class IteratorImpl implements SetVlBIterator {
	
		@ImportedMethod(name = "is_found")
		abstract public boolean isFoundD(@CppType("Bytes") @DirectBuffer ByteBuffer key);
		
		@ImportedMethod(name = "is_found")
		abstract public boolean isFoundB(@CppType("Bytes") ByteBuffer key);
		
		
		@ImportedMethod(name = "insert")
		abstract public void insertB(@CppType("Bytes") ByteBuffer key);
		@ImportedMethod(name = "insert")
		abstract public void insertD(@CppType("Bytes") @DirectBuffer ByteBuffer key);
		
	}
	
	@ImportedMethod(name = "find")
	public abstract SetVlBIterator findB(@CppType("Bytes") ByteBuffer key);
	
	@ImportedMethod(name = "find")
	public abstract SetVlBIterator findD(@CppType("Bytes") @DirectBuffer ByteBuffer key);
	
	@ImportedMethod
	public abstract boolean remove(@CppType("Bytes") ByteBuffer key);	
	
	@ImportedMethod(name = "insert_key")
	public abstract boolean insert(@CppType("Bytes") ByteBuffer key);
	
	@ImportedMethod
	public abstract boolean contains(@CppType("Bytes") ByteBuffer key);
}
