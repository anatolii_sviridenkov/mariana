
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.inmem.ctr.set;

import mariana.bridge.Entry;
import mariana.bridge.Parent;
import mariana.v1.api.Iterator;
import mariana.v1.api.ctr.set.Set;
import mariana.v1.api.ctr.set.lng.SetL;
import mariana.v1.api.ctr.set.lng.SetLIterator;
import mariana.v1.inmem.alloc.InMemoryTypeMap;

@Entry(javaType = Set.class, pairType = InMemSetL.class)
@Entry(javaType = SetL.class, pairType = InMemSetL.class)
@Entry(javaType = Iterator.class, pairType = InMemSetL.IteratorImpl.class)
@Entry(javaType = SetLIterator.class, pairType = InMemSetL.IteratorImpl.class)

@Parent(InMemoryTypeMap.class)
public interface InMemSetLTypeMap {

}
