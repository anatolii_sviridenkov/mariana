
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.inmem.ctr.multimap;

import mariana.bridge.Ctr;
import mariana.bridge.Iter;
import mariana.v1.api.CtrFactories;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLB;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLBIterator;



@Ctr(name = "memoria::v1::Map<BigInt, Vector<UByte>>", defaults = InMemMultimapLBTypeMap.class, factory = CtrFactories.MULTIMAP)
public abstract class InMemMultimapLB implements MultimapLB {
	
	@Iter
	public static abstract class IteratorImpl implements MultimapLBIterator {
		
	}
}
