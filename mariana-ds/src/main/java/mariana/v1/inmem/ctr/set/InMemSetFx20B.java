
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.inmem.ctr.set;

import java.nio.ByteBuffer;

import mariana.bridge.CppType;
import mariana.bridge.Ctr;
import mariana.bridge.DirectBuffer;
import mariana.bridge.ImportedMethod;
import mariana.bridge.Iter;
import mariana.v1.api.ctr.set.bytes.fx.SetFxB;
import mariana.v1.api.ctr.set.bytes.fx.SetFxBIterator;
import mariana.v1.api.CtrFactories;



@Ctr(name = "memoria::v1::Set<FixedArray<20>>", defaults = InMemSetFx20BTypeMap.class, factory = CtrFactories.SET)
public abstract class InMemSetFx20B implements SetFxB {

	@Iter
	public static abstract class IteratorImpl implements SetFxBIterator {
	
		@ImportedMethod(name = "is_found")
		abstract public boolean isFoundD(@CppType("FixedArray<20>") @DirectBuffer ByteBuffer key);
		
		@ImportedMethod(name = "is_found")
		abstract public boolean isFoundB(@CppType("FixedArray<20>") ByteBuffer key);
		
		
		@ImportedMethod(name = "insert")
		abstract public void insertB(@CppType("FixedArray<20>") ByteBuffer key);
		@ImportedMethod(name = "insert")
		abstract public void insertD(@CppType("FixedArray<20>") @DirectBuffer ByteBuffer key);
	}
	
	@ImportedMethod(name = "find")
	public abstract SetFxBIterator findB(@CppType("FixedArray<20>") ByteBuffer key);
	
	@ImportedMethod(name = "find")
	public abstract SetFxBIterator findD(@CppType("FixedArray<20>") @DirectBuffer ByteBuffer key);
	
	@ImportedMethod
	public abstract boolean remove(@CppType("FixedArray<20>") ByteBuffer key);	
	
	@ImportedMethod(name = "insert_key")
	public abstract boolean insert(@CppType("FixedArray<20>") ByteBuffer key);
	
	@ImportedMethod
	public abstract boolean contains(@CppType("FixedArray<20>") ByteBuffer key);
}