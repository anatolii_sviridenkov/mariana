
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.inmem.alloc;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import mariana.bridge.CxxObject;
import mariana.bridge.Factory;
import mariana.bridge.Import;
import mariana.bridge.ImportedMethod;
import mariana.bridge.tools.InvokerTool;
import mariana.v1.api.alloc.Allocator;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.io.AbstractInputBufferStream;
import mariana.v1.io.AbstractOutputBufferStream;

@Import(cppType = "mariana::v1::InMemAllocatorPtr", defaults = InMemoryTypeMap.class, bufferSize = 4096)
public abstract class InMemoryAllocator extends Allocator implements CxxObject {
		
	private ByteBuffer buffer_ = ByteBuffer.allocateDirect(128).order(ByteOrder.LITTLE_ENDIAN);

	private static final MethodHandles.Lookup lookup = MethodHandles.lookup();
	private static MethodHandle creatorHandle 	= InvokerTool.getPairHandle(lookup, InMemoryAllocator.class, "createPair", long.class);
	private static MethodHandle finderHandle 	= InvokerTool.getPairHandle(lookup, InMemoryAllocator.class, "findOrCreatePair", long.class);
	
	
	@Factory
	public static InMemoryAllocator create() {
		long hh = createPair();
		return newInstance(hh);
	}

	
	static InMemoryAllocator newInstance(long hh) {
		try {
			return (InMemoryAllocator) creatorHandle.invokeExact(hh);
		}
		catch (RuntimeException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}
	
	private static InMemoryAllocator getOrCreatePair(long hh) 
	{
		try {
			return (InMemoryAllocator) finderHandle.invokeExact(hh);
		} 
		catch (RuntimeException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}

	
	@ImportedMethod
	abstract public void store(AbstractOutputBufferStream out);
	
	private static Snapshot getOrCreateSnapshotPair(long hh)
	{
		return InMemorySnapshot.getOrCreatePeer(hh);
	}
	
	public static InMemoryAllocator load(AbstractInputBufferStream in) 
	{
		return newInstance(loadStream(in.getHandle()));
	}
	
	public static InMemoryAllocator load(String file) 
	{
		long hh = loadFile(file);
		return newInstance(hh);
	}
	
	public void store(String file) {
		store(getHandle(), file);
	}
	
	
	public void dump(String destination) {
		dump(getHandle(), destination);
	}
	

	private static native long createPair();
	
	private static native long loadFile(String file);
	private static native long loadStream(long streamHandle);
	
	private static native void store(long handle, String file);
	private static native void dump(long handle, String destination);
	
}
