
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.inmem.alloc;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.UUID;

import mariana.bridge.Import;
import mariana.bridge.tools.InvokerTool;
import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.tools.BufferTools;
import mariana.v1.tools.TypeMapping;

@Import(cppType = "mariana::v1::InMemSnapshotPtr", defaults = InMemoryTypeMap.class)
public abstract class InMemorySnapshot implements Snapshot {

	private static final MethodHandles.Lookup lookup = MethodHandles.lookup();

	private static MethodHandle creatorHandle 	= InvokerTool.getPairHandle(lookup, InMemorySnapshot.class, "createPair", long.class);
	private static MethodHandle finderHandle 	= InvokerTool.getPairHandle(lookup, InMemorySnapshot.class, "findOrCreatePair", long.class);
	
	private static final TypeMapping typeMapping = Mariana.getTypeMapping(InMemoryTypeMap.class).get(); 
	
	private ByteBuffer buffer_ = ByteBuffer.allocateDirect(128).order(ByteOrder.LITTLE_ENDIAN);

	
	static InMemorySnapshot newInstance(long hh) {
		try {
			return (InMemorySnapshot) creatorHandle.invokeExact(hh);
		}
		catch (RuntimeException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}
	
	
	
	static InMemorySnapshot getOrCreatePeer(long hh) 
	{
		try {
			return (InMemorySnapshot) finderHandle.invokeExact(hh);
		} 
		catch (RuntimeException e) {
			throw e;
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private <Ctr> Class<Ctr> getCtrImplClass(Class<Ctr> clazz) 
	{
		if (clazz.getAnnotation(mariana.bridge.Ctr.class) != null) 
		{
			return clazz;
		}
		else {
			return (Class<Ctr>) typeMapping.locate(clazz);
		}
	}
	


	@Override
	public <Ctr> Ctr create(Class<Ctr> clazz, UUID name) 
	{
		buffer_.rewind();
		
		Class<Ctr> targetClass = getCtrImplClass(clazz);
		
		BufferTools.write(buffer_, name);
		
		String nativeTypeName = InvokerTool.getNativeType(targetClass);
		return create(getHandle(), nativeTypeName, buffer_, buffer_.limit());
	}

	
	@Override
	public <Ctr> Ctr create(Class<Ctr> clazz) 
	{
		Class<Ctr> targetClass = getCtrImplClass(clazz);
		
		String nativeTypeName = InvokerTool.getNativeType(targetClass);
		return create(getHandle(), nativeTypeName);
	}

	@Override
	public <Ctr> Ctr findOrCreate(Class<Ctr> clazz, UUID ctrName) 
	{
		Class<Ctr> targetClass = getCtrImplClass(clazz);
		
		buffer_.rewind();
		
		BufferTools.write(buffer_, ctrName);
		
		String nativeTypeName = InvokerTool.getNativeType(targetClass);
		
		return findOrCreate(getHandle(), nativeTypeName, buffer_, buffer_.limit());
	}

	@Override
	public <Ctr> Ctr find(Class<Ctr> clazz, UUID name)
	{		
		buffer_.rewind();
		
		Class<Ctr> targetClass = getCtrImplClass(clazz);
		
		BufferTools.write(buffer_, name);
		
		String nativeTypeName = InvokerTool.getNativeType(targetClass);
		
		return find(getHandle(), nativeTypeName, buffer_, buffer_.limit());
	}
	
	
	public void dump(String destination) {
		dump(getHandle(), destination);
	}
	


	
	
	
	// Native stuff
	
	private static native <Ctr> Ctr create(long handle, String clazz, ByteBuffer buffer, int limit);
	
	private static native <Ctr> Ctr create(long handle, String clazz);
		
	private static native <Ctr> Ctr findOrCreate(long handle, String clazz, ByteBuffer buffer, int limit);

	private static native <Ctr> Ctr find(long handle, String clazz, ByteBuffer buffer, int limit);
	
	private static native void dump(long handle, String destination); 
}