
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.tools;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Charsets;

import mariana.v1.api.alloc.SnapshotDescriptor;
import mariana.v1.tools.codecs.UInt63Codec;

public class BufferTools {
	
	public static void write(ByteBuffer buffer, UUID uuid) 
	{
		buffer.putLong(Long.reverseBytes(uuid.getMostSignificantBits()));
		buffer.putLong(Long.reverseBytes(uuid.getLeastSignificantBits()));
	}
	
	public static UUID read(ByteBuffer buffer) 
	{
		return new UUID(Long.reverseBytes(buffer.getLong()), Long.reverseBytes(buffer.getLong()));
	}
	
	public static UUID readUUID(ByteBuffer buffer) 
	{
		return new UUID(Long.reverseBytes(buffer.getLong()), Long.reverseBytes(buffer.getLong()));
	}
	
	
	public static void write(ByteBuffer buffer, SnapshotDescriptor descr) 
	{
		write(buffer, descr.getParentId());
		write(buffer, descr.getSnapshotId());
		buffer.putInt(descr.getStatus());
		
		UInt63Codec codec = new UInt63Codec();
		
		if (descr.getDescription() != null) 
		{			
			byte[] data = descr.getDescription().getBytes(Charsets.UTF_8);
			codec.encode(buffer, data.length);
			buffer.put(data);
		}
		else {
			codec.encode(buffer, 0);
		}
		
		buffer.putInt(descr.getChildren().size());
		
		for (UUID childId: descr.getChildren()) 
		{
			write(buffer, childId);
		}
	}
	
	public static SnapshotDescriptor readSnapshotDescriptor(ByteBuffer buffer) 
	{
		UUID parentId = readUUID(buffer);
		UUID snapshotId = readUUID(buffer);
		int status = buffer.getInt();
		
		UInt63Codec codec = new UInt63Codec();
		
		codec.decode(buffer);
		
		int strDataLen = codec.getIntValue();
		byte[] data = new byte[strDataLen];
		buffer.get(data);
		
		String description = new String(data, Charsets.UTF_8);
		
		int length = buffer.getInt();
		
		List<UUID> children = new ArrayList<>();
		
		for (int c = 0; c < length; c++) {
			children.add(readUUID(buffer));
		}
		
		return new SnapshotDescriptor(parentId, snapshotId, Collections.unmodifiableList(children), description, status);
	}
	
	public static String toString(byte[] buf) {
		return toString(ByteBuffer.wrap(buf));
	}
	
	
	public static String toString(ByteBuffer buf) 
	{
		StringBuilder sb = new StringBuilder();
		
		for (int c = 0; c < buf.limit(); c++) 
		{
			int i = buf.get(c) & 0xFF;
			if (i < 16) 
			{
				sb.append("0");
			}
			
			sb.append(Integer.toHexString(i));
		}
		
		return sb.toString();
	}
	
	public static long getLong(ByteBuffer buffer)
	{
		long v = buffer.get() & 0xFFl;

		v |= (buffer.get() & 0xFFl) << 8;
		v |= (buffer.get() & 0xFFl) << 16;
		v |= (buffer.get() & 0xFFl) << 24;
		v |= (buffer.get() & 0xFFl) << 32;
		v |= (buffer.get() & 0xFFl) << 40;
		v |= (buffer.get() & 0xFFl) << 48;
		v |= (buffer.get() & 0xFFl) << 56;
		
		return v;
	}

	public static void put(ByteBuffer buffer, long v)
	{
		buffer.put((byte)(v & 0xFF));
		buffer.put((byte)((v >> 8) & 0xFF));
		buffer.put((byte)((v >> 16) & 0xFF));
		buffer.put((byte)((v >> 24) & 0xFF));
		buffer.put((byte)((v >> 32) & 0xFF));
		buffer.put((byte)((v >> 40) & 0xFF));
		buffer.put((byte)((v >> 48) & 0xFF));
		buffer.put((byte)((v >> 56) & 0xFF));
	}
}
