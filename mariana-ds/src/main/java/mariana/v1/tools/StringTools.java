package mariana.v1.tools;

public class StringTools {
	public static String capitalizeFirst(String text) 
	{
		return text.substring(0, 1).toUpperCase() + text.substring(1);
	}
}
