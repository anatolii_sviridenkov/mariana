
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.tools.codecs;

import java.nio.ByteBuffer;

public class UInt63Codec {
	
	public static final int LENGTH_MAX 		= 9;
	public static final int LENGTH_MIN 		= 1;
	public static final int LENGTH_HEADER 	= 1;
	public static final long NULL			= -1;
	
	
	private static final int UpperBound 	= 248;
	
	
	
	private long value;
	
	public long getValue() {
		return value;
	}
	
	public int getIntValue() {
		return (int)value;
	}

	public ValuePtr describe(ByteBuffer buffer, int idx)
	{
		return new ValuePtr(buffer, idx, length(buffer, idx));
	}
	
	public int length(ByteBuffer buffer) {
		return length(buffer, buffer.position());
	}
	
	public boolean isNull(ByteBuffer buffer, int idx) 
	{
		return buffer.get(idx) == 0;
	}
	
	public boolean isNull(ByteBuffer buffer) 
	{
		return buffer.get(buffer.position()) == 0;
	}

	public int length(ByteBuffer buffer, int idx) 
	{
		int head = buffer.get(idx) & 0xFF;
		if (head < UpperBound)
		{
			return 1;
		}
		else {
			return head - UpperBound + 1;
		}
	}

	public int length(long value)
	{
        if (value < UpperBound - 1)
        {
            return 1;
        }
        else {
            return 1 + byte_length(value);
        }
	}
	
	public int decode(ByteBuffer buffer) 
	{
		int p = buffer.position();
		int len = decode(buffer, buffer.position());
		
		buffer.position(p + len);
		
		return len;
	}

	public int decode(ByteBuffer buffer, int idx)
	{
        int header = buffer.get(idx) & 0xFF;

        if (header < UpperBound)
        {
            value = header - 1;
            return 1;
        }
        else
        {
            value = 0;
            int len = header - UpperBound;

            value = deserialize(buffer, idx + 1, len);

            return len + 1;
        }
	}

	public int encode(ByteBuffer buffer, ValuePtr value, int idx) 
	{
		copy(value.getBuffer(), 0, buffer, idx, value.getLength());
		return value.getLength();
	}

	

	public int encode(ByteBuffer buffer, long value) 
	{
		int p = buffer.position();
		
		int len = encode(buffer, value, p);
		
		buffer.position(p + len);
		
		return len;
	}
	
	public int encode(ByteBuffer buffer, long value, int idx)
	{
        if (value < UpperBound - 1)
        {
            buffer.put(idx, (byte)(value + 1));
            return 1;
        }
        else {
            int len = serialize(buffer, value, idx + 1);

            buffer.put(idx, (byte)(len + UpperBound));

            return 1 + len;
        }

	}

	public void copy(ByteBuffer src, int from, ByteBuffer tgt, int to, int size) 
	{
		for (int c = 0; c < size; c++)
		{
			tgt.put(c + to, src.get(c + from));
		}
	}

	private int serialize(ByteBuffer buffer, long value, int idx)
	{
		int len = bytes(value);

		for (int c = 0; c < len; c++)
		{
			buffer.put(idx++, (byte)(value >> (c << 3)));
		}

		return len;
	}

	private long deserialize(ByteBuffer buffer, int idx, int len)
	{
		long value = 0;
		for (int c = 0; c < len; c++)
		{
			value |= ((long)(buffer.get(idx++) & 0xFF)) << (c << 3);
		}
		
		return value;
	}


	public static int msb(long digits)
	{
		return digits != 0 ? 63 - Long.numberOfLeadingZeros(digits) : 0;
	}
	
	public static int bytes(long digits)
	{
		int v = msb(digits) + 1;
		return (v >> 3) + ((v & 0x7) != 0 ? 1 : 0);
	}

	public static int byte_length(long data)
	{
		return bytes(data);
	}
	
	public static int log2(int value) {
	    return Integer.SIZE - Integer.numberOfLeadingZeros(value);
	}
	
	public static int log2(long value) {
	    return Long.SIZE - Long.numberOfLeadingZeros(value);
	}
	
	public static void main(String[] args) 
	{
		ByteBuffer buf = ByteBuffer.allocate(10);
		
		UInt63Codec codec = new UInt63Codec();
		
//		System.out.println(codec.length(65535));
//		System.out.println(codec.length(Long.MAX_VALUE));
//		
//		codec.encode(buf, 255, 0);
//		
//		System.out.println(BufferTools.toString(buf));
		
		long t0 = System.currentTimeMillis();
		
		long len = 0;
		
		for (int c = -1; c < 100000000; c++) 
		{
			buf.rewind();
			len += codec.encode(buf, c);
			buf.rewind();
			codec.decode(buf);
			
			if (c != codec.getValue()) 
			{
				System.out.println("FAIL: " + c + " -- " + codec.getValue());
			}
		}
		
		System.out.println("Done... in " + (System.currentTimeMillis() - t0)+" total len: " + len);
	}
	

	
	

}
