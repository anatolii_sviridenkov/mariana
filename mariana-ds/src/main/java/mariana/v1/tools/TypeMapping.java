
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import mariana.bridge.Entry;
import mariana.bridge.Parent;
import mariana.bridge.TypeMap;
import mariana.bridge.TypeMaps;
import mariana.bridge.tools.APTConstants;

public final class TypeMapping {
	
	private final Map<Class<?>, Class<?>> typeMap = new HashMap<>();
	private final List<Entry> entries = new ArrayList<>();
	
	
	private TypeMapping parent;
	private List<TypeMapping> children = new ArrayList<>();
	
	private final Class<?> mappingClass;
	
	private TypeMapping(Class<?> mapping) 
	{
		this.mappingClass = mapping;
		TypeMap tm = mapping.getAnnotation(TypeMap.class);
		
		if (tm != null) 
		{
			for (Entry e: tm.value()) 
			{
				typeMap.put(e.javaType(), e.pairType());
				entries.add(e);
			}
		}
	}
	
	public Optional<Class<?>> getParentMappingClass() 
	{
		Parent p = mappingClass.getAnnotation(Parent.class);
		if (p != null) 
		{
			return Optional.of(p.value());
		}
		else {
			return Optional.empty();
		}
	}
	
	
	private Class<?> getLocal(Class<?> tm) 
	{
		Class<?> mapping = typeMap.get(tm);
		
		if (mapping != null) {
			return mapping;
		}
				
		for (Entry entry: entries)
		{
			if (tm.isAssignableFrom(entry.javaType())) 
			{
				return entry.pairType();
			}
		}
		
		return null;
	}
	
	public Optional<Class<?>> get(Class<?> tm) 
	{	
		Class<?> mapping = getLocal(tm);
		
		if (mapping != null) {
			return Optional.of(mapping);
		}
		
		if (parent != null) 
		{
			return parent.get(tm);
		}
		else {
			return Optional.empty();
		}
	}

	public TypeMapping getParent() {
		return parent;
	}
	
	public List<TypeMapping> getChildren() {
		return Collections.unmodifiableList(children);
	}

	
	
	private void setParentMapping(TypeMapping parentMapping) 
	{
		this.parent = parentMapping;		
		parent.addChild(this);
	}
	
	private void addChild(TypeMapping mapping) 
	{
		children.add(mapping);
	}
	
	
	public static Map<Class<?>, TypeMapping> buildMappings() 
	{
		try {
			Map<Class<?>, TypeMapping> mappings = new HashMap<>();
			
			Class<?> metadataClass = TypeMapping.class.getClassLoader().loadClass(APTConstants.METADATA_FULL_CLASS_NAME);
			
			Class<?>[] maps = metadataClass.getAnnotation(TypeMaps.class).value();
			
			for (Class<?> map: maps) 
			{
				mappings.put(map, new TypeMapping(map));
			}
			
			for (TypeMapping mapping: mappings.values()) 
			{
				Optional<Class<?>> parent = mapping.getParentMappingClass();
				if (parent.isPresent()) 
				{
					mapping.setParentMapping(mappings.get(parent.get()));
				}
			}
			
			return mappings;
		} 
		catch (ClassNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}


	public Class<?> getMappingClass() 
	{
		return mappingClass;
	}

	public Class<?> locate(Class<?> clazz) 
	{
		List<Class<?>> targets = locateAll(clazz);
		
		if (targets.size() == 1) {
			return targets.get(0);
		}
		else if (targets.size() == 0) {
			throw new RuntimeException("No implementation class is found for " + clazz.getName());
		}
		else {
			throw new RuntimeException("more than one implementation is found for " + clazz.getName() + ": " + targets);
		}
	}
	
	List<Class<?>> locateAll(Class<?> clazz) {
		List<Class<?>> list = new ArrayList<>();
		
		locateAll(clazz, list);
		
		return list;
	}
	
	private void locateAll(Class<?> clazz, List<Class<?>> list) 
	{
		for (TypeMapping child: children) 
		{
			child.locateAll(clazz, list);
		}
		
		Class<?> target = getLocal(clazz);
		if (target != null)
		{
			list.add(target);
		}
	}
}