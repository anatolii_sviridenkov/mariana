package mariana.v1.tools;

public class Ticker {
	private long threshold;
	private long thresholdIncrement;
	private long ticks;
	
	private long startTime = System.currentTimeMillis();
	private long tThreshold = startTime;
	
	public Ticker(long increment) 
	{
		thresholdIncrement = increment;
		threshold = increment - 1;
		ticks = 0;
	}
	
	public boolean isThreshold() {
		return ticks >= threshold;
	}
	
	public void nextThreshold() {
		threshold += thresholdIncrement;
		tThreshold = System.currentTimeMillis();
	}
	
	public long duration() {
		return System.currentTimeMillis() - tThreshold;
	}
	
	public void tick() {
		ticks++;
	}
	
	public void tick(long n) {
		ticks += n;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getThresholdTime() {
		return tThreshold;
	}

	public long getTicks() {
		return ticks;
	}
}
