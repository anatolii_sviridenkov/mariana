package mariana.v1.tools.pool;

public interface PoolObject extends AutoCloseable {
	Object getNext();
	void setNext(PoolObject next);
	Pool getPool();
	void close();
}
