
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.alloc;

import java.util.UUID;

import mariana.bridge.CxxObject;
import mariana.bridge.ImportedMethod;
import mariana.v1.io.AbstractOutputBufferStream;

public abstract class Allocator implements CxxObject {
	
	
	@ImportedMethod
	public abstract Snapshot find(UUID shapshotId);
	
	@ImportedMethod(name = "find_branch")
	public abstract Snapshot findBranch(String branchName);
	
	@ImportedMethod
	public abstract SnapshotDescriptor describe(UUID shapshotId);
	
	@ImportedMethod
	public abstract Snapshot master();
	
	@ImportedMethod(name = "set_master")
	public abstract void setMaster(UUID snaphostId);
	
	@ImportedMethod(name = "set_branch")
	public abstract void setBranch(String branchName, UUID snaphostId);
	
	public abstract void store(AbstractOutputBufferStream out);
	
	public abstract void store(String file);
		
	public abstract void dump(String destination);
	
	@ImportedMethod
	public abstract void lock();
	
	@ImportedMethod
	public abstract void unlock();
	
	@ImportedMethod(name = "try_lock")
	public abstract void tryLock();
}
