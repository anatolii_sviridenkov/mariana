
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.alloc;

import java.util.UUID;

import mariana.bridge.CxxObject;
import mariana.bridge.ImportedMethod;


public interface Snapshot extends CxxObject {
	
	@ImportedMethod
	void commit();
	
	@ImportedMethod
	void drop();
	
	default void dropParent() {
		try (Snapshot parent = this.parent())
		{
			parent.drop();
		}
	}
	
	@ImportedMethod
	Snapshot branch();
	
	@ImportedMethod
	Snapshot parent();
	
	@ImportedMethod
	UUID uuid();
	
	@ImportedMethod(name = "is_active")
	boolean isActive();
	
	@ImportedMethod(name = "is_committed")
	boolean isCommitted();
	
	@ImportedMethod(name = "is_marked_to_clear")
	boolean isMarkedToClear();
	
	@ImportedMethod(name = "is_data_locked")
	boolean isDataLocked();
	
	@ImportedMethod(name = "set_as_master")
	void setAsMaster();
	
	@ImportedMethod(name = "set_as_branch")
	void setAsBranch(String branchName);
	
	@ImportedMethod(name = "drop_ctr")
	boolean dropCtr(UUID name);
	
	
	@ImportedMethod
	String metadata();

	@ImportedMethod(name = "set_metadata")
	void setMetadata(String metadata);
		
	@ImportedMethod(name = "has_parent")
	boolean hasParent();
	
	@ImportedMethod(name = "lock_data_for_import")
	void lockDataForImport();

	@ImportedMethod(name = "import_new_ctr_from")
	void importNewCtrFrom(Snapshot snp, UUID name);
	
	@ImportedMethod(name = "import_ctr_from")
	void importCtrFrom(Snapshot snp, UUID name);
	
	@ImportedMethod(name = "copy_new_ctr_from")
	void copyNewCtrFrom(Snapshot snp, UUID name);
	
	@ImportedMethod(name = "copy_ctr_from")
	void copyCtrFrom(Snapshot snp, UUID name);
	
	<Ctr> Ctr create(Class<Ctr> clazz, UUID name);
	<Ctr> Ctr create(Class<Ctr> clazz);
	
	<Ctr> Ctr findOrCreate(Class<Ctr> clazz, UUID ctrName);
	<Ctr> Ctr find(Class<Ctr> clazz, UUID name);
	
	void dump(String destination);
	
	@ImportedMethod(name = "dump_open_containers")
	void dumpOpenConainersNames();
	
	@ImportedMethod(name = "has_open_containers")
	boolean hasOpenContainers();
	
	@ImportedMethod(name = "dump_persistent_tree")
	void dumpPersistentTree();
}
