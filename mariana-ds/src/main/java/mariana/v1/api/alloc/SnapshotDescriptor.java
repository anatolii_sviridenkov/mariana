package mariana.v1.api.alloc;

import java.util.List;
import java.util.UUID;

import mariana.bridge.StructType;

@StructType
public class SnapshotDescriptor {
	private UUID parentId;
	private UUID snapshotId;
	private List<UUID> children;
	private String description;
	private int status;
	
	public SnapshotDescriptor(UUID parentId, UUID snapshotId, List<UUID> children, String description, int status) 
	{
		this.parentId 	= parentId;
		this.snapshotId = snapshotId;
		this.children 	= children;
		this.description = description;
		this.status 	= status;
	}
	
	public UUID getSnapshotId() {
		return snapshotId;
	}
	
	public List<UUID> getChildren() {
		return children;
	}
	
	public int getStatus() {
		return status;
	}

	public UUID getParentId() {
		return parentId;
	}

	public String getDescription() {
		return description;
	}
	
	@Override
	public String toString() {
		return "SnapshotDescriptor[parent:" + parentId +", uuid:" + snapshotId + ", status:" + status + ", description:" + description + ", children:" + children +"]";
	}
}
