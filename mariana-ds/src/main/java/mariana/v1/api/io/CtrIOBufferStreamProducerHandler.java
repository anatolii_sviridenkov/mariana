
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.io;

import java.nio.ByteBuffer;

import mariana.bridge.Callback;
import mariana.bridge.Export;
import mariana.bridge.JObject;
import mariana.v1.api.RootTypeMap;
import mariana.v1.tools.CppObjectBase;

@Export(defaults = RootTypeMap.class, template = "mariana::v1::MarianaBTSSInputBuffer")
public class CtrIOBufferStreamProducerHandler extends CppObjectBase {
	private final ByteBuffer buffer;
	private BufferProducer producer; 

	public CtrIOBufferStreamProducerHandler(int size, BufferProducer consumer) 
	{
		this.producer = consumer;
		this.buffer = ByteBuffer.allocateDirect(size);
		createInstance();
	}
	
	public CtrIOBufferStreamProducerHandler(BufferProducer consumer)
	{
		this(65536, consumer);
	}

	@Callback
	public ByteBuffer getBuffer() {
		return buffer;
	}

	@Callback
	protected int populateBuffer(@JObject ByteBuffer buffer)
	{
		buffer.rewind();
		return producer.populate(buffer);
	}
	
	@Override
	public void close() {
		if (getHandle() != 0)
		{
			destroyInstance();			
		}
	}

	private native void createInstance();
	private native void destroyInstance();
	
}
