package mariana.v1.api;

public interface StreamNavigator {
	int streams();
	int toIndex();
	
	long toData(long position);
	long toData();
}
