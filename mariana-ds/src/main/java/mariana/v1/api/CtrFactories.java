
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api;

public interface CtrFactories {
	String MULTIMAP = "memoria/v1/extensions/multimap/multimap_extension.hpp";
	String MAP 		= "memoria/v1/extensions/map/map_extension.hpp";
	String SET 		= "memoria/v1/extensions/set/set_extension.hpp";
}
