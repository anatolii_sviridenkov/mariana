
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api;

import mariana.bridge.ImportedMethod;
import mariana.v1.api.io.BufferConsumer;
import mariana.v1.api.io.BufferProducer;
import mariana.v1.api.io.CtrIOBufferConsumerHandler;
import mariana.v1.api.io.CtrIOBufferStreamProducerHandler;

public interface BTSSIterator extends Iterator {
	
	@ImportedMethod
	void remove();
	
	@ImportedMethod(name = "read_buffer")
	long readBuffer(CtrIOBufferConsumerHandler handler, int size);
	
	@ImportedMethod(name = "read_buffer")
	long readBuffer(CtrIOBufferConsumerHandler handler);
	
	default long read(int size, BufferConsumer consumer) 
	{
		try (CtrIOBufferConsumerHandler h = new CtrIOBufferConsumerHandler(consumer)) {
			return readBuffer(h, size);
		}
	}
	
	default long read(BufferConsumer consumer) 
	{
		try (CtrIOBufferConsumerHandler h = new CtrIOBufferConsumerHandler(16384, consumer)) 
		{
			return readBuffer(h);
		}
	}
	
	
	
	@ImportedMethod(name = "insert_iobuffer")
	long insertBuffer(CtrIOBufferStreamProducerHandler handler, int inputBufferSize);
	
	@ImportedMethod(name = "insert_iobuffer")
	long insertBuffer(CtrIOBufferStreamProducerHandler handler);
	
	
	default long insert(BufferProducer producer) 
	{
		try (CtrIOBufferStreamProducerHandler h = new CtrIOBufferStreamProducerHandler(producer)) {
			return insertBuffer(h);
		}
	}
	
	
	default long insert(int ioBufferSize, BufferProducer producer) 
	{
		try (CtrIOBufferStreamProducerHandler h = new CtrIOBufferStreamProducerHandler(ioBufferSize, producer)) {
			return insertBuffer(h);
		}
	}
	
	default long insert(int ioBufferSize, int inputBufferSize, BufferProducer producer) 
	{
		try (CtrIOBufferStreamProducerHandler h = new CtrIOBufferStreamProducerHandler(ioBufferSize, producer)) {
			return insertBuffer(h, inputBufferSize);
		}
	}
}
