package mariana.v1.api.ctr.multimap.lng.bytes;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.buffer.IOBufferProducer;
import mariana.v1.tools.codecs.UInt63Codec;

public class MultimapLBEntryProducer implements IOBufferProducer {
	
	public static final int KEY_STREAM 	 = 0;
	public static final int VALUE_STREAM = 1;
	
	private final long key;
	private final byte[] data;
	private final int length;
	
	private int idx = -1;
	
	public MultimapLBEntryProducer(long key, byte[] data) 
	{
		this.key  = key;
		this.data = data;
		this.length = data.length;
	}
	
	public MultimapLBEntryProducer(long key, byte[] data, int length) 
	{
		this.key  = key;
		this.data = data;
		
		this.length = length;
	}
	
	@Override	
	public int apply(IOBuffer buffer) 
	{
		int entries = 0;
		if (idx < 0) 
		{
			buffer.putSymbol(KEY_STREAM, 1);
			buffer.putLong(key);
			idx = 0;
			entries += 2;
		}
		
		if (length > 0) 
		{
			int dataRemaining   = length - idx;
			int bufferRemaining = buffer.remaining() - UInt63Codec.LENGTH_MAX;

			if (bufferRemaining > 0) 
			{		
				int toWrite = dataRemaining <= bufferRemaining ? dataRemaining : bufferRemaining;		
				buffer.putSymbol(VALUE_STREAM, toWrite);
				buffer.putByteArray(data, idx, toWrite);

				entries += toWrite + 1;

				idx += toWrite;  
			}
		}
		
		return idx < length ? entries : -entries;		
	}
}
