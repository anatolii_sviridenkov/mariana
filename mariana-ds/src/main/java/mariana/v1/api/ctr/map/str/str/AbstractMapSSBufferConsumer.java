
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.map.str.str;

import java.nio.ByteBuffer;

import mariana.v1.api.io.BufferConsumer;
import mariana.v1.tools.codecs.Int64Codec;

public abstract class AbstractMapSSBufferConsumer implements BufferConsumer, MapSSEntryConsumer<String, String> {

	private Int64Codec codec = new Int64Codec();
	private byte[] tmpBuf = new byte[1024];
	    						
	@Override
	public int consume(ByteBuffer buffer, int entries) 
	{
		for (int e = 0; e < entries; e++) 
		{
			codec.decode(buffer);
			
			int keyLen = codec.getIntValue();
			
			buffer.get(tmpBuf, 0, keyLen);
			
			String key = new String(tmpBuf, 0, keyLen);
			
			codec.decode(buffer);
			
			int valueLen = codec.getIntValue();
			
			buffer.get(tmpBuf, 0, valueLen);
		
			String value = new String(tmpBuf, 0, valueLen);
			
			if (!consume(key, value)) 
			{
				return -e;
			}
		}
		
		return entries;
	}
}
