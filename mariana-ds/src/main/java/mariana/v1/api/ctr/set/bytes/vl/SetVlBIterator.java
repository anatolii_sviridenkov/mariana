
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.set.bytes.vl;

import java.nio.ByteBuffer;

import mariana.bridge.DirectBuffer;
import mariana.bridge.ImportedMethod;
import mariana.v1.api.ctr.set.bytes.SetBIterator;

public interface SetVlBIterator extends SetBIterator {
	@ImportedMethod
	byte[] key();

	
	default boolean isFound(ByteBuffer key) 
	{
		if (key.isDirect()) {
			return isFoundD(key);
		}
		else {
			return isFoundB(key);
		}
	}
	
	@ImportedMethod(name = "is_found")
	boolean isFoundD(@DirectBuffer ByteBuffer key);
	
	@ImportedMethod(name = "is_found")
	boolean isFoundB(ByteBuffer key);

	
	default void insert(ByteBuffer key) 
	{
		if (key.isDirect()) 
		{
			insertD(key);
		}
		else {
			insertB(key);
		}
	}
	
	@ImportedMethod(name = "insert")
	void insertB(@DirectBuffer ByteBuffer key);
	@ImportedMethod(name = "insert")
	void insertD(@DirectBuffer ByteBuffer key);
}