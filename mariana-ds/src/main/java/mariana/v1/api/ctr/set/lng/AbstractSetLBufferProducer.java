
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.set.lng;

import java.nio.ByteBuffer;

import mariana.v1.api.io.BufferProducer;
import mariana.v1.tools.BufferTools;

public abstract class AbstractSetLBufferProducer implements BufferProducer, SetLEntryProvider {

	private boolean entryIsDone = true;
	private long key;
	
	@Override
	public int populate(ByteBuffer buffer) 
	{
		int total = 0;
		
		for(; this.hasNext(); total++) 
		{
			if (entryIsDone) 
			{
				this.next();
				
				key = getKey();
			}

			if (encodeIfFits(buffer, key)) 
			{
				entryIsDone = true;
			}
			else {
				entryIsDone = false;
				return total;
			}
		}
		
		return -total;
	}
	
	
	private boolean encodeIfFits(ByteBuffer buffer, long data) 
	{
		int len = 8;
		int pos = buffer.position();
		
		if (pos + len <= buffer.limit()) 
		{
			BufferTools.put(buffer, data);
			return true;
		}
		else {
			return false;
		}
	}
}
