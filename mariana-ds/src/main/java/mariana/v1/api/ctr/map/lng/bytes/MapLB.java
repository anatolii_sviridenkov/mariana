
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.map.lng.bytes;

import java.nio.ByteBuffer;

import mariana.bridge.DirectBuffer;
import mariana.bridge.ImportedMethod;
import mariana.v1.api.ctr.map.Map;


public interface MapLB extends Map {
	
	@ImportedMethod
	MapLBIterator find(long key);
	
	@ImportedMethod
	boolean remove(long key);
	
	default MapLBIterator assign(long key, byte[] value) 
	{
		return assign(key, value, 0, value.length);
	}
		
	default MapLBIterator assign(long key, byte[] value, int offset, int length) 
	{
		return assignB(key, ByteBuffer.wrap(value, offset, length));
	}
	
	
	default MapLBIterator assign(long key, ByteBuffer value) 
	{
		if (value.isDirect()) 
		{
			return assignD(key, value);
		}
		else {
			return assignB(key, value);
		}
	}
	
	@ImportedMethod(name = "assign")
	MapLBIterator assignB(long key, ByteBuffer value);
	
	@ImportedMethod(name = "assign")
	MapLBIterator assignD(long key, @DirectBuffer ByteBuffer value);
	
	@ImportedMethod
	MapLBIterator begin();
	
	@ImportedMethod
	MapLBIterator end();
	
	@ImportedMethod
	MapLBIterator seek(long idx);
}
