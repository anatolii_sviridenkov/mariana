
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.map.str.str;

import mariana.bridge.ImportedMethod;
import mariana.v1.api.ctr.map.MapIterator;

public abstract class MapSSIterator implements MapIterator {
	@ImportedMethod
	public abstract String key();

	@ImportedMethod
	public abstract String value();

	@ImportedMethod(name = "is_found")
	public abstract boolean isFound(String key);

	@ImportedMethod
	public abstract void insert(String key, String value);

	@ImportedMethod
	public abstract void assign(String value);
}