
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.map.str.str;

import java.nio.ByteBuffer;

import com.google.common.base.Charsets;

import mariana.v1.api.io.BufferProducer;
import mariana.v1.tools.codecs.Int64Codec;

public abstract class AbstractMapSSBufferProducer implements BufferProducer, MapSSEntryProvider<String, String> {

	private String key;
	private String value;
	
	private Int64Codec codec = new Int64Codec();
	
	@Override
	public int populate(ByteBuffer buffer) 
	{
		int total = 0;
		
		for(; this.hasNext(); total++) 
		{
			if (key == null) 
			{
				this.next();
				
				key 	= getKey();
				value 	= getValue();
			}
			
			byte[] keyData = key.getBytes(Charsets.UTF_8);
			
			if (encodeIfFits(buffer, keyData)) 
			{			
				byte[] valueData = value.getBytes(Charsets.UTF_8);
				
				if (encodeIfFits(buffer, valueData)) 
				{			
					key = value = null;
				}
				else {
					return total;
				}
			}
			else {
				return total;
			}
		}
		
		return -total;
	}
	
	private boolean encodeIfFits(ByteBuffer buffer, byte[] data) 
	{
		int len1 	= codec.length(data.length);
		int total 	= len1 + data.length;
		int pos 	= buffer.position();
		
		if (pos + total <= buffer.limit()) 
		{
			codec.encode(buffer, data.length);
			buffer.put(data);
			return true;
		}
		else {
			return false;
		}
	}
}
