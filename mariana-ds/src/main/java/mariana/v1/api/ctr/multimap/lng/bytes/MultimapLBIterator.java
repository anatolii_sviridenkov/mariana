
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.multimap.lng.bytes;

import mariana.bridge.ImportedMethod;
import mariana.v1.api.ctr.multimap.MultimapIterator;
import mariana.v1.io.buffer.IOBufferConsumer;
import mariana.v1.io.buffer.IOBufferProducer;
import mariana.v1.io.ctr.IOBufferConsumerAdapter;
import mariana.v1.io.ctr.IOBufferProducerAdapter;

public interface MultimapLBIterator extends MultimapIterator {
	@ImportedMethod
	long key();
	
	@ImportedMethod
	byte value();
	
	@ImportedMethod
	void remove(long length);
	
	@ImportedMethod(name = "count_values")
	long size();

	@ImportedMethod(name = "run_pos")
	long runPos();
	
	@ImportedMethod(name = "next_key")
	boolean nextKey();
	
	@ImportedMethod(name = "to_prev_key")
	void prevKey();
	
	
	@ImportedMethod(name = "read_keys")
	long readKeys(IOBufferConsumerAdapter consumer, long length);
	
	default long readKeys(IOBufferConsumer consumer) {
		return readKeys(consumer, Long.MAX_VALUE);
	}
	
	default long readKeys(IOBufferConsumer consumer, long length) {
		try (IOBufferConsumerAdapter adapter = new IOBufferConsumerAdapter(consumer)) {
			return readKeys(adapter, length);
		}
	}
	
	default void readValues(IOBufferConsumer consumer) {
		try (IOBufferConsumerAdapter adapter = new IOBufferConsumerAdapter(consumer)) {
			readValues(adapter, Long.MAX_VALUE);
		}
	}
	
	default void readValues(IOBufferConsumer consumer, long size) {
		try (IOBufferConsumerAdapter adapter = new IOBufferConsumerAdapter(consumer)) {
			readValues(adapter, size);
		}
	}
	
	@ImportedMethod(name = "read_values_c")
	void readValues(IOBufferConsumerAdapter consumer, long size);

	@ImportedMethod(name = "populate_values")
	long populateValues(MultimapLBRunWalkerAdapter consumer, long size, boolean moveRemainingToStart);
	
	@ImportedMethod(name = "populate_entries")
	long populate(MultimapLBEntryWalkerAdapter consumer, long size, boolean moveRemainingToStart);
	
	@ImportedMethod(name = "is_found")
	boolean isFound(long key);

	@ImportedMethod(name = "bulkio_insert_p")
	void insert(IOBufferProducerAdapter adapter);
	
	default void insert(IOBufferProducer producer) 
	{
		try (IOBufferProducerAdapter adapter = new IOBufferProducerAdapter(producer)) {
			insert(adapter);
		}
	}

}