
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.set.bytes;

import java.nio.ByteBuffer;

import mariana.v1.api.io.BufferProducer;
import mariana.v1.tools.codecs.Int64Codec;

public abstract class AbstractSetBBufferProducer implements BufferProducer, SetBEntryProvider<byte[]> {

	private byte[] key;
	
	private Int64Codec codec = new Int64Codec();
	
	@Override
	public int populate(ByteBuffer buffer) 
	{
		int total = 0;
		
		for(; this.hasNext(); total++) 
		{
			if (key == null) 
			{
				this.next();
				
				key = getKey();				
			}
			
			if (encodeIfFits(buffer, key))
			{			
				key = null;
			}
			else {
				return total;
			}
		}
		
		return -total;
	}
	
	private boolean encodeIfFits(ByteBuffer buffer, ByteBuffer data) 
	{
		int len 	 = data.remaining();		
		int lenOfLen = codec.length(len);
		int total 	 = lenOfLen + len;
		int pos 	 = buffer.position();
		
		if (pos + total <= buffer.limit()) 
		{
			codec.encode(buffer, len);
			buffer.put(data);
			return true;
		}
		else {
			return false;
		}
	}
	
	private boolean encodeIfFits(ByteBuffer buffer, byte[] data) 
	{
		int len 	 = data.length;
		int lenOfLen = codec.length(len);
		int total 	 = lenOfLen + len;
		int pos 	 = buffer.position();
		
		if (pos + total <= buffer.limit()) 
		{
			codec.encode(buffer, len);
			buffer.put(data);
			return true;
		}
		else {
			return false;
		}
	}
}
