package mariana.v1.api.ctr.multimap.lng.bytes;

import mariana.v1.io.buffer.DynamicByteArray;
import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.buffer.IOBufferConsumer;


public class MultimapLBValueConsumer implements IOBufferConsumer {

	private final DynamicByteArray array = new DynamicByteArray();
	
	@Override
	public int apply(IOBuffer buffer, int entries) 
	{
		array.append(buffer, buffer.limit());
		return entries;
	}

	public DynamicByteArray getArray() {
		return array;
	}
}
