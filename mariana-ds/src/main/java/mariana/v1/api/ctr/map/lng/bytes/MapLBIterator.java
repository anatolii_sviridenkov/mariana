
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.map.lng.bytes;

import java.nio.ByteBuffer;

import mariana.bridge.DirectBuffer;
import mariana.bridge.ImportedMethod;
import mariana.v1.api.ctr.map.MapIterator;

public interface MapLBIterator extends MapIterator {
	@ImportedMethod
	long key();

	@ImportedMethod
	byte[] value();

	@ImportedMethod(name = "is_found")
	boolean isFound(long key);

	default void insert(long key, byte[] value) {
		insert(key, value, 0, value.length);
	}
	
	default void insert(long key, ByteBuffer value) 
	{
		if (value.isDirect()) {
			insertD(key, value);
		}
		else {
			insertB(key, value);
		}
	}
	
	
	default void insert(long key, byte[] value, int offset, int length) 
	{
		insertB(key, ByteBuffer.wrap(value, offset, length));
	}

	
	@ImportedMethod(name = "insert")
	void insertB(long key, ByteBuffer value);
	
	@ImportedMethod(name = "insert")
	void insertD(long key, @DirectBuffer ByteBuffer value);
	
	
	default void assign(byte[] value) 
	{
		assign(value, 0, value.length);
	}
	
	
	
	default void assign(byte[] value, int offset, int length)
	{
		assign(ByteBuffer.wrap(value, offset, length));
	}
	
	
	default void assign(ByteBuffer value) {
		if (value.isDirect()) 
		{
			assignD(value);
		}
		else {
			assignB(value);
		}
	}
	
	@ImportedMethod(name = "assign")
	void assignB(ByteBuffer value);
	
	@ImportedMethod(name = "assign")
	void assignD(@DirectBuffer ByteBuffer value);
}