package mariana.v1.api.ctr.multimap.lng.bytes;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.buffer.IOBufferConsumer;

public class MultimapLBKeyConsumerAdaptor implements IOBufferConsumer {

	private LongConsumer consumer;
	
	public MultimapLBKeyConsumerAdaptor(LongConsumer consumer) {
		this.consumer = consumer;		
	}
	
	@Override
	public int apply(IOBuffer buffer, int entries) 
	{
		for (int c = 0; c < entries; c++) 
		{
			consumer.accept(buffer.getLong());
		}
		
		buffer.done();
		
		return entries;
	}

}
