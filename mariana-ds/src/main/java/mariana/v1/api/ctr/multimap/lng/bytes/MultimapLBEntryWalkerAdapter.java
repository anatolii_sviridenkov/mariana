
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.multimap.lng.bytes;

import mariana.bridge.Export;
import mariana.bridge.ImportedMethod;
import mariana.v1.api.CtrFactories;
import mariana.v1.api.RootTypeMap;
import mariana.v1.io.ctr.AbstractBtflWalkerAdapter;

@Export(
	defaults = RootTypeMap.class,
	base     = "mariana::v1::CtrGEDecls<memoria::v1::Map<BigInt, Vector<UByte>>>",
	template = "mariana::v1::BTFLWalkerAdapter",
	include  = CtrFactories.MULTIMAP
)
public class MultimapLBEntryWalkerAdapter extends AbstractBtflWalkerAdapter {

	public MultimapLBEntryWalkerAdapter(int size) 
	{
		super(size);
		createInstance();
	}
	
	public MultimapLBEntryWalkerAdapter() 
	{			
		this(8192);
	}
	
	@ImportedMethod
	public void resetIOBuffer(int pos, int limit) 
	{
		__resetIOBuffer(getHandle(), pos, limit);
	}
	
	@Override
	public void close() 
	{
		if (getHandle() != 0)
		{
			destroyInstance();
		}
	}
	
	private native void createInstance();
	private native void destroyInstance();
		
	private static native void __resetIOBuffer(long handle, int pos, int limit);
}
