
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.set.lng;

import mariana.bridge.ImportedMethod;
import mariana.v1.api.ctr.set.Set;


public interface SetL extends Set {
	
	@ImportedMethod(name = "find")
	SetLIterator find(long key);
	
	
	@ImportedMethod
	boolean remove(long key);	
	
	@ImportedMethod(name = "insert_key")
	boolean insert(long key);
	
	@ImportedMethod
	boolean contains(long key);
	
	@ImportedMethod
	SetLIterator begin();
	
	@ImportedMethod
	SetLIterator end();
	
	@ImportedMethod
	SetLIterator seek(long idx);
}
