
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.set.bytes;

import java.nio.ByteBuffer;

import mariana.bridge.DirectBuffer;
import mariana.bridge.ImportedMethod;
import mariana.v1.api.ctr.set.Set;


public interface SetB extends Set {
	
	default SetBIterator find(ByteBuffer key) {
		if (key.isDirect()) 
		{
			return findD(key);
		}
		else {
			return findB(key);
		}
	}
	
	@ImportedMethod(name = "find")
	SetBIterator findB(ByteBuffer key);
	
	@ImportedMethod(name = "find")
	SetBIterator findD(@DirectBuffer ByteBuffer key);
	
	@ImportedMethod
	boolean remove(ByteBuffer key);
	
	@ImportedMethod(name = "insert_key")
	boolean insert(ByteBuffer key);
	
	@ImportedMethod
	boolean contains(ByteBuffer key);
	
	@ImportedMethod
	SetBIterator begin();
	
	@ImportedMethod
	SetBIterator end();
	
	@ImportedMethod
	SetBIterator seek(long idx);
}
