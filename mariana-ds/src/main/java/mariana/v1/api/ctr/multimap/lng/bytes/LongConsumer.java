package mariana.v1.api.ctr.multimap.lng.bytes;

public interface LongConsumer {
	void accept(long value);
}
