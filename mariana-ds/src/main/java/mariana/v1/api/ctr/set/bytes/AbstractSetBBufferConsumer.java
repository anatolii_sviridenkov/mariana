
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.api.ctr.set.bytes;

import java.nio.ByteBuffer;

import mariana.v1.api.io.BufferConsumer;
import mariana.v1.tools.codecs.Int64Codec;

public abstract class AbstractSetBBufferConsumer implements BufferConsumer, SetBEntryConsumer<ByteBuffer> {

	private Int64Codec codec = new Int64Codec();
	
	@Override
	public int consume(ByteBuffer buffer, int entries) 
	{
		for (int e = 0; e < entries; e++) 
		{
			codec.decode(buffer);
			
			int keyLen = codec.getIntValue();
			
			ByteBuffer slice = buffer.slice();
			
			slice.limit(keyLen);
			
			if (!consume(slice)) 
			{
				return -e;
			}
			
			buffer.position(buffer.position() + keyLen);
		}
		
		return entries;
	}
}
