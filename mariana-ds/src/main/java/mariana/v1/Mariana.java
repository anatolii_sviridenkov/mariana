
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Optional;

import mariana.v1.tools.TypeMapping;

public class Mariana {
	
	private static final Map<Class<?>, TypeMapping> mappings;
	
	static {
		mappings = TypeMapping.buildMappings();		
		NarSystem.loadLibrary();
	}
	
	public static Optional<TypeMapping> getTypeMapping(Class<?> clazz) 
	{
		return Optional.ofNullable(mappings.get(clazz));
	}
	
	public static void init() {}
	
	public static void destroy() {}
	
	
	public static native void dumpCounters();
	
	public static native void testBB(ByteBuffer bb);
	
	public static native void dumpKnownContainers();
	
	public static void main(String[] args) {
		dumpKnownContainers();
	}
}
