
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.io;

import java.nio.ByteBuffer;

import mariana.bridge.Callback;
import mariana.bridge.ClosableObject;
import mariana.bridge.Export;
import mariana.bridge.ImportedMethod;
import mariana.v1.api.RootTypeMap;
import mariana.v1.api.io.OutputBufferStream;
import mariana.v1.tools.CppObjectBase;

@Export(
	base = "mariana::v1::DefaultOutputStreamHandler",
	template = "mariana::v1::OutputStreamHandlerJavaImpl",
	
	defaults = RootTypeMap.class	
)
public abstract class AbstractOutputBufferStream extends CppObjectBase implements OutputBufferStream, ClosableObject {
	
	private ByteBuffer buffer;
	
	public AbstractOutputBufferStream(int bufferSize) 
	{
		buffer = ByteBuffer.allocateDirect(bufferSize);
		
		createInstance();
	}
	
	public AbstractOutputBufferStream() {
		this(65536);
	}
	

	public void close() {
		if (getHandle() != 0)
		{
			drain();
			closeStream();
			destroyInstance();			
		}
	}
	
	@Callback
	private ByteBuffer createBuffer() {
		return buffer;
	}
	
	
	@Callback(name = "close")
	abstract public void closeStream();
	
	@Callback
	public int bufferSize() {
		return buffer.capacity();
	}

	@Callback(classOnly = true)
	abstract public void flush();
	
	@Callback(classOnly = true)
	abstract public void consume(ByteBuffer buffer, int start, int length); 
	
	
	
	@ImportedMethod
	public void drain() 
	{
		long hh = getHandle();
		if (hh != 0) {
			__drain(hh);
		}
	}
	
	private static native void __drain(long handle);
	
	private native void createInstance();
	private native void destroyInstance();
}
