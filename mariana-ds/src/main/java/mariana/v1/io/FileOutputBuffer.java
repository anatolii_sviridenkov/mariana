
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.io;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileOutputBuffer extends AbstractOutputBufferStream {

	private RandomAccessFile raf;
	
	public FileOutputBuffer(String file) {
		try {			
			raf = new RandomAccessFile(file, "rw");
			raf.getChannel().truncate(0);
		} 
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public FileOutputBuffer(File file) {
		try {
			raf = new RandomAccessFile(file, "rw");
			raf.getChannel().truncate(0);
		} 
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public FileOutputBuffer(RandomAccessFile file) {
		raf = file;
	}
	


	@Override
	public void flush() {
		try {
			raf.getFD().sync();
		} 
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void closeStream() {
		try {
			if (raf.getChannel().isOpen()) {
				raf.close();
			}
		} 
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void consume(ByteBuffer buffer, int start, int length) 
	{		
		try {
			buffer.limit(start + length);
			buffer.position(start);
			
			if (raf != null) {
				FileChannel channel = raf.getChannel();
				channel.write(buffer);
			}			
		} 
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
