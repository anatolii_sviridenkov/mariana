
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.io.ctr;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import mariana.bridge.Callback;
import mariana.bridge.JObject;
import mariana.v1.tools.CppObjectBase;


public abstract class AbstractBtflWalkerAdapter extends CppObjectBase {	
	
	protected ByteBuffer buffer;
	
	public AbstractBtflWalkerAdapter(int size) 
	{			
		buffer = ByteBuffer.allocateDirect(size).order(ByteOrder.LITTLE_ENDIAN);		
	}
	
	abstract public void resetIOBuffer(int pos, int limit); 
	
	@Callback
	public ByteBuffer getBuffer() {
		return buffer;
	}
	
	@Callback
	public void configureDirectByteBuffer(@JObject ByteBuffer buffer, int pos, int limit) 
	{
		buffer.limit(limit);
		buffer.position(pos);
	}
}
