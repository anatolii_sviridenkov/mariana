
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.io.ctr;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import mariana.bridge.Callback;
import mariana.bridge.Export;
import mariana.bridge.JObject;
import mariana.v1.api.RootTypeMap;
import mariana.v1.io.buffer.IOBufferConsumer;
import mariana.v1.io.buffer.IOByteBuffer;
import mariana.v1.tools.CppObjectBase;

@Export(defaults = RootTypeMap.class, template = "mariana::v1::IOBufferConsumerAdapter")
public class IOBufferConsumerAdapter extends CppObjectBase {	
	private IOBufferConsumer consumer;
	
	public IOBufferConsumerAdapter(IOBufferConsumer consumer) 
	{	
		this.consumer = consumer;
		createInstance();
	}

	@Callback
	protected long processBuffer(@JObject ByteBuffer buffer, int position, int limit, int entries)
	{
		buffer.rewind();
		buffer.limit(limit);
		buffer.position(position);
		
		IOByteBuffer ioBuffer = new IOByteBuffer(buffer.order(ByteOrder.LITTLE_ENDIAN));
		
		long processed = consumer.apply(ioBuffer, entries);
		
		return (processed << 32) | buffer.position();
	}
	
	@Override
	public void close() 
	{
		if (getHandle() != 0)
		{
			destroyInstance();			
		}
	}

	private native void createInstance();
	private native void destroyInstance();
}
