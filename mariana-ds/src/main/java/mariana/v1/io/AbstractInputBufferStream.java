
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.io;

import java.nio.ByteBuffer;

import mariana.bridge.Callback;
import mariana.bridge.ClosableObject;
import mariana.bridge.Export;
import mariana.bridge.ImportedMethod;
import mariana.v1.api.RootTypeMap;
import mariana.v1.api.io.InputBufferStream;
import mariana.v1.tools.CppObjectBase;

@Export(base = "mariana::v1::DefaultInputStreamHandler", template = "mariana::v1::InputStreamHandlerJavaImpl", defaults = RootTypeMap.class)
public abstract class AbstractInputBufferStream extends CppObjectBase implements InputBufferStream, ClosableObject {
	
	private ByteBuffer buffer;
	
	public AbstractInputBufferStream(int bufferSize) 
	{
		buffer = ByteBuffer.allocateDirect(bufferSize);
		
		createInstance();
	}
	
	public AbstractInputBufferStream() {
		this(65536);
	}
	

	public void close() {
		if (getHandle() != 0)
		{
			closeStream();
			destroyInstance();
		}
	}
	
	@ImportedMethod
    public int available() {
		return __available(getHandle());
	}
	
	@Callback(name = "close")
	abstract public void closeStream();

	
	@Callback
	private ByteBuffer createBuffer() {
		return buffer;
	}
	
	@Callback
	public int bufferSize() {
		return buffer.capacity();
	}
	
	@Callback(classOnly = true)
	abstract public int read(ByteBuffer buffer, int start, int length); 

	
	private native void createInstance();
	private native void destroyInstance();
	
	private static native int __available(long handle);
	private static native long __pos(long handle);
}
