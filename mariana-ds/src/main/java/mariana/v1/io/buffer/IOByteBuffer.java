package mariana.v1.io.buffer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import mariana.v1.tools.codecs.Int64Codec;
import mariana.v1.tools.codecs.UInt63Codec;

public class IOByteBuffer implements IOBuffer {

	private final ByteBuffer delegate;

	private Int64Codec vlenCodec = new Int64Codec();
	private UInt63Codec uvlenCodec = new UInt63Codec();

	private SymbolsRun symbolsRun = new SymbolsRun();

	private int symbols = 2;


	public IOByteBuffer(int capacity) {
		delegate = ByteBuffer.allocate(capacity).order(ByteOrder.LITTLE_ENDIAN);
	}

	public IOByteBuffer(ByteBuffer delegate) {
		this.delegate = delegate;
	}

	@Override
	public int limit() {
		return delegate.limit();
	}
	
	public void limit(int limit) {
		this.delegate.limit(limit);
	}

	@Override
	public int remaining() {
		return delegate.remaining();
	}

	@Override
	public int pos() {
		return delegate.position();
	}
	
	public void pos(int pos) {
		delegate.position(pos);
	}

	@Override
	public void mark() {
		delegate.mark();
	}

	@Override
	public void flip() {
		delegate.flip();
	}

	@Override
	public void rewind() {
		delegate.rewind();
	}

	@Override
	public void reset() {
		delegate.reset();
	}

	public void done() {
		delegate.position(delegate.limit());
	}
	
	public void reuse() 
	{
		delegate.limit(delegate.capacity());
		delegate.rewind();
	}	

	@Override
	public void skip(int n) {
		delegate.position(delegate.position() + n);
	}

	@Override
	public boolean putBoolean(boolean value) {
		if (delegate.hasRemaining()) {
			delegate.put(value ? (byte) 1 : (byte) 0);
			return true;
		}

		return false;
	}

	public boolean hasBoolean() {
		return delegate.hasRemaining();
	}

	@Override
	public boolean getBoolean() {
		return delegate.get() == 0 ? false : true;
	}

	@Override
	public boolean putByte(byte value) {
		if (delegate.hasRemaining()) {
			delegate.put(value);
			return true;
		}

		return false;
	}

	@Override
	public boolean hasByte() {
		return delegate.hasRemaining();
	}

	@Override
	public byte getByte() {
		return delegate.get();
	}

	@Override
	public boolean putShort(short value) {
		if (delegate.remaining() >= 2) {
			delegate.putShort(value);
			return true;
		}

		return false;
	}

	@Override
	public boolean hasShort() {
		return delegate.remaining() >= 2;
	}

	@Override
	public short getShort() {
		return delegate.getShort();
	}

	@Override
	public boolean putChar(char value) {
		if (delegate.remaining() >= 2) {
			delegate.putChar(value);
			return true;
		}

		return false;
	}

	@Override
	public boolean hasChar() {
		return delegate.remaining() >= 2;
	}

	@Override
	public char getChar() {
		return delegate.getChar();
	}

	@Override
	public boolean putInt(int value) {
		if (delegate.remaining() >= 4) {
			delegate.putInt(value);
			return true;
		}

		return false;
	}

	@Override
	public boolean hasInt() {
		return delegate.remaining() >= 4;
	}

	@Override
	public int getInt() {
		return delegate.getInt();
	}

	@Override
	public boolean putLong(long value) {
		if (delegate.remaining() >= 8) {
			delegate.putLong(value);
			return true;
		}

		return false;
	}

	@Override
	public boolean hasLong() {
		return delegate.remaining() >= 8;
	}

	@Override
	public long getLong() {
		return delegate.getLong();
	}

	@Override
	public boolean putDouble(double value) {
		if (delegate.remaining() >= 8) {
			delegate.putDouble(value);
			return true;
		}

		return false;
	}

	@Override
	public boolean hasDouble() {
		return delegate.remaining() >= 8;
	}

	@Override
	public double getDouble() {
		return delegate.getDouble();
	}

	@Override
	public boolean putFloat(float value) {
		if (delegate.remaining() >= 4) {
			delegate.putFloat(value);
			return true;
		}

		return false;
	}

	@Override
	public boolean hasFloat() {
		return delegate.remaining() >= 4;
	}

	@Override
	public float getFloat() {
		return delegate.getFloat();
	}

	@Override
	public boolean hasSVLen() {
		int remaining = delegate.remaining();
		if (remaining >= Int64Codec.LENGTH_MAX) {
			return true;
		} else if (remaining >= Int64Codec.LENGTH_HEADER) {
			return vlenCodec.length(delegate) <= remaining;
		} else {
			return false;
		}
	}

	@Override
	public boolean hasUVLen() 
	{
		int remaining = delegate.remaining();
		if (remaining >= UInt63Codec.LENGTH_MAX) 
		{
			return true;
		} 
		else if (remaining >= UInt63Codec.LENGTH_HEADER) 
		{
			return uvlenCodec.length(delegate) <= remaining;
		} 
		else {
			return false;
		}
	}

	public long getSVLen() {
		vlenCodec.decode(delegate);
		return vlenCodec.getValue();
	}

	public boolean putSVLen(long value) {
		int length = vlenCodec.length(value);
		if (delegate.remaining() >= length) {
			vlenCodec.encode(delegate, value);
			return true;
		}

		return false;
	}

	public int svlength(long value) {
		return vlenCodec.length(value);
	}

	public int svlength() {
		return vlenCodec.length(delegate);
	}

	public long getUVLen() {
		uvlenCodec.decode(delegate);
		return uvlenCodec.getValue();
	}

	public boolean putUVLen(long value) {
		int length = uvlenCodec.length(value);
		if (delegate.remaining() >= length) {
			uvlenCodec.encode(delegate, value);
			return true;
		}

		return false;
	}

	public int uvlength(long value) {
		return uvlenCodec.length(value);
	}

	public int uvlength() {
		return uvlenCodec.length(delegate);
	}

	@Override
	public boolean putSymbol(int symbol, int length) 
	{
		long encodedRun = encodeSymbolsRun(symbol, symbols, length);

		if (uvlenCodec.length(encodedRun) <= delegate.remaining()) {
			uvlenCodec.encode(delegate, encodedRun);
			return true;
		}

		return false;
	}
	
	public int runLength(int symbol, int length) 
	{
		long encodedRun = encodeSymbolsRun(symbol, symbols, length);
		return uvlenCodec.length(encodedRun); 
	}

	@Override
	public void updateSymbolsRun(int pos, int symbol, int length) 
	{
		int currentRunLength = uvlenCodec.length(delegate, pos);
		
		long encodedRun = encodeSymbolsRun(symbol, symbols, length);

		int encodedRunLength = uvlenCodec.length(encodedRun);
		
        if (encodedRunLength > currentRunLength)
        {            
            int delta = encodedRunLength - currentRunLength;            
            insertSpace(pos, delta);
        }
        else {
        	int delta = currentRunLength - encodedRunLength;
            removeSpace(pos, delta);
        }
        
        uvlenCodec.encode(delegate, encodedRun, pos);
	}

	@Override
	public SymbolsRun getSymbolsRun() {
		long encodedRun = getUVLen();

		decodeSymbolsRun(symbols, encodedRun, symbolsRun);

		return symbolsRun;
	}

	public static long encodeSymbolsRun(int symbol, int symbols, int length) 
	{
		int bitsPerSymbol = UInt63Codec.log2(symbols - 1);
		int symbolMask = (1 << bitsPerSymbol) - 1;

		if (length > 0) 
		{
			return (symbol & symbolMask) | (length << bitsPerSymbol);
		} 
		else {
			throw new RuntimeException("Symbols run length must be >= 0 : " + length);
		}
	}

	public static SymbolsRun decodeSymbolsRun(int symbols, long value) {
		int bitsPerSymbol = UInt63Codec.log2(symbols - 1);
		int symbolMask = (1 << bitsPerSymbol) - 1;

		return new SymbolsRun((int) (value & symbolMask), (int) (value >> bitsPerSymbol));
	}

	public static void decodeSymbolsRun(int symbols, long value, SymbolsRun run) {
		int bitsPerSymbol = UInt63Codec.log2(symbols - 1);
		int symbolMask = (1 << bitsPerSymbol) - 1;

		run.setSymbol((int) (value & symbolMask));
		run.setLength((int) (value >> bitsPerSymbol));
	}

	@Override
	public void putByteArray(byte[] data, int start, int length) {
		delegate.put(data, start, length);
	}

	@Override
	public void getByteArray(byte[] data, int start, int length) {
		delegate.get(data, start, length);
	}

	public ByteBuffer getDelegate() {
		return delegate;
	}
	
	public void moveRemainingToStart() 
	{
		int remaining = delegate.remaining();
		if (remaining > 0) 
		{
			move(delegate.position(), 0, remaining);
		}
		
		delegate.rewind();
		delegate.limit(delegate.capacity());
		delegate.position(remaining);
	}
	
	public void insertSpace(int pos, int length) 
	{
		if (length > 0) 
		{
			int limit = delegate.limit();
			delegate.limit(limit + length);

			int dpos = delegate.position();

			if (pos < dpos) 
			{
				delegate.position(dpos + length); 
			}

			move(pos, pos + length, limit - pos);
		}
	}
	
	public void removeSpace(int pos, int length) 
	{
		if (length > 0) 
		{
			int limit = delegate.limit();
			
			move(pos + length, pos, limit - (pos + length));

			int dpos = delegate.position();

			if (pos + length <= dpos) 
			{
				delegate.position(dpos - length); 
			}
			else if (pos < dpos) {
				delegate.position(pos);
			}

			delegate.limit(limit - length);
		}
	}

	public void move(int from, int to, int length) 
	{
		if (delegate.isDirect()) 
		{
			int capacity = delegate.capacity();

			if (from < 0) throw new IndexOutOfBoundsException("From is negative: " + from);
			if (from > capacity) throw new IndexOutOfBoundsException("From is out of bounds: " + from);

			if (to < 0) throw new IndexOutOfBoundsException("To is negative: " + to);
			if (to > capacity) throw new IndexOutOfBoundsException("To is out of bounds: " + to);

			if (length < 0) throw new IndexOutOfBoundsException("Length is negative: " + length);

			if (from + length > capacity) throw new IndexOutOfBoundsException("From + Length exceeds capacity: " + from + " " + length + " " + capacity);
			if (to + length > capacity) throw new IndexOutOfBoundsException("To + Length exceeds capacity: " + to + " " + length + " " + capacity);

			moveDirect(delegate, from, to, length);
		} 
		else {
			byte[] array = delegate.array();
			System.arraycopy(array, from, array, to, length);
		}
	}

	private static native void moveDirect(ByteBuffer buffer, int from, int to, int length);
	
	@Override
	public String toString() {
		return delegate.toString();
	}
}
