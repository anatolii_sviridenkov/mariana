package mariana.v1.io.buffer;

import mariana.v1.tools.codecs.UInt63Codec;

public interface IOBuffer {
	
	public static final long UVLEN_NULL = UInt63Codec.NULL;
	
	int limit();
	void limit(int limit);
	
	int remaining();
	int pos();
	void pos(int pos);
	void mark();
	void flip();
	void rewind();
	void reset();
	void skip(int n);
	
	void done();
	
	boolean putBoolean(boolean value);
	boolean hasBoolean();
	boolean getBoolean();
	
	boolean putByte(byte value);
	
	boolean hasByte();
	byte getByte();
	
	boolean putShort(short value);
	
	boolean hasShort();
	short getShort();
	
	boolean putChar(char value);
	boolean hasChar();
	char getChar();
	
	boolean putInt(int value);
	
	boolean hasInt();
	int getInt();
	
	boolean putLong(long value);
	
	boolean hasLong();
	long getLong();
	
	boolean putDouble(double value);
	
	boolean hasDouble();
	double getDouble();
	
	
	
	boolean putFloat(float value);
	
	boolean hasFloat();
	float getFloat();

	
	
	boolean putSVLen(long value);
	
	boolean hasSVLen();
	long getSVLen();
	
	int svlength(long value);	
	int svlength();
	
	boolean putUVLen(long value);
	
	boolean hasUVLen();
	long getUVLen();
	
	int uvlength(long value);	
	int uvlength();	
	
	boolean putSymbol(int symbol, int length);
	int runLength(int symbol, int length);
	
	SymbolsRun getSymbolsRun();
	void updateSymbolsRun(int pos, int symbol, int length);
	
	void putByteArray(byte[] data, int start, int length); 
	
	default void putByteArray(byte[] data) {
		putByteArray(data, 0, data.length);
	}
	
	
	void getByteArray(byte[] data, int start, int length);
	
	default void getByteArray(byte[] data) {
		getByteArray(data, 0, data.length);
	}
}
