package mariana.v1.io.buffer;

public interface IOBufferProducer {
	int apply(IOBuffer buffer);
}
