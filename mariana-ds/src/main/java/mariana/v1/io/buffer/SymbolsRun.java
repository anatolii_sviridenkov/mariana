package mariana.v1.io.buffer;
public class SymbolsRun implements Cloneable {
	private int symbol;
	private int length;
	
	public SymbolsRun() {}
	public SymbolsRun(int symbol, int length) {
		this.symbol = symbol;
		this.length = length;
	}
	
	public int getSymbol()  {return symbol;}
	public long getLength() {return length;}
	
	public void setSymbol(int symbol)  {this.symbol = symbol;}
	public void setLength(int length) {this.length = length;}
	
	public SymbolsRun clone() 
	{
		return new SymbolsRun(symbol, length);
	}
	
	@Override
	public String toString() {
		return "SymbolsRun[symbol=" + symbol + ", length=" + length + "]";
	}
}