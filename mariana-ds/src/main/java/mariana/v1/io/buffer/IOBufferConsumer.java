package mariana.v1.io.buffer;

public interface IOBufferConsumer {
	int apply(IOBuffer buffer, int entries);
}
