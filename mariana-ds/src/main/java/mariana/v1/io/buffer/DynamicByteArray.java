package mariana.v1.io.buffer;

public class DynamicByteArray {
	
	private byte[] array;
	private int size;
	
	public DynamicByteArray() {
		array = null; 
	}
	

	public DynamicByteArray(int size) {
		array = new byte[size];
	}
	
	public int size() {
		return size;
	}
	
	public byte[] array() {
		return array;
	}
	
	public void reset() {
		size = 0;
	}
	
	public void append(byte[] source, int start, int length) 
	{
		ensureCapacity(length);
		System.arraycopy(source, size, this.array, size, length);
		size += length;
	}
	
	public void append(byte[] source) {
		append(source, 0, source.length);
	}
	
	public void append(IOBuffer source, int length) 
	{
		ensureCapacity(length);
		source.getByteArray(array, size, length);		
		size += length;
	}
	
	protected void ensureCapacity(int length) 
	{
		if (array != null) 
		{
			int available = array.length - size;

			if (available < length ) 
			{
				int newSize 	 = array.length * 2;
				int newAvailable = newSize - size; 

				if (newAvailable < length) 
				{
					newSize += length - newAvailable;
				}

				byte[] newArray = new byte[newSize];

				System.arraycopy(array, 0, newArray, 0, size);

				this.array = newArray;
			}
		}
		else {
			array = new byte[length];
		}
	}


	public byte[] toArray() {
		byte[] data = new byte[this.size()];
		System.arraycopy(array, 0, data, 0, size);
		return data;
	}
}
