
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memoria/v1/extensions/multimap/multimap_extension_names.hpp>
#include <memoria/v1/extensions/multimap/multimap_tools.hpp>

namespace memoria {
namespace v1 {

MEMORIA_V1_ITERATOR_PART_BEGIN(v1::multimap::IterExtensionName)

	using Container = typename Base::Container;

    using Value 	=  typename Container::Types::Value;
    using Key 		=  typename Container::Types::Key;

    using CtrSizeT 	= typename Container::Types::CtrSizeT;

public:

    template <typename IOBuffer>
    long read_values_c(bt::BufferConsumer<IOBuffer>* consumer, long size)
    {
        auto& self = this->self();
        return self.bulkio_scan_run(consumer, 1, size);
    }


    template <typename WalkerAdapter>
    long populate_values(WalkerAdapter* walker_adapter, long size, bool moveRemainingToStart)
    {
    	using IOBuffer = typename WalkerAdapter::IOBuffer;

    	auto& self = this->self();

    	if (!walker_adapter->walker())
    	{
    		walker_adapter->walker() = self.template create_scan_run_walker<IOBuffer>(size);
    	}

    	if (moveRemainingToStart)
    	{
    		walker_adapter->buffer()->moveRemainingToStart();
    	}

    	long total = self.bulkio_populate(*walker_adapter->walker().get(), walker_adapter->buffer());

    	walker_adapter->updateByteBuffer();

    	return total;
    }

    template <typename WalkerAdapter>
    long populate_entries(WalkerAdapter* walker_adapter, long size, bool moveRemainingToStart)
    {
    	using IOBuffer = typename WalkerAdapter::IOBuffer;

    	auto& self = this->self();

    	if (!walker_adapter->walker())
    	{
    		walker_adapter->walker() = self.template create_scan_ge_walker<IOBuffer>(size);
    	}

    	if (moveRemainingToStart)
    	{
    		walker_adapter->buffer()->moveRemainingToStart();
    	}

    	long total = self.bulkio_populate(*walker_adapter->walker().get(), walker_adapter->buffer());

    	walker_adapter->updateByteBuffer();

    	return total;
    }


    template <typename IOBuffer>
    auto bulkio_insert_p(BufferProducer<IOBuffer>* provider, const Int initial_capacity = 20000)
    {
        auto& self = this->self();
        return self.ctr().bulkio_insert(self, *provider, initial_capacity);
    }




MEMORIA_V1_ITERATOR_PART_END

}}
