
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memoria/v1/containers/multimap/mmap_factory.hpp>

#include <memoria/v1/extensions/multimap/multimap_tools.hpp>
#include <memoria/v1/extensions/multimap/multimap_extension_names.hpp>
#include <memoria/v1/extensions/multimap/multimap_container.hpp>
#include <memoria/v1/extensions/multimap/multimap_iterator.hpp>
#include <memoria/v1/extensions/multimap/multimap_free_functions.hpp>

#include <mariana/io_buffer/iobuffer_producer.hpp>
#include <mariana/io_buffer/iobuffer_consumer.hpp>



namespace memoria {
namespace v1 {
namespace bt {

template <typename Profile, typename Key, typename Value>
struct ContainerExtensionsTF<Profile, Map<Key, Vector<Value>>> {
	using Type = TL<multimap::CtrExtensionName>;
};

template <typename Profile, typename Key, typename Value>
struct IteratorExtensionsTF<Profile, Map<Key, Vector<Value>>> {
	using Type = TL<multimap::IterExtensionName>;
};


}
}}
