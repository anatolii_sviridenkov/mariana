
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memoria/v1/extensions/set/set_extension_names.hpp>
#include <memoria/v1/extensions/set/set_tools.hpp>

#include <memory>

namespace memoria {
namespace v1 {

MEMORIA_V1_ITERATOR_PART_BEGIN(v1::set::IterExtensionName)

	using Container = typename Base::Container;

    using Key 		=  typename Container::Types::Key;

    using CtrSizeT 	= typename Container::Types::CtrSizeT;

public:

    template <typename Input>
    CtrSizeT insert_input(Input* input)
    {
    	auto& self = this->self();

        using InputProvider = memoria::v1::set::MarianaSetInputProvider<Container, Input>;

        auto bulk = std::make_unique<InputProvider>(self.ctr(), input);

        return self.ctr().insert(self, *bulk.get());
    }


    template <typename Output>
    auto read_output(Output* output, CtrSizeT length)
    {
        auto& self = this->self();
        auto readed = self.ctr().template describe_single_substream<IntList<0, 1>>(self, 0, length, *output);

        output->flush();

        return readed;
    }

    template <typename Output>
    auto read_output(Output* output)
    {
    	auto& self = this->self();
    	return read_output(output, self.ctr().size());
    }

MEMORIA_V1_ITERATOR_PART_END

}}
