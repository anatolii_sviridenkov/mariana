
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memoria/v1/containers/set/set_factory.hpp>

#include <mariana/java_array.hpp>

namespace memoria {
namespace v1 {
namespace set {

using namespace mariana::v1;
using namespace mariana::v1::core;


template <typename CtrT, typename EntryBuffer>
class MarianaSetInputProvider: public memoria::v1::btss::AbstractBTSSInputProvider<CtrT, CtrT::Types::LeafDataLength> {

    using Base = memoria::v1::btss::AbstractBTSSInputProvider<CtrT, CtrT::Types::LeafDataLength>;

protected:

    using typename Base::CtrSizeT;
    using typename Base::Position;
    using typename Base::InputBuffer;

    bool finished_ = false;

    EntryBuffer* entry_buffer_;

public:
    MarianaSetInputProvider(CtrT& ctr, EntryBuffer* entry_buffer):
        Base(ctr, entry_buffer->buffer_size()), entry_buffer_(entry_buffer)
    {
    }

    virtual ~MarianaSetInputProvider() noexcept {}

    virtual void start_buffer(InputBuffer* buffer) {}
    virtual void end_buffer(InputBuffer* buffer, Int size) {}

    virtual Int get(InputBuffer* buffer, Int pos)
    {
    	if (!finished_)
        {
    		int size = 0;
        	if (pos == 0)
        	{
        		if (entry_buffer_->fetch())
        		{
        			buffer->fill_buffer(entry_buffer_);
        			size = entry_buffer_->size();
        		}
        		else {
        			finished_ = true;
        			return -1;
        		}
        	}

        	return size;
        }
        else {
        	return -1;
        }
    }
};



template <typename Base>
class SetBInputJavaImpl: public Base {
	using typename Base::Class;

	jobject jbuffer_;
	UByte* data_;

	Int buffer_size_;
	Int buffer_position_ = 0;
	Int size_ = 0;

public:
	SetBInputJavaImpl(JNIEnv* env, jobject obj):
		Base(env, obj)
	{
		jbuffer_ = env->NewGlobalRef(this->getBuffer());
		data_ 	 = T2T<UByte*>(env->GetDirectBufferAddress(jbuffer_));

		buffer_size_ = env->GetDirectBufferCapacity(jbuffer_);
	}

	virtual ~SetBInputJavaImpl()
	{
		this->env()->DeleteGlobalRef(jbuffer_);
	}

	Int buffer_size() const {
		return buffer_size_;
	}

	Int buffer_position() const {
		return buffer_position_;
	}

	Int size() const {
		return size_;
	}

	bool fetch()
	{
		size_ = this->fetchEntries();
		buffer_position_ = this->bufferPosition();
		return size_ > 0;
	}

	template <typename StreamObj>
	void fill(StreamTag<0>, StreamTag<0>, StreamObj* buffer) {
		buffer->fill(size_);
	}

	template <typename StreamObj>
    void fill(StreamTag<0>, StreamTag<1>, StreamObj* buffer)
	{
		buffer->fill(data_, size_, make_sv<Int>(buffer_position_));
    }
};




template <typename Base>
class SetBOutputJavaImpl: public Base {
	using typename Base::Class;

	jobject jbuffer_;
	UByte* data_;

	size_t buffer_size_;
	size_t buffer_position_ = 0;
	Int size_ = 0;

public:
	SetBOutputJavaImpl(JNIEnv* env, jobject obj):
		Base(env, obj)
	{
		jbuffer_ = env->NewGlobalRef(this->getBuffer());
		data_ 	 = T2T<UByte*>(env->GetDirectBufferAddress(jbuffer_));

		buffer_size_ = env->GetDirectBufferCapacity(jbuffer_);
	}

	virtual ~SetBOutputJavaImpl()
	{
		this->env()->DeleteGlobalRef(jbuffer_);
	}

    template <typename V>
    bool put(StreamTag<0>, StreamTag<1>, Int block, V&& value)
    {
    	if (value.length() + buffer_position_ > buffer_size_)
    	{
    		flush();
    	}

    	CopyBuffer(value.addr(), data_ + buffer_position_, value.length());

    	buffer_position_ += value.length();
    	size_++;

        return true;
    }

    void flush()
    {
    	if (buffer_position_ > 0)
    	{
    		this->processBlock(buffer_position_, size_);
    		buffer_position_ = 0;
    		size_ = 0;
    	}
    }

    void next() {}
};




}
}}
