
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memoria/v1/extensions/map/map_extension_names.hpp>
#include <memoria/v1/extensions/map/map_tools.hpp>

namespace memoria {
namespace v1 {

MEMORIA_V1_ITERATOR_PART_BEGIN(v1::map::IterExtensionName)

	using Container = typename Base::Container;

    using Value =  typename Container::Types::Value;
    using Key 	=  typename Container::Types::Key;

    using CtrSizeT = typename Container::Types::CtrSizeT;

public:

    template <typename Input>
    CtrSizeT insert_input(Input* input, Int ib_capacity)
    {
    	auto& self = this->self();

        using InputProvider = memoria::v1::map::MarianaMapInputProvider<Container, Input>;

        auto bulk = std::make_unique<InputProvider>(self.ctr(), input, ib_capacity);

        return self.ctr().insert(self, *bulk.get());
    }


    template <typename Output>
    auto read_output(Output* output, CtrSizeT length)
    {
        auto& self = this->self();
        auto readed = self.ctr().template read_entries<0>(self, length, *output);

        output->flush();

        return readed;
    }

    template <typename Output>
    auto read_output(Output* output)
    {
    	auto& self = this->self();
    	return read_output(output, self.ctr().size());
    }


    class EmptyAdaptor {
    	BigInt count_ = 0;
    public:
    	BigInt count() const {return count_;}

        template <typename V>
        void put(StreamTag<0>, StreamTag<0>, Int block, V&& entry) {}

        template <typename V>
        void put(StreamTag<0>, StreamTag<1>, Int block, V&& key) {
            count_++;
        }

        template <typename V>
        void put(StreamTag<0>, StreamTag<2>, Int block, V&& value) {
        	count_++;
        }

        void next()
        {
        	count_++;
        }
    };

    auto scan_bm()
    {
    	auto& self = this->self();
    	return scan_bm(self.ctr().size());
    }

    auto scan_bm(CtrSizeT length)
    {
        auto& self = this->self();

        EmptyAdaptor adaptor;
        self.ctr().template read_entries<0>(self, length, adaptor);

        return adaptor.count();
    }

MEMORIA_V1_ITERATOR_PART_END

}}
