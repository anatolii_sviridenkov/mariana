
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <memoria/v1/containers/map/map_factory.hpp>

#include <mariana/java_array.hpp>
#include <mariana/string/utf8_string.hpp>

namespace memoria {
namespace v1 {
namespace map {

using namespace mariana::v1;


template <typename CtrT, typename EntryBuffer, Int BufferSize = 1000>
class MarianaMapInputProvider: public memoria::v1::btss::AbstractBTSSInputProvider<CtrT, CtrT::Types::LeafDataLength> {

    using Base = memoria::v1::btss::AbstractBTSSInputProvider<CtrT, CtrT::Types::LeafDataLength>;

protected:

    using typename Base::CtrSizeT;
    using typename Base::Position;
    using typename Base::InputBuffer;

    Int input_start_    = 0;
    Int input_size_     = 0;

    bool finished_ = false;

    Int expected_buffer_size_;

    static constexpr Int INPUT_END = BufferSize;

    EntryBuffer* entry_buffer_;

public:
    MarianaMapInputProvider(CtrT& ctr, EntryBuffer* entry_buffer, Int capacity = 10000):
        Base(ctr, capacity), entry_buffer_(entry_buffer)
    {
    	expected_buffer_size_ = entry_buffer_->buffer_size();
    }

    virtual ~MarianaMapInputProvider() noexcept {}

    virtual Int get(InputBuffer* buffer, Int pos)
    {
        if (input_start_ == input_size_)
        {
        	if (!finished_)
            {
            	input_start_ = 0;
            	input_size_ = entry_buffer_->fill();
            	finished_ = input_size_ < expected_buffer_size_;
            }
            else {
            	return -1;
            }
        }

        if (input_start_ < input_size_)
        {
            auto inserted = buffer->append_vbuffer(entry_buffer_, input_start_, input_size_ - input_start_);

            input_start_ += inserted;
            return inserted;
        }

        return -1;
    }
};



template <typename Base>
class MapSSInputJavaImpl: public Base {
	using typename Base::Class;

	JavaArray<U8String, jobjectArray> keys_;
	JavaArray<U8String, jobjectArray> values_;

	int buffer_size_;

	BigInt one_ = 1;

	U8String key_;
	U8String value_;

public:
	MapSSInputJavaImpl(JNIEnv* env, jobject obj):
		Base(env, obj),
		keys_(env, this->keys()),
		values_(env, this->values())
	{
		buffer_size_ = this->getBufferSize();
	}

	virtual ~MapSSInputJavaImpl()
	{}

	Int buffer_size() const {
		return buffer_size_;
	}

	const auto& buffer(StreamTag<0>, StreamTag<0>, Int idx, Int block) {
		return one_;
	}

    const auto& buffer(StreamTag<0>, StreamTag<1>, Int idx, Int block) {
    	key_ = keys_.get(idx);
    	return key_;
    }

    const auto& buffer(StreamTag<0>, StreamTag<2>, Int idx, Int block) {
    	value_ = values_.get(idx);
    	return value_;
    }
};


template <typename Base>
class MapSSOutputJavaImpl: public Base {
	using typename Base::Class;

	JavaArray<U8String, jobjectArray> keys_;
	JavaArray<U8String, jobjectArray> values_;

	int buffer_size_;
	int pos_ = 0;

public:
	MapSSOutputJavaImpl(JNIEnv* env, jobject obj):
		Base(env, obj),
		keys_(env, this->keys()),
		values_(env, this->values())
	{
		buffer_size_ = this->getBufferSize();
	}

	virtual ~MapSSOutputJavaImpl()
	{}

	Int buffer_size() const {
		return buffer_size_;
	}

	int pos() const {
		return pos_;
	}

    template <typename V>
    void put(StreamTag<0>, StreamTag<0>, Int block, V&& entry) {}

    template <typename V>
    void put(StreamTag<0>, StreamTag<1>, Int block, V&& key) {
        keys_.set(pos_, key);
    }

    template <typename V>
    void put(StreamTag<0>, StreamTag<2>, Int block, V&& value) {
        values_.set(pos_, value);
    }

    void flush()
    {
    	if (pos_ > 0)
    	{
    		this->processBlock(pos_);
    		pos_ = 0;
    	}
    }

    void next()
    {
        pos_++;

        if (pos_ == buffer_size_)
        {
        	flush();
        }
    }
};






}
}}
