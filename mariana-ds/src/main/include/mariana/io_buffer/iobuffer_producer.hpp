
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once


#include <memoria/v1/core/types/type2type.hpp>
#include <memoria/v1/core/tools/uuid.hpp>
#include <memoria/v1/core/tools/iobuffer/io_buffer.hpp>
#include <memoria/v1/prototypes/bt/tools/bt_tools.hpp>

#include <mariana/exceptions.hpp>
#include <mariana/java_object.hpp>
#include <mariana/io_buffer/iobuffer_decls.hpp>

#include <memory>
#include <unordered_map>
#include <functional>
#include <string>

#include <jni.h>

namespace mariana {
namespace v1 {



template <typename Base>
class MarianaBTSSInputBuffer: public Base, public BufferProducer<MarianaIOBuffer> {
	using typename Base::Class;

	jobject direct_buffer_;
	jlong buffer_size_;
	std::unique_ptr<MarianaIOBuffer> io_buffer_;

public:
	MarianaBTSSInputBuffer(JNIEnv* env, jobject obj):
		Base(env, obj)
	{
		direct_buffer_ 	= env->NewGlobalRef(this->getBuffer());

		buffer_size_ 	= env->GetDirectBufferCapacity(direct_buffer_);
		UByte* data		= T2T<UByte*>(env->GetDirectBufferAddress(direct_buffer_));

		io_buffer_ = std::make_unique<MarianaIOBuffer>(data, buffer_size_);
	}

	virtual ~MarianaBTSSInputBuffer()
	{
		this->env()->DeleteGlobalRef(direct_buffer_);
	}

	virtual MarianaIOBuffer& buffer() {
		return *io_buffer_.get();
	}

	virtual Int populate(MarianaIOBuffer& buffer)
	{
		return this->populateBuffer(direct_buffer_);
	}
};


template <typename Base>
class IOBufferProducerAdapter: public Base, public BufferProducer<MarianaIOBuffer> {

	jobject direct_buffer_ = nullptr;

	using Base::env;

public:
	IOBufferProducerAdapter(JNIEnv* env, jobject obj):
		Base(env, obj)
	{
	}

	virtual ~IOBufferProducerAdapter()
	{
		this->env()->DeleteGlobalRef(direct_buffer_);
	}




	virtual Int populate(MarianaIOBuffer& buffer)
	{
		if (!direct_buffer_)
		{
			auto buffer_local = env()->NewDirectByteBuffer(buffer.array(), buffer.size());
			direct_buffer_ = env()->NewGlobalRef(buffer_local);
		}

		BigInt status = this->populateBuffer(direct_buffer_, buffer.pos(), buffer.limit());

		buffer.pos((status) & 0xFFFFFFFF);
		buffer.flip();

		return (status >> 32) & 0xFFFFFFFF;
	}
};


}}
