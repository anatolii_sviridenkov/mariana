
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/types/types.hpp>
#include <memoria/v1/core/types/type2type.hpp>

#include <memoria/v1/core/tools/stream.hpp>
#include <memoria/v1/core/tools/iobuffer/io_buffer.hpp>

#include <mariana/io_buffer/iobuffer_decls.hpp>
#include <mariana/exceptions.hpp>
#include <mariana/byte_buffer.hpp>
#include <mariana/ctr_types.hpp>

#include <memory>

#include <jni.h>

namespace mariana {
namespace v1 {

template <typename CtrName, typename IOBufferT = MarianaIOBuffer>
struct CtrRunDecls {
	using IOBuffer = IOBufferT;
	using BFTLWalkerH = decltype(std::declval<IterType<CtrName>>().template create_scan_run_walker<IOBuffer>());
};

template <typename CtrName, typename IOBufferT = MarianaIOBuffer>
struct CtrGEDecls {
	using IOBuffer = IOBufferT;
	using BFTLWalkerH = decltype(std::declval<IterType<CtrName>>().template create_scan_ge_walker<IOBuffer>());
};

template <typename Base>
class BTFLWalkerAdapter: public Base {

	using Base::env;

public:
	using typename Base::BFTLWalkerH;
	using typename Base::IOBuffer;

private:
	BFTLWalkerH walker_;

	jobject direct_buffer_;
	jlong buffer_size_;
	std::unique_ptr<IOBuffer> io_buffer_;

public:
	BTFLWalkerAdapter(JNIEnv* env, jobject obj):
		Base(env, obj)
	{
		direct_buffer_ 	= env->NewGlobalRef(this->getBuffer());

		buffer_size_ 	= env->GetDirectBufferCapacity(direct_buffer_);
		UByte* data		= T2T<UByte*>(env->GetDirectBufferAddress(direct_buffer_));

		io_buffer_ 		= std::make_unique<MarianaIOBuffer>(data, buffer_size_);
	}

	BFTLWalkerH& walker() {return walker_;}
	const BFTLWalkerH& walker() const {return walker_;}

	virtual ~BTFLWalkerAdapter()
	{
		this->env()->DeleteGlobalRef(direct_buffer_);
	}

	virtual IOBuffer* buffer() {
		return io_buffer_.get();
	}

	void updateByteBuffer()
	{
		this->configureDirectByteBuffer(direct_buffer_, io_buffer_->pos(), io_buffer_->limit());
	}

	void resetIOBuffer(int pos, int limit)
	{
		io_buffer_->limit(limit);
		io_buffer_->pos(pos);
	}
};

}
}
