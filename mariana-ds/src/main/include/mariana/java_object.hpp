
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/tools/pair.hpp>
#include <memoria/v1/core/types/type2type.hpp>

#include <mariana/exceptions.hpp>
#include <mariana/tools.hpp>

#include <memory>

#include <jni.h>

namespace mariana {
namespace v1 {

using namespace memoria::v1;

class JavaPairObject;

class JavaClassMetadataBase {
protected:
	jclass clazz_;
	jfieldID handle_field_id_;
	const char* name_;

	JavaClassMetadataBase(JNIEnv* env, const char* name): name_(name)
	{
		jclass clazz = env->FindClass(name_);

		if (clazz)
		{
			clazz_ = T2T<jclass>(env->NewGlobalRef(clazz));

			handle_field_id_ = env->GetFieldID(clazz_, "handle", "J");

			if (!handle_field_id_)
			{
				throw Exception(MA_RAW_SRC, SBuf() << "Can't find 'handle' field in class " << name_);
			}
		}
		else {
			throw Exception(MA_RAW_SRC, SBuf() << "Can't find class " << name_);
		}
	}

public:
	void destroy(JNIEnv* env)
	{
		env->DeleteGlobalRef(clazz_);
	}

	jclass clazz() {return clazz_;}
	const jclass clazz() const {return clazz_;}

	jfieldID handle_field_id() {return handle_field_id_;}
	const jfieldID handle_field_id() const {return handle_field_id_;}

	jlong handle(JNIEnv* env, jobject obj)
	{
		return env->GetLongField(obj, handle_field_id_);
	}

	void set_handle(JNIEnv* env, jobject obj, jlong handle)
	{
		env->SetLongField(obj, handle_field_id_, handle);
	}

	void clear_handle(JNIEnv* env, jobject obj)
	{
		set_handle(env, obj, 0);
	}
};



class JavaCreatableClassMetadataBase: public JavaClassMetadataBase  {
protected:
	jmethodID ctr_method_id_;

	JavaCreatableClassMetadataBase(JNIEnv* env, const char* name): JavaClassMetadataBase(env, name)
	{
		ctr_method_id_ = env->GetMethodID(clazz_, "<init>", "(J)V");

		if (!ctr_method_id_)
		{
			throw Exception(MA_RAW_SRC, SBuf() << "Can't find pair instance constructor in class " << name_);
		}
	}

public:


	const jmethodID ctr_method_id() const {return ctr_method_id_;}
	jmethodID ctr_method_id() {return ctr_method_id_;}

	jobject new_instance(JNIEnv* env, BigInt handle)
	{
		jobject result = env->NewObject(clazz_,  ctr_method_id_, handle);
		if (result)
		{
			return result;
		}
		else {
			throw JavaException(MA_RAW_SRC, env, SBuf() << "Can't create new instance of type " << name_);
		}
	}

	template <typename T>
	jobject new_instance_for(JNIEnv* env, const std::shared_ptr<T>& ptr)
	{
		BigInt handle = to_handle(ptr);

		try {
			return new_instance(env, handle);
		}
		catch (...)
		{
			using PtrT = std::shared_ptr<T>;

			PtrT* ptr = T2T<PtrT*>(handle);

			delete ptr;

			throw;
		}
	}
};





template <typename Base = memoria::v1::EmptyType>
class JavaObjectST: public Base {
protected:
	JNIEnv* env_;
	jobject obj_;
public:
	JavaObjectST(JNIEnv* env, jobject obj): env_(env), obj_(obj)
	{}

	virtual ~JavaObjectST() noexcept {}

	JNIEnv* env() {return env_;}
	const JNIEnv* env() const {return env_;}

	jobject obj() {return obj_;}
	const jobject obj() const {return obj_;}
};


template <typename Base = memoria::v1::EmptyType>
class JavaObjectMT: public Base {
protected:
	JavaVM* vm_;
	jobject obj_;
public:
	JavaObjectMT(JNIEnv* env, jobject obj): obj_(obj)
	{
		if (env->GetJavaVM(&vm_) != JNI_OK)
		{
			throw Exception(MA_RAW_SRC, "Can't obtain JavaVM from JNIEnv for JavaPair");
		}
	}

	virtual ~JavaObjectMT() noexcept {}

	jobject obj() {return obj_;}
	const jobject obj() const {return obj_;}

	JNIEnv* env()
	{
		JNIEnv* env;
		if (vm_->GetEnv((void**)&env, JNI_VERSION_1_8) == JNI_OK)
		{
			return env;
		}
		else {
			throw Exception(MA_RAW_SRC, "Can't obtain JNIEnv from JavaVM for ${pair.cppJavaObjectName()}");
		}
	};
};



class JavaPairObject: public ScriptPair {
protected:
	JavaVM* vm_;
public:
	JavaPairObject(JNIEnv* env, jobject obj): ScriptPair(obj)
	{
		if (env->GetJavaVM(&vm_) != JNI_OK)
		{
			throw Exception(MA_RAW_SRC, "Can't obtain JavaVM from JNIEnv for JavaPair");
		}
	}

	virtual ~JavaPairObject() noexcept {}

	JNIEnv* env()
	{
		JNIEnv* env;
		if (vm_->GetEnv((void**)&env, JNI_VERSION_1_8) == JNI_OK)
		{
			return env;
		}
		else {
			throw Exception(MA_RAW_SRC, "Can't obtain JNIEnv from JavaVM for ${pair.cppJavaObjectName()}");
		}
	};

	jobject obj() {return T2T<jobject>(this->ptr());}
	const jobject obj() const {return T2T<jobject>(this->ptr());}

	JavaVM* vm() {return vm_;}
	const JavaVM* vm() const {return vm_;}
};


}}
