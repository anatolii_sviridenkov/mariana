
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/types/types.hpp>
#include <memoria/v1/core/types/type2type.hpp>

#include <memoria/v1/core/tools/stream.hpp>


#include <mariana/exceptions.hpp>
#include <mariana/byte_buffer.hpp>

#include <jni.h>

namespace mariana {
namespace v1 {

using namespace memoria::v1;


class DefaultInputStreamHandler: public InputStreamHandler {
public:
	virtual ~DefaultInputStreamHandler() throw () {}



    virtual Int available() 	= 0;
    virtual Int bufferSize() 	= 0;
    virtual void close() 		= 0;


    virtual size_t read(void* mem, size_t offset, size_t length) = 0;

    virtual Byte readByte() {
        return readT<Byte>();
    }

    virtual UByte readUByte() {
        return readT<UByte>();
    }

    virtual Short readShort() {
        return readT<Short>();
    }

    virtual UShort readUShort() {
        return readT<UShort>();
    }

    virtual Int readInt() {
        return readT<Int>();
    }

    virtual UInt readUInt() {
        return readT<UInt>();
    }

    virtual BigInt readBigInt() {
        return readT<BigInt>();
    }

    virtual UBigInt readUBigInt() {
        return readT<UBigInt>();
    }

    virtual bool readBool() {
        return readT<Byte>();
    }

    virtual float readFloat() {
        return readT<float>();
    }

    virtual double readDouble() {
        return readT<double>();
    }

private:
    template <typename T>
    T readT()
    {
        T value;

        auto len = read(&value, 0, sizeof(T));

        if (len == sizeof(T)) {
            return value;
        }
        else {
            throw Exception(MA_SRC, "Can't read value from InputStreamHandler");
        }
    }
};



class DefaultOutputStreamHandler: public OutputStreamHandler {

public:

    virtual ~DefaultOutputStreamHandler() noexcept {}


    virtual Int bufferSize() 	= 0;
    virtual void close()		= 0;

    virtual void flush() 		= 0;
    virtual void write(const void* mem, size_t offset, size_t length) = 0;

    virtual void write(Byte value) {
        writeT(value);
    }

    virtual void write(UByte value) {
        writeT(value);
    }

    virtual void write(Short value) {
        writeT(value);
    }

    virtual void write(UShort value) {
        writeT(value);
    }

    virtual void write(Int value) {
        writeT(value);
    }

    virtual void write(UInt value) {
        writeT(value);
    }

    virtual void write(BigInt value) {
        writeT(value);
    }

    virtual void write(UBigInt value) {
        writeT(value);
    }

    virtual void write(bool value) {
        writeT((Byte)value);
    }

    virtual void write(float value) {
        writeT(value);
    }

    virtual void write(double value) {
        writeT(value);
    }


private:
    template <typename T>
    void writeT(const T& value) {
        write(&value, 0, sizeof(T));
    }
};



template <typename Base>
class InputStreamHandlerJavaImpl: public Base {
	using typename Base::Class;

	UByte* data_;
	size_t capacity_;
	size_t buffer_pos_ = 0;
	size_t available_ = 0;

	jobject buffer_;

	bool eof_ = false;

	size_t total_ = 0;
public:
	InputStreamHandlerJavaImpl(JNIEnv* env, jobject obj): Base(env, obj)
	{
		buffer_   = env->NewGlobalRef(this->createBuffer());

		data_ 	  = T2T<UByte*>(env->GetDirectBufferAddress(buffer_));
		capacity_ = env->GetDirectBufferCapacity(buffer_);
	}

	virtual Int available() {
		return available_ - buffer_pos_;
	}

	virtual BigInt pos() const
	{
		return total_;
	}

	virtual size_t read(void* mem, size_t offset, size_t length)
	{
		size_t sum = 0;
		while ((!eof_) && length > 0)
		{
			size_t s0 = read0(mem, offset, length);

			sum += s0;
			offset += s0;
			length -= s0;
		}

		return sum;
	}


private:
	virtual size_t read0(void* mem, size_t offset, size_t length)
	{
		UByte* memb = T2T<UByte*>(mem);
		if (buffer_pos_ == available_)
		{
			if (!prefetch()) {
				return 0;
			}
		}

		if (buffer_pos_ + length <= available_)
		{
			CopyByteBuffer(data_ + buffer_pos_, memb + offset, length);
			buffer_pos_ += length;

			total_ += length;

			return length;
		}
		else {
			size_t to_read = available_ - buffer_pos_;

			CopyByteBuffer(data_ + buffer_pos_, memb + offset, to_read);

			buffer_pos_ += to_read;

			total_ += to_read;

			return to_read;
		}

	}


	bool prefetch()
	{
		int result = this->env_->CallIntMethod(this->obj_, Class::instance()->get_read_mid(), buffer_, (jint)0, (jint) capacity_);
		CheckJavaException(this->env_);

		if (result >= 0)
		{
			buffer_pos_ = 0;
			available_ = result;

			return true;
		}
		else {
			available_ = 0;
			eof_ = true;

			return false;
		}
	}
};




template <typename Base>
class OutputStreamHandlerJavaImpl: public Base {
	using typename Base::Class;

	UByte* data_;
	size_t capacity_;
	size_t buffer_pos_ = 0;

	jobject buffer_;

public:
	OutputStreamHandlerJavaImpl(JNIEnv* env, jobject obj): Base(env, obj)
	{
		buffer_   = env->NewGlobalRef(this->createBuffer());

		data_ 	  = T2T<UByte*>(env->GetDirectBufferAddress(buffer_));
		capacity_ = env->GetDirectBufferCapacity(buffer_);
	}

	virtual ~OutputStreamHandlerJavaImpl()
	{
		this->env_->DeleteGlobalRef(buffer_);
	}

	virtual void flush()
	{
		drain();

		this->env_->CallVoidMethod(this->obj_, Class::instance()->get_flush_mid());
		CheckJavaException(this->env_);
	}

	virtual void write(const void* mem, size_t offset, size_t length)
	{
		const UByte* memb = T2T<const UByte*>(mem);

		for (size_t c = 0; c < length;)
		{
			size_t available = capacity_ - buffer_pos_;
			size_t remainder = length - c;

			if (remainder >= available)
			{
				CopyByteBuffer(memb + offset + c, data_ + buffer_pos_, available);
				buffer_pos_ += available;

				drain();

				c += available;
			}
			else {
				CopyByteBuffer(memb + offset + c, data_ + buffer_pos_, remainder);
				buffer_pos_ += remainder;

				c += remainder;
			}
		}
	}

	virtual void close()
	{
		drain();

		this->env_->CallVoidMethod(this->obj_, Class::instance()->get_closeStream_mid());
		CheckJavaException(this->env_);
	}

	void drain()
	{
		if (buffer_pos_ > 0)
		{
			this->env_->CallVoidMethod(this->obj_, Class::instance()->get_consume_mid(), buffer_, 0, (jint) buffer_pos_);
			CheckJavaException(this->env_);

			buffer_pos_ = 0;
		}
	}
};


}
}
