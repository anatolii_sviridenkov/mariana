
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/types/types.hpp>
#include <memoria/v1/core/types/type2type.hpp>

#include <mariana/exceptions.hpp>
#include <mariana/io_buffer/iobuffer_decls.hpp>

#include <jni.h>

namespace mariana {
namespace v1 {

using namespace memoria::v1;

class ByteBufferU: public MarianaIOBuffer {
	using Base = MarianaIOBuffer;

	using Base::array_;
	using Base::limit_;


public:

	ByteBufferU(JNIEnv* env, jobject buffer, size_t limit): ByteBufferU(env, buffer, 0, limit) {}


	ByteBufferU(JNIEnv* env, jobject buffer, size_t pos, size_t limit):
		Base(
			T2T<UByte*>(env->GetDirectBufferAddress(buffer)),
			pos,
			limit
		)
	{
	}


	operator Bytes() {
		return Bytes(array_, limit_, false);
	}
};


using ByteBuffer = ByteBufferU;


}}
