
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once


#include <memoria/v1/core/types/type2type.hpp>
#include <memoria/v1/core/tools/uuid.hpp>

#include <mariana/exceptions.hpp>
#include <mariana/java_object.hpp>
#include <mariana/ctr_types.hpp>

#include <memory>
#include <unordered_map>
#include <functional>
#include <string>

#include <jni.h>

namespace mariana {
namespace v1 {

template <typename Alloc, typename Result = jobject>
class CtrFactory {
public:
	using Snapshot = decltype(std::declval<Alloc>().master());
	using MyType = CtrFactory<Alloc, Result>;

	using CreateFn 		 = std::function<Result (JNIEnv* env, Snapshot&)>;
	using CreateByNameFn = std::function<Result (JNIEnv* env, Snapshot&, const UUID&)>;
	using FindOrCreateFn = std::function<Result (JNIEnv* env, Snapshot&, const UUID&)>;
	using FindFn 		 = std::function<Result (JNIEnv* env, Snapshot&, const UUID&)>;

	template <typename CtrName>
	using CtrT = decltype(std::declval<Snapshot>()->template create<CtrName>());

	template <typename CtrName>
	using IterT = decltype(std::declval<CtrT<CtrName>>()->begin());

private:
	struct Factories {
		CreateFn create_fn_;
		CreateByNameFn create_by_name_fn_;
		FindOrCreateFn find_or_create_fn_;
		FindFn find_fn_;
	};


	std::unordered_map<std::string, Factories> factories_;

	static MyType* instance_;
public:
	CtrFactory() {}

	void register_factories(
			const std::string& 	type,
			CreateFn 			create_fn,
			CreateByNameFn 		create_by_name_fn,
			FindOrCreateFn 		find_or_create_fn,
			FindFn 				find_fn
	)
	{
		factories_[type] = Factories {
			create_fn,
			create_by_name_fn,
			find_or_create_fn,
			find_fn
		};
	}

	auto create(const std::string& type, JNIEnv* env, Snapshot& snp)
	{
		return factory(type).create_fn_(env, snp);
	}

	auto create(const std::string& type, JNIEnv* env, Snapshot& snp, const UUID& name)
	{
		return factory(type).create_by_name_fn_(env, snp, name);
	}

	auto find_or_create(const std::string& type, JNIEnv* env, Snapshot& snp, const UUID& name)
	{
		return factory(type).find_or_create_fn_(env, snp, name);
	}

	auto find(const std::string& type, JNIEnv* env, Snapshot& snp, const UUID& name)
	{
		return factory(type).find_fn_(env, snp, name);
	}

	static MyType* instance() {return instance_;}

	static void create_instance() {
		instance_ = new MyType();
	}

	static void destroy_instance() {
		delete instance_;
	}

private:
	Factories& factory(const std::string& type)
	{
		auto i = factories_.find(type);
		if ( i != factories_.end())
		{
			return i->second;
		}
		else {
			throw Exception(MA_RAW_SRC, SBuf() << "Container type '" << type << "' is not registered");
		}
	}
};

template <typename Alloc, typename Result>
CtrFactory<Alloc, Result>* CtrFactory<Alloc, Result>::instance_ = nullptr;



using InMemCtrFactory 		= CtrFactory<InMemAllocator>;

}}
