
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/memoria.hpp>
#include <memoria/v1/allocators/persistent-inmem/allocator.hpp>
#include <memoria/v1/core/tools/type_name.hpp>

#include <memoria/v1/core/types/type2type.hpp>
#include <memoria/v1/core/tools/uuid.hpp>


#include <mariana/ctr_types.hpp>
#include <mariana/exceptions.hpp>
#include <mariana/java_object.hpp>
#include <mariana/io_stream.hpp>
#include <mariana/ctr_factory.hpp>

#include <mariana/string/utf8_string_codec.hpp>
#include <mariana/io_buffer/iobuffer_consumer.hpp>
#include <mariana/io_buffer/iobuffer_producer.hpp>
#include <mariana/io_buffer/iobuffer_btflwalker.hpp>

#include <memory>
#include <unordered_map>
#include <functional>
#include <string>

#include <jni.h>

namespace mariana {
namespace v1 {

using namespace memoria::v1;

void InitJavaPairs(JNIEnv* env);
void DestroyJavaPairs(JNIEnv* env);
void InitContainerMetadata(JNIEnv* env);
void DumpKnownContainers(JNIEnv* env);

void RegisterContainerFactories(JNIEnv* env);

void RegisterNatives(JNIEnv* env);
void UnregisterNatives(JNIEnv* env);

}}
