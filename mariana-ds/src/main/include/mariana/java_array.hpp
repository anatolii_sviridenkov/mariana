
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/tools/pair.hpp>
#include <memoria/v1/core/types/type2type.hpp>

#include <mariana/exceptions.hpp>
#include <mariana/tools.hpp>
#include <mariana/java_value.hpp>
#include <mariana/string/utf8_string.hpp>

#include <memory>

#include <jni.h>

namespace mariana {
namespace v1 {

using namespace memoria::v1;

template <typename CppType, typename JniType>
class JavaArray;

template <>
class JavaArray<U8String, jobjectArray>{

	jobjectArray array_;
	JNIEnv* env_;
	jsize size_;

public:
	using Value = U8String;

	JavaArray(JNIEnv* env, jobjectArray array): env_(env)
	{
		array_ = (jobjectArray)env->NewGlobalRef(array);
		size_  = env->GetArrayLength(array);
	}

	Value get(int idx) const
	{
		jstring str = T2T<jstring>(env_->GetObjectArrayElement(array_, idx));

		if (!str)
		{
			throw Exception(MA_RAW_SRC, "Null value is not supported");
		}

		return Value(env_, str);
	}


	void set(int idx, const char* value)
	{
		LocalRef<jstring> str(env_, env_->NewStringUTF(value));

		set(idx, str.value());
	}

	void set(int idx, const std::string& value)
	{
		set(idx, value.c_str());
	}

	void set(int idx, jstring value)
	{
		env_->SetObjectArrayElement(array_, idx, value);

		CheckJavaException(env_);
	}

	~JavaArray()
	{
		this->env_->DeleteGlobalRef(array_);
	}

	jsize size() const {return size_;}
};


}}
