
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/tools/pair.hpp>
#include <memoria/v1/core/types/type2type.hpp>

#include <mariana/exceptions.hpp>
#include <mariana/tools.hpp>
#include <mariana/string/utf8_string.hpp>

#include <memory>

#include <jni.h>

namespace mariana {
namespace v1 {

using namespace memoria::v1;

template <typename T> class JavaValue;

template <>
class JavaValue<jstring> {

	using MyType = JavaValue<jstring>;

	JNIEnv* env_;
	jstring string_;

	const char* utf_string_ = nullptr;
	jboolean is_copy_ = false;
	bool owner_ = true;
public:
	JavaValue(JNIEnv* env, jstring string):
		env_(env), string_(string), utf_string_(env->GetStringUTFChars(string, &is_copy_))
	{}

	JavaValue(const MyType& other) = delete;

	JavaValue(MyType&& other)
	{
		env_ 		= other.env_;
		string_ 	= other.string_;
		utf_string_ = other.utf_string_;
		is_copy_ 	= other.is_copy_;
		owner_ 		= other.owner_;

		other.owner_ = false;
	}

	~JavaValue()
	{
		if (owner_)
		{
			env_->ReleaseStringUTFChars(string_, utf_string_);
			env_->DeleteLocalRef(string_);
		}
	}

	jstring value() {return string_;}
	const jstring value() const {return string_;}

	operator const char*() {
		return utf_string_;
	}

	operator std::string() const {
		return std::string(utf_string_);
	}
};

template <typename T> struct JavaValueFactory;

template <typename NativeT_, typename JavaT_>
struct PrimitiveJavaValueFactory {

	using NativeT 	= NativeT_;
	using JavaT 	= JavaT_;

	NativeT make(JNIEnv* env, JavaT value)
	{
		return value;
	}
};

template <>
struct JavaValueFactory<jboolean>: PrimitiveJavaValueFactory<bool, jboolean> {};

template <>
struct JavaValueFactory<jbyte>: PrimitiveJavaValueFactory<Byte, jbyte> {};

template <>
struct JavaValueFactory<jchar>: PrimitiveJavaValueFactory<UShort, jchar> {};

template <>
struct JavaValueFactory<jshort>: PrimitiveJavaValueFactory<Short, jshort> {};

template <>
struct JavaValueFactory<jint>: PrimitiveJavaValueFactory<Int, jint> {};

template <>
struct JavaValueFactory<jlong>: PrimitiveJavaValueFactory<BigInt, jlong> {};

template <>
struct JavaValueFactory<jfloat>: PrimitiveJavaValueFactory<float, jfloat> {};

template <>
struct JavaValueFactory<jdouble>: PrimitiveJavaValueFactory<double, jdouble> {};


template <>
struct JavaValueFactory<jstring> {

	using NativeT 	= JavaValue<jstring>;
	using JavaT 	= jstring;

	NativeT make(JNIEnv* env, JavaT value)
	{
		return NativeT(env, value);
	}
};


template <typename T>
class LocalRef {
	JNIEnv* env_;
	T value_;
public:
	LocalRef(JNIEnv* env, T value): env_(env), value_(value) {}

	~LocalRef()
	{
		env_->DeleteLocalRef(value_);
	}

	JNIEnv* env() {return env_;}
	T value() {return value_;}
};



}}
