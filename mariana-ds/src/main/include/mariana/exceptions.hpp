
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/exceptions/exceptions.hpp>
#include <memoria/v1/core/types/type2type.hpp>
#include <memoria/v1/core/packed/tools/packed_allocator_types.hpp>

#include <exception>

#include <jni.h>

namespace mariana {
namespace v1 {

using namespace memoria::v1;


class JavaException: public memoria::v1::Exception {

	JNIEnv* env_;

public:

	JavaException(const char* source, JNIEnv* env, StringRef message): memoria::v1::Exception(source, message)   {}
	JavaException(const char* source, JNIEnv* env, const SBuf& message): memoria::v1::Exception(source, message) {}

	JNIEnv* env() {return env_;}
	const JNIEnv* env() const {return env_;}
};


class ExceptionsClasses {
	jclass mariana_exception_;

	static ExceptionsClasses* instance_;

public:
	ExceptionsClasses(JNIEnv* env)
	{
		mariana_exception_ = find_class(env, "mariana/bridge/MarianaException");
	}

	jclass mariana_exception() {return mariana_exception_;}
	const jclass mariana_exception() const {return mariana_exception_;}

	static ExceptionsClasses* instance() {return instance_;}

	void destroy(JNIEnv* env)
	{
		env->DeleteGlobalRef(mariana_exception_);
	}

	static void destroy_instance(JNIEnv* env) {
		instance_->destroy(env);
		delete instance_;
	}

	static void create_instance(JNIEnv* env) {
		instance_ = new ExceptionsClasses(env);
	}

	static jclass find_class(JNIEnv* env, const char* class_name)
	{
		jclass clazz = env->FindClass(class_name);

		if (clazz)
		{
			return T2T<jclass>(env->NewGlobalRef(clazz));
		}
		else {
			throw JavaException(MA_RAW_SRC, env, SBuf() << "Can't find class " << class_name);
		}
	}
};



struct ExHelper {
	template <typename Fn>
	static auto handle(JNIEnv* env, Fn&& fn)
	{
		using RtnType = decltype(fn());

		try {
			return fn();
		}
		catch (Exception& ex)
		{
			if (!env->ExceptionCheck())
			{
				String src = ex.source();
				String msg = ex.message();

				String message = src + ": " + msg;

				env->ThrowNew(ExceptionsClasses::instance()->mariana_exception(), message.c_str());
			}
		}
		catch (PackedOOMException& ex)
		{
			if (!env->ExceptionCheck())
			{
				env->ThrowNew(ExceptionsClasses::instance()->mariana_exception(), ex.source());
			}
		}
		catch (std::exception& ex)
		{
			if (!env->ExceptionCheck())
			{
				env->ThrowNew(ExceptionsClasses::instance()->mariana_exception(), ex.what());
			}
		}
		catch (...)
		{
			if (!env->ExceptionCheck())
			{
				env->ThrowNew(ExceptionsClasses::instance()->mariana_exception(), "Unrecognized exception");
			}
		}

		return RtnType();
	}
};

inline void CheckJavaException(JNIEnv* env)
{
	if (env->ExceptionCheck())
	{
		throw JavaException(MA_RAW_SRC, env, SBuf() << "Exception in java code. Check JavaVM state for details.");
	}
}

}}
