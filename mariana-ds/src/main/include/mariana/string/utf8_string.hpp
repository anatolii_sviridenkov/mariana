
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/types/types.hpp>
#include <memoria/v1/core/types/type2type.hpp>

#include <mariana/exceptions.hpp>

#include <ostream>

#include <jni.h>

namespace mariana {
namespace v1 {

using namespace memoria::v1;

class U8String {
public:
	using CharT 	= char;
private:
	using MyType 	= U8String;

	const CharT* data_;
	size_t length_;
	JNIEnv* env_;
	jstring string_;
	bool owner_;

public:
	U8String():
		data_(nullptr), length_(0),
		env_(nullptr), string_(nullptr),
		owner_(false)
	{}

	U8String(JNIEnv* env, jstring string): env_(env)
	{
		alloc_jni(string);
	}

	U8String(const CharT* data, size_t length, bool owner = false):
		length_(length),
		env_(nullptr), string_(nullptr),
		owner_(owner)
	{
		if (!owner)
		{
			data_ = data;
		}
		else {
			CharT* tmp = alloc(length_);

			CopyBuffer(data, tmp, length_);

			data_ = tmp;
		}
	}

	U8String(const U8String& other): U8String(other.data_, other.length_, true) {}

	U8String(U8String&& other):
		data_(other.data_), length_(other.length_),
		env_(other.env_), string_(other.string_),
		owner_(other.owner_)
	{
		other.owner_ = false;
	}

	~U8String()
	{
		if (owner_)
		{
			release();
		}
	}

	jstring java_string() {
		return string_;
	}

	const CharT* data() const {
		return data_;
	}

	size_t length() const {
		return length_;
	}

	size_t size() const {
		return length_;
	}

	operator std::string() const
	{
		return std::string(data_, length_);
	}


	MyType& operator=(const MyType& other)
	{
		if (owner_)
		{
			release();
		}

		CharT* tmp = alloc(other.length_);

		CopyBuffer(other.data_, tmp, other.length_);

		data_ 	= tmp;
		length_ = other.length_;

		owner_ = true;

		return *this;
	}

	MyType& operator=(MyType&& other)
	{
		if (owner_)
		{
			release();
		}

		env_ 	= other.env_;
		string_ = other.string_;

		data_ 	= other.data_;
		length_ = other.length_;

		owner_ 	= other.owner_;

		other.owner_ = false;

		return *this;
	}


	const CharT& operator[](size_t idx) const {
		return data_[idx];
	}

	bool operator!=(const MyType& other) const {
		return !operator==(other);
	}


	bool operator==(const MyType& other) const
	{
		if (length_ == other.length_)
		{
			for (size_t c = 0; c < length_; c++)
			{
				if (data_[c] != other.data_[c])
				{
					return false;
				}
			}

			return true;
		}

		return false;
	}

	bool operator<(const MyType& other) const
	{
		return std::lexicographical_compare(data_, data_ + length_, other.data_, other.data_ + other.length_);
	}

	bool operator<=(const MyType& other) const
	{
		return operator<(other) || operator==(other);
	}

	bool operator>(const MyType& other) const
	{
		return !operator<=(other);
	}

	bool operator>=(const MyType& other) const
	{
		return !operator<(other);
	}

	void swap(MyType& other)
	{
		std::swap(data_, other.data_);
		std::swap(length_, other.length_);
		std::swap(env_, other.env_);
		std::swap(string_, other.string_);
		std::swap(owner_, other.owner_);
	}

private:
	static CharT* alloc(size_t length)
	{
		CharT* tmp = T2T<CharT*>(malloc(length));

		if (tmp == nullptr)
		{
			throw Exception(MA_RAW_SRC, SBuf() << "Can't allocate data buffer for U8String of size " << length );
		}

		return tmp;
	}

	void release()
	{
		if (string_)
		{
			env_->ReleaseStringUTFChars(string_, data_);
			env_->DeleteGlobalRef(string_);
			string_ = nullptr;
		}
		else {
			::free(const_cast<CharT*>(data_));
		}

		data_ = nullptr;
	}

	void alloc_jni(jstring str)
	{
		string_ = (_jstring *) env_->NewGlobalRef(str);

		if (!string_)
		{
			CheckJavaException(env_);
		}

		data_ = env_->GetStringUTFChars(string_, nullptr);
		if (!data_)
		{
			CheckJavaException(env_);
		}

		length_ = env_->GetStringUTFLength(string_);

		owner_ = true;
	}
};


inline std::ostream& operator<<(std::ostream& out, const U8String& str)
{
	out << (std::string)str;
	return out;
}


}}

namespace memoria {
namespace v1 {

template <typename T> struct TypeHash;

template <>
struct TypeHash<mariana::v1::U8String> {
	static constexpr UInt Value = TypeHash<String>::Value;
};


}
}
