
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#pragma once

#include <memoria/v1/core/types/types.hpp>
#include <memoria/v1/core/types/type2type.hpp>

#include <memoria/v1/core/tools/uuid.hpp>
#include <memoria/v1/core/tools/static_array.hpp>
#include <memoria/v1/core/tools/fixed_array.hpp>

#include <memoria/v1/allocators/persistent-inmem/allocator.hpp>

#include <mariana/exceptions.hpp>
#include <mariana/byte_buffer.hpp>
#include <mariana/string/utf8_string.hpp>
//#include <mariana/fixed/fixed_array.hpp>

#include <memory>
#include <unordered_map>
#include <functional>

#include <jni.h>

namespace mariana {
namespace v1 {

using namespace memoria::v1;
using namespace memoria::v1::core;

template <typename Buffer, typename T>
T read(Buffer& buffer, TypeTag<T>);

template <typename T>
T read(JNIEnv* env, jobject buffer, jint limit)
{
	ByteBuffer buf_wrapper(env, buffer, 0, limit);

	return read(buf_wrapper, TypeTag<T>());
}



template <typename Buffer, typename T>
void write(Buffer& buffer, const T& value);


template <typename Buffer>
UUID read(Buffer& buffer, TypeTag<UUID>)
{
	UBigInt hi = buffer.getUBigInt();
	UBigInt lo = buffer.getUBigInt();

	return UUID(hi, lo);
}

template <typename Buffer>
void write(Buffer& buffer, const UUID& value)
{
	buffer.put((UBigInt) value.hi());
	buffer.put((UBigInt) value.lo());
}

template <typename Buffer, typename ID>
void write(Buffer& buffer, const persistent_inmem::SnapshotMetadata<ID>& value)
{
	write(buffer, value.parent_id());
	write(buffer, value.snapshot_id());

	buffer.put((Int)value.status());

	buffer.put(value.description());

	buffer.put((Int)value.children().size());

	for (const auto child: value.children()) {
		write(buffer, child);
	}
}

template <typename T>
jlong to_handle(const std::shared_ptr<T>& ptr)
{
	return T2T<jlong>(new std::shared_ptr<T>(ptr));
}

template <typename T>
T& to_ref(jlong handle)
{
	return *T2T<T*>(handle);
}

template <typename T>
T* to_ptr(jlong handle)
{
	return T2T<T*>(handle);
}

template <typename T, typename... Args>
PairPtr make_pair_object(Args&&... args)
{
	return std::make_unique<T>(std::forward<Args>(args)...);
}


class UTFStringHandler {
	JNIEnv* env_;
	jstring string_;
	const char* utf_string_;
	jboolean is_copy_;
	bool owner_ = true;

public:
	UTFStringHandler(JNIEnv* env, jstring string):
		env_(env), string_(string), utf_string_(env->GetStringUTFChars(string, &is_copy_))
	{}

	UTFStringHandler(const UTFStringHandler&) = delete;

	UTFStringHandler(UTFStringHandler&& other)
	{
		env_ 		= other.env_;
		string_ 	= other.string_;
		utf_string_ = other.utf_string_;
		is_copy_ 	= other.is_copy_;
		owner_ 		= other.owner_;

		other.owner_ = false;
	}

	~UTFStringHandler()
	{
		if (owner_) {
			env_->ReleaseStringUTFChars(string_, utf_string_);
			env_->DeleteLocalRef(string_);
		}
	}

	jstring string() {return string_;}
	const jstring string() const {return string_;}

	const char* utf_string() const {return utf_string_;}

	operator const char*() const {
		return utf_string();
	}

	operator std::string() const {
		return std::string(utf_string());
	}

	jboolean is_copy() const {return is_copy_;}
};



struct JStringType {};
struct ByteBufferType {};

struct ByteArrayType {};
struct CharArrayType {};
struct ShortArrayType {};
struct IntArrayType {};
struct FloatArrayType {};
struct DoubleArrayType {};
struct BooleanArrayType {};



template <typename T>
struct DirectInputParam {
	template <typename TT>
	static T wrap(JNIEnv* env, TT& val) {
		return val;
	}
};

template <>
struct DirectInputParam<UTFStringHandler> {
	template <typename TT>
	static auto wrap(JNIEnv* env, TT& val) {
		return UTFStringHandler(env, val);
	}
};

template <>
struct DirectInputParam<U8String> {
	template <typename TT>
	static auto wrap(JNIEnv* env, TT& val) {
		return U8String(env, val);
	}
};


template <>
struct DirectInputParam<Bytes> {
	template <typename TT>
	static auto wrap(JNIEnv* env, TT& val, int pos, int limit, const ByteBufferType&) {
		return ByteBufferU(env, val, pos, limit);
	}


	static auto wrap(JNIEnv* env, jbyteArray array, int pos, int length, const ByteArrayType&)
	{
		Bytes buf(length);

		env->GetByteArrayRegion(array, pos, length, T2T<jbyte*>(buf.data()));

		CheckJavaException(env);

		return buf;
	}
};



template <Int Size>
struct DirectInputParam<FixedArray<Size>> {

	static auto wrap(JNIEnv* env, jobject buffer, int pos, int limit, const ByteBufferType&)
	{
		if (limit - pos <= Size)
		{
			UByte* data = T2T<UByte*>(env->GetDirectBufferAddress(buffer)) + pos;

			FixedArray<Size> array;

			for (Int c = 0; c < limit - pos; c++)
			{
				array[c] = data[c];
			}

			return array;
		}
		else {
			throw Exception(MA_RAW_SRC, SBuf() << "DirectByteBuffer is too large for FixedArray<" << Size << ">");
		}
	}


	static auto wrap(JNIEnv* env, jbyteArray array, int pos, int length, const ByteArrayType&)
	{
		if (length <= Size)
		{
			FixedArray<Size> fxarray;

			env->GetByteArrayRegion(array, pos, length, T2T<jbyte*>(fxarray.data()));

			CheckJavaException(env);

			return fxarray;
		}
		else {
			throw Exception(MA_RAW_SRC, SBuf() << "Java array of " << length << " elements is too large for FixedArray<" << Size << ">");
		}
	}
};




template <typename T>
struct DirectOutputParam {
	template <typename TT>
	static T wrap(JNIEnv* env, TT& val) {
		return val;
	}
};

template <>
struct DirectOutputParam<JStringType> {
	static auto wrap(JNIEnv* env, const std::string& val) {
		return env->NewStringUTF(val.c_str());
	}

	static auto wrap(JNIEnv* env, const StaticVector<std::string, 1>& val) {
		return env->NewStringUTF(val[0].c_str());
	}

	static auto wrap(JNIEnv* env, StaticVector<U8String, 1>&& val) {
		return wrap(env, std::move(val[0]));
	}

	static jstring wrap(JNIEnv* env, U8String&& val)
	{
		if (!val.java_string())
		{
			if (val.length() < 512)
			{
				char data[512];
				CopyBuffer(val.data(), data, val.length());
				data[val.length()] = 0;

				return env->NewStringUTF(data);
			}
			else {
				auto ptr = std::make_unique<char[]>(val.length() + 1);

				CopyBuffer(val.data(), ptr.get(), val.length());
				ptr.get()[val.length()] = 0;

				return env->NewStringUTF(ptr.get());
			}
		}
		else
		{
			return val.java_string();
		}
	}

	static auto wrap(JNIEnv* env, const char* val) {
		return env->NewStringUTF(val);
	}
};


template <>
struct DirectOutputParam<ByteBufferType> {
	static auto wrap(JNIEnv* env, Bytes&& val)
	{
		return env->NewDirectByteBuffer(val.take(), val.size());
	}
};


template <>
struct DirectOutputParam<ByteArrayType> {
	static auto wrap(JNIEnv* env, Bytes&& val)
	{
		jbyteArray array = env->NewByteArray(val.size());

		env->SetByteArrayRegion(array, 0, val.size(), T2T<jbyte*>(val.data()));

		return array;
	}

	static auto wrap(JNIEnv* env, const memoria::v1::core::StaticVector<Bytes, 1>& val)
	{
		jbyteArray array = env->NewByteArray(val[0].size());

		env->SetByteArrayRegion(array, 0, val[0].size(), T2T<jbyte*>(val[0].data()));

		return array;
	}

	template <Int Size>
	static auto wrap(JNIEnv* env, const memoria::v1::core::StaticVector<FixedArray<Size>, 1>& val) {
		return wrap(env, val[0]);
	}

	template <Int Size>
	static auto wrap(JNIEnv* env, const FixedArray<Size>& val)
	{
		jbyteArray array = env->NewByteArray(Size);

		env->SetByteArrayRegion(array, 0, Size, T2T<jbyte*>(val.data()));

		return array;
	}
};



template <typename T = EmptyType>
struct ReturnValueConverter {
	template <typename TT>
	static TT convert(JNIEnv* env, TT& v) {
		return v;
	}
};

template <>
struct ReturnValueConverter<jobjectArray> {

	static jobjectArray convert(JNIEnv* env, jobjectArray& v) {
		return v;
	}

	static jobjectArray convert(JNIEnv* env, jobject v) {
		return T2T<jobjectArray>(v);
	}
};


template <typename PairClass>
jobject create_java_pair(JNIEnv* env, long handle)
{
	return PairClass::instance()->new_instance(env, handle);
}

template <typename PairClass, typename T>
jobject create_java_pair(JNIEnv* env, const std::shared_ptr<T>& handle)
{
	return PairClass::instance()->new_instance_for(env, handle);
}


}}
