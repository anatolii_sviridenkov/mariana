
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <mariana/java_object.hpp>
#include <mariana/byte_buffer.hpp>
#include <mariana/tools.hpp>
#include <mariana/exceptions.hpp>
#include <mariana/snapshot.hpp>
#include <mariana/ctr_factory.hpp>

#include <memoria/v1/memoria.hpp>
#include <memoria/v1/allocators/persistent-inmem/allocator.hpp>
#include <memoria/v1/core/tools/type_name.hpp>

#include "mariana/global_declarations.hpp"

#include "generated_headers.hpp"

using namespace mariana::v1;
using namespace memoria::v1;




#define PAIR_CLASS(Method) Java_mariana_v1_inmem_alloc_InMemorySnapshot_##Method


extern "C" {



JNIEXPORT jobject JNICALL PAIR_CLASS(create__JLjava_lang_String_2Ljava_nio_ByteBuffer_2I)
  (JNIEnv* env, jclass, jlong handle, jstring type_name, jobject buffer, jint limit)
{
	return ExHelper::handle(env, [&]{
		auto& ref = to_ref<InMemSnapshotPtr>(handle);
		auto uuid = read<UUID>(env, buffer, limit);

		UTFStringHandler type_name_h(env, type_name);

		return InMemCtrFactory::instance()->create(type_name_h, env, ref, uuid);
	});
}


JNIEXPORT jobject JNICALL PAIR_CLASS(create__JLjava_lang_String_2)
  (JNIEnv* env, jclass, jlong handle, jstring type_name)
{
	return ExHelper::handle(env, [&]{
		auto& ref = to_ref<InMemSnapshotPtr>(handle);

		UTFStringHandler type_name_h(env, type_name);

		return InMemCtrFactory::instance()->create(type_name_h, env, ref);
	});
}


JNIEXPORT jobject JNICALL PAIR_CLASS(findOrCreate)
  (JNIEnv* env, jclass, jlong handle, jstring type_name, jobject buffer, jint limit)
{
	return ExHelper::handle(env, [&]{
		auto& ref = to_ref<InMemSnapshotPtr>(handle);
		auto uuid = read<UUID>(env, buffer, limit);

		UTFStringHandler type_name_h(env, type_name);

		return InMemCtrFactory::instance()->find_or_create(type_name_h, env, ref, uuid);
	});
}


JNIEXPORT jobject JNICALL PAIR_CLASS(find)
  (JNIEnv* env, jclass, jlong handle, jstring type_name, jobject buffer, jint limit)
{
	return ExHelper::handle(env, [&]{
		auto& ref = to_ref<InMemSnapshotPtr>(handle);
		auto uuid = read<UUID>(env, buffer, limit);

		UTFStringHandler type_name_h(env, type_name);

		return InMemCtrFactory::instance()->find(type_name_h, env, ref, uuid);
	});
}




JNIEXPORT void JNICALL PAIR_CLASS(dump)
  (JNIEnv* env, jclass, jlong handle, jstring destination)
{
	return ExHelper::handle(env, [&]{
		UTFStringHandler file_name(env, destination);

		auto& snp = to_ref<InMemSnapshotPtr>(handle);

		snp->dump(file_name.utf_string());
	});
}




#undef PAIR_CLASS

}
