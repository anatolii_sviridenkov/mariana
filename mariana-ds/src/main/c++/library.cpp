
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <memoria/v1/memoria.hpp>

#include <memoria/v1/core/tools/bitmap.hpp>

#include <mariana/byte_buffer.hpp>
#include <mariana/exceptions.hpp>
#include <mariana/snapshot.hpp>
#include <mariana/inmem_allocator.hpp>

#include <stdio.h>
#include <iostream>

#include "generated_headers.hpp"
#include "mariana_v1_io_buffer_IOByteBuffer.h"

using namespace memoria::v1;
using namespace std;
using namespace mariana::v1;

extern "C" {


JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    JNIEnv* env = NULL;
    jint result = -1;
    if (vm->GetEnv((void**) &env, JNI_VERSION_1_8) != JNI_OK)
    {
        cout << "GetEnv failed!" << endl;
        return result;
    }

    return ExHelper::handle(env, [&]{
    	MEMORIA_INIT(DefaultProfile<>);

    	ExceptionsClasses::create_instance(env);
    	CtrFactory<PersistentInMemAllocator<>>::create_instance();

    	InitContainerMetadata(env);
    	RegisterContainerFactories(env);

    	InitJavaPairs(env);

    	RegisterNatives(env);

    	return JNI_VERSION_1_8;
    });
}


JNIEXPORT void JNI_OnUnload(JavaVM* vm, void* reserved)
{
    JNIEnv* env = NULL;

    if (vm->GetEnv((void**) &env, JNI_VERSION_1_8) != JNI_OK)
    {
    	ExHelper::handle(env, [&] {
    		UnregisterNatives(env);

    		DestroyJavaPairs(env);

    		InMemCtrFactory::destroy_instance();
    		ExceptionsClasses::destroy_instance(env);

    		MetadataRepository<DefaultProfile<>>::cleanup();
    	});
    }
}

JNIEXPORT void JNICALL Java_mariana_v1_Mariana_dumpCounters (JNIEnv *, jclass)
{
	cout << "DebugCounters: " << DebugCounter << " " << DebugCounter1 << " " << DebugCounter2 << endl;
}


JNIEXPORT void JNICALL Java_mariana_v1_Mariana_testBB
  (JNIEnv* env, jclass, jobject bb)
{
	 UByte* data = (UByte*)env->GetDirectBufferAddress(bb);
	 jlong size = env->GetDirectBufferCapacity(bb);

	 cout  << "testBB: " << data << " " << size << endl;
}



JNIEXPORT void JNICALL Java_mariana_v1_io_buffer_IOByteBuffer_moveDirect
  (JNIEnv *, jclass, jobject, jint, jint, jint);


JNIEXPORT void JNICALL Java_mariana_v1_io_buffer_IOByteBuffer_moveDirect
  (JNIEnv * env, jclass, jobject bb, jint from , jint to, jint length)
{
	UByte* data = (UByte*)env->GetDirectBufferAddress(bb);

	CopyBuffer(data + from, data + to, length);
}


JNIEXPORT void JNICALL Java_mariana_v1_Mariana_dumpKnownContainers
  (JNIEnv * env, jclass)
{
	DumpKnownContainers(env);
}

}

