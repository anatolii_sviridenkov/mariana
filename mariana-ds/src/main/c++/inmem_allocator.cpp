
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <mariana/java_object.hpp>
#include <mariana/byte_buffer.hpp>
#include <mariana/tools.hpp>
#include <mariana/exceptions.hpp>
#include <mariana/inmem_allocator.hpp>

#include <memoria/v1/memoria.hpp>
#include <memoria/v1/allocators/persistent-inmem/allocator.hpp>
#include <memoria/v1/core/tools/type_name.hpp>

#include "mariana/global_declarations.hpp"

#include "generated_headers.hpp"

using namespace mariana::v1;
using namespace memoria::v1;


#define PAIR_CLASS(Method) Java_mariana_v1_inmem_alloc_InMemoryAllocator_##Method

extern "C" {

JNIEXPORT jlong JNICALL PAIR_CLASS(createPair)
  (JNIEnv* env, jclass)
{
	return ExHelper::handle(env, [&]{
		return to_handle(InMemAllocator::create());
	});
}



JNIEXPORT jlong JNICALL PAIR_CLASS(loadFile)
  (JNIEnv* env, jclass, jstring file)
{
	return ExHelper::handle(env, [&]{
		UTFStringHandler file_name(env, file);

		auto in = FileInputStreamHandler::create(file_name.utf_string());

		return to_handle(InMemAllocator::load(in.get()));
	});
}

JNIEXPORT jlong JNICALL PAIR_CLASS(loadStream)
  (JNIEnv* env, jclass, jlong streamHandle)
{
	return ExHelper::handle(env, [&]{

		auto alloc = mariana::v1::InMemAllocator::load(
			to_ptr<mariana::v1::InputStreamHandlerJavaImpl<mariana_v1_io_AbstractInputBufferStreamJavaObject>>(streamHandle)
		);

		return to_handle(alloc);
	});
}


JNIEXPORT void JNICALL PAIR_CLASS(store)
  (JNIEnv* env, jclass, jlong handle, jstring file)
{
	return ExHelper::handle(env, [&]{
		UTFStringHandler file_name(env, file);

		auto& alloc = to_ref<InMemAllocatorPtr>(handle);
		auto out = FileOutputStreamHandler::create(file_name.utf_string());

		alloc->store(out.get());
	});
}


JNIEXPORT void JNICALL PAIR_CLASS(dump)
  (JNIEnv* env, jclass, jlong handle, jstring destination)
{
	return ExHelper::handle(env, [&]{
		UTFStringHandler file_name(env, destination);

		auto& alloc = to_ref<InMemAllocatorPtr>(handle);

		FSDumpAllocator(alloc, file_name.utf_string());
	});
}


#undef PAIR_CLASS

}
