
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package memoria.v1;

import java.io.File;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import mariana.bridge.ImportedMethod;
import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.alloc.SnapshotDescriptor;
import mariana.v1.api.ctr.map.str.str.MapSS;
import mariana.v1.api.ctr.map.str.str.MapSSIterator;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.inmem.ctr.map.InMemMapSS;
import mariana.v1.inmem.ctr.map.InMemMapSS.MapSSIteratorImpl;
import mariana.v1.io.FileInputBuffer;
import mariana.v1.io.FileOutputBuffer;

@RunWith(JUnit4.class)
public class AllocatorTest 
{
	static
	{
		Mariana.init();
	}

	public String getName() {
		return this.getClass().getSimpleName();
	}
	
	@Test
	public final void testSnapshot() throws Exception
	{
		UUID name = null;
		
		int size = 100;
		String folder = "target" + File.separator + getName() + File.separator + "testSnapshot" + File.separator;
		
		new File(folder).mkdirs();
		
		try(InMemoryAllocator alloc = InMemoryAllocator.create()) 
		{
			try(Snapshot master = alloc.master()) 
			{
				String meta = "Some Text";
				try(Snapshot branch = master.branch()) 
				{
					branch.setMetadata(meta);
					
					SnapshotDescriptor mdescr = alloc.describe(master.uuid());
					
					Assert.assertEquals(mdescr.getSnapshotId(), master.uuid());
					Assert.assertEquals(mdescr.getParentId(), new UUID(0, 0));
					Assert.assertEquals(mdescr.getChildren().size(), 1);
					Assert.assertEquals(mdescr.getChildren().get(0), branch.uuid());
					
					SnapshotDescriptor descr = alloc.describe(branch.uuid());
					
					Assert.assertEquals(descr.getDescription(), meta);
					Assert.assertEquals(descr.getChildren().size(), 0);
					Assert.assertEquals(descr.getSnapshotId(), branch.uuid());
					Assert.assertEquals(descr.getParentId(), master.uuid());

					try(MapSS map = branch.create(MapSS.class, UUID.randomUUID())) 
					{
						name = map.name();

						try(MapSSIterator iter = map.begin())
						{
							for (int c = 0; c < size; c++) 
							{
								iter.insert("Boo " + c, "Zoo " + c);
							}

							Assert.assertEquals(size, map.size());
							Assert.assertEquals(size, iter.pos());
						}
					}

					branch.commit();
					branch.setAsMaster();

					try(Snapshot found = alloc.find(branch.uuid())) 
					{					
						Assert.assertEquals(branch.uuid(), found.uuid());
						Assert.assertEquals(branch.metadata(), found.metadata());

						try (MapSS map = found.find(InMemMapSS.class, name)) 
						{
							Assert.assertEquals(size, map.size());

							try(MapSSIteratorImpl iter = (MapSSIteratorImpl) map.begin()) 
							{
								while (!iter.isEnd()) {
									iter.next();
								}

								Assert.assertEquals(size, iter.pos());
							}
						}
					}

					try(FileOutputBuffer fileBuffer = new FileOutputBuffer(folder + "myalloc.dump")) 
					{
						alloc.store(fileBuffer);
					}
				}
			}
		}
		
    	try(FileInputBuffer ibuf = new FileInputBuffer(folder+"myalloc.dump"))
    	{
    		try (InMemoryAllocator alloc = InMemoryAllocator.load(ibuf)) 
    		{
    			try(Snapshot snp = alloc.master()) 
    			{
    				try (MapSS map = snp.find(InMemMapSS.class, name)) 
    				{
						Assert.assertEquals(size, map.size());
						
    					try(MapSSIteratorImpl iter = (MapSSIteratorImpl) map.begin()) 
    					{
    						while (!iter.isEnd()) {
    							iter.next();
    						}
    						
    						Assert.assertEquals(size, iter.pos());
    					}    					
    				}
    			}
    		}
    	}
	}
}
