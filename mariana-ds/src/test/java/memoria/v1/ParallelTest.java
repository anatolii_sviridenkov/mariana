
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package memoria.v1;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.set.bytes.SetBIterator;
import mariana.v1.api.ctr.set.bytes.fx.SetFxB;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.FileOutputBuffer;

@RunWith(JUnit4.class)
public class ParallelTest
{
    static
    {
        Mariana.init();
    }
    
	public String getName() {
		return this.getClass().getSimpleName();
	}
    

    static class Inserter implements Runnable {
    	
    	private Snapshot parent;
    	
    	private UUID name;
    	private byte[][] data;
    	
    	private UUID snapshotId;
    	
    	public Inserter(Snapshot parent, UUID name, byte[][] data) {
			this.parent = parent;
			this.name = name;
			this.data = data;
		}
    	
		@Override
		public void run() 
		{
			try(Snapshot snp = parent.branch()) 
			{
				snapshotId = snp.uuid();
				
				try(SetFxB set = snp.create(SetFxB.class, name)) 
				{
					for (int c = 0; c < data.length; c++) 
					{
						set.insert(ByteBuffer.wrap(data[c]));
					}
				}

				snp.commit();
			}
		}

		public UUID getSnapshotId() {
			return snapshotId;
		}

		public UUID getName() {
			return name;
		}
    }
    
    static class Checker implements Runnable {

    	private Snapshot snp;
    	private UUID ctrName;
		private byte[][] data;
    	
    	public Checker(Snapshot snp, UUID ctrName, byte[][] data) 
    	{
    		this.snp = snp;
			this.ctrName = ctrName;
			this.data = data;
    	}
    	
		@Override
		public void run() 
		{
			try (SetFxB set = snp.find(SetFxB.class, ctrName))
			{
				for (int c = 0; c < data.length; c++) 
				{
					ByteBuffer key = ByteBuffer.wrap(data[c]); 
					try (SetBIterator ii = set.find(key)) 
					{
						Assert.assertTrue(ii.isFound(key));
					}
				}
			}
			finally {
				snp.close();				
			}
		}
    }
    
    
    @Test
	public final void testParallelOperations() throws Exception
    {
    	String folder = "target" + File.separator + getName() + File.separator + "testParallelOperations" + File.separator;    	
		new File(folder).mkdirs();
    	
    	Random RNG = new Random(0);
    	
    	List<UUID> ctrNames = new ArrayList<>();
    	
    	int size 		= 50000;
		int dataLength 	= 20;
		int threads 	= 2;

		byte[][] array = new byte[size][];

		for (int c = 0; c < array.length; c++) 
		{
			byte[] data = new byte[dataLength];
			RNG.nextBytes(data);
			array[c] = data;
		}
    	
    	
    	try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{    			
    			
    			
    			List<Inserter> inserters 	 = new ArrayList<>();
    			List<Thread> inserterThreads = new ArrayList<>();

    			for (int c = 0; c < threads; c++)
    			{
    				UUID ctr1 = UUID.randomUUID();
    				Inserter inserter = new Inserter(master, ctr1, array);
    				
    				inserters.add(inserter);

    				Thread thread = new Thread(inserter);
    				inserterThreads.add(thread);
    				thread.start();
    			}
    			
    			for (Thread th: inserterThreads) {
					th.join();
    			}
    			
    			try(Snapshot snp = master.branch()) 
    			{    				
    				snp.lockDataForImport();
    				
    				for (Inserter inserter: inserters) 
    				{
    					ctrNames.add(inserter.getName());
    					
    					try (Snapshot src = alloc.find(inserter.getSnapshotId())) 
    					{
    						snp.importNewCtrFrom(src, inserter.getName());
    						src.drop();
    					}
    				}
    				
    				snp.commit();
    				snp.setAsMaster();
    			}
    			
    			List<Thread> checkers = new ArrayList<>();
    			
    			for (Inserter inserter: inserters)
				{
    				Checker checker = new Checker(alloc.master(), inserter.getName(), array); 
    				Thread thread = new Thread(checker);
    				checkers.add(thread);
    				thread.start();
				}
    			
    			for (Thread th: checkers) {
					th.join();
    			}
    		}
    		
    		try(FileOutputBuffer fileBuffer = new FileOutputBuffer(folder + "mt.memoria")) 
    		{
    			alloc.store(fileBuffer);
    		}
    	}
    	
    	
    	try (InMemoryAllocator alloc = InMemoryAllocator.load(folder + "mt.memoria")) 
    	{
    		List<Thread> checkers = new ArrayList<>();
    		
    		for (UUID ctrName: ctrNames) 
    		{
    			Checker checker = new Checker(alloc.master(), ctrName, array); 
				Thread thread = new Thread(checker);
				checkers.add(thread);
				thread.start();
    		}
    		
    		for (Thread th: checkers) {
				th.join();
			}
    	}
    }
}
