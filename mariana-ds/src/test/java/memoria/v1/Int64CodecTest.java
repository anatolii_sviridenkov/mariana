
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package memoria.v1;

import java.nio.ByteBuffer;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import mariana.v1.Mariana;
import mariana.v1.tools.codecs.Int64Codec;

@RunWith(JUnit4.class)
public class Int64CodecTest 
{
	static
	{
		Mariana.init();
	}

	@Test
	public void testCodec() 
	{
		Int64Codec codec = new Int64Codec();
		
		ByteBuffer buf = ByteBuffer.allocate(20);
		
		for (int c = -1000000; c < 1000000; c++) 
		{
			int len = codec.length(c);
			
			Assert.assertEquals(len, codec.encode(buf, c, 0));
			Assert.assertEquals(len, codec.decode(buf, 0));
			
			Assert.assertEquals(c, codec.getValue());
		}
	}

}
