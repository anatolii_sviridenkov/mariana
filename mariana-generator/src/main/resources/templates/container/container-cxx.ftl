<#--
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
-->

<#import "/lib/method-jni-lib.ftl" as nm>

//#include "${pair.headerFileName}.hpp"
#include "generated_headers.hpp"

namespace mariana {
namespace v1 {

${pair.cppJavaMetadataClassName}* ${pair.cppJavaMetadataClassName}::instance_ = nullptr;

}}

using namespace mariana::v1;

extern "C" {

<@nm.native_methods pair.imports />

JNIEXPORT jobject JNICALL ${pair.jniPrefix}_getJavaPair (JNIEnv* env, jclass, jlong handle)
{
	return ExHelper::handle(env, [&] () -> jobject {
		if (handle)
		{
			auto& obj = to_ref<${pair.cppClass}>(handle);

			if (obj->pair()) {
				return T2T<jobject>(obj->pair()->ptr());
			}
			else {
				return nullptr;
			}
		}
		else {
			return nullptr;
		}
	});
}


JNIEXPORT void JNICALL ${pair.jniPrefix}_setJavaPair (JNIEnv* env, jclass, jlong handle, jobject pair)
{
	return ExHelper::handle(env, [&]{
		auto& obj = to_ref<${pair.cppClass}>(handle);
		obj->pair() = make_pair_object<${pair.cppJavaObjectName}>(env, pair);
	});
}


JNIEXPORT void JNICALL ${pair.jniPrefix}_destroyPair (JNIEnv* env, jclass, jlong handle)
{
    return ExHelper::handle(env, [&]{        
        delete to_ptr<${pair.cppClass}>(handle);
    });
}



}