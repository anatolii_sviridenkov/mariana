<#--
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
-->


#pragma once

<#import "/lib/callback-lib.ftl" as cb>

#include "mariana/java_object.hpp"
#include "mariana/tools.hpp"
#include "mariana/exceptions.hpp"
#include <mariana/global_declarations.hpp>

<#if pair.hasInclude()>
#include <${pair.include}>
</#if>

namespace mariana {
namespace v1 {

class ${pair.cppJavaMetadataClassName}: public JavaClassMetadataBase {
	using Base = JavaClassMetadataBase;

	static ${pair.cppJavaMetadataClassName}* instance_;
	
	<#list pair.exports as m>
	jmethodID ${m.midName}_mid_;
	</#list>

	${pair.cppJavaMetadataClassName}(JNIEnv* env, const char* name): Base(env, name) 
	{
	<#list pair.exports as m>
		${m.midName}_mid_ = env->GetMethodID(this->clazz(), "${m.javaFnName}", "${m.javaSignature}");
		CheckJavaException(env);
		
	</#list>
	}

public:
	static void create_instance(JNIEnv* env) {
		instance_ = new ${pair.cppJavaMetadataClassName}(env, "${pair.pairJavaJNIClassName}");
	}

	static void destroy_instance(JNIEnv* env) {
		instance_->destroy(env);
		delete instance_;
	}

	static ${pair.cppJavaMetadataClassName}* instance() {
		return instance_;
	}
	
	<#list pair.exports as m>
	jmethodID get_${m.midName}_mid() {return ${m.midName}_mid_;}
	</#list>
};



class ${pair.cppJavaObjectName}: public JavaObjectST<${pair.base}> {
	using Base = JavaObjectST<${pair.base}>;

public:
	using Class = ${pair.cppJavaMetadataClassName};

	${pair.cppJavaObjectName}(JNIEnv* env, jobject obj): Base(env, env->NewGlobalRef(obj)) 
	{
		Class::instance()->set_handle(env, obj, T2T<jlong>(this));
	}
	
    virtual ~${pair.cppJavaObjectName}() 
    {	   
        JNIEnv* env = this->env();
        Class::instance()->clear_handle(env, this->obj());
        env->DeleteGlobalRef(this->obj());
	}
	
	<@cb.callbacks pair.implementableExports pair.cppJavaMetadataClassName/>
};



}}