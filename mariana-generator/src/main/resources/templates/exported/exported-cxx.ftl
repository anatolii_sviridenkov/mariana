<#--
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
-->

<#import "/lib/method-jni-lib.ftl" as nm>

#include "generated_headers.hpp"

namespace mariana {
namespace v1 {

${pair.cppJavaMetadataClassName}* ${pair.cppJavaMetadataClassName}::instance_ = nullptr;

}}

using namespace mariana::v1;

extern "C" {

<@nm.native_methods pair.imports />


JNIEXPORT void JNICALL ${pair.jniPrefix}_createInstance (JNIEnv* env, jobject obj)
{
	using Cpp = ${pair.targetCppClass}<${pair.cppJavaObjectName}>;

	ExHelper::handle(env, [&]{
		new Cpp(env, obj);
	});
}


JNIEXPORT void JNICALL ${pair.jniPrefix}_destroyInstance (JNIEnv* env, jobject obj)
{
	using Cpp = ${pair.targetCppClass}<${pair.cppJavaObjectName}>;

	return ExHelper::handle(env, [&]{
		auto cpp_clazz = ${pair.cppJavaMetadataClassName}::instance();
		jlong handle = cpp_clazz->handle(env, obj);
		delete T2T<Cpp*>(handle);
	});
}

}