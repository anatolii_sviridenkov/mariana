<#--
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
-->

<#import "/lib/method-jni-lib.ftl" as nm>

#include "generated_headers.hpp"

#include <mariana/tools.hpp>
#include <mariana/byte_buffer.hpp>
#include <mariana/global_declarations.hpp>

#include <jni.h>

extern "C" {

<#list pair as p>
<@nm.native_methods_declarations p.imports />
</#list>

}

namespace mariana {
namespace v1 {

void RegisterNatives(JNIEnv* env) {
<#list pair as p>
{
    JNINativeMethod methods_${p.jniPrefix}[] = {
        <#list p.imports as fn>
            {"${fn.javaJniFnName}", "${fn.javaJniSignature}", (void*)${fn.jniFnName}},
        </#list>
    };
    int result = env->RegisterNatives(
        ${p.cppJavaMetadataClassName}::instance()->clazz(),
        methods_${p.jniPrefix},
        sizeof(methods_${p.jniPrefix}) / sizeof(JNINativeMethod)
    );
    
    if (result != 0) {
        CheckJavaException(env);    
    }
}    
</#list>
}

void UnregisterNatives(JNIEnv* env) {
<#list pair as p>
{
    int result = env->UnregisterNatives(
        ${p.cppJavaMetadataClassName}::instance()->clazz()
    );
    
    if (result != 0) {
        CheckJavaException(env);    
    }
}    
</#list>
}

void InitJavaPairs(JNIEnv* env) 
{
<#list pair as p>	
	${p.cppJavaMetadataClassName}::create_instance(env);
</#list>
}

void DestroyJavaPairs(JNIEnv* env) 
{
<#list pair as p>	
	${p.cppJavaMetadataClassName}::destroy_instance(env);
</#list>
}


void InitContainerMetadata(JNIEnv* env) 
{
<#list containers as c>	
	DInit<${c.ctrName}>();
</#list>
}

void DumpKnownContainers(JNIEnv* env) {
<#list containers as c> 
    cout << "DInit<${c.ctrName}>();" << endl;
</#list>
}


void RegisterContainerFactories(JNIEnv* env) 
{
	InMemCtrFactory* factory = InMemCtrFactory::instance();

<#list containers as c>	
	factory->register_factories(
		"${c.ctrName}",
		
		[](JNIEnv* env, auto&& snp) -> jobject {
			return create_java_pair<${c.cppJavaMetadataClassName}>(env, create<${c.ctrName}>(snp));
		},
		
		[](JNIEnv* env, auto&& snp, const UUID& name) -> jobject {
			return create_java_pair<${c.cppJavaMetadataClassName}>(env, create<${c.ctrName}>(snp, name));
		},
		
		[](JNIEnv* env, auto&& snp, const UUID& name) -> jobject {
			return create_java_pair<${c.cppJavaMetadataClassName}>(env, find_or_create<${c.ctrName}>(snp, name));
		}, 
		
		[](JNIEnv* env, auto&& snp, const UUID& name) -> jobject {
			return create_java_pair<${c.cppJavaMetadataClassName}>(env, find<${c.ctrName}>(snp, name));
		}
	);
	
</#list>

}


}}




<#list pair as p>
#include "${p.cppFileName}.cpp-inc"
</#list>
