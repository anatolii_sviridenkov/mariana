<#--
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
-->


<#macro callbacks list cpp_java_metadata_class>
	<#list list as m>
	<#if m.returnsVoid>
	virtual void ${m.cppFnName} (<#list m.parameters as p>${p.cppCBType} ${p.name}<#sep>, </#list>) <#if m.const>const</#if>
	{
		jmethodID mid = ${cpp_java_metadata_class}::instance()->get_${m.midName}_mid(); 

		<#list m.parameters as p>
			<#if p.direct>
			<#else>
				JNIWrapper<${p.cppCBType}> ${p.name}_wrp___(env, p.name());	
			</#if>
		</#list>

		env_->CallVoidMethod<@compress single_line=true>( obj_, mid
			<#list m.parameters as p>
				,<#if p.direct>
					${p.name}
				<#else>
					${p.name}_wrp___.jni();
				</#if>
			</#list>
		);</@compress>

		CheckJavaException(env_);
	}

	<#else>
	virtual ${m.cppReturnType} ${m.cppFnName} (<#list m.parameters as p>${p.cppCBType} ${p.name}<#sep>, </#list>) <#if m.const>const</#if>
	{
		jmethodID mid = ${cpp_java_metadata_class}::instance()->get_${m.midName}_mid(); 

		<#list m.parameters as p>
			<#if p.direct>
			<#else>
				JNIWrapper<${p.cppCBType}> ${p.name}_wrp___(env, ${p.cppName});	
			</#if>
		</#list>

		auto result = env_->Call${m.jniCallType}Method<@compress single_line=true>( obj_, mid
			<#list m.parameters as p>
				,<#if p.direct>
					${p.name}
				<#else>
					${p.name}_wrp___.jni()
				</#if>
			</#list>
		);</@compress>

		CheckJavaException(env_);

		return ReturnValueConverter<${m.returnTypeConverter}>::convert(this->env_, result);
	}

	</#if>
	</#list>
</#macro>