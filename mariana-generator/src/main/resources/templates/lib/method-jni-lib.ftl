<#--
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
-->

<#macro native_call fn structs=false>
		<#if fn.hasStructs || structs>
	 	ByteBuffer buf_wrapper(env__, buffer__, pos__);
	 	</#if>

		<#nested>

		<#if fn.direct || fn.freeFunction>
		<#if fn.direct>
		${fn.owner.castFn}<${fn.owner.cppClass}>(handle__)->${fn.cppFnName} (
        <#else>
        ${fn.cppFnName}(*${fn.owner.castFn}<${fn.owner.cppClass}>(handle__), 
        </#if>        
            env__, clazz__ 
            <#list fn.directParameters as dp>
                , ${dp.name}
            </#list>
            <#if fn.hasStructs>
                <#list fn.structParameters as sp>
                    , read(buf_wrapper, TypeTag<${sp.cppType}>())
                </#list>
            </#if>
        )

		<#else>
		 
        ${fn.owner.castFn}<${fn.owner.cppClass}>(handle__)->${fn.cppFnName}(
			<#list fn.directParameters as dp>
				<#if dp.primitive>
					${dp.name}
				<#elseif dp.callbackObject>
					to_ptr<${dp.cppClass}>(${dp.name})
				<#elseif dp.cppObject>
					to_ref<${dp.cppClass}>(${dp.name})
	            <#elseif dp.directByteBuffer>
                    DirectInputParam<${dp.cppType}>::wrap(env__, ${dp.name}, __${dp.name}_pos, __${dp.name}_limit, ByteBufferType())
                <#elseif dp.byteBuffer>
                    DirectInputParam<${dp.cppType}>::wrap(env__, ${dp.name}, __${dp.name}_pos, __${dp.name}_limit, ByteArrayType())
				<#elseif dp.direct>
					DirectInputParam<${dp.cppType}>::wrap(env__, ${dp.name})
				</#if>
				<#sep>, </#sep>
			</#list>
			<#if fn.hasStructs>
				<#if fn.hasDirects>,</#if>
				<#list fn.structParameters as sp>
	 				read(buf_wrapper, TypeTag<${sp.cppType}>())
					<#sep>,</#sep>
	 			</#list>
			</#if>
		)
		
		</#if>
</#macro>


<#macro direct_native_call fn structs=false>
        <#if fn.hasStructs || structs>
        ByteBuffer buf_wrapper(env__, buffer__, pos__);
        </#if>

        ${fn.owner.castFn}<${fn.owner.cppClass}>(handle__)->${fn.cppFnName}(
            env__, clazz__ 
            <#list fn.directParameters as dp>
                , ${dp.name}                
            </#list>
            <#if fn.hasStructs>                
                <#list fn.structParameters as sp>
                    , read(buf_wrapper, TypeTag<${sp.cppType}>())
                </#list>
            </#if>
        )
</#macro>



<#macro jni_call_params fn returns_struct=false>
    <@compress single_line=true> 
        (JNIEnv* env__, jclass clazz__, jlong handle__ 
    <#list fn.directParameters as dp>
        ,
        <#if dp.directByteBuffer>
            jobject ${dp.name},
            jint __${dp.name}_pos,
            jint __${dp.name}_limit
        <#elseif dp.byteBuffer>
            jbyteArray ${dp.name},
            jint __${dp.name}_pos,
            jint __${dp.name}_limit
        <#else>
            <#if dp.cppObject>jlong<#else>${dp.jniType}</#if> ${dp.name}
        </#if>
    </#list>
    <#if returns_struct>
        , jobject buffer__, jint pos__
    </#if>
    )</@compress>
</#macro>





<#macro native_methods_declarations method_list>
<#list method_list as fn>
<#if fn.returnsStruct>
    void JNICALL ${fn.jniFnName} <@jni_call_params fn true/>;
<#elseif fn.returnsPair>
    jlong JNICALL ${fn.jniFnName} <@jni_call_params fn fn.hasStructs/>;
<#elseif fn.returnsVoid>
    void JNICALL ${fn.jniFnName} <@jni_call_params fn fn.hasStructs/>;
<#elseif fn.returnsPrimitive>
    ${fn.jniReturnType} JNICALL ${fn.jniFnName} <@jni_call_params fn fn.hasStructs/>;
<#else>
    ${fn.jniReturnType} JNICALL ${fn.jniFnName} <@jni_call_params fn fn.hasStructs/>;
</#if>
</#list>
</#macro>


<#macro native_methods method_list>
<#list method_list as fn>
<#if fn.returnsStruct>
void JNICALL ${fn.jniFnName} <@jni_call_params fn true/>
{
    return ExHelper::handle(env__, [&]{
        <@native_call fn true>
        auto result =
        </@native_call>;

        buf_wrapper.rewind();       
        write(buf_wrapper, result);     
    });
}

<#elseif fn.returnsPair>
jlong JNICALL ${fn.jniFnName} <@jni_call_params fn fn.hasStructs/>
{
    return ExHelper::handle(env__, [&]{ 
        <@native_call fn>
        return to_handle(
        </@native_call>);
    });
}

<#elseif fn.returnsVoid>
void JNICALL ${fn.jniFnName} <@jni_call_params fn fn.hasStructs/>
{
    return ExHelper::handle(env__, [&]{
        <@native_call fn />;
    });
}

<#elseif fn.returnsPrimitive>
${fn.jniReturnType} JNICALL ${fn.jniFnName} <@jni_call_params fn fn.hasStructs/>
{
    return ExHelper::handle(env__, [&]() -> ${fn.jniReturnType} {
        <@native_call fn>
        return
        </@native_call>;
    });
}


<#else>

${fn.jniReturnType} JNICALL ${fn.jniFnName} <@jni_call_params fn fn.hasStructs/>
{
    return ExHelper::handle(env__, [&]() -> ${fn.jniReturnType} {
        <@native_call fn>
        return DirectOutputParam<${fn.jniOutputType}>::wrap(env__, 
        </@native_call>);
    });
}

</#if>
</#list>

</#macro>