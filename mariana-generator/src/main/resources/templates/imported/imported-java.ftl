<#--
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
-->

package ${pair.packageName};

import mariana.bridge.*;
import mariana.bridge.tools.*;
import mariana.v1.api.*;
import mariana.v1.tools.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class ${pair.javaClassName} <#if pair.isClass()>extends ${pair.baseJavaClassName} implements CxxObject<#else> implements ${pair.baseJavaClassName}, CxxObject</#if> {

    private ByteBuffer buffer = ByteBuffer.allocateDirect(#{pair.bufferSize}).order(ByteOrder.LITTLE_ENDIAN);

    private long handle;

    private ${pair.javaClassName}(long handle) {
       this.handle = handle;
       setJavaPair(handle, this);
    }

    public long getHandle() {
       return handle;
    }

    static ${pair.baseJavaClassName} createPair(long handle) {
       return new ${pair.javaClassName}(handle);
    }

    static ${pair.baseJavaClassName} findOrCreatePair(long handle)
    {
       ${pair.baseJavaClassName} pair = getJavaPair(handle);

       if (pair != null) {
           return pair;
       }
       else {
           return new ${pair.javaClassName}(handle); 
       }
    }

    public void close() {
       destroyPair(getHandle());
    }

    public String toString() {
       return getClass().getName()+"["+getHandle()+"]";
    }

    protected void finalize() {
       if (handle != 0) {
           System.err.println("Finalizing live native object: " + this);
       }
    }


    <#list pair.imports as m>
    <#if m.returnsJavaStruct>
    public ${m.javaReturnType} ${m.javaFnName} (<#list m.parameters as p>${p.javaType} ${p.name}<#sep>, </#list>) 
    {
        <#list m.structParameters>
        buffer.rewind();
            <#items as sp>
        BufferTools.write(buffer, ${sp.name});
            </#items>
        </#list>

        <#list m.cppParameters as cp>
            <#if cp.callbackObject>
        ${cp.targetJavaType} ${cp.name}_cp___ = (${cp.targetJavaType}) ${cp.name};
            <#else>
        ${cp.targetJavaPairType} ${cp.name}_cp___ = (${cp.targetJavaPairType}) ${cp.name};
            </#if>
        </#list>

        <#list m.byteBufferParameters as bp>
            assert(!${bp.name}.isDirect());
            
            byte[] __${bp.name}_array;
            int __${bp.name}_offset;
            int __${bp.name}_length;
            
            if (!${bp.name}.isReadOnly()) 
            {
                __${bp.name}_array  = ${bp.name}.array();
                __${bp.name}_offset = ${bp.name}.arrayOffset() + ${bp.name}.position();
                __${bp.name}_length = ${bp.name}.remaining();
            }
            else {
                int __pos_ = ${bp.name}.position();
                __${bp.name}_length = ${bp.name}.remaining();
                
                __${bp.name}_array  = new byte[__${bp.name}_length];
                ${bp.name}.get(__${bp.name}_array);
                ${bp.name}.position(__pos_);
                
                __${bp.name}_offset = 0;
            }
            
        </#list>

        <#list m.directByteBufferParameters as bp>
            assert(${bp.name}.isDirect());
        </#list>

        ${m.javaJniFnName}<@compress single_line=true>(
            handle
            <#list m.directParameters as dp>
                <#if dp.isCppObject>
                , ${dp.name}_cp___.getHandle()
                <#elseif dp.directByteBuffer>
                , ${dp.name}, ${dp.name}.position(), ${dp.name}.limit()
                <#elseif dp.byteBuffer>
                , __${bp.name}_array, __${bp.name}_offset, __${bp.name}_length
                <#else>
                , ${dp.name}
                </#if>
            </#list>,
            buffer, buffer.capacity()
        );</@compress>

        buffer.rewind();
        return BufferTools.read${m.javaReturnTypeSimple}(buffer);
    }

    private static native void ${m.javaJniFnName} <@compress single_line=true>(
        long handle
        <#list m.directParameters as dp>
        <#if dp.cppObject>
            , long ${dp.getSimpleName}
        <#elseif dp.directByteBuffer>
            , ByteBuffer ${dp.getSimpleName}
            , int __${dp.name}_pos
            , int __${dp.getSimpleName}_limit
        <#elseif dp.byteBuffer>
            , byte[] ${dp.name}, int __${dp.name}_offset, int __${dp.name}_length            
        <#else>
            , ${dp.javaType} ${dp.name}
        </#if>
        </#list>
        , ByteBuffer buffer__, int pos__
    );</@compress>

    <#else>
    public ${m.javaReturnType} ${m.javaFnName} (<#list m.parameters as p>${p.javaType} ${p.name}<#sep>, </#list>) 
    {
        <#if m.hasStructs>
        buffer.rewind();
            <#list m.structParameters as sp>
        BufferTools.write(buffer, ${sp.name});
            </#list>
        </#if>

        <#list m.cppParameters as cp>
            <#if cp.callbackObject>
        ${cp.targetJavaType} ${cp.name}_cp___ = (${cp.targetJavaType}) ${cp.name};
            <#else>
        ${cp.targetJavaPairType} ${cp.name}_cp___ = (${cp.targetJavaPairType}) ${cp.name};
            </#if>
        </#list>


        <#list m.byteBufferParameters as bp>
            assert(!${bp.name}.isDirect());
            
            byte[] __${bp.name}_array;
            int __${bp.name}_offset;
            int __${bp.name}_length;
            
            if (!${bp.name}.isReadOnly()) 
            {
                __${bp.name}_array  = ${bp.name}.array();
                __${bp.name}_offset = ${bp.name}.arrayOffset() + ${bp.name}.position();
                __${bp.name}_length = ${bp.name}.remaining();
            }
            else {
                int __pos_ = ${bp.name}.position();
                __${bp.name}_length = ${bp.name}.remaining();
                
                __${bp.name}_array  = new byte[__${bp.name}_length];
                ${bp.name}.get(__${bp.name}_array);
                ${bp.name}.position(__pos_);
                
                __${bp.name}_offset = 0;
            }
            
        </#list>

        <#list m.directByteBufferParameters as bp>
            assert(${bp.name}.isDirect());
        </#list>


        <#if m.returnsPair>
        long hh = ${m.javaJniFnName}<@compress single_line=true>(
            handle
            <#list m.directParameters as dp>
                <#if dp.cppObject>
                , ${dp.name}_cp___.getHandle()
                <#elseif dp.directByteBuffer>
                , ${dp.name}, ${dp.name}.position(), ${dp.name}.limit()
                <#elseif dp.byteBuffer>
                , __${dp.name}_array, __${dp.name}_offset, __${dp.name}_length
                <#else>
                , ${dp.name}
                </#if>
            </#list>
            <#if m.hasStructs>
            , buffer, buffer.capacity()
            </#if>
        );</@compress>

        return ${m.returns.fullJavaClassName}.findOrCreatePair(hh);
        <#else>
        <#if !m.void>return</#if> ${m.javaJniFnName}<@compress single_line=true>(
            handle
            <#list m.directParameters as dp>
                <#if dp.cppObject>
                , ${dp.name}_cp___.getHandle()
                <#elseif dp.directByteBuffer>
                , ${dp.name}, ${dp.name}.position(), ${dp.name}.limit()
                <#elseif dp.byteBuffer>
                , __${dp.name}_array, __${dp.name}_offset, __${dp.name}_length
                <#else>
                , ${dp.name}
                </#if>
            </#list>
            <#if m.hasStructs>
            , buffer, buffer.capacity()
            </#if>
        );</@compress>
        </#if>
    }

    private static native <#if m.returnsPair>long<#else>${m.javaReturnType}</#if> ${m.javaJniFnName} <@compress single_line=true>(
        long handle
        <#list m.directParameters as dp>
            <#if dp.cppObject>
                , long ${dp.name}
            <#elseif dp.directByteBuffer>
            , ByteBuffer ${dp.name}
            , int __${dp.name}_pos
            , int __${dp.name}_limit
            <#elseif dp.byteBuffer>
            , byte[] ${dp.name}, int __${dp.name}_offset, int __${dp.name}_length 
            <#else>
                , ${dp.javaType} ${dp.name}
            </#if>
        </#list>
        <#if m.hasStructs>
        , ByteBuffer buffer__, int pos__
        </#if>
    );</@compress>
    </#if>

    </#list>


    private static native void setJavaPair(long handle, Object pair);
    private static native ${pair.baseJavaClassName} getJavaPair(long handle);

    private static native void destroyPair(long handle);
}