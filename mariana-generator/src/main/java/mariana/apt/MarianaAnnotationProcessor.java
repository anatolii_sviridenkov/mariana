
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt;

import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import mariana.apt.model.Model;
import mariana.apt.model.common.AbstractResource;
import mariana.apt.model.ctr.ContainerResource;
import mariana.apt.model.ctr.IteratorResource;
import mariana.apt.model.exported.ExportedResource;
import mariana.apt.model.imported.ImportedResource;
import mariana.bridge.Ctr;
import mariana.bridge.Export;
import mariana.bridge.Import;
import mariana.bridge.RootTypeMap;
import mariana.bridge.TypeMap;
import mariana.bridge.TypeMaps;
import mariana.bridge.tools.APTConstants;


@SupportedSourceVersion(SourceVersion.RELEASE_6)
@SupportedAnnotationTypes({
   "*"
})

@TypeMaps({})
public class MarianaAnnotationProcessor extends AbstractProcessor {
	

	
	private final List<TypeElement> importedTypes 	= new ArrayList<>();
	private final List<TypeElement> containerTypes 	= new ArrayList<>();
	private final List<TypeElement> exportedTypes 	= new ArrayList<>();
	private final List<Element> 	typeMaps 		= new ArrayList<>();
		
	private Model model;
	
	private final Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
	
	
	
	
	public MarianaAnnotationProcessor() 
	{
		cfg.setClassForTemplateLoading(MarianaAnnotationProcessor.class, "/templates");
	    cfg.setDefaultEncoding("UTF-8");
	    cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
	    cfg.setLogTemplateExceptions(false);
	}
	
	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		
		if (model == null) {
			model = new Model(processingEnv.getTypeUtils(), processingEnv.getElementUtils());			
		}
	}
	
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) 
	{
		try {
			if (!roundEnv.processingOver())
			{
				register(annotations, roundEnv);
			}
			else {
				render(roundEnv);
			}

			return true;
		}
		catch (RuntimeException ex) {
			throw ex;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public void register(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws Exception 
	{
		for (Element element : roundEnv.getElementsAnnotatedWith(Import.class)) {
			if (element instanceof TypeElement) {
				importedTypes.add((TypeElement) element);
			}
		}

		for (Element element : roundEnv.getElementsAnnotatedWith(Export.class)) {
			if (element instanceof TypeElement) {
				exportedTypes.add((TypeElement) element);
			}
		}
		
		for (Element element : roundEnv.getElementsAnnotatedWith(Ctr.class)) {
			if (element instanceof TypeElement) {
				containerTypes.add((TypeElement) element);
			}
		}
		
		
		for (Element element : roundEnv.getElementsAnnotatedWith(TypeMap.class)) {
			typeMaps.add(element);
		}
		
		for (Element element : roundEnv.getElementsAnnotatedWith(RootTypeMap.class)) {
			typeMaps.add(element);
		}
	}

	public void render(RoundEnvironment roundEnv) throws Exception 
	{
		for (Element map: typeMaps) 
		{
			model.addTypeMapping(map);
		}
		
		model.buildTypeMappings();
		
		
		
		for (TypeElement type: importedTypes) 
		{
			model.addImportedResource(type);			
		}
		
		for (TypeElement type: containerTypes) 
		{
			model.addContainerResource(type);
		}
		
		for (TypeElement type: exportedTypes) 
		{
			model.addExportedResource(type);
		}
		
		createJavaResource(this, "metadata/metadata-java.ftl", this.getFullMetadataClassName());
		
		for (AbstractResource resource: model.getResources()) 
		{
			if (resource instanceof ImportedResource) 
			{
				ImportedResource re = (ImportedResource) resource;
				
				createJavaResource(re, "imported/imported-java.ftl", re.getFullJavaClassName());
				createFileResource(re, "imported/imported-header.ftl", "include", re.getTypeName()+".hpp");
				createFileResource(re, "imported/imported-cxx.ftl", "cxx", re.getTypeName()+".cpp-inc");
			}
			else if (resource instanceof ExportedResource) 
			{
				ExportedResource re = (ExportedResource) resource;
				
				createFileResource(re, "exported/exported-header.ftl", "include", re.getTypeName()+".hpp");
				createFileResource(re, "exported/exported-cxx.ftl", "cxx", re.getTypeName()+".cpp-inc");
			}
			else if (resource instanceof ContainerResource) 
			{
				ContainerResource re = (ContainerResource) resource;
				
				createJavaResource(re, "imported/imported-java.ftl", re.getFullJavaClassName());
				createFileResource(re, "container/container-header.ftl", "include", re.getTypeName()+".hpp");
				createFileResource(re, "container/container-cxx.ftl", "cxx", re.getTypeName()+".cpp-inc");
			}
			else if (resource instanceof IteratorResource) 
			{
				IteratorResource re = (IteratorResource) resource;
				
				createJavaResource(re, "imported/imported-java.ftl", re.getFullJavaClassName());
				createFileResource(re, "container/iterator-header.ftl", "include", re.getTypeName()+".hpp");
				createFileResource(re, "container/iterator-cxx.ftl", "cxx", re.getTypeName()+".cpp-inc");
			}			
			else {
				throw new RuntimeException("Unknown resource type: "+resource.getClass());
			}
		}
		
		Collection<ContainerResource> containers = model.getContainers();
		
		createFileResource(model.getResources(), containers, "generated-headers.ftl", "include", "generated_headers.hpp");
		createFileResource(model.getResources(), containers, "generated-sources.ftl", "cxx", "generated_sources.cpp");
	}
	
	private void createFileResource(Object model, String template, String pkg, String output) throws Exception 
	{
		Template temp = cfg.getTemplate(template);
		
		Map<String, Object> root = new HashMap<>();
        
        root.put("pair", model);
		
		FileObject res = processingEnv.getFiler().createResource(StandardLocation.SOURCE_OUTPUT, pkg, output);
		try(OutputStreamWriter writer = new OutputStreamWriter(res.openOutputStream(), "UTF-8");) 
		{
			temp.process(root, writer);
		}
	}
	
	private void createFileResource(Object model, Object ctr, String template, String pkg, String output) throws Exception 
	{
		Template temp = cfg.getTemplate(template);
		
		Map<String, Object> root = new HashMap<>();
        
        root.put("pair", model);
        root.put("containers", ctr);
		
		FileObject res = processingEnv.getFiler().createResource(StandardLocation.SOURCE_OUTPUT, pkg, output);
		try(OutputStreamWriter writer = new OutputStreamWriter(res.openOutputStream(), "UTF-8");) 
		{
			temp.process(root, writer);
		}
	}

	
	private void createJavaResource(Object model, String template, String output) throws Exception 
	{
		JavaFileObject file = processingEnv.getFiler().createSourceFile(output);
		
        Template temp = cfg.getTemplate(template);
        
        Map<String, Object> root = new HashMap<>();
        
        root.put("pair", model);
        
		try(OutputStreamWriter writer = new OutputStreamWriter(file.openOutputStream(), "UTF-8");) 
		{
			temp.process(root, writer);
		}
	}

	public Model getModel() {
		return model;
	}
	
	public String getMetadataClassPackage() {
		return APTConstants.METADATA_CLASS_PACKAGE;
	}
	
	public String getMetadataClassName() {
		return APTConstants.METADATA_CLASS_NAME;
	}
	
	public String getFullMetadataClassName() {
		return APTConstants.METADATA_FULL_CLASS_NAME;
	}

}
