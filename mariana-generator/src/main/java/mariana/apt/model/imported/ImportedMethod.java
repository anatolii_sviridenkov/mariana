
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model.imported;

import java.nio.ByteBuffer;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;

import mariana.apt.model.common.AbstractMethod;
import mariana.apt.model.common.AbstractParameterResource;
import mariana.apt.model.common.AbstractResource;
import mariana.apt.tools.JNITools;
import mariana.apt.tools.TypeTools;

public class ImportedMethod extends AbstractMethod {
	
	private final mariana.bridge.ImportedMethod annotation; 
	
	public ImportedMethod(AbstractResource owner, ExecutableElement element) 
	{
		super(owner, element);
		
		annotation = element.getAnnotation(mariana.bridge.ImportedMethod.class);
	}

	@Override
	public String getCppFnName() 
	{		
		if("".equals(annotation.name())) 
		{
			return element.getSimpleName().toString();
		}
		else {
			return annotation.name();
		}
	}
	
	public String getJniReturnType() 
	{
		return JNITools.getTypeName(element.getReturnType());				
	}
	
	public String getJavaJniFnName() 
	{
		return "__" + getJavaFnName();
	}
	
	public String getJniFnName() 
	{		
		StringBuilder sb = new StringBuilder();
		
		sb.append(JNITools.getJNIMethodName(owner.getFullJavaClassName(), getJavaJniFnName()));
		
		if (overload)
		{
			sb.append("__J");
			
			for (AbstractParameterResource param: this.getDirectParameters()) 
			{
				if (param.isCppObject()) 
				{
					sb.append("J");
				}
				else {
					sb.append(param.getJNITypeSignature());
				}
			}
			
			if (this.isHasStructs()) 
			{
				sb.append("J");
				sb.append(JNITools.escapeJNI(ByteBuffer.class.getName()));
			}
		}
		
		return sb.toString();
	}
	
	public String getJavaJniSignature() 
	{		
		StringBuilder sb = new StringBuilder();
		
		sb.append("(J");
		
		for (AbstractParameterResource param: this.getDirectParameters()) 
		{
			if (param.isCppObject()) 
			{
				sb.append("J");
			}
			else if (param.isDirectByteBuffer()) 
			{
				sb.append("L");
				sb.append(JNITools.escapeClassName(ByteBuffer.class.getName()));
				sb.append(";");
				sb.append("II");
			}
			else if (param.isByteBuffer()) 
			{
				sb.append("[BII");
			}
			else {
				sb.append(param.getJavaTypeSignature());
			}
		}

		if (this.isHasStructs() || this.isReturnsStruct()) 
		{
			sb.append("L");
			sb.append(JNITools.escapeClassName(ByteBuffer.class.getName()));
			sb.append(";");
			sb.append("I");
		}
		
		sb.append(")");
		
		if (this.isReturnsStruct()) {
			sb.append("V");
		}
		else if (this.isReturnsPair()) {
			sb.append("J");
		}
		else if (this.isReturnsVoid()) {
			sb.append("V");
		}
		else if (this.isReturnsPrimitive()) {
			sb.append(JNITools.getTypeSignature(element.getReturnType()));
		}
		else {
			sb.append(JNITools.getTypeSignature(element.getReturnType()));
		}
		
		return sb.toString();
	}
	
	
	public String getJniOutputType() 
	{
		TypeTools tools = this.owner.getModel().getTools();
		TypeMirror rt = this.element.getReturnType();
		
		if (tools.isString(rt)) 
		{
			return "JStringType";
		}
		else if (tools.isByteBuffer(rt)) {
			return "ByteBufferType";
		}
		else if (tools.isByteArray(rt)) {
			return "ByteArrayType";
		}
		else if (tools.isCharArray(rt)) {
			return "CharArrayType";
		}
		else if (tools.isShortArray(rt)) {
			return "ShortArrayType";
		}
		else if (tools.isIntArray(rt)) {
			return "IntArrayType";
		}
		else if (tools.isLongArray(rt)) {
			return "LongArrayType";
		}
		else if (tools.isFloatArray(rt)) {
			return "FloatArrayType";
		}
		else if (tools.isDoubleArray(rt)) {
			return "DoubleArrayType";
		}
		else if (tools.isBooleanArray(rt)) {
			return "BooleanArrayType";
		}
		else {
			throw new RuntimeException("Return type " + rt + " for method "+this.element+" of type "+getOwner().getType() +" has no output converter defined");
		}
	}
	
	public boolean isDirect() 
	{
		return annotation.direct();
	}
	
	public boolean isFreeFunction() 
	{
		return annotation.freeFunction();
	}
}
