
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model.imported;

import javax.lang.model.element.TypeElement;

import mariana.apt.model.Model;
import mariana.apt.model.TypeMapping;
import mariana.apt.model.common.AbstractResource;
import mariana.bridge.Import;

public class ImportedResource extends AbstractResource {

	private Import annotation;
	
	public ImportedResource(Model model, TypeElement type, TypeMapping mapping) {
		super(model, type, mapping);
		
		annotation = type.getAnnotation(Import.class);
	}

	@Override
	public String getJavaClassName() {		
		return type.getSimpleName()+"ImportedGen";
	}

	@Override
	public int getBufferSize() 
	{		
		return annotation.bufferSize();
	}

	@Override
	public boolean isPair() {
		return true;
	}
	
	public String getCppClass() {
		return annotation.cppType();
	}
}
