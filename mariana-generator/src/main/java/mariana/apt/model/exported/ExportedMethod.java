
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model.exported;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;

import mariana.apt.model.common.AbstractMethod;
import mariana.apt.model.common.AbstractResource;
import mariana.apt.tools.APTools;
import mariana.apt.tools.JNITools;
import mariana.bridge.Callback;

public class ExportedMethod extends AbstractMethod {
	
	private final Callback annotation;
	
	public ExportedMethod(AbstractResource owner, ExecutableElement element) {
		super(owner, element);		
		annotation = element.getAnnotation(Callback.class);
	}

	@Override
	public String getCppFnName() 
	{		
		if("".equals(annotation.name())) 
		{
			return element.getSimpleName().toString();
		}
		else {
			return annotation.name();
		}
	}

	public String getJniReturnType() 
	{
		if ("".equals(annotation.returnType())) 
		{		
			return JNITools.getCppReturnTypeForJavaType(element.getReturnType().toString());
		}
		else {
			return annotation.returnType();
		}		
	}
	
	public boolean isConst() {
		return annotation.isConst();
	}
	
	public boolean isClassOnly() {
		return annotation.classOnly();
	}
	
	public String getJniCallType() 
	{
		TypeMirror rt = element.getReturnType();
		
		if (rt instanceof PrimitiveType) 
		{
			return APTools.capitalizeFirst(rt.toString());			
		}
		else {
			return "Object";
		}
	}
	
	public String getCppReturnType() 
	{
		if (APTools.isDefined(annotation.returnType())) 
		{
			return annotation.returnType();
		}
		else {
			TypeMirror rt = element.getReturnType();			
			return JNITools.getTypeName(rt);
		}
	}
	
	public String getReturnTypeConverter() {
		return getCppReturnType();
	}
	
	public String getJniFnName() {
		throw new RuntimeException("Not applicable");
	}
}
