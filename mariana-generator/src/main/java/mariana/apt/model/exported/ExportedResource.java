
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model.exported;

import javax.lang.model.element.TypeElement;

import mariana.apt.model.Model;
import mariana.apt.model.TypeMapping;
import mariana.apt.model.common.AbstractResource;
import mariana.bridge.Export;

public class ExportedResource extends AbstractResource {

	private final Export annotation;
	private final String cppClass;
	
	
	public ExportedResource(Model model, TypeElement type, TypeMapping mapping) {
		super(model, type, mapping);
		
		annotation = type.getAnnotation(Export.class);
		
		this.cppClass	= this.getTargetCppClass()+"<"+this.getCppJavaObjectName()+">";
	}
	
	public String getTargetCppClass() {
		return annotation.template(); 
	}
	
	@Override
	public String getJavaClassName() {		
		return type.getSimpleName().toString();
	}

	@Override
	public int getBufferSize() {
		return 256;
	}

	@Override
	public boolean isPair() {
		return false;
	}

	public String getCastFn(){
		return "to_ptr";
	}
	
	public String getCppClass() {
		return cppClass;
	}
	
	public String getBase() {
		return annotation.base();
	}
	
	public boolean hasInclude() {
		return !"".equals(annotation.include());
	}
	
	public String getInclude() {
		return annotation.include();
	}
}
