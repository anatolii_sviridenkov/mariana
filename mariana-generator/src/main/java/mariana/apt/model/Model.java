
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import mariana.apt.model.common.AbstractResource;
import mariana.apt.model.ctr.ContainerResource;
import mariana.apt.model.ctr.IteratorResource;
import mariana.apt.model.exported.ExportedResource;
import mariana.apt.model.imported.ImportedResource;
import mariana.apt.tools.APTools;
import mariana.apt.tools.TypeTools;
import mariana.bridge.Ctr;
import mariana.bridge.Export;
import mariana.bridge.Import;
import mariana.bridge.Iter;

public class Model {
	private final Types types;
	private final Elements elements;	
	private final TypeTools tools;
	
	private final Map<String, AbstractResource> resources = new HashMap<>();
	
	private List<TypeElement> typeMapElements = new ArrayList<>();
	
	public Model(Types types, Elements elements) 
	{
		this.types = types;
		this.elements = elements;
		
		this.tools = new TypeTools(types, elements);
	}

	public Elements getElements() {
		return elements;
	}

	public Types getTypes() {
		return types;
	}
	
	public void addTypeMapping(Element map) 
	{
		typeMapElements.add((TypeElement) map);
		tools.addTypeMapping(map.asType(), new TypeMapping(map, tools.getTypes()));
	}
	
	public void buildTypeMappings() 
	{
		tools.buildTypeMappings();
	}
	
	public void addImportedResource(TypeElement te)
	{
		String typeName = te.asType().toString();
		String tmName = APTools.getAsTypeMirror(() -> te.getAnnotation(Import.class).defaults()).toString();
		
		resources.put(typeName, new ImportedResource(this, te, tools.getTypeMapping(tmName)));
	}
	
	public void addExportedResource(TypeElement te)
	{
		String typeName = te.asType().toString();
		String tmName = APTools.getAsTypeMirror(() -> te.getAnnotation(Export.class).defaults()).toString();
		
		resources.put(typeName, new ExportedResource(this, te, tools.getTypeMapping(tmName)));
	}
	
	public void addContainerResource(TypeElement te)
	{
		String typeName = te.asType().toString();
		String tmName = APTools.getAsTypeMirror(() -> te.getAnnotation(Ctr.class).defaults()).toString();
		
		TypeMapping mapping = tools.getTypeMapping(tmName);
		
		resources.put(typeName, new ContainerResource(this, te, mapping));
		
		List<? extends Element> enclosedElements = te.getEnclosedElements();
		
		for (Element e: enclosedElements.stream().filter(ee -> ee.getAnnotation(Iter.class) != null).collect(Collectors.toList())) 
		{
			resources.put(e.asType().toString(), new IteratorResource(this, te, (TypeElement) e, mapping));
		}
	}
	
	public Collection<AbstractResource> getResources() 
	{
		return resources.values();
	}
	
	public Collection<String> getResourceNames() 
	{
		return resources.keySet();
	}
	
	public Collection<ContainerResource> getContainers() {
		return resources.values().stream().filter(r -> r instanceof ContainerResource).map(r -> (ContainerResource)r).collect(Collectors.<ContainerResource>toList());
	}
	
	public Optional<AbstractResource> getResource(String tm) 
	{
		AbstractResource res = resources.get(tm.toString());
		if (res != null) 
		{
			return Optional.of(res);
		}
		else {
			return Optional.empty();
		}
	}
	
	public Optional<AbstractResource> getResource(TypeMirror tm) 
	{
		return getResource(tm.toString());
	}
	
	public Optional<AbstractResource> getResource(TypeElement tm) 
	{
		return getResource(tm.toString());
	}

	public TypeTools getTools() {
		return tools;
	}

	public Optional<AbstractResource> getResourceFor(TypeMirror rt, TypeMapping mapping) 
	{
		Optional<AbstractResource> res = getResource(rt);
		
		if (res.isPresent()) 
		{
			return res;
		}
		else {
			Optional<TypeMirror> tm = mapping.getOp(rt);
			
			if (tm.isPresent()) {			
				return getResource(tm.get());
			}
			else {
				return Optional.empty();
			}
		}
	}
	
	public List<String> getTypeMapClasses() 
	{
		return typeMapElements.stream().map(e -> e.getQualifiedName().toString()).collect(Collectors.toList());
	}
}
