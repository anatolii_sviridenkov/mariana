
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model.ctr;

import javax.lang.model.element.TypeElement;

import mariana.apt.model.Model;
import mariana.apt.model.TypeMapping;
import mariana.apt.model.common.AbstractResource;
import mariana.bridge.Ctr;
import mariana.bridge.Iter;

public class IteratorResource extends AbstractResource {

	private final TypeElement ctrType;
	private final String javaClassName;
	
	private final Ctr ctrAnnotation;
	private final Iter iterAnnotation;
	
	private final String cppClass;
	
	public IteratorResource(Model model, TypeElement type, TypeElement iterType, TypeMapping mapping) 
	{
		super(model, iterType, mapping);

		this.ctrType = type;
		
		ctrAnnotation 	= type.getAnnotation(Ctr.class);
		iterAnnotation 	= type.getAnnotation(Iter.class);
		
		javaClassName = ctrType.getSimpleName().toString() + "_" + iterType.getSimpleName().toString() + "IteratorGen";
		
		this.cppClass   = "InMemCtrFactory::template IterT<" + ctrAnnotation.name() + ">";
	}

	@Override
	public String getJavaClassName() {		
		return javaClassName;
	}

	@Override
	public int getBufferSize() {
		return ctrAnnotation.bufferSize();
	}

	@Override
	public boolean isPair() {
		return true;
	}
	
	public String getCppClass() {
		return cppClass;
	}
}
