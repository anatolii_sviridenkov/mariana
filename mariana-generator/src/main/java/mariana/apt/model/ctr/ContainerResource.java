
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model.ctr;

import javax.lang.model.element.TypeElement;

import mariana.apt.model.Model;
import mariana.apt.model.TypeMapping;
import mariana.apt.model.common.AbstractResource;
import mariana.bridge.Ctr;

public class ContainerResource extends AbstractResource {

	private final Ctr annotation;
	private final String cppClass;
	
	public ContainerResource(Model model, TypeElement type, TypeMapping mapping) {
		super(model, type, mapping);
		
		annotation = type.getAnnotation(Ctr.class);
		
		this.cppClass   = "InMemCtrFactory::template CtrT<"+annotation.name()+">";		
	}

	@Override
	public String getJavaClassName() {		
		return type.getSimpleName() + "ContainerGen";
	}

	@Override
	public int getBufferSize() {
		return annotation.bufferSize();
	}

	@Override
	public boolean isPair() {
		return true;
	}
	
	public String getCtrName() {
		return annotation.name();
	}
	
	public String getFactory() {
		return annotation.factory();
	}
	
	public String getCppClass() {
		return cppClass;
	}
}
