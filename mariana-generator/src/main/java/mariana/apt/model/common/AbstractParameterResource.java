
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model.common;

import java.util.Optional;

import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;

import mariana.apt.model.TypeMapping;
import mariana.apt.tools.JNITools;
import mariana.apt.tools.TypeTools;
import mariana.bridge.DirectBuffer;
import mariana.bridge.Export;
import mariana.bridge.JObject;
import mariana.bridge.MarianaConstants;

public class AbstractParameterResource {
	protected final AbstractMethod owner;
	protected final VariableElement var;
	
	private final TypeTools tools;
	private final TypeMapping mapping;
	
	public AbstractParameterResource(AbstractMethod owner, VariableElement param)
	{
		this.owner = owner;
		this.var = param;
		
		this.tools = owner.getModel().getTools();
		this.mapping = owner.getMapping();
	}

	public VariableElement getParam() {
		return var;
	}

	public AbstractMethod getOwner() {
		return owner;
	}
	
	public String getJniType() {return JNITools.getTypeName(var.asType());}
	public String getName() {return var.getSimpleName().toString();}
	public boolean isPrimitive() {return tools.isPrimitive(var.asType());}
	public boolean isStruct() {return tools.isStruct(var.asType());}
	public boolean isDirect() {return tools.isDirect(var.asType());}
	public boolean isCppObject() {return tools.isCppObject(var.asType());}
	public boolean isCallbackObject() {return tools.isExported(var.asType());}
	public boolean isByteBuffer() {return tools.isByteBuffer(var.asType()) && var.getAnnotation(DirectBuffer.class) == null;}
	public boolean isDirectByteBuffer() {return tools.isByteBuffer(var.asType()) && var.getAnnotation(DirectBuffer.class) != null;}
	
	public String getCppType()
	{		
		return tools.getCppType(var);		
	}
	
	public String getCppCBType() 
	{
		if (var.getAnnotation(JObject.class) != null) 
		{
			return "jobject";
		}
		else {
			return tools.getCppType(var);
		}
	}
	
	public String getJavaType() {
		return var.asType().toString();
	}
	
	public String getCppClass() 
	{
		Optional<AbstractResource> res = owner.getModel().getResourceFor(var.asType(), mapping);
		
		if (res.isPresent()) {
			return res.get().getCppClass();
		}
		else {
			throw new RuntimeException("Resource for type "+var.asType()+" is not found");
		}
	}
	

	public TypeMirror getTargetJavaType() 
	{	
		TypeMirror tm = var.asType();
		TypeElement te = tools.getElements().getTypeElement(tm.toString()); 
		
		if (te.getAnnotation(Export.class) != null) 
		{
			return tm;
		}
		else {
			return mapping.get(var.asType());
		}
	}
	
	public String getTargetJavaPairType() {
		return getTargetJavaType().toString() + MarianaConstants.IMPORTED_JAVA_CLASS_SUFFIX;
	}
	
	public String getTargetCppPairType() 
	{
		Optional<AbstractResource> res = owner.getModel().getResourceFor(var.asType(), mapping);
		
		if (res.isPresent()) {
			return res.get().getCppClass();
		}
		else {
			throw new RuntimeException("Target parameter type for "+var+" is not a resource");
		}
	}
	
	public String getJNITypeSignature() 
	{
		return JNITools.escapeJNI(JNITools.getTypeSignature(var.asType()));
	}
	
	public String getJavaTypeSignature() 
	{
		return JNITools.getTypeSignature(var.asType());
	}
}