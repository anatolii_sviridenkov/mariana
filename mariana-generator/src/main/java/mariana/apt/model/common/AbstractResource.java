
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model.common;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;

import mariana.apt.model.Model;
import mariana.apt.model.TypeMapping;
import mariana.apt.model.exported.ExportedMethod;
import mariana.apt.model.imported.ImportedMethod;
import mariana.apt.tools.APTools;
import mariana.apt.tools.JNITools;

public abstract class AbstractResource {
	protected final Model model;
	protected final TypeElement type;
	protected final TypeMapping mapping;
	
	protected final String packageName;
	
	protected final List<ImportedMethod> imports = new ArrayList<>();
	protected final List<ExportedMethod> exports = new ArrayList<>();
	
	
	public AbstractResource(Model model, TypeElement type, TypeMapping mapping) 
	{
		this.model = model;
		this.type = type;
		this.mapping = mapping;
		
		this.packageName = model.getElements().getPackageOf(type).toString();
		
		for (ExecutableElement ee: APTools.collectVisibleImported(type, model.getElements()))
		{
			imports.add(new ImportedMethod(this, ee));
		}
		
		for (AbstractMethod m: imports) 
		{
			for (AbstractMethod mm: imports) {
				if (m != mm && m.getJavaFnName().equals(mm.getJavaFnName())) {
					m.setOverload(true);
				}
			}
		}
		
		
		for (ExecutableElement ee: APTools.collectVisibleExported(type, model.getElements()))
		{
			exports.add(new ExportedMethod(this, ee));
		}
		
		for (AbstractMethod m: exports) 
		{
			for (AbstractMethod mm: exports) {
				if (m != mm && m.getJavaFnName().equals(mm.getJavaFnName())) {
					m.setOverload(true);
				}
			}
		}		
	}

	public Model getModel() {
		return model;
	}

	public TypeElement getType() {
		return type;
	}

	public TypeMapping getMapping() {
		return mapping;
	}
	
	public String getTypeName() {
		return type.getQualifiedName().toString();
	}

	public String getPackageName() {
		return packageName;
	}
	
	abstract public String getJavaClassName();
	
	public String getFullJavaClassName() {
		return getPackageName() + "." + getJavaClassName();
	}
	
	public String getJniPrefix() 
	{
		return JNITools.getJNIMethodPrefix(getFullJavaClassName());
	}
	
	public boolean isClass() 
	{
		return type.getKind() == ElementKind.CLASS;
	}
	
	public String getBaseJavaClassName() 
	{
		return type.getQualifiedName().toString();
	}
	
	abstract public int getBufferSize();
	
	public List<ImportedMethod> getImports() {
		return imports;
	}
	
	public List<ExportedMethod> getExports(){
		return exports;
	}
	
	public List<ExportedMethod> getImplementableExports() 
	{		
		return exports.stream().filter(cb -> !cb.isClassOnly()).collect(Collectors.toList());
	}
	
	public String getCppJavaMetadataClassName() 
	{
		return type.getQualifiedName().toString().replace('.', '_') + "JavaClass";
	}
	
	public String getCppJavaObjectName() {
		return type.getQualifiedName().toString().replace('.', '_') + "JavaObject";
	}
	
	public String getCppFileName() {
		return type.getQualifiedName().toString();
	}
	
	public String getHeaderFileName() {
		return type.getQualifiedName().toString();
	}
	
	public String getPairJavaJNIClassName() {
		return getFullJavaClassName().replace('.', '/');
	}
	
	public String getCastFn(){
		return "to_ref";
	}

	
	public abstract boolean isPair();
	public abstract String getCppClass();
}
