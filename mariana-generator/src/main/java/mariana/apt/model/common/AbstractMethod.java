
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.NoType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;

import mariana.apt.model.Model;
import mariana.apt.model.TypeMapping;
import mariana.apt.tools.JNITools;

public abstract class AbstractMethod {
	protected final AbstractResource owner;
	protected final ExecutableElement element;
	
	protected final Model model;
	protected final TypeMapping mapping;
	
	protected final List<AbstractParameterResource> parameters = new ArrayList<>();
	
	protected boolean overload;
	
	public AbstractMethod(AbstractResource owner, ExecutableElement element) 
	{
		this.owner = owner;
		this.element = element;
		
		this.model 	 = owner.getModel();
		this.mapping = owner.getMapping();
		
		for (VariableElement var: element.getParameters()) 
		{
			parameters.add(new MethodParameter(this, var));
		}
	}
	
	public String getJavaFnName() 
	{
		return element.getSimpleName().toString();
	}
	
	
	public String getJavaSignature() 
	{
		return JNITools.getMethodSignature(element);
	}
	
	public String getMidName() 
	{
		return JNITools.getJNIMethodIdentifierName(element, overload);
	}

	public void setOverload(boolean b) {
		this.overload = b;
	}

	public boolean isOverload() {
		return overload;
	}
	
	public boolean isReturnsJavaStruct() {
		return model.getTools().isStruct(element.getReturnType());
	}
	
	public boolean isReturnsPair() 
	{
		TypeMirror rt = element.getReturnType();
		
		Optional<AbstractResource> resource = model.getResourceFor(rt, mapping);
		
		if (resource.isPresent()) 
		{
			return resource.get().isPair();
		}
		
		return model.getTools().isReturnPair(element.getReturnType());
	}
	
	public boolean isReturnsStruct() 
	{
		TypeMirror rtn = element.getReturnType();
		return model.getTools().isStruct(rtn);
	}
	
	public boolean isReturnsPrimitive() 
	{
		TypeMirror rtn = element.getReturnType();
		return rtn instanceof PrimitiveType;
	}
	
	public boolean isReturnsVoid() 
	{
		TypeMirror rtn = element.getReturnType();
		return rtn instanceof NoType;
	}
	
	public boolean isReturnsResource() 
	{
		TypeMirror rt = element.getReturnType();
		
		Optional<AbstractResource> resource = model.getResourceFor(rt, mapping);
		
		if (resource.isPresent()) 
		{
			return true;
		}
		
		return false;
	}
	

	
	public AbstractResource getReturns() 
	{
		TypeMirror rt = this.element.getReturnType();
		
		return model.getResourceFor(rt, mapping).get();
	}
	
	
	
	public String getJavaReturnType() {
		return element.getReturnType().toString();
	}
	
	abstract public String getJniReturnType(); 
		
	public String getJavaReturnTypeSimple() 
	{
		return this.model.getTypes().asElement(element.getReturnType()).getSimpleName().toString();
	}
	
	public boolean isVoid() 
	{
		return element.getReturnType() instanceof NoType;
	}
	
	

	public List<AbstractParameterResource> getParameters() {
		return parameters;
	}
	
	public List<AbstractParameterResource> getCppParameters() 
	{
		return parameters.stream().filter(p -> p.isCppObject()).collect(Collectors.toList());
	}
	
	public List<AbstractParameterResource> getDirectParameters() 
	{
		return parameters.stream().filter(p -> p.isDirect()).collect(Collectors.toList());
	}
	
	public List<AbstractParameterResource> getByteBufferParameters() 
	{
		return parameters.stream().filter(p -> p.isByteBuffer()).collect(Collectors.toList());
	}
	
	public List<AbstractParameterResource> getDirectByteBufferParameters() 
	{
		return parameters.stream().filter(p -> p.isDirectByteBuffer()).collect(Collectors.toList());
	}
	
	public List<AbstractParameterResource> getStructParameters() 
	{
		return parameters.stream().filter(p -> p.isStruct()).collect(Collectors.toList());
	}
	
	public boolean isHasDirects() {
		return parameters.stream().filter(p -> p.isDirect()).count() > 0;
	}
	
	public boolean isHasStructs() {
		return parameters.stream().filter(p -> p.isStruct()).count() > 0;
	}

	public AbstractResource getOwner() {
		return owner;
	}

	public ExecutableElement getElement() {
		return element;
	}

	public Model getModel() {
		return model;
	}

	public TypeMapping getMapping() {
		return mapping;
	}
	
	public abstract String getCppFnName();
}
