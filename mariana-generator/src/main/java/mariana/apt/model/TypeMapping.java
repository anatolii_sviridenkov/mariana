
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

import mariana.apt.tools.APTools;
import mariana.bridge.Entry;
import mariana.bridge.Parent;
import mariana.bridge.TypeMap;

public class TypeMapping {
	
	private final Types types;
	
	private final List<Object[]> typeMap = new ArrayList<>();

	private final Element element;
	
	private final TypeMirror parent;
	
	private TypeMapping parentMapping;
	
	public TypeMapping(Element element, Types types) 
	{
		this.element = element;
		this.types = types;
		
		Parent parentAnn = element.getAnnotation(Parent.class);
		
		if (parentAnn != null) 
		{
			parent = APTools.getAsTypeMirror(() -> parentAnn.value());
		}
		else {
			parent = null;
		}
		
		TypeMap typeMapAnn = element.getAnnotation(TypeMap.class);
		
		if (typeMapAnn != null) 
		{
			Entry[] map = typeMapAnn.value();

			for (Entry entry: map) 
			{
				TypeMirror key 		= APTools.getAsTypeMirror(() -> entry.javaType());
				TypeMirror value 	= APTools.getAsTypeMirror(() -> entry.pairType());

				typeMap.add(new Object[]{key, value});
			}
		}
	}
	
	public TypeMirror get(TypeMirror tm) 
	{	
		for (Object[] entry: typeMap)
		{
			TypeMirror key 		= (TypeMirror)entry[0];
			TypeMirror value 	= (TypeMirror)entry[1];
			
			if (types.isSameType(tm, key)) {
				return value;
			}
		}
		
		for (Object[] entry: typeMap)
		{
			TypeMirror key 		= (TypeMirror)entry[0];
			TypeMirror value 	= (TypeMirror)entry[1];
			
			if (types.isAssignable(key, tm)) {
				return value;
			}
		}
		
		if (parentMapping != null) 
		{
			return parentMapping.get(tm);
		}
		else {
			throw new RuntimeException("Type " + tm +" is not defined in TypeMap for "+element);
		}
	}
	
	public Optional<TypeMirror> getOp(TypeMirror tm) 
	{	
		for (Object[] entry: typeMap)
		{
			TypeMirror key 		= (TypeMirror)entry[0];
			TypeMirror value 	= (TypeMirror)entry[1];
			
			if (types.isSameType(tm, key)) {
				return Optional.of(value);
			}
		}
		
		for (Object[] entry: typeMap)
		{
			TypeMirror key 		= (TypeMirror)entry[0];
			TypeMirror value 	= (TypeMirror)entry[1];
			
			if (types.isAssignable(key, tm)) {
				return Optional.of(value);
			}
		}
		
		if (parentMapping != null) 
		{
			return parentMapping.getOp(tm);
		}
		else {
			return Optional.empty();
		}
	}

	public TypeMirror getParent() {
		return parent;
	}

	public TypeMapping getParentMapping() {
		return parentMapping;
	}

	public void setParentMapping(TypeMapping parentMapping) 
	{
		this.parentMapping = parentMapping;
	}
}