
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.tools;

import java.util.List;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.AnnotationValueVisitor;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;

public class AnnotationValueAccessor implements AnnotationValueVisitor<Object, Void> {

	@Override
	public Object visit(AnnotationValue av, Void p) {
		return av.getValue();
	}

	@Override
	public Object visit(AnnotationValue av) {
		return av.getValue();
	}

	@Override
	public Object visitBoolean(boolean b, Void p) {
		return b;
	}

	@Override
	public Object visitByte(byte b, Void p) {
		return b;
	}

	@Override
	public Object visitChar(char c, Void p) {
		return c;
	}

	@Override
	public Object visitDouble(double d, Void p) {
		return d;
	}

	@Override
	public Object visitFloat(float f, Void p) {
		return f;
	}

	@Override
	public Object visitInt(int i, Void p) {
		return i;
	}

	@Override
	public Object visitLong(long i, Void p) {
		return i;
	}

	@Override
	public Object visitShort(short s, Void p) {
		return s;
	}

	@Override
	public Object visitString(String s, Void p) {
		return s;
	}

	@Override
	public Object visitType(TypeMirror t, Void p) {
		return t;
	}

	@Override
	public Object visitEnumConstant(VariableElement c, Void p) {
		return c;
	}

	@Override
	public Object visitAnnotation(AnnotationMirror a, Void p) {
		return a;
	}

	@Override
	public Object visitArray(List<? extends AnnotationValue> vals, Void p) {
		return vals;
	}

	@Override
	public Object visitUnknown(AnnotationValue av, Void p) {
		return av.getValue();
	}
}
