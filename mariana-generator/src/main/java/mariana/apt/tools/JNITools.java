
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.tools;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;

import org.apache.commons.lang3.CharUtils;

public class JNITools {
	
	private static final Map<String, String> typeSignatureMap = new HashMap<>();
	
	private static final Map<String, String> typeMap = new HashMap<>();
	
	static {
		typeSignatureMap.put("byte",  	"B");
		typeSignatureMap.put("short", 	"S");
		typeSignatureMap.put("char",  	"C");
		typeSignatureMap.put("int",   	"I");
		typeSignatureMap.put("long",  	"J");
		typeSignatureMap.put("double",	"D");
		typeSignatureMap.put("float",	"F");
		typeSignatureMap.put("boolean",	"Z");
		typeSignatureMap.put("void",	"V");		
		
		typeMap.put("java.lang.Class",  "jclass");
		typeMap.put("java.lang.Object", "jobject");
		typeMap.put("java.lang.String", "jstring");
		typeMap.put("byte",  	"jbyte");
		typeMap.put("short", 	"jshort");
		typeMap.put("char",  	"jchar");
		typeMap.put("int",   	"jint");
		typeMap.put("long",  	"jlong");
		typeMap.put("double",	"jdouble");
		typeMap.put("float",	"jfloat");
		typeMap.put("boolean",	"jboolean");
		typeMap.put("void",		"void");
		typeMap.put("byte[]",	"jbyteArray");
		typeMap.put("short[]",	"jshortArray");
		typeMap.put("char[]",	"jcharArray");
		typeMap.put("boolean[]","jbooleanArray");
		typeMap.put("int[]",	"jintArray");
		typeMap.put("float[]",	"jfloatArray");
		typeMap.put("double[]",	"jdoubleArray");
		typeMap.put("java.lang.Object[]",	"jobjectArray");
		typeMap.put("java.lang.String[]",	"jobjectArray");
	}
	
	private static final Map<String, String> cppTypeMap = new HashMap<>();

	static {
		cppTypeMap.put("byte",  "Byte");
		cppTypeMap.put("short", "Short");
		cppTypeMap.put("char",  "UShort");
		cppTypeMap.put("int",   "Int");
		cppTypeMap.put("long",  "BigInt");
		cppTypeMap.put("float", "float");
		cppTypeMap.put("double","double");
		cppTypeMap.put("boolean","bool");
		
		cppTypeMap.put(ByteBuffer.class.getName(), "jobject");
		cppTypeMap.put(Class.class.getName(), "jclass");
		cppTypeMap.put(String.class.getName(), "jstring");
	}
	
	public static String getCppReturnTypeForJavaType(String javaType) 
	{
		String className = cppTypeMap.get(javaType);
		if (className != null) 
		{
			return className;
		}
		else {
			return "jobject";
		}
	}


	public static String getTypeName(TypeMirror clazz) 
	{
		String className = clazz.toString();
		
		String nativeClass = typeMap.get(className);
		
		if (nativeClass != null) {
			return nativeClass;
		}
		else if (clazz.getKind() == TypeKind.ARRAY) 
		{
			return "jobjectArray";
		}
		else {
			return "jobject";
		}
	}

	
	public static String getTypeSignature(TypeMirror type) 
	{
		if (type instanceof ArrayType) 
		{
			ArrayType array = (ArrayType) type;
			return "[" + getTypeSignature(array.getComponentType());
		}
		else if (type instanceof PrimitiveType) 
		{	
			return typeSignatureMap.get(type.toString());
		}
		else if (type instanceof NoType) 
		{	
			return "V";
		}
		else if (type instanceof TypeVariable) {
			return "L" + escapeClassName("java.lang.Object") + ";";
		}
		else {			
			return "L" + escapeClassName(type.toString()) + ";";
		}
	}
	
	public static String escapeClassName(String name) 
	{
		return name.replace('.', '/');
	}
	
	
	public static String getMethodSignature(ExecutableElement method) 
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("(");
		
		for (VariableElement param: method.getParameters()) 
		{
			sb.append(getTypeSignature(param.asType()));
		}		
		
		sb.append(")");
		
		sb.append(getTypeSignature(method.getReturnType()));
		
		return sb.toString();
	}
	
	public static String escapeJNI(String text) 
	{
		StringBuilder sb = new StringBuilder();
		
		for (int c = 0; c < text.length(); c++) 
		{
			char ch = text.charAt(c);
			if (ch == '_') 
			{
				sb.append("_1");
			}
			else if (ch == '.' || ch == '/') 
			{
				sb.append("_");
			}
			else if (CharUtils.isAscii(ch)) 
			{
				sb.append(ch);				
			}
			else {
				sb.append("_0");
				sb.append(CharUtils.unicodeEscaped(ch).toLowerCase().substring(2));
			}
		}
		
		String newText = sb.toString();
		
		return newText.replace(";", "_2").replace("[", "_3");
	}
	
	public static String getJNIMethodName(ExecutableElement method) 
	{
		return getJNIMethodName(method, "");
	}
	
	public static String getJNIMethodName(ExecutableElement method, String namePostfix) 
	{
		if (method.getKind() == ElementKind.METHOD)
		{
			StringBuilder sb = new StringBuilder();

			sb.append("Java_");
			sb.append(escapeJNI(method.getEnclosingElement().toString() + namePostfix));
			sb.append("_");
			sb.append(escapeJNI(method.getSimpleName().toString()));

			return sb.toString();
		}
		else {
			throw new RuntimeException("Method "+method+" of "+method.getEnclosingElement()+" can't have JNI signature");
		}
	}
	
	public static String getJNIMethodPrefix(String className) 
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Java_");
		sb.append(escapeJNI(className));

		return sb.toString();
	}
	
	
	public static String getJNIMethodName(String targetType, String methodName) 
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Java_");
		sb.append(escapeJNI(targetType));
		sb.append("_");
		sb.append(escapeJNI(methodName));

		return sb.toString();
	}
	
	
	public static String getJNIMethodIdentifierName(ExecutableElement method, boolean overload) 
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(escapeJNI(method.getSimpleName().toString()));
		
		if (overload) 
		{
			sb.append("__");

			for (VariableElement param: method.getParameters()) 
			{
				sb.append(escapeJNI(getTypeSignature(param.asType())));
			}
		}
		
		return sb.toString();
	}	
}
