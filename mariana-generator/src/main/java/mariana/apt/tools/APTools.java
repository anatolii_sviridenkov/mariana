
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;

import mariana.bridge.Callback;
import mariana.bridge.ImportedMethod;

public class APTools {
	public static List<ExecutableElement> collectAllImportedMethods(TypeElement type) 
	{
		List<ExecutableElement> list = new ArrayList<ExecutableElement>();

		for (Element ee : type.getEnclosedElements()) 
		{
			if (ee.getAnnotation(ImportedMethod.class) != null && ee instanceof ExecutableElement)
			{
				list.add((ExecutableElement)ee);
			}
		}

		TypeMirror superclass = type.getSuperclass();

		if (superclass instanceof DeclaredType) 
		{
			DeclaredType dt = (DeclaredType) superclass;			
			list.addAll(collectAllImportedMethods((TypeElement) dt.asElement()));
		}

		for (TypeMirror intf: type.getInterfaces()) 
		{
			DeclaredType dt = (DeclaredType) intf;
			list.addAll(collectAllImportedMethods((TypeElement) dt.asElement()));
		}

		return list;
	}
	
	public static List<ExecutableElement> collectAllExportedMethods(TypeElement type) 
	{
		List<ExecutableElement> list = new ArrayList<ExecutableElement>();

		for (Element ee : type.getEnclosedElements()) 
		{
			if (ee.getAnnotation(Callback.class) != null && ee instanceof ExecutableElement)
			{
				list.add((ExecutableElement)ee);
			}
		}

		TypeMirror superclass = type.getSuperclass();

		if (superclass instanceof DeclaredType) 
		{
			DeclaredType dt = (DeclaredType) superclass;			
			list.addAll(collectAllExportedMethods((TypeElement) dt.asElement()));
		}

		for (TypeMirror intf: type.getInterfaces()) 
		{
			DeclaredType dt = (DeclaredType) intf;
			list.addAll(collectAllExportedMethods((TypeElement) dt.asElement()));
		}

		return list;
	}

	public static List<ExecutableElement> filterOverriding(List<ExecutableElement> methods, TypeElement type, Elements elements) 
	{
		List<ExecutableElement> visible = new ArrayList<>();
		
		for (ExecutableElement overridden: methods) 
		{
			boolean bOverridden = false;
			
			for (ExecutableElement overrider: methods) 
			{
				if (elements.overrides(overrider, overridden, type))
				{
					bOverridden = true;
					break;
				}
			}
			
			if (!bOverridden) 
			{
				visible.add(overridden);
			}
		}
		
		return visible;
	}
	
	public static List<ExecutableElement> collectVisibleImported(TypeElement type, Elements elements) 
	{
		return filterOverriding(collectAllImportedMethods(type), type, elements);
	}
	
	public static List<ExecutableElement> collectVisibleExported(TypeElement type, Elements elements) 
	{
		return filterOverriding(collectAllExportedMethods(type), type, elements);
	}
	
	


	public static <T> TypeMirror getAsTypeMirror(Supplier<T> fn) {
		try {
			T value = fn.get();
			throw new RuntimeException("Value succesfully returned: "+value);
		}
		catch (MirroredTypeException ex) {
			return ex.getTypeMirror();
		}
	}

	public static boolean isType(TypeMirror tm, String className) 
	{
		return tm.toString().equals(className);
	}

	public static boolean isDefined(TypeMirror tm) 
	{
		return !isType(tm, java.lang.Object.class.getName());
	}
	
	public static boolean isDefined(String tm) 
	{
		return !"".equals(tm);
	}

	@SuppressWarnings("unchecked")
	public static <T> T getAnnotationValue(AnnotationMirror am, String attr) 
	{
		for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> ee: am.getElementValues().entrySet()) 
		{
			if (ee.getKey().getSimpleName().toString().equals(attr)) 
			{
				return (T) ee.getValue().accept(new AnnotationValueAccessor(), null);
			}
		}
		
		throw new RuntimeException("Attribute " + attr + " is not found in annotation " + am);
	}
	
	public static String capitalizeFirst(String st) {
		return st.substring(0, 1).toUpperCase() + st.substring(1);
	}
}
