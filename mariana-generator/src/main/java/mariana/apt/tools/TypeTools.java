
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.apt.tools;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.NoType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import mariana.apt.model.TypeMapping;
import mariana.bridge.CppType;
import mariana.bridge.CxxObject;
import mariana.bridge.Export;
import mariana.bridge.Import;
import mariana.bridge.Iter;
import mariana.bridge.StructType;

public class TypeTools {
	
	private final Types types;
	private final Elements elements;
	
	private final TypeElement cxxObjectElement;
	private final TypeElement byteBufferElement;
	
	private Map<String, TypeMapping> typeMapping = new HashMap<>();
	
	public TypeTools(Types types, Elements elements) 
	{
		this.types 	  = types;
		this.elements = elements;
		
		cxxObjectElement = elements.getTypeElement(CxxObject.class.getName());
		byteBufferElement = elements.getTypeElement(ByteBuffer.class.getName());
	}
	
	public boolean isValid(TypeMirror type) 
	{
		return isDirect(type) || isStruct(type) || isCppObject(type);
	}
	
	public boolean isPrimitive(TypeMirror type) {
		return type instanceof PrimitiveType;
	}
	
	public boolean isDirect(TypeMirror type) {
		return isPrimitive(type) 
				|| type.toString().equals(String.class.getName())
				|| type.toString().equals(ByteBuffer.class.getName())
				|| isCppObject(type);
	}
	
	public boolean isStruct(TypeMirror type)
	{
		if (type.toString().equals(java.util.UUID.class.getName())) 
		{
			return true;
		}
		else {
			TypeElement typeElement = elements.getTypeElement(type.toString());
			if (typeElement != null) 
			{
				return typeElement.getAnnotation(StructType.class) != null;
			}
			else {
				return false;
			}
		}
	}
	

	
	public boolean isCppObject(TypeMirror tm) 
	{
		return types.isAssignable(tm, cxxObjectElement.asType());
	}
	
	public boolean isByteBuffer(TypeMirror tm) {
		return ByteBuffer.class.getName().equals(tm.toString());
	}
	
	public boolean isByteArray(TypeMirror tm) {
		return byte[].class.getCanonicalName().equals(tm.toString());
	}
	
	public boolean isShortArray(TypeMirror tm) {
		return short[].class.getCanonicalName().equals(tm.toString());
	}
	
	public boolean isCharArray(TypeMirror tm) {
		return char[].class.getCanonicalName().equals(tm.toString());
	}
	
	public boolean isIntArray(TypeMirror tm) {
		return int[].class.getCanonicalName().equals(tm.toString());
	}
	
	public boolean isLongArray(TypeMirror tm) {
		return long[].class.getCanonicalName().equals(tm.toString());
	}
	
	public boolean isFloatArray(TypeMirror tm) {
		return float[].class.getCanonicalName().equals(tm.toString());
	}
	
	public boolean isDoubleArray(TypeMirror tm) {
		return double[].class.getCanonicalName().equals(tm.toString());
	}
	
	public boolean isBooleanArray(TypeMirror tm) {
		return boolean[].class.getCanonicalName().equals(tm.toString());
	}
	
	public boolean isString(TypeMirror tm) {
		return String.class.getName().equals(tm.toString());
	}

	
	public void assertTypesValidForImportedMethod(ExecutableElement element) 
	{
		for (VariableElement e: element.getParameters()) 
		{
			if (!isValid(e.asType()))
			{
				throw new RuntimeException("Type " + e.asType() + " for parameter " + e + " if method " + element + " in " + element.getEnclosingElement() +" is not valid for JNI methods");
			}
		}
		
		TypeMirror rt = element.getReturnType();
		
		if (!(isValid(rt) || rt instanceof NoType || isReturnPair(rt)))
		{
			throw new RuntimeException("Return type " + element.getReturnType() + " for method " + element + " in " + element.getEnclosingElement() +" is not valid for JNI methods");
		}
	}
	
	
	public String getCppType(Element element) 
	{
		CppType cppType = element.getAnnotation(CppType.class);
		
		if (cppType != null) {
			return cppType.value();
		}
		else {
			TypeMirror type = element.asType();
			if (type instanceof PrimitiveType) 
			{
				return JNITools.getTypeName(type);
			}
			else {
				String typeName = type.toString();
				if (String.class.getName().equals(typeName)) 
				{
					return "mariana::v1::U8String";
				}
				else if (UUID.class.getName().equals(typeName)) {
					return "memoria::v1::UUID";
				}
				else if (ByteBuffer.class.getName().equals(typeName)) {
					return "memoria::v1::Bytes";
				}
				else {
					throw new RuntimeException("Can't convert java type "+type+" to a native type");
				}
			}
		}
	}
	
	

	

	public boolean isReturnPair(TypeMirror rtn) 
	{
		return types.isAssignable(rtn, cxxObjectElement.asType());
	}

	public Types getTypes() {
		return types;
	}

	public Elements getElements() {
		return elements;
	}

	public boolean isImported(TypeMirror rtn) 
	{		
		TypeElement te = elements.getTypeElement(rtn.toString());		
		return te.getAnnotation(Import.class) != null;
	}
	
	public boolean isImported(TypeElement rtn) 
	{		
		return rtn.getAnnotation(Import.class) != null;
	}
	
	public boolean isExported(TypeMirror rtn) 
	{		
		TypeElement te = elements.getTypeElement(rtn.toString());		
		return te.getAnnotation(Export.class) != null;
	}
	
	public boolean isExported(TypeElement rtn) 
	{		
		return rtn.getAnnotation(Export.class) != null;
	}
	
	public boolean isIter(TypeMirror rtn) 
	{		
		TypeElement te = elements.getTypeElement(rtn.toString());		
		return te.getAnnotation(Iter.class) != null;
	}
	
	public boolean isIter(TypeElement rtn) 
	{		
		return rtn.getAnnotation(Iter.class) != null;
	}
	
	public void addTypeMapping(TypeMirror tm, TypeMapping mapping) {
		typeMapping.put(tm.toString(), mapping);
	}

	
	public TypeMapping getTypeMapping(String tm) 
	{
		TypeMapping m = typeMapping.get(tm);
		
		if (m != null) {
			return m;
		}
		else {
			throw new RuntimeException("Can't find type mapping for "+tm + ", mapping = " + typeMapping.keySet());
		}
	}

	public TypeElement getNative(TypeMirror tm) 
	{
		return elements.getTypeElement(tm.toString());
	}

	public void buildTypeMappings() 
	{
		for (TypeMapping mapping: typeMapping.values()) 
		{
			if (mapping.getParent() != null) 
			{
				mapping.setParentMapping(typeMapping.get(mapping.getParent()));
			}
		}
	}
	
	public boolean isSame(ExecutableElement first, ExecutableElement second)
	{
		return first.getKind() == second.getKind() && first.getSimpleName() == second.getSimpleName();
	}

	
}
