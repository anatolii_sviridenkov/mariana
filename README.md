Mariana is a general purpose Java data processing framework built on top of [Memoria](https://bitbucket.org/vsmirnov/memoria/).

Memoria provides flexible [non-relational schemes](https://bitbucket.org/vsmirnov/memoria/wiki/MemoriaForBigData) on top of block-oriented storage. It leverages C++14 template metaprogramming facilities to enable creation of myriads different data structures. Mariana is built around its own JNI binding generator that is specific to Memoira and its type system, gluing it to Java seamlessly. 

```java

public class Snippet
{
    static
    {
        Mariana.init(); // Initialize native library
    }
    
    public static void main( String[] args ) throws Exception
    {
        // Create new in-memory allocator to hold all data
        try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
        {
            // Create new snapshot. Data model in Mariana/Memoria is confluently persistent, 
            // much like in Git. Create, remove, merge, export, import sanpshots with data
            // change sets. 
            try(Snapshot snp = alloc.master().branch()) 
            {
                // Create simple map String -> String
                try(MapSS map = snp.create(MapSS.class))
                {
                    map.assign("Boo", "Zoo").close();

                    // Iterate over data
                    try(MapIterator iter = map.begin()) {
                        while (!iter.isEnd()) {
                            System.out.println("Key: " + iter.key() + " -- Value: " + iter.value());
                            iter.next();
                        }
                    }

                    // Finish snapshot, so not further updates are possible
                    snp.commit();

                    // Set this shapshot as master.
                    snp.setAsMaster();
                }
            }

            // Store entire allocator's data to disk.
            alloc.store("target/myalloc.dump");
        }
    }
}
```
### Build instructions ###

Mariana currently needs GCC 5.2+/clang 3.5+. Only Linux is supported at the moment. To build Mariana, run:

`mariana$ build.sh clean install`