
// Copyright 2016 Victor Smirnov
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariana.v1.framework;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.scijava.nativelib.DefaultJniExtractor;
import org.scijava.nativelib.NativeLoader;

import com.google.common.base.Throwables;

import mariana.v1.Mariana;
import mariana.v1.hashmap.ListMMapEntryConsumer2;
import mariana.v1.hashmap.MMapEntriesProducer;
import mariana.v1.hashmap.MMapEntry;
import mariana.v1.hashmap.MMapEntryParser;
import mariana.v1.io.buffer.IOByteBuffer;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.SequenceInputHandler;
import mariana.v1.io.serialization.SequenceOutputHandler;
import mariana.v1.io.serialization.UTF8StringInputHandler;
import mariana.v1.io.serialization.UTF8StringOutputHandler;

//@RunWith(JUnit4.class)
public class MultimapSerializationTest 
{
	static
	{
		Mariana.init();
	}

	public String getName() {
		return this.getClass().getSimpleName();
	}
	
	//@Test
	public final void testSerializations() throws Exception
	{
		int totalValues = 100000;

		for (int bufferSize = 256; bufferSize <= 65536; bufferSize *= 2) 
		{
			for (int bound = 10; bound <= 10000; bound *= 10) {
				doTestFor(totalValues, bound, bufferSize);
			}
		}
	}

	private void doTestFor(int totalValues, int bound, int bufferSize) 
	{
		ObjectOutputHandler outputHandler = new SequenceOutputHandler(new UTF8StringOutputHandler());

		ArrayList<MMapEntry> list = new ArrayList<>();

		Random RNG = new Random();

		for (int c = 0, d = 0; c < totalValues;) 
		{
			int length = RNG.nextInt(bound);

			List<Object> llist = new ArrayList<>();
			for (int dd = 0; dd < length; dd++, d++) 
			{
				llist.add("Str___" + d);
			}

			list.add(new MMapEntry(c, llist));

			c += length;
		}

		MMapEntriesProducer producer = new MMapEntriesProducer(list.iterator(), outputHandler);

		List<IOByteBuffer> buffers = new ArrayList<>();

		while(true)
		{
			IOByteBuffer buffer = new IOByteBuffer(ByteBuffer.allocateDirect(bufferSize).order(ByteOrder.LITTLE_ENDIAN));

			int entries = producer.apply(buffer);

			buffer.flip();
			buffers.add(buffer);

			if (entries <= 0) {
				break;
			}
		}

		ObjectInputHandler inputHandler = new SequenceInputHandler(new UTF8StringInputHandler());

		ListMMapEntryConsumer2 entryConsumer = new ListMMapEntryConsumer2();


		MMapEntryParser consumer = new MMapEntryParser(inputHandler, entryConsumer); 

		consumer.start();

		for (IOByteBuffer buffer: buffers) 
		{
			consumer.apply(buffer, 0);
		}

		consumer.finish();

		Assert.assertEquals(list.size(), entryConsumer.getList().size());

		Iterator<MMapEntry> i1 = list.iterator();
		Iterator<MMapEntry> i2 = entryConsumer.getList().iterator();

		while (i1.hasNext()) 
		{
			MMapEntry e1 = i1.next();
			MMapEntry e2 = i2.next();

			Assert.assertEquals(e1.getKey(), e2.getKey());

			Iterator<Object> ei1 = e1.getValues().iterator();
			Iterator<Object> ei2 = e2.getValues().iterator();

			while (ei1.hasNext()) 
			{
				Object s1 = ei1.next();
				Object s2 = ei2.next();

				Assert.assertEquals(s1, s2);
			}
		}
	}
}
