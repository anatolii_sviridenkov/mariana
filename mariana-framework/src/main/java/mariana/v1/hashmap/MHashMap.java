package mariana.v1.hashmap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLB;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLBEntryWalkerAdapter;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLBIterator;
import mariana.v1.io.buffer.IOByteBuffer;
import mariana.v1.io.serialization.FlatSequenceParser;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.SequenceInputHandler;

public class MHashMap<Key, Value> implements AutoCloseable {
	
	protected MultimapLB mmap;
	
	private ObjectOutputHandler bucketItemOutputHandler = null;
	private ObjectInputHandler  bucketItemInputHandler = null; 

	private SequenceInputHandler bucketInputHandler;
		
	public class Iterator implements java.util.Iterator<BucketItem<Key, Value>>, AutoCloseable {

		private MultimapLBIterator mmapIterator;
		private MultimapLBEntryWalkerAdapter walker;
		
		private EntriesScanner<BucketItem<Key, Value>> scanner = new EntriesScanner<>();
		private java.util.Iterator<BucketItem<Key, Value>> valuesIter;
		
		private long fetches = 0;
		private boolean done = false;

		private MMapEntryParser parser = new MMapEntryParser(bucketItemInputHandler, scanner);
				
		public Iterator(MultimapLBIterator mmapIterator) 
		{
			this.mmapIterator = mmapIterator;
			if (mmapIterator != null) {
				walker = new MultimapLBEntryWalkerAdapter();
			}
		}
		
		public void close() 
		{
			if (mmapIterator != null) 
			{
				mmapIterator.close();
				walker.close();
			}
		}
		
		public boolean hasNext() 
		{
			if (mmapIterator == null) {
				return false;
			}
			
			if (valuesIter == null) 
			{
				fetch();
				return valuesIter.hasNext();
			}
			
			if (valuesIter.hasNext()) {
				return true;
			}
			else if (!done) 
			{
				fetch();
				return valuesIter.hasNext();
			}
			else {
				return false;	
			}
		}
		
		public BucketItem<Key, Value> next() {
			return valuesIter.next();
		}
		
		private void fetch()
		{			
			scanner.clear();
			
			IOByteBuffer ioBuffer = new IOByteBuffer(walker.getBuffer());
			
			long total = mmapIterator.populate(walker, Long.MAX_VALUE, fetches > 0);
			
			ioBuffer.flip();
			parser.apply(ioBuffer, 1);
			
			fetches++;			
			
			done = total <= 0;
			valuesIter = scanner.getValues().iterator();
		}
	}
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected MHashMap(
			MultimapLB mmap, 
			ObjectOutputHandler keyOutputHandler, 
			ObjectInputHandler keyInputHandler,
			ObjectOutputHandler valueOutputHandler, 
			ObjectInputHandler valueInputHandler
	)
	{
		this.mmap = mmap;
		
		bucketItemOutputHandler = new BucketItemOutputHandler(keyOutputHandler, valueOutputHandler);
		bucketItemInputHandler  = new BucketItemInputHandler(keyInputHandler, valueInputHandler);
		
		keyOutputHandler.setPropertyGetter(bucketItem-> {
			return ((BucketItem)bucketItem).getKey();
		});
		
		valueOutputHandler.setPropertyGetter(bucketItem-> {
			return ((BucketItem)bucketItem).getValue();
		});
		
		keyInputHandler.setPropertySetter((bucketItem, value) -> {
			((BucketItem<Key, Value>)bucketItem).setKey((Key) value);
		});
		
		valueInputHandler.setPropertySetter((bucketItem, value) -> {
			((BucketItem<Key, Value>)bucketItem).setValue((Value) value);
		});
		
		bucketInputHandler  = new SequenceInputHandler(bucketItemInputHandler);
	}
	
	public UUID name() {		
		return mmap.name();
	}
	
	public Iterator iterator() 
	{
		return new Iterator(mmap.begin());
	}
	
	@SuppressWarnings("unchecked")
	public Value get(Key key) 
	{
		int hashCode = key.hashCode();		
		
		try (MultimapLBIterator ii = mmap.find(hashCode)) 
		{
			if (ii.isFound(hashCode)) 
			{
				ii.next();
				List<BucketItem<Key, Value>> list = new ArrayList<>();
				ii.readValues(new FlatSequenceParser(bucketInputHandler, bucketItem -> {
					list.add((BucketItem<Key, Value>)bucketItem);
				}));
				
				for (BucketItem<Key, Value> item: list) 
				{
					if (item.getKey().equals(key)) 
					{
						return item.getValue();
					}
				}
			}
		}
		
		return null;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void set(Key key, Value value)
	{
		int hashCode = key.hashCode();
		
		try (MultimapLBIterator ii = mmap.find(hashCode)) 
		{
			ArrayList<BucketItem<Key, Value>> bucket;
			
			if (ii.isFound(hashCode)) 
			{
				ii.next();
				
				ArrayList<BucketItem<Key, Value>> list = new ArrayList<>();
				ii.readValues(new FlatSequenceParser(bucketInputHandler, bucketItem -> {
					list.add((BucketItem<Key, Value>)bucketItem);
				}));
					
				bucket = list;
				
				ii.prevKey();
				ii.remove(1);
				
				boolean found = false;
				
				for (BucketItem<Key, Value> item: bucket) 
				{
					if (item.getKey().equals(key)) 
					{
						item.setValue(value);
						found = true;
						break;
					}
				}
				
				if (!found) 
				{
					bucket.add(new BucketItem<Key, Value>(key, value));
				}
			}
			else {
				bucket = new ArrayList<>();
				bucket.add(new BucketItem<Key, Value>(key, value));
			}
			
			ii.insert(new MMapEntriesProducer(
					Collections.singletonList(new MMapEntry(hashCode, (ArrayList)bucket)).iterator(), 
					bucketItemOutputHandler
			));
		}
	}
	
	public void remove(Key key) 
	{
		
	}
	
	@SuppressWarnings("unchecked")
	public Value firdOrCreate(Key key, Supplier<Value> supplier) 
	{		
		int hashCode = key.hashCode();
		
		Value value;
		
		try (MultimapLBIterator ii = mmap.find(hashCode))
		{
			ArrayList<BucketItem<Key, Value>> bucket;
			
			if (ii.isFound(hashCode)) 
			{
				ii.next();
				ArrayList<BucketItem<Key, Value>> list = new ArrayList<>();
				ii.readValues(new FlatSequenceParser(bucketInputHandler, bucketItem -> {
					list.add((BucketItem<Key, Value>)bucketItem);
				}));
				
				bucket = list;
				
				
				
				for (BucketItem<Key, Value> item: bucket) 
				{
					if (item.getKey().equals(key)) 
					{
//						System.out.println("Found Key " + key + " -- " + item.getValue());
						return item.getValue();
					}
				}
				
				ii.prevKey();
				ii.remove(1);
				
				value = supplier.get();
				
//				System.out.println("NOT(1) Found Key " + key + " -- " + value);
				
				bucket.add(new BucketItem<Key, Value>(key, value));				
			}
			else {
				bucket = new ArrayList<>();
				value = supplier.get();
				
//				System.out.println("NOT(2) Found Key " + key + " -- " + value);
				bucket.add(new BucketItem<Key, Value>(key, value));
			}
			
			ii.insert(new MMapEntriesProducer(
					Collections.singletonList(new MMapEntry(hashCode, (ArrayList)bucket)).iterator(), 
					bucketItemOutputHandler
			));
			
			return value;
		}
	}
	
	
	
	public static <Key, Value> MHashMap<Key, Value> findOrCreate(
			Snapshot snapshot, 
			UUID name,
			ObjectOutputHandler keyOutputHandler, 
			ObjectInputHandler keyInputHandler,
			ObjectOutputHandler valueOutputHandler,
			ObjectInputHandler valueInputHandler
	)
	{
		return new MHashMap<Key, Value>(snapshot.findOrCreate(MultimapLB.class, name), keyOutputHandler, keyInputHandler, valueOutputHandler, valueInputHandler);
	}
	
	public static <Key, Value> MHashMap<Key, Value> find(
			Snapshot snapshot, 
			UUID name,
			ObjectOutputHandler keyOutputHandler, 
			ObjectInputHandler keyInputHandler,
			ObjectOutputHandler valueOutputHandler,
			ObjectInputHandler valueInputHandler
	)
	{
		return new MHashMap<Key, Value>(snapshot.find(MultimapLB.class, name), keyOutputHandler, keyInputHandler, valueOutputHandler, valueInputHandler);
	}

	@Override
	public void close() {
		mmap.close();
	}

	public MultimapLB getMmap() {
		return mmap;
	}
	
	
}
