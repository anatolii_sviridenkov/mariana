package mariana.v1.hashmap;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.buffer.IOBufferConsumer;
import mariana.v1.io.buffer.SymbolsRun;
import mariana.v1.io.serialization.ObjectInputHandler;

public class MMapEntryParser implements IOBufferConsumer {

	private final ObjectInputHandler inputHandler;
	
	private boolean skipRemainig;	
	private final MMapEntryConsumer entryConsumer;
	
	private boolean data = false;
	
	public MMapEntryParser(ObjectInputHandler inputHandler, MMapEntryConsumer entryConsumer) 
	{
		this.inputHandler   = inputHandler;
		this.entryConsumer  = entryConsumer;

		inputHandler.setPropertySetter((tgt, value) -> {
			((MMapEntryParser)tgt).entryConsumer.value(value);
		});
		
		inputHandler.reset(this);
	}
	
	@Override
	public int apply(IOBuffer buffer, int entries) 
	{
		while(buffer.remaining() > 0) 
		{
			SymbolsRun run  = buffer.getSymbolsRun();
			int runLength   = (int) run.getLength();
			int symbol 		= run.getSymbol();
			
			if (symbol == 0) 
			{			
				if (data) 
				{
					entryConsumer.endValues();
					entryConsumer.endEntry();
				}
				
				for (int e = 0; e < runLength - 1; e++) 
				{
					long key = buffer.getLong();
				
					entryConsumer.startEntry(key);
					entryConsumer.startValues();
					entryConsumer.endValues();
					entryConsumer.endEntry();
					
					inputHandler.reset(this);
				}

				entryConsumer.startEntry(buffer.getLong());
				entryConsumer.startValues();

				skipRemainig = false;
				data = true;
			}
			else if (!skipRemainig) 
			{
				int l0 = buffer.limit();
				
				buffer.limit(buffer.pos() + runLength);
				
				if (inputHandler.readFrom(buffer))
				{
					inputHandler.reset(this);
					skipRemainig = true;
				}
				
				buffer.limit(l0);				
			}
			else {
				buffer.skip(runLength);
			}
		}
		
		buffer.done();
		
		return entries;
	}

	public boolean hasData() {
		return data;
	}

	public void start() {
		entryConsumer.start();
	}
	
	public void finish() 
	{
		if (hasData()) 
		{
			entryConsumer.endValues();
			entryConsumer.endEntry();
		}
		
		entryConsumer.end();
	}
}
