package mariana.v1.hashmap;

import java.util.ArrayList;

public class BucketHolder<B> {
	
	private ArrayList<B> bucket;
	
	public ArrayList<B> getBucket() {
		return bucket;
	}

	public void setBucket(ArrayList<B> bucket) {
		this.bucket = bucket;
	}
}
