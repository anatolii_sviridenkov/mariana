package mariana.v1.hashmap;

import mariana.v1.io.serialization.AbstractClassOutputHandler;
import mariana.v1.io.serialization.ObjectOutputHandler;

public class BucketItemOutputHandler<Key, Value> extends AbstractClassOutputHandler  {

	public BucketItemOutputHandler(ObjectOutputHandler keyHandler, ObjectOutputHandler valueHandler) 
	{
		super(new ObjectOutputHandler[]{keyHandler, valueHandler});
	}	
}
