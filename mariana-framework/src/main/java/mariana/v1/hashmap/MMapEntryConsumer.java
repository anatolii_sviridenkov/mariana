package mariana.v1.hashmap;

public interface MMapEntryConsumer {
	void start();
	void startEntry(long key);
	void startValues();
	void value(Object value);
	void endValues();
	void endEntry();
	void end();
}
