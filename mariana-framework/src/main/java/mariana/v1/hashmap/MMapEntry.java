package mariana.v1.hashmap;

public class MMapEntry {
	
	private long key;
	private Iterable<Object> values;
	
	public MMapEntry(long key, Iterable<Object> values) {
		this.key = key;
		this.values = values;		
	}
	
	public long getKey() {
		return key;
	}
	
	public void setKey(long key) {
		this.key = key;
	}

	public Iterable<Object> getValues() {
		return values;
	}

	public void setValues(Iterable<Object> values) {
		this.values = values;
	}
}
