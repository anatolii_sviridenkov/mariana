package mariana.v1.hashmap;

import java.util.ArrayList;
import java.util.List;

public class EntriesScanner<V> implements MMapEntryConsumer {

	private List<V> values = new ArrayList<>(10000);
	
	@Override
	public void start() {}

	@Override
	public void startEntry(long key) {}

	@Override
	public void startValues() {}

	@SuppressWarnings("unchecked")
	@Override
	public void value(Object value) {
		values.add((V) value);
	}

	@Override
	public void endValues() {}

	@Override
	public void endEntry() {}

	@Override
	public void end() {}

	public List<V> getValues() {
		return values;
	}

	public void clear() {
		values.clear();
	}

}
