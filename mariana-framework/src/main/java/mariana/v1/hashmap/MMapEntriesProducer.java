package mariana.v1.hashmap;

import java.util.Iterator;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.buffer.IOBufferProducer;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.SequenceOutputHandler;
import mariana.v1.tools.codecs.UInt63Codec;

public class MMapEntriesProducer implements IOBufferProducer {

	private static final int DEFAULT_RUN_LENGTH = 1024;
	
	private final ObjectOutputHandler outputHandler;
	
	boolean headerWritten = false;
	
	private Iterator<MMapEntry> iterator;
	
	private MMapEntry currentEntry;
	
	public MMapEntriesProducer(Iterator<MMapEntry> iterator, ObjectOutputHandler valuesOutputHandler) 
	{
		this.iterator 		= iterator;
		this.outputHandler 	= new SequenceOutputHandler(valuesOutputHandler);
		
		outputHandler.setPropertyGetter(tgt -> {
			return ((MMapEntriesProducer)tgt).currentEntry.getValues().iterator();
		});
	}
	
	@Override
	public int apply(IOBuffer buffer) 
	{
		int entries = 0;
		
		while(buffer.remaining() > UInt63Codec.LENGTH_MAX) 
		{
			if (!headerWritten) 
			{
				if (iterator.hasNext()) 
				{
					currentEntry = iterator.next();
					outputHandler.reset(this);

					buffer.mark();
					
					if (buffer.putSymbol(0, 1)) 
					{
						if (buffer.putLong(currentEntry.getKey())) 
						{
							entries += 2;
						}
						else {
							buffer.reset();
							return entries;
						}
					}
					else {
						buffer.reset();
						return entries;
					}
					
					headerWritten = true;
				}
				else {
					return -entries;
				}
			}
			
			int runPos = buffer.pos();
			int runLength = makeDataRun(buffer);
			if (runLength > 0) 
			{
				int pos0 = buffer.pos();
				int ll0 = buffer.limit();
				buffer.limit(pos0 + runLength);
				boolean done = outputHandler.writeTo(buffer);

				int pos1 = buffer.pos();

				if (pos1 > pos0) 
				{
					int actualRunLength = pos1 - pos0;
					entries += 1 + actualRunLength;
					
					if (pos1 < pos0 + runLength) 
					{						
						buffer.updateSymbolsRun(runPos, 1, actualRunLength);
					}
					
					buffer.limit(ll0);

					if (done) 
					{
						headerWritten = false;						
					}
				}
				else {
					buffer.limit(ll0);
					buffer.pos(runPos);

					if (done) {
						headerWritten = false;
					}
				}
			}
			else {
				return entries;
			}
		}
		
		return entries;
	}
	

	private int makeDataRun(IOBuffer buffer) 
	{
		int remaining = buffer.remaining() - UInt63Codec.LENGTH_MAX;
		int maxRunLength = DEFAULT_RUN_LENGTH < remaining ? DEFAULT_RUN_LENGTH : remaining;

		if (maxRunLength > 0) 
		{
			buffer.putSymbol(1, maxRunLength);
			return maxRunLength;
		}
		else {
			return 0;
		}
	}
}
