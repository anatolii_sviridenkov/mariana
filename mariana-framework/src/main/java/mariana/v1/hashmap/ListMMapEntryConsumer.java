package mariana.v1.hashmap;

import java.util.ArrayList;
import java.util.List;

public class ListMMapEntryConsumer<T> implements MMapEntryConsumer {

	private List<T> list;
	
	@Override
	public void start() {}

	@Override
	public void startEntry(long key) {
		list = new ArrayList<>();
	}

	@Override
	public void startValues() {}

	@SuppressWarnings("unchecked")
	@Override
	public void value(Object value) {
		list.add((T) value);
	}

	@Override
	public void endValues() {}

	@Override
	public void endEntry() {}

	@Override
	public void end() {}

	public List<T> getList() {
		return list;
	}

}
