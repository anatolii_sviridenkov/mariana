package mariana.v1.hashmap;

import mariana.v1.io.serialization.AbstractClassInputHandler;
import mariana.v1.io.serialization.ObjectInputHandler;

public class BucketItemInputHandler<Key, Value> extends AbstractClassInputHandler  {

	public BucketItemInputHandler(ObjectInputHandler keyHandler, ObjectInputHandler valueHandler) 
	{
		super(new ObjectInputHandler[]{keyHandler, valueHandler});		
	}

	@Override
	protected Object newInstance() {
		return new BucketItem<Key, Value>();
	}
}
