package mariana.v1.hashmap;

import java.util.ArrayList;
import java.util.List;

public class ListMMapEntryConsumer2 implements MMapEntryConsumer {

	private List<MMapEntry> list = new ArrayList<>();
	
	private MMapEntry current;
	private ArrayList<Object> values;
	
	@Override
	public void start() {}

	@Override
	public void startEntry(long key) {
		current = new MMapEntry(key, null);
	}

	@Override
	public void startValues() {
		values = new ArrayList<>();
	}

	@Override
	public void value(Object value) {
		values.add(value);
	}

	@Override
	public void endValues() {
		current.setValues(values);
	}

	@Override
	public void endEntry() {
		list.add(current);
	}

	@Override
	public void end() {}

	public List<MMapEntry> getList() {
		return list;
	}
}
