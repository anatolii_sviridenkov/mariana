package mariana.v1.hashmap;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.buffer.IOBufferProducer;
import mariana.v1.io.serialization.OutputHandler;
import mariana.v1.tools.codecs.UInt63Codec;

public class MMapEntryProducer implements IOBufferProducer {

	private final long hashCode;
	private final OutputHandler outputHandler;
	
	boolean headerWritten = false;
	boolean dataReadFinished = false;
	boolean hasData = false;
	
	private static final int DEFAULT_RUN_LENGTH = 1024;
	
	public MMapEntryProducer(long hashCode, OutputHandler outputHandler) 
	{
		this.hashCode 		= hashCode;
		this.outputHandler 	= outputHandler;
	}
	
	@Override
	public int apply(IOBuffer buffer) 
	{
		int entries = 0;
		
		if (!headerWritten) 
		{
			buffer.putSymbol(0, 1);
			buffer.putLong(hashCode);
			
			entries += 2;
			
			headerWritten = true;
		}
		
		while(buffer.remaining() > UInt63Codec.LENGTH_MAX) 
		{
			int runPos = buffer.pos();
			int runLength = makeDataRun(buffer);
			if (runLength > 0) 
			{
				int pos0 = buffer.pos();
				int ll0  = buffer.limit();
				buffer.limit(pos0 + runLength);
				boolean done = outputHandler.writeTo(buffer);

				int pos1 = buffer.pos();

				if (pos1 > pos0) 
				{
					int actualRunLength = pos1 - pos0;
					entries += 1 + actualRunLength;
					
					if (pos1 < pos0 + runLength) 
					{						
						buffer.updateSymbolsRun(runPos, 1, actualRunLength);
					}
					
					buffer.limit(ll0);

					if (done) 
					{
						return -entries;
					}
				}
				else {
					buffer.limit(ll0);
					buffer.pos(runPos);

					return done? -entries : entries;
				}
			}
			else {
				return entries;
			}
		}
		
		return entries;
	}
	

	private int makeDataRun(IOBuffer buffer) 
	{
		int remaining = buffer.remaining() - UInt63Codec.LENGTH_MAX;
		int maxRunLength = DEFAULT_RUN_LENGTH < remaining ? DEFAULT_RUN_LENGTH : remaining;
		
		buffer.putSymbol(1, maxRunLength);
		
		return maxRunLength;
	}
}
