package mariana.v1.hashmap;

public class BucketItem<Key, Value> {
	private Key key;
	private Value value;
	
	public BucketItem() {}
	
	public BucketItem(Key key, Value value) 
	{
		this.key 	= key;
		this.value 	= value;
	}

	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		return "BucketItem[" + key + "," + value + "]";
	}
}
