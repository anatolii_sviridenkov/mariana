package mariana.v1.hashmap;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.UTF8StringInputHandler;
import mariana.v1.io.serialization.UTF8StringOutputHandler;
import mariana.v1.tools.Ticker;

public class MHashMapTest {
	static {
		Mariana.init();
	}
	
	public static void main(String[] args) 
	{
		ObjectInputHandler  keyInputHandler  = new UTF8StringInputHandler();
		ObjectOutputHandler keyOutputHandler = new UTF8StringOutputHandler();
		
		ObjectInputHandler  valueInputHandler  = new UTF8StringInputHandler();
		ObjectOutputHandler valueOutputHandler = new UTF8StringOutputHandler();
		
		try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{
    			try (Snapshot snp = master.branch()) 
    			{
    				try(MHashMap<String, String> mmap = MHashMap.<String, String>findOrCreate(
    						snp, 
    						UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fdaaa"), 
    						keyOutputHandler,
    						keyInputHandler,
    						valueOutputHandler,
    						valueInputHandler
    				))
    				{
    					HashMap<String, String> map = new HashMap<>();
    					
    					for (int c = 0; c < 1000000; c++)
    					{
    						map.put("Key__"+c, "Value__" + c);
    					}
    					
    					Ticker ticker = new Ticker(100000);
    					
    					long t0 = System.currentTimeMillis();
    					for (Map.Entry<String, String> entry: map.entrySet()) 
    					{
    						mmap.set(entry.getKey(), entry.getValue());
    						
    						if (ticker.isThreshold()) {
    							System.out.println("Inserted " + ticker.getTicks() + " in " + ticker.duration());
    							ticker.nextThreshold();
    						}
    						
    						ticker.tick();
    					}
    					System.out.println("Insertion time: "+(System.currentTimeMillis() - t0));
    					
    					long t1 = System.currentTimeMillis();
    					for (Map.Entry<String, String> entry: map.entrySet()) 
    					{
    						String value = mmap.get(entry.getKey());
    						
    						if (!value.equals(entry.getValue())) 
    						{
    							System.out.println("Fail!: " + entry.getKey() + " -- " + entry.getValue() + " -- " + value);
    						}
    					}
    					
    					System.out.println("Reading time: "+(System.currentTimeMillis() - t1));
    					
//    					int cnt = 0;
    					
    					long t2 = System.currentTimeMillis();
    					
    					try(MHashMap<String, String>.Iterator ii = mmap.iterator()) 
    					{
    						while (ii.hasNext()) 
    						{
//    							System.out.println(ii.next() + " -- " + cnt);
//    							cnt++;
    							ii.next();
    						}
    					}
    					
    					System.out.println("Scan time: "+(System.currentTimeMillis() - t2));
    				}
    				
    				snp.commit();

    				snp.setAsMaster();
    				
//    				snp.dump("target/hmap.dir");
    			}
    		}
    	}
	}
}
