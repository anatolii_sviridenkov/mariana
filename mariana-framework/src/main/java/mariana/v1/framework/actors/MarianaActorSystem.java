package mariana.v1.framework.actors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MarianaActorSystem {
//	private static MarianaActorSystem INSTANCE = new MarianaActorSystem();
	
	private final ExecutorService executorService = Executors.newCachedThreadPool();
	private final MarianaActors actors;
	
	public MarianaActorSystem() 
	{
		actors = new MarianaMultiThreadedActors(executorService);
	}
	
//	public static MarianaActorSystem getInstance()
//	{
//		return INSTANCE;
//	}
	
	public void shutdown()
	{
		executorService.shutdown();
	}

	public MarianaActors getActors() {
		return actors;
	}
}
