package mariana.v1.framework.actors;
import fi.jumi.actors.ActorRef;
import fi.jumi.actors.ActorThread;
import fi.jumi.actors.eventizers.Event;
import fi.jumi.actors.eventizers.Eventizer;
import fi.jumi.actors.eventizers.EventizerProvider;
import fi.jumi.actors.listeners.FailureHandler;
import fi.jumi.actors.listeners.MessageListener;
import fi.jumi.actors.queue.MessageQueue;
import fi.jumi.actors.queue.MessageSender;



public abstract class MarianaActors {

    private final EventizerProvider eventizerProvider;
    private final FailureHandler failureHandler;
    private final MessageListener messageListener;

    protected MarianaActors(EventizerProvider eventizerProvider, FailureHandler failureHandler, MessageListener messageListener) {
        this.eventizerProvider = eventizerProvider;
        this.failureHandler = failureHandler;
        this.messageListener = messageListener;
    }

    public ActorThread startActorThread() 
    {
        ActorsDispatchingThread actorThread = new ActorsDispatchingThread();
        startActorsThread(actorThread);
        return actorThread;
    }

    abstract void startActorsThread(MarianaActorMessageProcessor actorThread);

    private class ActorsDispatchingThread implements ActorThread, MarianaActorMessageProcessor {

        private final MessageQueue<Runnable> taskQueue = new MessageQueue<Runnable>();

        @Override
        public <T> ActorRef<T> bindActor(Class<T> type, T rawActor) {
            Eventizer<T> eventizer = eventizerProvider.getEventizerForType(type);
            T proxy = eventizer.newFrontend(new MarianaMessageSender<T>(this, rawActor));
            return ActorRef.wrap(type.cast(proxy));
        }

        @Override
        public void stop() {
            taskQueue.send(()->{
            	Thread.currentThread().interrupt();
            });
        }

        public void send(Message<?> task) {
            taskQueue.send(task);
        }

        @Override
        public void processNextMessage() throws InterruptedException {
            Runnable task = taskQueue.take();
            process(task);
        }

        @Override
        public boolean processNextMessageIfAny() 
        {
            Runnable task = taskQueue.poll();
            if (task == null) 
            {
                return false;
            }
            
            process(task);
            return true;
        }

        private void process(Runnable task) {
            task.run();
        }
    }

    
    private class MarianaMessageSender<T> implements MessageSender<Event<T>> {
        private final ActorsDispatchingThread actorThread;
        private final T actorObject;

        public MarianaMessageSender(ActorsDispatchingThread actorThread, T actorObject) {
            this.actorThread = actorThread;
            this.actorObject = actorObject;
        }

        public void send(final Event<T> message) {
            messageListener.onMessageSent(message);
            actorThread.send(new Message<T>(actorObject, message));
        }
        
        @Override
        public String toString() {
        	return actorObject.toString();
        }
    }

    
    private class Message<T> implements Runnable {
        private final T actorObject;
        private final Event<T> message;

        public Message(T actorObject, Event<T> message) {
            this.actorObject = actorObject;
            this.message = message;
        }

        public void run() {
            messageListener.onProcessingStarted(actorObject, message);
            try {
                message.fireOn(actorObject);
            } 
            catch (Throwable t) {
                failureHandler.uncaughtException(actorObject, message, t);
            } 
            finally {
                messageListener.onProcessingFinished();
            }
        }
    }
}
