package mariana.v1.framework.actors;

import java.lang.reflect.Proxy;

import fi.jumi.actors.eventizers.Event;
import fi.jumi.actors.eventizers.Eventizer;
import fi.jumi.actors.eventizers.dynamic.EventToDynamicListener;
import fi.jumi.actors.queue.MessageSender;

public class MarianaEventizer<T> implements Eventizer<T> {

    private final Class<T> type;

    public MarianaEventizer(Class<T> type) {
        MarianaEventizers.validateActor(type);
        this.type = type;
    }

    @Override
    public Class<T> getType() {
        return type;
    }

    @Override
    public T newFrontend(MessageSender<Event<T>> target) {
        return type.cast(Proxy.newProxyInstance(
                type.getClassLoader(),
                new Class<?>[]{type},
                new MarianaListenerToEvent<T>(target))
        );
    }

    @Override
    public MessageSender<Event<T>> newBackend(T target) {
        return new EventToDynamicListener<T>(target);
    }
}
