package mariana.v1.framework.actors;


class MarianaBlockingActorProcessor implements Runnable {
	private final MarianaActorMessageProcessor actorThread;

	public MarianaBlockingActorProcessor(MarianaActorMessageProcessor actorThread) {
		this.actorThread = actorThread;
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) 
			{
				actorThread.processNextMessage();
			}
		} 
		catch (InterruptedException e) {
			// finish processing
		}
	}
}