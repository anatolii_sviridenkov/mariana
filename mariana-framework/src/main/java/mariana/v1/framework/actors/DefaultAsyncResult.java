package mariana.v1.framework.actors;

public class DefaultAsyncResult<T> implements Async<T> {

	private T value;
	
	public DefaultAsyncResult(T value) {
		this.value = value;
	}
	
	@Override
	public T get() {
		return value;
	}

	@Override
	public boolean isDone() {
		return true;
	}
}
