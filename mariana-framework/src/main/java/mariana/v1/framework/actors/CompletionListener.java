package mariana.v1.framework.actors;

public interface CompletionListener<T> {
	void methodFinished(CompletionPort<T> result);
}
