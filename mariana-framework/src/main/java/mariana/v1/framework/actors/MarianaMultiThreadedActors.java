package mariana.v1.framework.actors;
import java.util.concurrent.Executor;

import fi.jumi.actors.eventizers.EventizerProvider;
import fi.jumi.actors.listeners.CrashEarlyFailureHandler;
import fi.jumi.actors.listeners.FailureHandler;
import fi.jumi.actors.listeners.MessageListener;
import fi.jumi.actors.listeners.NullMessageListener;


public class MarianaMultiThreadedActors extends MarianaActors {

    private final Executor executor;

    public MarianaMultiThreadedActors(Executor executor, EventizerProvider eventizerProvider, FailureHandler failureHandler, MessageListener messageListener) {
        super(eventizerProvider, failureHandler, messageListener);
        this.executor = executor;
    }
    
    public MarianaMultiThreadedActors(Executor executor) {
        super(new MarianaEventizerProvider(), new CrashEarlyFailureHandler(), new NullMessageListener());
        this.executor = executor;
    }

    @Override
    void startActorsThread(MarianaActorMessageProcessor actorThread) {
        executor.execute(new MarianaBlockingActorProcessor(actorThread));
    }
}