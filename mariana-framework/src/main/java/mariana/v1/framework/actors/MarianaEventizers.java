package mariana.v1.framework.actors;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class MarianaEventizers {

    private MarianaEventizers() {}

    public static void validateActor(Class<?> type) 
    {
        checkIsInterface(type);
        
        for (Method method : type.getMethods()) 
        {
            checkReturnTypeIsValid(type, method);
            checkMethodDoesNotThrowExceptions(type, method);
        }
    }

    private static void checkIsInterface(Class<?> type) 
    {
        if (!type.isInterface()) 
        {
            throw new IllegalArgumentException("Expecting actor interface, but got " + type);
        }
    }

    private static void checkReturnTypeIsValid(Class<?> type, Method method) 
    {
        Class<?> returnType = method.getReturnType();
        if (Async.class.isAssignableFrom(returnType)) 
        {
        	// ok
        }
        else if (!returnType.equals(Void.TYPE)) 
        {
            throw new IllegalArgumentException("Expecting actor method either void or return " + Async.class.getName() + ", " +
                    "but method " + method.getName() + " of " + type + " had return type " + returnType.getName());
        }
    }

    private static void checkMethodDoesNotThrowExceptions(Class<?> type, Method method) 
    {
    	Class<?>[] exceptionTypes = method.getExceptionTypes();
        if (exceptionTypes.length > 0) 
        {
            throw new IllegalArgumentException("Expecting actor's methods do not have declared exceptions, " +
                    "but method " + method.getName() + " of " + type + " throws " + format(exceptionTypes));
        }
    }

    private static String format(Class<?>[] types) 
    {
        List<String> names = new ArrayList<String>();
        
        for (Class<?> type : types) {
            names.add(type.getName());
        }
        
        String s = names.toString();
        return s.substring(1, s.length() - 1);
    }
}
