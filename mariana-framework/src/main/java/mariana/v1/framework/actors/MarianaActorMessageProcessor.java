package mariana.v1.framework.actors;

interface MarianaActorMessageProcessor {

    void processNextMessage() throws InterruptedException;

    boolean processNextMessageIfAny();
}
