package mariana.v1.framework.actors;

public class CompletionPort<T> implements Async<T> {

	private T result;
	private Throwable exception;
	private volatile boolean done = false;
	private CompletionListener<T> listener;
	
	public CompletionPort() {}
	
	@Override
	public synchronized T get() 
	{		
		while(!done) 
		{
			try {
				this.wait();
			} 
			catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
		
		if (exception != null) 
		{
			throw new ActorMethodException(exception);
		}
		
		return result;
	}
	
	public synchronized Throwable getException() {
		return exception;
	}
	
	public synchronized void waitFor(CompletionListener<T> litener) 
	{		
		this.listener = litener;
		
		if (done) {
			listener.methodFinished(this);
		}
	}
	
	
	
	synchronized void result(T result) {
		this.result = result;
		this.done = true;
		
		this.notify();
		
		if (listener != null) 
		{
			listener.methodFinished(this);
		}
	}
	
	synchronized void exception(Throwable exception) {
		this.exception = exception;
		this.done = true;
		
		this.notify();
		
		if (listener != null) 
		{
			listener.methodFinished(this);
		}
	}
	
	public synchronized void join() {
		while(!done) 
		{
			try {
				this.wait();
			} 
			catch (InterruptedException e) {}
		}
	}

	@Override
	public synchronized boolean isDone() {
		return done;
	}
}
