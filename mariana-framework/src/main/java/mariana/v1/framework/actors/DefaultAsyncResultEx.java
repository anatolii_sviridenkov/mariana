package mariana.v1.framework.actors;

public class DefaultAsyncResultEx<T> implements Async<T> {

	private final T value;
	private final Throwable exception;
	
	public DefaultAsyncResultEx(T value) 
	{
		this.value = value;
		this.exception = null;
	}
	
	public DefaultAsyncResultEx(T value, Throwable exception) 
	{
		this.value = null;
		this.exception = exception;
	}
	
	public DefaultAsyncResultEx(Async<T> result) 
	{
		if (result.getException() != null) 
		{
			this.exception 	= result.getException();
			this.value 		= null;
		}
		else {
			this.value 		= result.get();
			this.exception 	= null;
		}
	}
	
	@Override
	public T get() {
		return value;
	}

	@Override
	public boolean isDone() {
		return true;
	}

	public Throwable getException() {
		return exception;
	}
}
