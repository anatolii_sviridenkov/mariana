package mariana.v1.framework.actors;

public class ActorMethodException extends RuntimeException {

	private static final long serialVersionUID = 8685466072163937416L;

	public ActorMethodException() {
		super();
	}

	public ActorMethodException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ActorMethodException(String message, Throwable cause) {
		super(message, cause);
	}

	public ActorMethodException(String message) {
		super(message);
	}

	public ActorMethodException(Throwable cause) {
		super(cause);
	}

}
