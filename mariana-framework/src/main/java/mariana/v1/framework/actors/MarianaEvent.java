package mariana.v1.framework.actors;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import fi.jumi.actors.eventizers.Event;
import fi.jumi.actors.eventizers.EventToString;

public class MarianaEvent<T> implements Event<T> {
	
	private transient Method method;
    private final Object[] args;

	@SuppressWarnings("rawtypes")
	private CompletionPort completionPort;

    public MarianaEvent(Method method, Object[] args, @SuppressWarnings("rawtypes") CompletionPort resultHandler) {
        this.method = method;
        this.args = args;
		this.completionPort = resultHandler;
    }

    
    @SuppressWarnings({"unchecked", "rawtypes"})
	@Override
    public void fireOn(T target) 
    {
    	if (completionPort == null)
    	{
    		try {
    			method.invoke(target, args);
    		} 
    		catch (IllegalAccessException e) {
    			throw new RuntimeException(e);
    		}
    		catch (InvocationTargetException e) {
    			throw new RuntimeException(e);
    		}
    	}
    	else {
    		try {    			
				Async r = (Async) method.invoke(target, args);
				r.join();
    			completionPort.result(r.get());
    		}
    		catch (InvocationTargetException e) {
    			completionPort.exception(e.getTargetException());
    		}
    		catch (Throwable e) {
    			completionPort.exception(e);
    		} 
    	}
    }

    @Override
    public String toString() {
        return EventToString.format(method.getDeclaringClass().getSimpleName(), method.getName(), args == null ? new Object[0] : args);
    }
}
