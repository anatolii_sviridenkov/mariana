package mariana.v1.framework.actors;

import java.util.concurrent.ConcurrentHashMap;

import fi.jumi.actors.eventizers.Eventizer;
import fi.jumi.actors.eventizers.EventizerProvider;

public class MarianaEventizerProvider implements EventizerProvider {

    private final ConcurrentHashMap<Class<?>, Eventizer<?>> cache = new ConcurrentHashMap<Class<?>, Eventizer<?>>();

    @SuppressWarnings("unchecked")
    @Override
    public <T> Eventizer<T> getEventizerForType(Class<T> type) {
        Eventizer<T> eventizer = (Eventizer<T>) cache.get(type);
        if (eventizer == null) {
            eventizer = new MarianaEventizer<T>(type);
            cache.put(type, eventizer);
        }
        return eventizer;
    }
}