package mariana.v1.framework.actors;

public interface Async<T> {
	T get();
	boolean isDone();
	
	static <T> Async<T> of(T value) {
		return new DefaultAsyncResult<T>(value);
	}
	
	static <T> Async<T> ofNull() {
		return new DefaultAsyncResult<T>(null);
	}
	
	default Throwable getException() {
		return null;
	}
	
	default void waitFor(CompletionListener<T> listener) {
		throw new UnsupportedOperationException();
	}
	
	default void join() {
	}
	
	
}
