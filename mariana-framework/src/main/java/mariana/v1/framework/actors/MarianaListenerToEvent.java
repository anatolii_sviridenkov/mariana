package mariana.v1.framework.actors;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import fi.jumi.actors.eventizers.Event;
import fi.jumi.actors.queue.MessageSender;

public class MarianaListenerToEvent<T> implements InvocationHandler {

    private final MessageSender<Event<T>> target;
	

    public MarianaListenerToEvent(MessageSender<Event<T>> target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable 
    {
        if (method.getDeclaringClass().equals(Object.class)) 
        {
            return method.invoke(this, args);
        }
        
        if (Async.class.isAssignableFrom(method.getReturnType())) 
        {
        	@SuppressWarnings("rawtypes")
			CompletionPort port = new CompletionPort();
        	
        	target.send(new MarianaEvent<T>(method, args, port));
        	
        	return port;
        }
        else {
        	target.send(new MarianaEvent<T>(method, args, null));        
        	return null;
        }
    }
    
    @Override
    public String toString() {
    	return target.toString();
    }
}