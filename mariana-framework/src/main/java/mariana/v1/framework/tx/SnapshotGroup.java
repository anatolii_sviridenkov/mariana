package mariana.v1.framework.tx;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Throwables;

import mariana.v1.api.alloc.Snapshot;

public class SnapshotGroup implements AutoCloseable {
	private List<Snapshot> snapshots = new ArrayList<>();
	
	public SnapshotGroup() {}
	
	public SnapshotGroup(List<Snapshot> snapshots) 
	{
		this.snapshots = new ArrayList<>(snapshots);
	}
	
	public List<Snapshot> snapshots() 
	{
		return Collections.unmodifiableList(snapshots);
	}
	
	public void add(Snapshot snp) {
		snapshots.add(snp);
	}
	
	public boolean remove(Snapshot snp) 
	{
		return snapshots.remove(snp);
	}
	
	
	public void commit() 
	{
		Throwable error = null;
		
		for (int snpNum = 0; snpNum < snapshots.size(); snpNum++)
		{
			try {
				snapshots.get(snpNum).commit();
			}
			catch (Throwable ex)
			{
				error = ex;
			}
		}
		
		
		if (error != null) 
		{
			Throwable error2 = null;
			
			for (Snapshot snp: snapshots)
			{
				try {
					snp.drop();
				}
				catch (Throwable ex)
				{
					error2 = ex;
				}
			}
			
			if (error2 != null)
			{
				throw new RollbackException(error);
			}
			else {
				throw Throwables.propagate(error);
			}
		}
	}
	
	public void rollback() 
	{
		Throwable error = null;
		
		for (Snapshot snp: snapshots)
		{
			try {
				snp.drop();
			}
			catch (Throwable ex)
			{
				error = ex;
			}
		}
		
		if (error != null)
		{
			throw new RollbackException(error);
		}
	}
	
	public void close() 
	{
		Throwable error = null;
		
		for (Snapshot snp: snapshots)
		{
			try {
				snp.close();
			}
			catch (Throwable ex)
			{
				error = ex;
			}
		}
		
		if (error != null)
		{
			throw Throwables.propagate(error);
		}
	}
}
