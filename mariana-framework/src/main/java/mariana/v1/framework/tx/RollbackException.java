package mariana.v1.framework.tx;

import mariana.bridge.MarianaException;

public class RollbackException extends MarianaException {

	private static final long serialVersionUID = 4825245661607282288L;

	public RollbackException() {
		super();
	}

	public RollbackException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RollbackException(String message, Throwable cause) {
		super(message, cause);
	}

	public RollbackException(String message) {
		super(message);
	}

	public RollbackException(Throwable cause) {
		super(cause);
	}
}
