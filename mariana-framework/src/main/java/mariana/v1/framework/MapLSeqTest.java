package mariana.v1.framework;

import java.util.Arrays;
import java.util.UUID;

import mariana.v1.Mariana;
import mariana.v1.api.alloc.Snapshot;
import mariana.v1.inmem.alloc.InMemoryAllocator;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.UTF8StringInputHandler;
import mariana.v1.io.serialization.UTF8StringOutputHandler;
import mariana.v1.kvmap.MapLSeq;

public class MapLSeqTest {
	
	static {
		Mariana.init();
	}
	
	public static void main(String[] args) 
	{
		ObjectInputHandler  inputHandler  = new UTF8StringInputHandler();
		ObjectOutputHandler outputHandler = new UTF8StringOutputHandler();
		
		try (InMemoryAllocator alloc = InMemoryAllocator.create()) 
    	{
    		try(Snapshot master = alloc.master()) 
    		{
    			try (Snapshot snp = master.branch()) 
    			{
    				try(MapLSeq mmap = MapLSeq.findOrCreate(snp, UUID.fromString("a962d32d-254f-4180-9c73-f5666c37fd64"), inputHandler, outputHandler))
    				{
    					Object[] data1 = new Object[1000];
    					for (int c = 0; c < data1.length; c++) 
    					{
    						data1[c] = "Str__" + c;
    					}
    					
    					mmap.set(1, Arrays.asList(data1));
    					mmap.set(2, Arrays.asList(data1));
    					mmap.set(3, Arrays.asList(data1));
    					
//    					mmap.get(1, obj -> {
//    						System.out.println("Value" + obj);
//    					});
    					    					
//    					try (MultimapLBIterator ii = mmap.begin()) 
//    					{
//    						try (MultimapLBWalkerAdapter adapter = new MultimapLBWalkerAdapter()) 
//    						{
//    							ii.next();
//    							while (!ii.isEnd()) {    								
//    								System.out.println(ii.populateValues(adapter, Long.MAX_VALUE));
//    								System.out.println(adapter.getBuffer());
//    							}
//    						}
//    					}
    					
    					try (MapLSeq.ValueIterator ii = mmap.get(1)) 
    					{
    						while (ii.hasNext()) 
    						{
    							System.out.println(ii.next());
    						}
    					}
    				}
    				
    				snp.commit();

    				snp.setAsMaster();
    				
    				snp.dump("target/mapls.dir");
    			}
    		}
    	}
	}
}
