package mariana.v1.framework;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import mariana.v1.Mariana;
import mariana.v1.hashmap.ListMMapEntryConsumer;
import mariana.v1.hashmap.MMapEntryParser;
import mariana.v1.hashmap.MMapEntryProducer;
import mariana.v1.io.buffer.IOByteBuffer;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.SequenceInputHandler;
import mariana.v1.io.serialization.SequenceOutputHandler;
import mariana.v1.io.serialization.UTF8StringInputHandler;
import mariana.v1.io.serialization.UTF8StringOutputHandler;

/**
 * Hello world!
 *
 */
public class App 
{
	static {
		Mariana.init();
	}
	
	ArrayList<Object> value;
	
	public App(ArrayList<Object> value) 
	{
		this.value = value;
	}
	
    public static void main( String[] args )
    {    	
    	ObjectOutputHandler outputHandler = new SequenceOutputHandler(new UTF8StringOutputHandler());
    	
    	ArrayList<Object> list = new ArrayList<Object>();
    	
    	Random RNG = new Random(0);
    	
    	long t0 = System.currentTimeMillis();
    	
    	for (int c = 0; c < 20000000; c++) {
    		list.add("Str___" + c);
    	}
    	
    	System.out.println("List creation time: " + (System.currentTimeMillis() - t0));
    	
    	App app = new App(list);
    	
    	outputHandler.setPropertyGetter(src -> {
    		return ((App)src).value.iterator();
    	});
    	
    	outputHandler.reset(app);
    	
        MMapEntryProducer producer = new MMapEntryProducer(1, outputHandler);
        
        long t1 = System.currentTimeMillis();
        
        List<IOByteBuffer> buffers = new ArrayList<>();
        
        while(true)
        {
        	IOByteBuffer buffer = new IOByteBuffer(ByteBuffer.allocate(4096));
        	
        	int entries = producer.apply(buffer);
//        	System.out.println("entries: " + entries);
        	
        	buffer.flip();
        	buffers.add(buffer);
        	
        	if (entries < 0) {
        		break;
        	}
        }
        
        System.out.println("Writing time: " + (System.currentTimeMillis() - t1));
        
        long t2 = System.currentTimeMillis();
        
        ObjectInputHandler inputHandler = new SequenceInputHandler(new UTF8StringInputHandler());
        
        ListMMapEntryConsumer<String> entryConsumer = new ListMMapEntryConsumer<>();
        
        
        MMapEntryParser consumer = new MMapEntryParser(inputHandler, entryConsumer); 

        for (IOByteBuffer buffer: buffers) 
        {        	
        	consumer.apply(buffer, 0);
        }
        
        System.out.println("List size: " + entryConsumer.getList().size());
        
        System.out.println("Reading time: " + (System.currentTimeMillis() - t2));
        
        long t3 = System.currentTimeMillis();
        
        Iterator<Object> i1 = list.iterator();
        Iterator<String> i2 = entryConsumer.getList().iterator();
        
        while (i1.hasNext()) 
        {
        	Object s1 = i1.next();
        	String s2 = i2.next();
        	if (!s1.equals(s2)) 
        	{
        		System.out.println("Fail: " + s1 + " -- " + s2);
        	}
        }
        
        System.out.println("Check time: " + (System.currentTimeMillis() - t3));
        
        System.out.println("done...");
    }
}
