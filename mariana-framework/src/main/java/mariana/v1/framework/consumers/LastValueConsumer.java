package mariana.v1.framework.consumers;

import mariana.v1.io.serialization.SequenceConsumer;

public class LastValueConsumer<T> implements SequenceConsumer {

	private T value;
	
	@SuppressWarnings("unchecked")
	@Override
	public void accept(Object value) {
		this.value = (T) value;
	}

	public T getValue() {
		return value;
	}

}
