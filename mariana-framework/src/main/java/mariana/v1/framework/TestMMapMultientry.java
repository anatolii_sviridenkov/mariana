package mariana.v1.framework;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import mariana.v1.Mariana;
import mariana.v1.hashmap.ListMMapEntryConsumer2;
import mariana.v1.hashmap.MMapEntriesProducer;
import mariana.v1.hashmap.MMapEntry;
import mariana.v1.hashmap.MMapEntryParser;
import mariana.v1.io.buffer.IOByteBuffer;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.SequenceInputHandler;
import mariana.v1.io.serialization.SequenceOutputHandler;
import mariana.v1.io.serialization.UTF8StringInputHandler;
import mariana.v1.io.serialization.UTF8StringOutputHandler;


public class TestMMapMultientry 
{
	static {
		Mariana.init();
	}
	
    public static void main( String[] args )
    {    	
    	ObjectOutputHandler outputHandler = new SequenceOutputHandler(new UTF8StringOutputHandler());
    	
    	ArrayList<MMapEntry> list = new ArrayList<>();
    	
    	Random RNG = new Random();
    	
    	long t0 = System.currentTimeMillis();
    	
    	int totalValues = 20000000;
    	int bound = 1000;
    	
    	for (int c = 0, d = 0; c < totalValues;) 
    	{
    		int length = RNG.nextInt(bound);
    		
    		List<Object> llist = new ArrayList<>();
    		for (int dd = 0; dd < length; dd++, d++) 
    		{
    			llist.add("Str___" + d);
    		}
    		
    		list.add(new MMapEntry(c, llist));

    		c += length;
    	}
    	
    	System.out.println("List creation time: " + (System.currentTimeMillis() - t0) +", size = " + list.size());

        MMapEntriesProducer producer = new MMapEntriesProducer(list.iterator(), outputHandler);

        long t1 = System.currentTimeMillis();

        List<IOByteBuffer> buffers = new ArrayList<>();
        
        while(true)
        {
        	IOByteBuffer buffer = new IOByteBuffer(ByteBuffer.allocate(4096).order(ByteOrder.LITTLE_ENDIAN));
        	
        	int entries = producer.apply(buffer);
//        	System.out.println("entries: " + entries);
        	
        	buffer.flip();
        	buffers.add(buffer);
        	
        	if (entries <= 0) {
        		break;
        	}
        }
        
        System.out.println("Writing time: " + (System.currentTimeMillis() - t1));
        
        long t2 = System.currentTimeMillis();
        
        ObjectInputHandler inputHandler = new SequenceInputHandler(new UTF8StringInputHandler());
        
        ListMMapEntryConsumer2 entryConsumer = new ListMMapEntryConsumer2();
        
        
        MMapEntryParser consumer = new MMapEntryParser(inputHandler, entryConsumer); 

        consumer.start();
        
        for (IOByteBuffer buffer: buffers) 
        {        	
        	consumer.apply(buffer, 0);
        }
        
        consumer.finish();
        
        System.out.println("List size: " + entryConsumer.getList().size());
        
        System.out.println("Reading time: " + (System.currentTimeMillis() - t2));
        
        long t3 = System.currentTimeMillis();
        
        Iterator<MMapEntry> i1 = list.iterator();
        Iterator<MMapEntry> i2 = entryConsumer.getList().iterator();
        
        while (i1.hasNext()) 
        {
        	MMapEntry e1 = i1.next();
        	MMapEntry e2 = i2.next();
        	
        	if (e1.getKey() != e2.getKey()) {
        		System.out.println("Key Fail: " + e1.getKey() + " -- " + e2.getKey());
        	}
        	
        	Iterator<Object> ei1 = e1.getValues().iterator();
        	Iterator<Object> ei2 = e2.getValues().iterator();
        	
        	while (ei1.hasNext()) 
        	{
        		Object s1 = ei1.next();
        		Object s2 = ei2.next();
        		
        		if (!s1.equals(s2)) 
        		{
        			System.out.println("Fail: " + s1 + " -- " + s2);
        		}
        	}
        }
        
        System.out.println("Check time: " + (System.currentTimeMillis() - t3));
        
        System.out.println("done...");
    }
}
