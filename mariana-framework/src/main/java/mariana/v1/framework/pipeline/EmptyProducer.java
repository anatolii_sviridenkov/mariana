package mariana.v1.framework.pipeline;

import mariana.v1.framework.actors.Async;

public class EmptyProducer<T> implements CloseableStreamProducer<T> {
	@Override
	public Async<T> produce() {
		return Async.ofNull();
	}

	@Override
	public Async<Void> close() {		
		return Async.ofNull();
	}
}
