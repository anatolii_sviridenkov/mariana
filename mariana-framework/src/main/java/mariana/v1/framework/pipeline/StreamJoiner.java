package mariana.v1.framework.pipeline;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import mariana.v1.framework.actors.Async;
import mariana.v1.framework.actors.CompletionListener;
import mariana.v1.framework.actors.CompletionPort;

public class StreamJoiner<T> implements StreamProducer<T> {

	protected StreamProducer<T>[] producers;
	private Async<T>[] ports;
	private CompletionListener<T>[] listeners;
	
	private BlockingQueue<Async<T>> queue;
	
	@SuppressWarnings("rawtypes")
	private static final Async END_MARKER = new Async() {
		
		@Override
		public Object get() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isDone() {
			throw new UnsupportedOperationException();
		}
	};
	
	private AtomicInteger failures = new AtomicInteger(0);
	private AtomicInteger completions = new AtomicInteger(0);
	
	
	@SuppressWarnings("unchecked")
	public StreamJoiner(List<StreamProducer<T>> producers)
	{
		this.producers 	= new StreamProducer[producers.size()];
		this.ports 		= new Async[producers.size()];
		this.listeners 	= new CompletionListener[producers.size()];
		
		this.queue 		= new ArrayBlockingQueue<>(producers.size() * 2);
		
		for (int c = 0; c < producers.size(); c++) 
		{
			this.producers[c] = producers.get(c);
		
			final int cc = c;
			this.listeners[c] = new CompletionListener<T>() {
				@Override
				public void methodFinished(CompletionPort<T> result) 
				{
					try {
						if (failures.get() > 0) 
						{
							// forget result, but...
							if (result.getException() != null) 
							{
								// log the exception
								result.getException().printStackTrace();
							}
						}
						else if (result.getException() != null)
						{
							if (failures.incrementAndGet() == 1) 
							{
								queue.put(result);
							}
							else {
								// log exception
								result.getException().printStackTrace();
							}
						}
						else if (result.get() != null)
						{
							queue.put(result);
							
							ports[cc] = StreamJoiner.this.producers[cc].produce();
							ports[cc].waitFor(this);
						}
						else {
							if (completions.incrementAndGet() == ports.length) 
							{	
								//throw new RuntimeException();
								queue.put((Async<T>) END_MARKER);
							}
						}
					} 
					catch (InterruptedException e) {
						// do nothing
						System.out.println("Interrupted????");
					}
				}
			};
		}
		
		for (int c = 0; c < this.producers.length; c++) 
		{
			this.producers[c].produce().waitFor(listeners[c]);
		}
	}
	
	
	
	@Override
	public Async<T> produce()
	{		
		try {
			Async<T> e = queue.take();
			if (e != END_MARKER) 
			{
				return Async.of(e.get());
			}
			else {		
				// all producers have finished now
				return Async.of(null);
			}
		} 
		catch (InterruptedException e1) {
			return Async.of(null);
		}
	}
}
