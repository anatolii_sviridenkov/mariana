package mariana.v1.framework.pipeline;

import java.util.List;

public class HashCodeDistributor<Target, Value> implements StreamConsumer<Value> {
	
	private Object[] consumers;
	private final int size;
	private AsyncPipe<Target, Value> pipe;
	
	public HashCodeDistributor(List<Target> consumers, AsyncPipe<Target, Value> pipe)
	{
		this.pipe = pipe;
		this.size = consumers.size();
		this.consumers = new StreamConsumer[size];
		
		for (int c = 0; c < size; c++) 
		{
			this.consumers[c] = consumers.get(c);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void consume(Value item)
	{
		int channel = item.hashCode() % size;
		
		Target target = (Target)consumers[channel];
		
		pipe.feed(channel, target, item);
	}
}
