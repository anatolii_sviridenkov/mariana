package mariana.v1.framework.pipeline;

import mariana.v1.framework.actors.Async;

public interface StreamProducer<T> {
	Async<T> produce();
}
