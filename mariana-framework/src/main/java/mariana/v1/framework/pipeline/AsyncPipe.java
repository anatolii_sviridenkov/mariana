package mariana.v1.framework.pipeline;

public interface AsyncPipe<T, V> {
	void feed(int pipeNum, T target, V value);
}
