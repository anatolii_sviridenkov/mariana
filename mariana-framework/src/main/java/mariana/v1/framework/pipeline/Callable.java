package mariana.v1.framework.pipeline;

public interface Callable<V> extends java.util.concurrent.Callable<V> {
	V call() ;
}
