package mariana.v1.framework.pipeline;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.google.common.base.Throwables;

import mariana.v1.framework.actors.Async;
import mariana.v1.framework.actors.CompletionListener;
import mariana.v1.framework.actors.CompletionPort;

public class FastCompletionIterator<T> implements Iterable<T>, Iterator<T> {

	private BlockingQueue<Async<T>> queue;
	
	private int done;
	private int total;
	
	public FastCompletionIterator(List<Async<T>> results) 
	{
		total = results.size();
		queue = new ArrayBlockingQueue<Async<T>>(results.size());
		
		for (Async<T> result: results) 
		{
			result.waitFor(new CompletionListener<T>() {
				@Override
				public void methodFinished(CompletionPort<T> result) 
				{
					queue.add(result);
				}
			});
		}
	}
	
	@Override
	public boolean hasNext() 
	{		
		return done < total;
	}

	@Override
	public T next() 
	{
		try {
			Async<T> result = queue.take();
			return result.get();
		}
		catch (InterruptedException e) 
		{
			throw Throwables.propagate(e);
		}
	}

	@Override
	public Iterator<T> iterator() {
		return this;
	}
}
