package mariana.v1.framework.pipeline;

import mariana.v1.framework.actors.Async;

public interface SyncPipe0<T, R> {
	Async<R> feed(int pipeNum, T target);
}
