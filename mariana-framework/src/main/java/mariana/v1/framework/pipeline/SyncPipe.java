package mariana.v1.framework.pipeline;

import mariana.v1.framework.actors.Async;

public interface SyncPipe<T, V> {
	Async<T> feed(int pipeNum, V value);
}
