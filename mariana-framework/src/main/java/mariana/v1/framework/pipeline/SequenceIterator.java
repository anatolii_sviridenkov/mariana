package mariana.v1.framework.pipeline;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class SequenceIterator<T> implements Iterator<T> {

	private StreamProducer<List<T>> producer;
	
	private Iterator<T> current;
	private boolean initialized;

	public SequenceIterator(StreamProducer<List<T>> producer) 
	{
		this.producer = producer;
	}
	
	private Iterator<T> getNonIterator() 
	{
		while (true) 
		{
			List<T> element = producer.produce().get();
			if (element != null) 
			{
				if (element.size() > 0) 
				{
					return element.iterator();
				}
			}
			else {
				return null;
			}
		}
	}
	
	@Override
	public boolean hasNext() 
	{
		if (current != null) 
		{
			if (current.hasNext()) 
			{
				return true;
			}
			else {
				current = getNonIterator();
				if (current != null) 
				{					
					return true;
				}
			}
		}
		else if (!initialized) 
		{
			initialized = true;
			
			current = getNonIterator();
			if (current != null) 
			{				
				return true;
			}
		}
				
		return false;
	}

	@Override
	public T next() 
	{
		if (current != null) 
		{
			if (current.hasNext()) 
			{
				return current.next();
			}
			else {
				current = getNonIterator();
				if (current != null) 
				{				
					return current.next();
				}
				else {
					throw new NoSuchElementException();
				}
			}
		}
		else if (!initialized) 
		{
			initialized = true;
			
			current = getNonIterator();
			if (current != null) 
			{				
				return current.next();
			}
			else {
				throw new NoSuchElementException();
			}
		}
		else {
			throw new NoSuchElementException();
		}
	}

}
