package mariana.v1.framework.pipeline;

import java.util.List;

public class HashCodeStreamDistributor<T> implements StreamConsumer<T> {
	
	private StreamConsumer<T> consumers[];
	private final int size;
	
	@SuppressWarnings("unchecked")
	public HashCodeStreamDistributor(List<StreamConsumer<T>> consumers) 
	{
		this.size = consumers.size();
		this.consumers = new StreamConsumer[size];
		
		for (int c = 0; c < size; c++) 
		{
			this.consumers[c] = consumers.get(c);
		}
	}

	@Override
	public void consume(T item)
	{
		consumers[item.hashCode() % size].consume(item);
	}
}
