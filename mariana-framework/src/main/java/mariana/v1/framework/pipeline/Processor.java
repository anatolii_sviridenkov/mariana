package mariana.v1.framework.pipeline;

import mariana.v1.framework.actors.Async;

public interface Processor<Input, Output> {
	Async<Output> process(Input item);
}
