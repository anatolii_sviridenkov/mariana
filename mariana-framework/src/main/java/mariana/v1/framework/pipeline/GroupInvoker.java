package mariana.v1.framework.pipeline;

import java.util.ArrayList;
import java.util.List;

import mariana.v1.framework.actors.Async;
import mariana.v1.framework.actors.DefaultAsyncResultEx;

public class GroupInvoker<Target, Result> {
	
	private List<Target> targets;
	private SyncPipe0<Target, Result> pipe;

	public GroupInvoker(List<Target> targets, SyncPipe0<Target, Result> pipe) 
	{
		this.pipe = pipe;
		this.targets = new ArrayList<>(targets);
	}
	
	public List<Async<Result>> invoke()
	{
		List<Async<Result>> results = new ArrayList<>();
		
		int cc = 0;
		for (Target t: targets) 
		{
			results.add(pipe.feed(cc++, t));
		}
		
		List<Async<Result>> results2 = new ArrayList<>();
		
		for (Async<Result> r: results) {
			r.join();
			results2.add(new DefaultAsyncResultEx<Result>(r));
		}
		
		return results2;
	}
}
