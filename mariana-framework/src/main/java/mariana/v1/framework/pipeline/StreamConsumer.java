package mariana.v1.framework.pipeline;

public interface StreamConsumer<T> {
	void consume(T item);
}
