package mariana.v1.framework.pipeline;

import java.util.List;

public class CloseableSequenceIterator<T> extends SequenceIterator<T> implements CloseableIterator<T> {

	private CloseableStreamProducer<List<T>> producer2;

	public CloseableSequenceIterator(CloseableStreamProducer<List<T>> producer) {
		super(producer);
		this.producer2 = producer;
	}

	@Override
	public void close()
	{
		producer2.close().get();
	}
}
