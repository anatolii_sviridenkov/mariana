package mariana.v1.framework.pipeline;

import mariana.v1.framework.actors.Async;

public interface CloseableStreamProducer<T> extends StreamProducer<T> {
	Async<Void> close();
}
