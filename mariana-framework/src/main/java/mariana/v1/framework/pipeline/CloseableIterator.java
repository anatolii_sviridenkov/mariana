package mariana.v1.framework.pipeline;

import java.util.Iterator;

public interface CloseableIterator<T> extends Iterator<T>, AutoCloseable {
	void close();
		
//	T current();
}
