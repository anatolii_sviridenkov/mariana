package mariana.v1.kvmap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Throwables;

import mariana.v1.api.alloc.Snapshot;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLB;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLBIterator;
import mariana.v1.api.ctr.multimap.lng.bytes.MultimapLBRunWalkerAdapter;
import mariana.v1.hashmap.MMapEntriesProducer;
import mariana.v1.hashmap.MMapEntry;
import mariana.v1.io.buffer.IOByteBuffer;
import mariana.v1.io.serialization.FlatSequenceParser;
import mariana.v1.io.serialization.ObjectInputHandler;
import mariana.v1.io.serialization.ObjectOutputHandler;
import mariana.v1.io.serialization.SequenceConsumer;

public class MapLSeq implements AutoCloseable {
	
	private MultimapLB mmap;

	static class ValueHolder<Value> {
		public Value value; 
	}

	private ObjectInputHandler inputHandler;
	private ObjectOutputHandler outputHandler;
	

	public class ValueIterator implements AutoCloseable, java.util.Iterator<Object> {
		
		private MultimapLBIterator mmapIterator;
		private MultimapLBRunWalkerAdapter walker;
		
		private final List<Object> values = new ArrayList<>();
		private java.util.Iterator<Object> valuesIter;
		
		private long fetches = 0;
		private boolean done = false;
		
		private FlatSequenceParser parser = new FlatSequenceParser(inputHandler, value -> {
			values.add(value);
		});

				
		public ValueIterator(MultimapLBIterator mmapIterator) 
		{
			this.mmapIterator = mmapIterator;
			if (mmapIterator != null) {
				walker = new MultimapLBRunWalkerAdapter();
			}
		}
		
		public void close() 
		{
			if (mmapIterator != null) 
			{
				mmapIterator.close();
				walker.close();
			}
		}
		
		public boolean hasNext() 
		{
			if (mmapIterator == null) {
				return false;
			}
			
			if (valuesIter == null) 
			{
				fetch();
				return valuesIter.hasNext();
			}
			
			if (valuesIter.hasNext()) {
				return true;
			}
			else if (!done) 
			{
				fetch();
				return valuesIter.hasNext();
			}
			else {
				return false;	
			}
		}
		
		public Object next() {
			return valuesIter.next();
		}
		
		private void fetch()
		{			
			values.clear();
			
			IOByteBuffer ioBuffer = new IOByteBuffer(walker.getBuffer());
			
			long total = mmapIterator.populateValues(walker, Long.MAX_VALUE, fetches > 0);
			
			ioBuffer.flip();
			parser.apply(ioBuffer, 1);
			
			fetches++;			
			
			done = total <= 0;
			valuesIter = values.iterator();
		}
	}	
	
	
	private MapLSeq(MultimapLB mmap, ObjectInputHandler inputHandler, ObjectOutputHandler outputHandler) {
		this.mmap = mmap;
		this.inputHandler = inputHandler;
		this.outputHandler = outputHandler;
	}
	
	
	public MultimapLBIterator begin() {
		return mmap.begin();
	}
	
	public void get(long key, SequenceConsumer consumer) 
	{
		try (MultimapLBIterator ii = mmap.find(key)) 
		{
			if (ii.isFound(key)) 
			{
				ii.next();
				ii.readValues(new FlatSequenceParser(inputHandler, consumer));
			}
		}
	}
	
	public ValueIterator get(long key) 
	{
		MultimapLBIterator ii = mmap.find(key);
		
		try {

			if (ii.isFound(key)) 
			{
				ii.next();
				return new ValueIterator(ii);
			}

			return new ValueIterator(null);
		}
		catch (Throwable ex) 
		{
			ii.close();
			throw Throwables.propagate(ex);
		}
	}
	
	public void set(long key, Iterable<Object> value) 
	{
		try (MultimapLBIterator ii = mmap.find(key)) 
		{
			if (ii.isFound(key)) 
			{
				ii.remove(1);
			}
			
			ii.insert(new MMapEntriesProducer(
					Collections.singletonList(new MMapEntry(key, value)).iterator(), 
					outputHandler
			));
		}
	}
	
	public boolean remove(long key)
	{
		try (MultimapLBIterator ii = mmap.find(key)) 
		{
			if (ii.isFound(key)) 
			{
				ii.remove(1);
				return true;
			}
		}
		
		return false;
	}
	
	
	
	public static MapLSeq findOrCreate(Snapshot snp, UUID name, ObjectInputHandler inputHandler, ObjectOutputHandler outputHandler) 
	{
		return new MapLSeq(snp.findOrCreate(MultimapLB.class, name), inputHandler, outputHandler);
	}


	@Override
	public void close() {
		mmap.close();
	}
}
