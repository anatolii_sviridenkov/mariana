package mariana.v1.kvmap;

import com.google.common.base.Throwables;

import mariana.v1.io.serialization.InputHandler;
import mariana.v1.io.serialization.OutputHandler;
import mariana.v1.io.serialization.SerializationHandlerFactory;

public class SerHF implements SerializationHandlerFactory{

	private Class<?> inputHandlerClass;
	private Class<?> outputHandlerClass;
	
	public SerHF(Class<?> inputHandlerClass, Class<?> outputHandlerClass) 
	{
		this.inputHandlerClass  = inputHandlerClass;
		this.outputHandlerClass = outputHandlerClass;		
	}
	
	@Override
	public Class<?> getTargetClass() {
		return Object.class;
	}

	@Override
	public OutputHandler newOutputHandler() 
	{		
		try {
			return (OutputHandler) outputHandlerClass.newInstance();
		} 
		catch (InstantiationException | IllegalAccessException | IllegalArgumentException | SecurityException e) 
		{
			throw Throwables.propagate(e);
		}
	}

	@Override
	public InputHandler newInputHandler() 
	{
		try {
			return (InputHandler) inputHandlerClass.newInstance();
		} 
		catch (InstantiationException | IllegalAccessException | IllegalArgumentException | SecurityException e) 
		{
			throw Throwables.propagate(e);
		}
	}

}
