package mariana.v1.io.serialization;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class SerializationTools {
	public static Class<?>[] getGenericSpecializations(Object source) 
	{
		Type[] actualTypeArguments = ((ParameterizedType)source.getClass().getGenericSuperclass()).getActualTypeArguments();
		
		Class<?> types[] = new Class<?>[actualTypeArguments.length];
		
		for (int c = 0; c < types.length; c++) {
			types[c] = (Class<?>) actualTypeArguments[c];
		}
		
		return types;
	}
	
	public static ObjectInputHandler getInputHandlerFor(Object source) 
	{
		Class<?> clazz = getGenericSpecializations(source)[0];
		return (ObjectInputHandler) SerializerRegistry.getFactory(clazz).newInputHandler();
	}
	
	public static ObjectOutputHandler getOutputHandlerFor(Object source) 
	{
		Class<?> clazz = getGenericSpecializations(source)[0];
		return (ObjectOutputHandler) SerializerRegistry.getFactory(clazz).newOutputHandler();
	}
}
