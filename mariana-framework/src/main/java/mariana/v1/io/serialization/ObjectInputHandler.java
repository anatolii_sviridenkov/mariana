package mariana.v1.io.serialization;

public interface ObjectInputHandler extends InputHandler {
	void setPropertySetter(PropertySetter setter);
}
