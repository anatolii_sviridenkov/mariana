package mariana.v1.io.serialization;

import com.google.common.base.Charsets;

import mariana.v1.io.buffer.IOBuffer;

public class UTF8StringOutputHandler implements ObjectOutputHandler {

	private byte[] utfData;
	private boolean headerWritten;
	private int idx;
	
	private PropertyGetter getter;
	
	public UTF8StringOutputHandler() {}
	public UTF8StringOutputHandler(PropertyGetter getter) {
		this.getter = getter;
	}
	
	public void reset(Object data) 
	{
		headerWritten = false;
		idx 		  = 0;
		
		String str 	= (String) getter.apply(data);
		utfData 	= str != null ? str.getBytes(Charsets.UTF_8) : null;
	}

	@Override
	public boolean writeTo(IOBuffer buf)
	{		
		if (!headerWritten)
		{
			if (utfData != null) 
			{
				int llength = buf.uvlength(utfData.length);
				if (buf.remaining() >= llength) 
				{
					buf.putUVLen(utfData.length);
					headerWritten = true;
				}
				else {
					return false;
				}
			}
			else {
				int llength = buf.uvlength(IOBuffer.UVLEN_NULL);
				if (buf.remaining() >= llength) 
				{
					buf.putUVLen(IOBuffer.UVLEN_NULL);
					return true;
				}
				else {
					return false;
				}
			}
		}
		
		int bufferRemaining = buf.remaining();
		int dataRemaining   = utfData.length - idx;
		int toWrite         = dataRemaining <= bufferRemaining ? dataRemaining : bufferRemaining;
		
		buf.putByteArray(utfData, idx, toWrite);
		
		idx += toWrite;
		
		return toWrite == dataRemaining;
	}

	@Override
	public void setPropertyGetter(PropertyGetter getter) {
		this.getter = getter;
	}
}
