package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;

public interface OutputHandler {
	boolean writeTo(IOBuffer buf);
	void reset(Object source);
}
