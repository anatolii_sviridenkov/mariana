package mariana.v1.io.serialization;

public interface PropertySetter {
	void accept(Object tgt, Object value);
}
