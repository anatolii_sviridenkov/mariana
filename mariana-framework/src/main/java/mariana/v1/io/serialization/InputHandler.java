package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;

public interface InputHandler {
	boolean readFrom(IOBuffer buf);
	void reset(Object target);
	void finish();
}
