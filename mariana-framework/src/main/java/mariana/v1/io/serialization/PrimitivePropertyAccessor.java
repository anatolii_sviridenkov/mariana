package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;;

public interface PrimitivePropertyAccessor {
	boolean apply(Object obj, IOBuffer buffer);
}
