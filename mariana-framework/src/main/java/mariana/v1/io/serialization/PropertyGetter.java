package mariana.v1.io.serialization;

public interface PropertyGetter {
	Object apply(Object source);
}
