package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;

public class PrimitiveOutputHandler implements OutputHandler {
	
	private Object source;
	
	private PrimitivePropertyAccessor accessor;
	
	public PrimitiveOutputHandler(PrimitivePropertyAccessor accessor) {
		this.accessor = accessor;
	}
	
	@Override
	public boolean writeTo(IOBuffer buf) 
	{
		return accessor.apply(source, buf);
	}

	@Override
	public void reset(Object source) 
	{
		this.source = source;
	}
}
