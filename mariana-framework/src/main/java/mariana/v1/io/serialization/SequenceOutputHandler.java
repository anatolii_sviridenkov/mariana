package mariana.v1.io.serialization;

import java.util.Iterator;

import mariana.v1.io.buffer.IOBuffer;

public class SequenceOutputHandler implements ObjectOutputHandler {

	protected ObjectOutputHandler typeHandler;

	private Iterator<?> iterator;

	private Object current;

	private PropertyGetter getter;
	
	public SequenceOutputHandler(ObjectOutputHandler typeHandler)
	{
		this.typeHandler = typeHandler;
		typeHandler.setPropertyGetter(src -> {
			return ((SequenceOutputHandler)src).current;
		});
	}


	@Override
	public boolean writeTo(IOBuffer buf) 
	{
		if (current != null)
		{
			if (typeHandler.writeTo(buf)) {
				current = null;
			}
			else {
				return false;
			}
		}

		while (iterator.hasNext()) 
		{
			current = iterator.next();
			typeHandler.reset(this);

			if (typeHandler.writeTo(buf)) {
				current = null;
			}
			else {
				return false;
			}
		}

		return true;
	}


	@Override
	public void reset(Object source) 
	{
		this.iterator = (Iterator<?>) getter.apply(source);
	}

	@Override
	public void setPropertyGetter(PropertyGetter getter) {
		this.getter = getter;
	}


	public Object getCurrent() {
		return current;
	}
}
