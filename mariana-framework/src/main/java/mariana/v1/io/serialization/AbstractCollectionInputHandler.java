package mariana.v1.io.serialization;

import java.util.Collection;

import mariana.v1.io.buffer.IOBuffer;

public abstract class AbstractCollectionInputHandler implements ObjectInputHandler {

	protected ObjectInputHandler typeHandler;

	@SuppressWarnings("rawtypes")
	private Collection collection;
	
	private long total;
	
	private boolean current = false;
	private boolean headerFinished;

	private Object target;

	private PropertySetter setter;
	
	@SuppressWarnings("unchecked")
	public AbstractCollectionInputHandler(ObjectInputHandler typeHandler)
	{
		this.typeHandler = typeHandler;
		this.typeHandler.setPropertySetter((tgt, value) -> {
			((AbstractCollectionInputHandler)tgt).collection.add(value);			
		});
	}
	
	public void finish() {}

	@SuppressWarnings("unchecked")
	@Override
	public boolean readFrom(IOBuffer buf) 
	{
		if (!headerFinished)
		{
			if (buf.hasUVLen()) 
			{
				total = buf.getUVLen();
				
				if (total >= 0) 
				{
					collection = newCollectionInstance();
					headerFinished = true;
				}
				else {
					setter.accept(target, null);
					return true;
				}
			}
			else {
				return false;
			}
		}
		
		if (current) 
		{
			if (typeHandler.readFrom(buf)) {
				current = false;
				total--;
			}
			else {
				return false;
			}
		}

		while (total > 0) 
		{
			typeHandler.reset(this);

			if (!typeHandler.readFrom(buf))
			{
				current = true;
				return false;
			}
			else {
				
			}
			
			total--;
		}
		
		setter.accept(target, collection);

		return true;
	}


	@Override
	public void reset(Object target) 
	{
		this.target 	= target;
		this.collection = null;

		headerFinished  = false;
	}
	
	abstract protected Collection<?> newCollectionInstance();
	
	public void setPropertySetter(PropertySetter setter) {
		this.setter = setter;
	}
}
