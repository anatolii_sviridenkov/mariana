package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;

public abstract class AbstractClassOutputHandler implements ObjectOutputHandler {
		
	protected OutputHandler[] handlers;
	
	private int idx;

	private PropertyGetter getter;
	
	protected AbstractClassOutputHandler(OutputHandler[] handlers) {
		this.handlers = handlers;
	}
	
	@Override
	public boolean writeTo(IOBuffer buf) 
	{
		for (; idx < handlers.length; idx++) 
		{
			if (!handlers[idx].writeTo(buf)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void reset(Object source) 
	{
		Object src2 = getter.apply(source);
		
		idx = 0;
		for (int c = 0; c < handlers.length; c++) {
			handlers[c].reset(src2);
		}
	}

	@Override
	public void setPropertyGetter(PropertyGetter getter) {
		this.getter = getter;		
	}
}
