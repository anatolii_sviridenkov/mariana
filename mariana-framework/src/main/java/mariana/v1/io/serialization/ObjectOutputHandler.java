package mariana.v1.io.serialization;

public interface ObjectOutputHandler extends OutputHandler {
	void setPropertyGetter(PropertyGetter getter);
}
