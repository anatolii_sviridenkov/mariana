package mariana.v1.io.serialization;

import java.util.Collection;
import java.util.Iterator;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.tools.codecs.UInt63Codec;

public class CollectionOutputHandler implements ObjectOutputHandler {

	protected ObjectOutputHandler typeHandler;

	private Collection<?> collection;
	private Iterator<?> iterator;

	private Object current;
	private boolean headerWritten;

	private PropertyGetter getter;
	
	public CollectionOutputHandler(ObjectOutputHandler typeHandler)
	{
		this.typeHandler = typeHandler;
		typeHandler.setPropertyGetter(src -> {
			return ((CollectionOutputHandler)src).current;
		});
	}


	@Override
	public boolean writeTo(IOBuffer buf) 
	{
		if (!headerWritten)
		{
			if (collection != null) 
			{
				int size = collection.size();
				int llength = buf.uvlength(size);

				if (buf.remaining() >= llength) 
				{
					buf.putUVLen(size);
					headerWritten = true;
				}
				else {
					return false;
				}
			}
			else if (buf.remaining() >= UInt63Codec.LENGTH_MIN) 
			{
				buf.putUVLen(IOBuffer.UVLEN_NULL);
				return true;
			}
			else {
				return false;
			}
		}
		
		if (current != null)
		{
			if (typeHandler.writeTo(buf)) {
				current = null;
			}
			else {
				return false;
			}
		}

		while (iterator.hasNext()) 
		{
			current = iterator.next();
			typeHandler.reset(this);

			if (typeHandler.writeTo(buf)) {
				current = null;
			}
			else {
				return false;
			}
		}

		return true;
	}


	@Override
	public void reset(Object source) 
	{
		headerWritten   = false;
		this.collection = (Collection<?>) getter.apply(source);
		this.iterator   = collection.iterator();
	}

	@Override
	public void setPropertyGetter(PropertyGetter getter) {
		this.getter = getter;
	}
}
