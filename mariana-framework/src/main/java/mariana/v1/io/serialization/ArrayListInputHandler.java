package mariana.v1.io.serialization;

import java.util.ArrayList;
import java.util.Collection;

public class ArrayListInputHandler<T> extends AbstractCollectionInputHandler {

	public ArrayListInputHandler(ObjectInputHandler itemHandler) 
	{
		super(itemHandler);
	}

	protected Collection<T> newCollectionInstance() {
		return new ArrayList<T>();
	}
}
