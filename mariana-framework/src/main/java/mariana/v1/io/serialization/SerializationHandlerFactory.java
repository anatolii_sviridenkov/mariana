package mariana.v1.io.serialization;

public interface SerializationHandlerFactory {
	
	Class<?> getTargetClass();
	
	OutputHandler newOutputHandler();
	InputHandler  newInputHandler();
}
