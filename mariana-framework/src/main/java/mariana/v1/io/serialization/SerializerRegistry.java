package mariana.v1.io.serialization;

import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.base.Throwables;

public class SerializerRegistry {
	
	private static ConcurrentHashMap<Class<?>, SerializationHandlerFactory> factories = new ConcurrentHashMap<>();
	
	static {
		ServiceLoader<SerializationHandlerFactory> loader = ServiceLoader.load(SerializationHandlerFactory.class);

    	for (SerializationHandlerFactory f: loader) {
    		factories.put(f.getTargetClass(), f);
    	}
	}
	
	public static SerializationHandlerFactory getFactory(Class<?> clazz) {
		return factories.get(clazz);
	}
	
	public static SerializationHandlerFactory getFactory(String clazz) {
		try {
			return factories.get(Thread.currentThread().getContextClassLoader().loadClass(clazz));
		} 
		catch (ClassNotFoundException e) {
			throw Throwables.propagate(e);
		}
	}
}
