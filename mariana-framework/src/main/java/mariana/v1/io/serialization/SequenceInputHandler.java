package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;

public class SequenceInputHandler implements ObjectInputHandler {

	protected ObjectInputHandler typeHandler;

	private boolean current = false;
	
	private PropertySetter setter;
	
	private Object target; 
	
	public SequenceInputHandler(ObjectInputHandler typeHandler)
	{
		this.typeHandler = typeHandler;
		this.typeHandler.setPropertySetter((tgt, value) -> {
			((SequenceInputHandler)tgt).setter.accept(target, value);
		});
	}
	
	@Override
	public boolean readFrom(IOBuffer buf) 
	{
		if (current) 
		{
			if (typeHandler.readFrom(buf)) 
			{
				current = false;	
			}
			else {
				return false;
			}
		}

		while (true) 
		{
			typeHandler.reset(this);

			if (!typeHandler.readFrom(buf))
			{
				current = true;
				return false;
			}
		}
		
		//setter.accept(target, collection);
		//return true;
	}


	@Override
	public void reset(Object target) 
	{
		this.target = target;
	}
	

	public void setPropertySetter(PropertySetter setter) {
		this.setter = setter;
	}

	@Override
	public void finish() {
		
	}
}
