package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;

public class PrimitiveInputHandler implements InputHandler {

	private Object target;
	
	private PrimitivePropertyAccessor accessor;
	
	public PrimitiveInputHandler(PrimitivePropertyAccessor accessor) {
		this.accessor = accessor;
	}
	
	@Override
	public boolean readFrom(IOBuffer buf) 
	{
		return accessor.apply(target, buf);
	}

	@Override
	public void reset(Object target) {
		this.target = target;
	}

	@Override
	public void finish() {}
}
