package mariana.v1.io.serialization;

public interface SequenceConsumer {
	void accept(Object value);
}
