package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;

public class UVLenObjectInputHandler implements ObjectInputHandler {

	private Object target;
	private PropertySetter setter;

	public UVLenObjectInputHandler() {}
	public UVLenObjectInputHandler(PropertySetter setter) {
		this.setter = setter;
	}
	
	@Override
	public boolean readFrom(IOBuffer buf) 
	{
		if (buf.hasUVLen()) 
		{
			setter.accept(target, buf.getUVLen());
			return true;
		}
		
		return false;
	}

	@Override
	public void reset(Object target) {
		this.target = target;
	}

	@Override
	public void finish() {}

	@Override
	public void setPropertySetter(PropertySetter setter) {
		this.setter = setter;
	}

}
