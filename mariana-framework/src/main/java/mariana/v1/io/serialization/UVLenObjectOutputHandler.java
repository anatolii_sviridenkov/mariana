package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.tools.codecs.UInt63Codec;

public class UVLenObjectOutputHandler implements ObjectOutputHandler {

	private PropertyGetter getter;

	private Long value;
	
	public UVLenObjectOutputHandler() {}
	public UVLenObjectOutputHandler(PropertyGetter getter) {
		this.getter = getter;
	}
	
	@Override
	public boolean writeTo(IOBuffer buf) 
	{
		if (value != null) 
		{			
			int len = buf.uvlength(value);
			if (buf.remaining() >= len) 
			{
				buf.putUVLen(value);
				return true;
			}
		}
		else {
			int len = buf.uvlength(UInt63Codec.NULL);			
			if (buf.remaining() >= len) 
			{
				buf.putUVLen(UInt63Codec.NULL);
				return true;
			}
		}

		return false;
	}

	@Override
	public void reset(Object source) 
	{
		value = (Long) getter.apply(source);
		if (value != null) 
		{
			if (value < 0) 
			{
				throw new RuntimeException("UVLen encoding does not support negative numbers");
			}
		}
	}

	@Override
	public void setPropertyGetter(PropertyGetter getter) {
		this.getter = getter;
	}

}
