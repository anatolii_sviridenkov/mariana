package mariana.v1.io.serialization;

public interface ObjectPropertySetter {
	void apply(Object tgt, Object value);
}
