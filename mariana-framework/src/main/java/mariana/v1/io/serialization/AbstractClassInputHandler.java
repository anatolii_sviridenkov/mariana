package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;

public abstract class AbstractClassInputHandler implements ObjectInputHandler {

	protected InputHandler[] handlers;
	
	private int idx;

	protected PropertySetter setter;
	
	private Object target;
	private Object newInstance;
	
	protected AbstractClassInputHandler(InputHandler[] handlers) {
		this.handlers = handlers;
	}
	
	@Override
	public boolean readFrom(IOBuffer buf) 
	{
		for (; idx < handlers.length; idx++) 
		{
			if (!handlers[idx].readFrom(buf)) {
				return false;
			}
		}
		
		setter.accept(target, newInstance);
		
		return true;
	}
	
	protected void resetHandlers(Object target) 
	{
		idx = 0;

		for (int c = 0; c < handlers.length; c++) {
			handlers[c].reset(target);
		}
	}
	
	@Override
	public void reset(Object target) 
	{
		this.target = target; 
		this.newInstance = newInstance();
		
		resetHandlers(newInstance);
	}
	
	public void setPropertySetter(PropertySetter setter) 
	{
		this.setter = setter;
	}
	
	@Override
	public void finish() {}
	
	protected abstract Object newInstance();
}
