package mariana.v1.io.serialization;

import mariana.v1.io.buffer.IOBuffer;
import mariana.v1.io.buffer.IOBufferConsumer;

public class FlatSequenceParser implements IOBufferConsumer {

	private final SequenceConsumer consumer;
	
	private final SequenceInputHandler inputHandler; 
	
	private boolean done;
	
	public FlatSequenceParser(ObjectInputHandler valueInputHandler, SequenceConsumer consumer) 
	{
		this.inputHandler 	= new SequenceInputHandler(valueInputHandler);
		this.consumer 		= consumer;
		
		inputHandler.setPropertySetter((obj, value) -> {
			((FlatSequenceParser)obj).consumer.accept(value);
		});
		
		inputHandler.reset(this);
	}
	
	@Override
	public int apply(IOBuffer buffer, int entries) 
	{
		if (!done) {
			done = inputHandler.readFrom(buffer);
		}
		
		return entries;
	}

	public boolean isDone() {
		return done;
	}

}
