package mariana.v1.io.serialization;

public interface ObjectPropertyGetter {
	void apply(Object tgt, Object value);
}
