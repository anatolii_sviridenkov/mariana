package mariana.v1.io.serialization;

public class UTF8StringSerializationHandlerFactory implements SerializationHandlerFactory {

	@Override
	public Class<?> getTargetClass() {		
		return String.class;
	}

	@Override
	public OutputHandler newOutputHandler() {
		return new UTF8StringOutputHandler();
	}

	@Override
	public InputHandler newInputHandler() {
		return new UTF8StringInputHandler();
	}

}
