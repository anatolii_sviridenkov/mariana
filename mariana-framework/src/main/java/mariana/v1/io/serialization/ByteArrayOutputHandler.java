package mariana.v1.io.serialization;

import com.google.common.base.Charsets;

import mariana.v1.io.buffer.IOBuffer;

public class ByteArrayOutputHandler implements ObjectOutputHandler {

	private byte[] arrayData;
	private boolean headerWritten;
	private int idx;
	
	private PropertyGetter getter;
	
	public ByteArrayOutputHandler() {}
	public ByteArrayOutputHandler(PropertyGetter getter) {
		this.getter = getter;
	}
	
	public void reset(Object data) 
	{
		headerWritten = false;
		idx 		  = 0;
		
		arrayData 	= (byte[]) getter.apply(data);
	}

	@Override
	public boolean writeTo(IOBuffer buf)
	{		
		if (!headerWritten)
		{
			if (arrayData != null) 
			{
				int llength = buf.uvlength(arrayData.length);
				if (buf.remaining() >= llength) 
				{
					buf.putUVLen(arrayData.length);
					headerWritten = true;
				}
				else {
					return false;
				}
			}
			else {
				int llength = buf.uvlength(IOBuffer.UVLEN_NULL);
				if (buf.remaining() >= llength) 
				{
					buf.putUVLen(IOBuffer.UVLEN_NULL);
					return true;
				}
				else {
					return false;
				}
			}
		}
		
		int bufferRemaining = buf.remaining();
		int dataRemaining   = arrayData.length - idx;
		int toWrite         = dataRemaining <= bufferRemaining ? dataRemaining : bufferRemaining;
		
		buf.putByteArray(arrayData, idx, toWrite);
		
		idx += toWrite;
		
		return toWrite == dataRemaining;
	}

	@Override
	public void setPropertyGetter(PropertyGetter getter) {
		this.getter = getter;
	}
}
