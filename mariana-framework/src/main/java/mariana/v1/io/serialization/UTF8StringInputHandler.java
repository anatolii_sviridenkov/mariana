package mariana.v1.io.serialization;

import com.google.common.base.Charsets;

import mariana.v1.io.buffer.DynamicByteArray;
import mariana.v1.io.buffer.IOBuffer;

public class UTF8StringInputHandler implements ObjectInputHandler {

	private int length = -2;	
	private DynamicByteArray baos;
	private Object target;
	
	private PropertySetter setter;

	public UTF8StringInputHandler() {}
	public UTF8StringInputHandler(PropertySetter setter) {
		this.setter = setter;
	}
	
	@Override
	public boolean readFrom(IOBuffer buf)
	{
		if (length < -1)
		{
			if (buf.hasUVLen())
			{
				length = (int) buf.getUVLen();
								
				if (length < 0) 
				{
					setter.accept(target, null);
					return true;
				}
			}
			else {
				return false;
			}
		}
		
		int remaining = buf.remaining();
		
		int toRead = length <= remaining ? length : remaining;
		
		if (baos == null) {
			baos = new DynamicByteArray(toRead);
		}
		
		baos.append(buf, toRead);
		
		length -= toRead;
		
		assert(length >= 0);
		
		if (length == 0)
		{
			setter.accept(target, getObject());
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public void reset(Object target) 
	{
		this.target = target;
		
		length = -2;
		
		if (baos != null) 
		{
			baos.reset();
		}
	}

	
	protected String getObject()
	{
		return new String(baos.array(), 0, baos.size(), Charsets.UTF_8);
	}

	@Override
	public void setPropertySetter(PropertySetter setter) {
		this.setter = setter;
	}

	@Override
	public void finish() {}
}
